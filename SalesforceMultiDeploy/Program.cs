﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;

namespace SalesforceMultiDeploy
{
    class Program
    {
        static void Main(string[] args)
        {
            string antGetCommand = "ant retrieveUnpackaged";
            string antDeployCommand = "ant deployUnpackaged -verbose";
            try
            {
                string[] lines = System.IO.File.ReadAllLines(@"orgs.txt");

                foreach (string line in lines)
                {
                    try
                    {
                        Dictionary<string, string> keyValuePairs = line.Split(';').Select(value => value.Split('=')).ToDictionary(pair => pair[0], pair => pair[1]);

                        string username = keyValuePairs["username"];
                        string password = keyValuePairs["password"];
                        string token = keyValuePairs["token"];
                        string url = keyValuePairs["url"];
                        string name = keyValuePairs["name"];


                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("Processing Org: " + name);
                        Console.ResetColor();

                        string buildProps = "sf.username = " + username + "\n";
                        buildProps += "sf.password = " + password + token + "\n";
                        buildProps += "sf.serverurl = " + url + "\n";
                        buildProps += "sf.maxPoll = 200\n";
                        buildProps += "sf.orgName = " + name;


                        System.IO.File.WriteAllText(@"build.properties", buildProps);
                        Console.WriteLine("Wrote Build Properties To File");

                        Console.WriteLine("Getting Org Components For Backup. Please Wait...");
                        // Start the child process.
                        System.Diagnostics.ProcessStartInfo getDataProcess = new System.Diagnostics.ProcessStartInfo("cmd", "/c " + antGetCommand);
                        // Redirect the output stream of the child process.
                        getDataProcess.UseShellExecute = false;
                        getDataProcess.RedirectStandardOutput = true;
                        getDataProcess.CreateNoWindow = true;

                        System.Diagnostics.Process proc = new System.Diagnostics.Process();
                        proc.StartInfo = getDataProcess;
                        proc.Start();

                        string result = proc.StandardOutput.ReadToEnd();

                        Console.WriteLine(result);

                        Console.WriteLine("Putting Org Components. Please Wait...");
                        System.Diagnostics.ProcessStartInfo putDataProcess = new System.Diagnostics.ProcessStartInfo("cmd", "/c " + antDeployCommand);
                        // Redirect the output stream of the child process.
                        putDataProcess.UseShellExecute = false;
                        putDataProcess.RedirectStandardOutput = true;
                        putDataProcess.CreateNoWindow = true;

                        proc.StartInfo = putDataProcess;
                        proc.Start();

                        result = proc.StandardOutput.ReadToEnd();

                        Console.WriteLine(result);
                        Console.WriteLine("Done pushing data for org. Continuing to next org.");

                    }
                    catch (Exception e)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("\tError:" + e.Message);
                        Console.ResetColor();
                    }
                }
            }
            catch(Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;    
                Console.WriteLine("\tError:" + e.Message);
                Console.ResetColor();
            }
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Push complete. You can close this window now");
            Console.ResetColor();
            System.Console.ReadKey();
        }
    }
}
