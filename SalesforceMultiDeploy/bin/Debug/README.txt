To use this tool you must have

1) Installed Java JRE and JDK
   - Remember to set the JAVA_HOME path

2) Installed ANT

Once you have the environment configured. You are ready to prep your deployment. 

1) In the deploy folder include the folders for all the content you wish to deploy.
2) In the orgs.txt file create one line item entry for each org you want to deploy the changes to. 
   for each line there should be 5 key/value pairs, separated by ; with keys and values delimited by a '=', The 5 attributes are:
   username, password, token, url, name
   For example

   username=redkite@testorg.com;password=Rxxxx2312;token=xGwj7z0o04qhACd1QdbmBlZtV;url=https://login.salesforce.com;name=Axiom Base Org

   *Note the name attribute must be a valid windows folder name. The URL attribute must also be an https domain.

3) Save your orgs.txt file and you should now be able to run the SalesforceMultiDeploy.exe file. Watch the console window for more info.