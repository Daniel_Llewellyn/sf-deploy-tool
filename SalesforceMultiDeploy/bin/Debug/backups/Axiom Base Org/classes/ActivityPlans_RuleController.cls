global class ActivityPlans_RuleController {

    public ActivityPlans_RuleController() {

    }


    public Activity_Plan_Rule__c thisRule { get; set;}
    
    public SelectOption[] allObjectFields      { get; set; }   
    public string selectedSourceField{get;set;}
    public string selectedCompareField{get;set;}
    public string selectedCompareFieldType{get;set;}
    public string parentObjectType{get;set;}
    public String message { get; set; }
    public boolean initError{get;set;}
    public Map<String, Schema.SObjectField> objectFields {get; set;}
    public string recordTypeName{get;set;}
 
    /** used by the task list component to find tasks for the given parent used by the Axiom_TaskRelatedList component. **/
    public id parentObjectId{get;set;} 
    
    /**used by task list component to further filter tasks returned used by the Axiom_TaskRelatedList component**/
    public string relatedTaskFilterString{get;set;} 
    
    /**custom order by statment to be used in the find related tasks query used by the Axiom_TaskRelatedList component**/
    public string orderRelatedTasksBy{get;set;}    
 
    public ActivityPlans_RuleController(ApexPages.StandardController controller) {
        try
        {
            initError = false;
            thisRule = (Activity_Plan_Rule__c) controller.getRecord();
            
            if(thisRule.id != null)
            {
                thisRule = [select Activity_Plan__c,
                               Comparison_Value__c,
                               Comparison_Type__c,
                               Field_Name__c,
                               Logical_Operator__c,
                               Name,
                               CreatedDate,
                               CreatedById,
                               RecordType.Name
                               from Activity_Plan_Rule__c where id = :thisRule.id];     
    
            }
            id parentPlanId = thisRule.Activity_Plan__c != null ? thisRule.Activity_Plan__c : ApexPages.currentPage().getParameters().get('CF00Ni000000CTHHL_lkid');  
            id recordTypeId = ApexPages.currentPage().getParameters().get('RecordType');
            if(recordTypeId != null)
            {
                RecordType thisRecordType = [select name, developerName from recordType where id = :recordTypeId];
                recordTypeName = thisRecordType.developerName;
            }
            system.debug(parentPlanId);
            if(parentPlanId == null)
            {
                throw new applicationException('Error: Unable to determine parent activity plan. Cannot deduce object type. Please create rules from the Activity Plan Related List');                   
            }                 
            //get parent object type
            Activity_Plan__c parentPlan = [select id, Object_Types__c from Activity_Plan__c where id = :parentPlanId];
            parentObjectType = parentPlan.Object_Types__c;
            
            allObjectFields      = new List<SelectOption>();
            selectedSourceField = thisRule.Field_Name__c;
            
            String[] types = new String[]{parentObjectType};
            Schema.DescribeSobjectResult[] results = Schema.describeSObjects(types);
            if(!results.isEmpty())
            {            
                
                objectFields = results[0].fields.getMap();
                list<string> fieldNames = new list<string>(objectFields.keySet());
                fieldNames.sort();
                
                for(String field : fieldNames)
                {
                    Schema.DescribeFieldResult dr = objectFields.get(field).getDescribe();
                    allObjectFields.add(new SelectOption(field, dr.getLabel()));
                } 
            } 
        }
        catch(exception ex)
        {
            initError = true;
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,ex.getMessage());
            ApexPages.addMessage(myMsg);         

        }   
    }

    /**
    * When a user chooses a source field to compare against, we should get data about that field to help the user only create rules which will make sense and not cause errors at runtime. Things like only allowing
    * number fields to have numeric values in the comparison value box if using static value compares, or not allowing a boolean to be compared against a number. Really the number of validations is almost endless
    * so we'll do the best we can to help the user, however I don't pretend to, nor do I aspire to create perfect validations for every possible situation.
    * @param fieldName the API name of the field on the object to get data for
    * @return field describe result/
    **/
    @remoteAction
    global static string getFieldType(string parentObjectType, string fieldName)
    {
        String[] types = new String[]{parentObjectType};
        Schema.DescribeSobjectResult[] results = Schema.describeSObjects(types);
                
        Schema.DescribeFieldResult fieldData = results[0].fields.getMap().get(fieldName).getDescribe();
        return fieldData.getType().name();        

    }

    /**
    * Handles saving of the editing task template record. Any special data massaging that needs to happen to get the data
    * from the UI format to acceptable database format happens here (like constructing a comma separated list of values from an array)
    * happens here.
    * @return pageReference where to return the user after save. Null means return to the same page.
    **/
    public PageReference save() {
        try
        {     
            //get the name of the field to run the comparions against
            thisRule.Field_Name__c = selectedSourceField;
            
            //ensure the field specified exists on this object
            if(!objectFields.containsKey(thisRule.Field_Name__c))
            {
                throw new applicationException('Error: Provided field ' + thisRule.Field_Name__c + ' Does not exist on object type ' + parentObjectType + ' Please note you cannot use relationship fields in rules.');
            }
            
            if(recordTypeName == 'Activity_Plan_Task_Template_Rule' && thisRule.Activity_Plan_Task_Template__c == null)
            {
                throw new applicationException('Error: You must include a related task template');
            }
            
            Schema.DescribeFieldResult fieldData = objectFields.get(thisRule.Field_Name__c).getDescribe();
            string fieldType =  fieldData.getType().name();
            //Set the comparison value to the propert value
            if(thisRule.Comparison_Type__c == 'Static Value')
            {
                try
                {
                    if(fieldType == 'ID')
                    {
                        thisRule.Comparison_Value__c = Id.valueOf(thisRule.Comparison_Value__c);                
                    }
                    else if(fieldType == 'Boolean')
                    {
                        thisRule.Comparison_Value__c = String.valueOf(Boolean.valueOf(thisRule.Comparison_Value__c));                  
                    }
                    else if(fieldType == 'Currency' || fieldType == 'Double' || fieldType == 'Percent')
                    {
                        thisRule.Comparison_Value__c = String.valueOf(Decimal.valueOf(thisRule.Comparison_Value__c));
                    }
                    else if(fieldType == 'Integer')
                    {
                        thisRule.Comparison_Value__c = String.valueOf(Integer.valueOf(thisRule.Comparison_Value__c));
                    }
                    else if(fieldType == 'Date')
                    {
                        thisRule.Comparison_Value__c = String.valueOf(Date.parse(thisRule.Comparison_Value__c));
                    }
                    else if(fieldType == 'DateTime')
                    {
                        thisRule.Comparison_Value__c = String.valueOf(DateTime.parse(thisRule.Comparison_Value__c));
                    }                                         
                    else
                    {
                        thisRule.Comparison_Value__c = thisRule.Comparison_Value__c;
                    }
                }
                catch(exception e)
                {
                    throw new applicationException('Error: The value provided for comparison "'+thisRule.Comparison_Value__c+'" does not match the type of value specified on the field which has type ' +fieldType);
                }
            }
            else if(thisRule.Comparison_Type__c == 'Object Field Value')
            {
                thisRule.Comparison_Value__c = selectedCompareField;
            }
            

            system.debug(thisRule);
            upsert thisRule;
            
            PageReference newpage = new PageReference('/'+thisRule.id);
    
            newpage.setRedirect(true);
            return newpage;   
        }
        catch(exception ex)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,ex.getMessage());
            ApexPages.addMessage(myMsg);     
            return null;    

        }          
    }    

    /**
    * finds any rules related to the current activityPlan. Used for inline visualforce page to build custom related list
    * of rules. Reads from the relatedTaskFilterString, orderRelatedTaskBy public variables to adjust the
    * list of rules returned to the component for iterating and table creation. Does not take any arguments directly but does
    * read data from the class instance which is invoked by the component.
    */    
    public list<Activity_Plan_Rule__c> relatedRules
    {
        get
        {
            
            string queryString = ActivityPlans_Utilities.buildSelectAllStatment('Activity_Plan_Rule__c'); 
            queryString += ' where Activity_Plan__c = :parentObjectId'; 
            if(relatedTaskFilterString != null)
            {
                queryString += ' and ' +relatedTaskFilterString;
            }
            if(orderRelatedTasksBy != null)
            {
                queryString += ' order by ' + orderRelatedTasksBy;
            }
            system.debug('\n\n\n\n------------- QUERYSTRING: ' + queryString);
            return database.query(queryString);
        }
        set;    
    }    
    public class applicationException extends Exception {}
}