public with sharing class Axiom_AuthRequestPortalController {

    public id userId                             { get; set; }
    public contact referencedContact             { get; set; }
    public SelectOption[] matterRecordTypes      { get; set; } 
    
    public string selectedMatterRecordType       { get; set; } 
    public boolean initException                 { get; set; }
    public Axiom_AuthRequestPortalController(ApexPages.StandardController controller) {
        try
        {
            User thisUser = [select name, id, Related_Contact_Id__c, userType from user where id = :UserInfo.getUserId()];
            
            if(thisUser.Related_Contact_Id__c == null)
            {
                throw new applicationException('Current user is not linked to a contact. Please specify a related contact on the user record in the "Related Contact Id" field.');
            } 
            
            referencedContact = [select Geography__c,Member_of_Legal_Team__c from contact where id= :thisUser.Related_Contact_Id__c limit 1];
            
            matterRecordTypes = new list<selectOption>();
            list<RecordType> matterRecordTypesQuery = [select developername, name from recordType where sObjectType = 'matter__c'];
            for ( RecordType r : matterRecordTypesQuery)
            {
                matterRecordTypes.add(new SelectOption(r.developerName, r.Name));
            }    
        }
        catch(exception e)
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,e.getMessage()));
            initException = true;
        }
                   
    }
    public class applicationException extends Exception {}
}