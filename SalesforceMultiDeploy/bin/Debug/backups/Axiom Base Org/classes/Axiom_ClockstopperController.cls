/******* Axiom Clock Stopper Controller *********
@Author Daniel Llewellyn/RedKite (dllewellyn@redkitetechnologies.com)
@Date ~9/2013
@Description Methods and properties related to the clockstopper Salesforce object.
**********************************************/

global class Axiom_ClockstopperController
{
    /**
    * calculates the business minutes accumulated by clock stopper objects. Each clock stopper has a start and a stop time and the difference between the two
    * is the net time. We need to account for business hours/weekends/holidays so we hand off the actual number crunching to a method in the utilities class
    * that handles that. This is meant to be invoked as a before trigger, but can also be used to calculate the net time for any passed in clockstopper. The 
    * modified clockstopper record is passed back to the caller.
    * @param clockStoppers a list of clockstopper records to caluclate the net time for
    * @return list<clockstopper__c> the updated list of clockstoppers with their new_time__c set according to business hours
    */
    public static list<Clockstopper__c> calculateNetTime(list<Clockstopper__c> clockStoppers)
    {
        for(Clockstopper__c thisClockStopper :  clockstoppers)
        {
            datetime stopTime = thisClockStopper.Stop_Time__c != null ? thisClockStopper.Stop_Time__c : datetime.now();
            if(thisClockStopper.Start_Time__c != null)
            {
                thisClockStopper.Net_Time__c = Axiom_Utilities.calculateBusinessMinutesBetweenDates(thisClockStopper.Start_Time__c, stopTime);
            }
        }
        return clockStoppers;
    }
    
    /**
    * clockstoppers that are related to matters need to get summed up and the total time recorded on the matter itself
    * since we don't have a master detail relationship we cannot use a standard rollup field. This method takes the place
    * of a rollup field by running an aggregate query and saving the results to the matter.
    * @param list of clockstoppers for which to update parent matters with the number of clockstoppers attached.
    * @return null
    */
    public static void sumMatterClockstoppers(list<Clockstopper__c> clockStoppers)
    {
        map<id,matter__c> matterIds = new map<id,matter__c>();
        
        //create list of matter id's to query for. Using a map instead of a set because later on 
        //we'll populate the values with the actualy matter objects and their updated values. Less 
        //code than creating a set to run the query, and a list to perform the update, two birds, one stone, etc.
        for(Clockstopper__c thisClockStopper :  clockstoppers)
        {
            if(thisClockStopper.Matter__c != null)
            {
                matterIds.put(thisClockStopper.Matter__c,null);
            }
        }        
        
        //aggregate query that sums the net time field from all clock stopper objects grouped by the matter
        //the belong to
        list<AggregateResult> clockStopperTotals = [select SUM(Net_Time__c)time, Matter__c
                                                    from Clockstopper__c
                                                    Where Matter__c in :matterIds.keySet() and 
                                                    matter__c != null
                                                    group by rollup(Matter__c)];
        
        //iterate query results and create update object list
        for(AggregateResult ar : clockstopperTotals)
        {
            matter__c thisMatter = new matter__c(id=(string) ar.get('Matter__c'));
            thisMatter.Total_Clockstopper_Time__c = (decimal) ar.get('time');
            matterIds.put(thisMatter.id,thisMatter);
        } 
        
        database.update(matterIds.values(),false);                                                   
    }
    

    /**
    * the idea here is when a clock stopper is modified we need to see if the task it is related to has any remaining 'Open'
    * clock stoppers. If not, and the task says that it does, update it to say it doesn't. If it does have open clockstopers and the
    * task says that it doesn't, update it to say that it does not. So basically
    * 1) Make a list of all the clockstoppers passed in
    * 2) Find all the tasks related to any of those clockstoppers
    * 3) Group the clockstoppers to their related task
    * 4) Look to see if any of the related clockstoppers are open. If so perform update if required, and vice versa.
    * @param clockStoppers a list of clockstoppers for which to find related tasks and update the open_clockstoppers__c flag if required
    * @return null
    */
    public static void setOpenClockstoppersFlagOnTask(list<Clockstopper__c> clockStoppers)
    {
        
        map<id, task> taskMap = new map<id,task>();
        map<id, list<Clockstopper__c>> taskToClockstoppersMap = new map<id,list<Clockstopper__c>>();
        set<task> updateTasks = new set<task>();

        //for every clockstopper create an entry in the task map with the id of the clock stopper, and another
        //entry in the map that contains a task id and a list of related clockstoppers.
        for(Clockstopper__c thisClockStopper :  clockstoppers)
        {
            if(thisClockStopper.Related_Task__c != null)
            {
                taskMap.put((id) thisClockStopper.Related_Task__c,null);
                taskToClockstoppersMap.put((id) thisClockStopper.Related_Task__c, new list<Clockstopper__c>());
            }   
        }  

        //okay now find any clockstoppers that are not closed for the related tasks we made a list of earlier.
        for(Clockstopper__c thisClockStopper : [select id, 
                                                    status__c,
                                                    Related_Task__c
                                             from clockstopper__c
                                             where Related_Task__c in : taskMap.keySet() and 
                                                   Related_Task__c != null])
        {
            list<Clockstopper__c> thisTasksClockStoppers = taskToClockstoppersMap.get((id) thisClockStopper.Related_Task__c); 
            thisTasksClockStoppers.add(thisClockstopper);       
            taskToClockstoppersMap.put((id) thisClockStopper.Related_Task__c, thisTasksClockStoppers);
        }                                          
        
        //now we need to find details about the tasks, mostly to see if the flag is set in the way it should be. We don't want to 
        //make pointless updates.
        for(task thisTask : [select Open_Clockstopper__c, id 
                             from task 
                             where id in :taskMap.keySet()])
        {
            taskMap.put(thisTask.id,thisTask);
        }
        
        //alright now time for the actual calculation of whether the task has open clockstoppers or not.
        for(id thisTaskId : taskToClockStoppersMap.keySet())
        {
            
            //get the details about the task we are looking at so we know if it is marked as open or closed           
            Task thisTask = taskMap.get(thisTaskId);
                    
            boolean hasOpenClockstoppers = false;
            
            for(Clockstopper__c thisStopper : taskToClockstoppersMap.get(thisTask.Id))
            {
                if(thisStopper.status__c != 'Closed')
                {
                    hasOpenClockstoppers = true;
                    break;
                }
            }
            if(!hasOpenClockstoppers && thisTask.Open_Clockstopper__c == true)
            {
                thisTask.Open_Clockstopper__c = false;
                updateTasks.add(thisTask);
            }
            if(hasOpenClockstoppers && thisTask.Open_Clockstopper__c == false)
            {
                thisTask.Open_Clockstopper__c = true;
                updateTasks.add(thisTask);
            }
        }                      
        
        if(!updateTasks.isEmpty())
        {
            list<task> updates = new list<task>();
            updates.addAll(updateTasks);
            
            database.update(updates,true);
        }
    }
}