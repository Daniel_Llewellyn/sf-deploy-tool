public class Axiom_CustomLogin {

 /** This is a hack to determine is the page is using SSL or not.  If CipherSuite is defined that means it is HTTPS **/
    public Boolean hasSSL { get { return ApexPages.currentPage().getHeaders().get('CipherSuite') != null; } }
    
    /** This is used to determine the device type **/
    public String userAgent { get { return ApexPages.currentPage().getHeaders().get('USER-AGENT');  } }
   
    /** This will return which Salesforce Server you are on, example: na8.salesforce.com **/
    public String currentServer { get {return ApexPages.currentPage().getHeaders().get('X-Salesforce-Forwarded-To'); } }
    
    public Axiom_CustomLogin()
    {
        if(!hasSSL)
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'This page must be accessed via SSL/HTTPS'));
        }
    }

}