/******* Axiom Matter Controller *********
@Author Daniel Llewellyn/RedKite (dllewellyn@redkitetechnologies.com)
@Date ~9/2013
@Description Methods and properties related to the salesforce Matter object. Also contains visualforce properties for matterProgress visualforce page. 
**********************************************/

global class Axiom_MatterController
{    
    public matter__c thisMatter;
 
    /**
    * property that returns as a percent the number of completed tasks related to this matter. Used by the matterProgress visualforce page to render the progressbar   
    **/
    public double completedTaskPercent{get{
        return Axiom_Utilities.getCompletedTaskPercent(thisMatter.id);
    }set;}
    
    /**
    * init method puts controller record (matter) in public scope
    **/
    public Axiom_MatterController(ApexPages.StandardController controller) {
        thisMatter = (matter__c) controller.getRecord();
    }

    /**
    * When a matter that is related to a request is created attempt to copy over any values where the fieldnames match 
    * on both objects exactly. Ex: if request has a field called Name and so does the matter attempt to copy the value
    * from the parent request's name field and put it in the matter's name field. Skips over any failures that may occure
    * due to data type/security/validation rule issues.
    * @param matters a list of matter records which to set values for from their parent requests
    * @return null
    **/
    public static void initMatterToRequest(list<matter__c> matters)
    {
        map<id,sobject> requestToMatterMap = new map<id,sObject>();
        for(matter__c thisMatter : matters)
        {
            if(thisMatter.request__c != null){           
                requestToMatterMap.put(thisMatter.request__c,thisMatter);
            }
        }        
        
        //because this method modifies the matter record directly and it's on a before insert trigger no updates or further logic is required.
        //simply calling this is enough to set the child objects to have their parents data.
        map<id,sObject> matterToVersionMap = Axiom_Utilities.prepopulateObject(requestToMatterMap, null);

    }
    
    /** 
    * Sets the number of matters total and the number of open matters on any request related to any of the matters passed in.
    * will also set the numerous matters flag if the request has multiple matters. 
    * Matter "closed" status is: Stage= Finalize & Step = Finalized & Status = Complete
    * for matters being open or closed, it simple accounts for their existance.
    * @param matters a list of matters for which to find related requests and update their matter fields.
    * @return null
    **/
    public static void setNumMattersOnRequest(list<matter__c> matters)
    {
        map<Id,Request__c> updateRequests = new map<Id,Request__c>();
        
        for(matter__c thisMatter : matters)
        {
            Request__c thisRequest = new Request__c(id=thisMatter.Request__c);
            thisRequest.Number_of_Matters__c = 0;
            thisRequest.Number_of_Open_Matters__c = 0;
            thisRequest.Numerous_Matters__c = false;
            
            updateRequests.put(thisMatter.Request__c, thisRequest);
        }
        //run the query to sum the data
        list<Matter__c> numMatters = [select  Request__c,
                                              isClosed__c                    
                                      From Matter__c
                                      where Request__c in : updateRequests.keySet()];
        
        for(Matter__c thisMatter: numMatters)
        {
            Request__c thisReq = updateRequests.get(thisMatter.Request__c);             
            thisReq.Number_of_Matters__c++;
            if(!thisMatter.isClosed__c )
            {
                thisReq.Number_of_Open_Matters__c++;
            }
                       
            if(thisReq.Number_of_Matters__c > 1 && thisReq.Numerous_Matters__c == false)
            {
                thisReq.Numerous_Matters__c = true;
            }
            updateRequests.put(thisReq.Id,thisReq);
        }
        update updateRequests.values();        
    }
    
    
    
    /**
    * Uses business hour logic to calculate the number of days to first substantive response on the matter object
    * Does not perform actual update statment as it it meant to be invoked by the Axiom_UpdateMattersBatch class. Matters
    * passed in must include their createdDate and should include First_Substantive_Response__c. 
    * If the First_Substantive_Response__c field is null then the current time will be used to calculate the number_of_days_to_FSR.
    * @param matters list of matters to calculate number_of_days_to_FSR__c for. 
    * @return list matters with Number_of_days_to_FSR__c field calculated and ready to be updated..
    **/
    public static list<Matter__c> setNumberOfDaysToFsr(list<Matter__c> matters){
        for(Matter__c thisMatter : matters)
        {
        	if(thisMatter.First_Substantive_Response__c == null) continue;
            datetime startTime = thisMatter.createdDate != null ? thisMatter.createdDate : datetime.now();
            datetime stopTime = thisMatter.First_Substantive_Response__c;
            thisMatter.Number_of_Days_to_FSR__c = Axiom_Utilities.calculateBusinessDaysBetweenDates(startTime, stopTime);   
        }
        return matters;
    }

    /**
    * Uses business hour logic to calculate the number of days to open on the matter object
    * Does not perform actual update statment as it it meant to be invoked by the Axiom_UpdateMattersBatch class. Matters
    * passed in must include their createdDate,Stop_Time__c, and status__c fields.
    * @param matters list of matters to calculate days_open__c for. 
    * @return list matters with days_open__c field calculated and ready to be updated..
    **/
    public static list<Matter__c> setNumberOfDaysOpen(list<Matter__c> matters){
        for(Matter__c thisMatter : matters)
        {
            datetime startTime = thisMatter.createdDate != null ? thisMatter.createdDate : datetime.now();
            datetime stopTime = thisMatter.Stop_Time__c != null ? thisMatter.Stop_Time__c : datetime.now();

            thisMatter.Days_Open__c = Axiom_Utilities.calculateBusinessDaysBetweenDates(startTime, stopTime);
        }
        return matters;
    }    

    
    /**
    * the idea here is when a matter is modified we need to see if the Request__c it is related to has any remaining 'Open'
    * clock stoppers. If not, and the Request__c says that it does, update it to say it doesn't. If it does have open clockstopers and the
    * Request__c says that it doesn't, update it to say that it does not. So basically
    * 1) Make a list of all the Matters passed in
    * 2) Find all the Requests related to any of those Matters
    * 3) Group the Matters to their related Request__c
    * 4) Look to see if any of the related Matters are open. If so perform update if required, and vice versa.
    * @param Matters a list of Matters for which to find related Requests and update the open_Matters__c flag if required
    * @return null
    **/
    public static void setOpenClockStoppersFlagOnRequest(list<Matter__c> Matters)
    {

        map<id, Request__c> RequestMap = new map<id,Request__c>();
        map<id, list<Matter__c>> RequestToMatterMap = new map<id,list<Matter__c>>();
        set<Request__c> updateRequests = new set<Request__c>();

        //for every Matter create an entry in the Request__c map with the id of the clock stopper, and another
        //entry in the map that contains a Request__c id and a list of related Matters.
        for(Matter__c thisMatter :  Matters)
        {
            if(thisMatter.Request__c != null)
            {
                RequestMap.put((id) thisMatter.Request__c,null);
                RequestToMatterMap.put((id) thisMatter.Request__c, new list<Matter__c>());
            }   
        }  

        //okay now find any Matters that are not closed for the related Requests we made a list of earlier.
        for(Matter__c thisMatter : [select id, 
                                            status__c,
                                            Request__c,
                                            Open_Clockstopper__c
                                     from Matter__c
                                     where Request__c in : RequestMap.keySet() and 
                                           Request__c != null])
        {
            list<Matter__c> thisRequestsMatters = RequestToMatterMap.get((id) thisMatter.Request__c); 
            thisRequestsMatters.add(thisMatter);       
            RequestToMatterMap.put((id) thisMatter.Request__c, thisRequestsMatters);
        }                                          
        
        //now we need to find details about the Requests, mostly to see if the flag is set in the way it should be. We don't want to 
        //make pointless updates.
        for(Request__c thisRequest    : [select Open_Clockstopper__c, id 
                                         from Request__c 
                                         where id in :RequestMap.keySet()])
        {
            RequestMap.put(thisRequest.id,thisRequest);
        }
        
        //alright now time for the actual calculation of whether the Request__c has open Matters or not.
        for(id thisRequestId : RequestToMatterMap.keySet())
        {
            
            //get the details about the Request__c we are looking at so we know if it is marked as open or closed           
            Request__c thisRequest = RequestMap.get(thisRequestId);
                    
            boolean hasOpenClockstoppers = false;
            
            for(Matter__c thisMatter : RequestToMatterMap.get(thisRequest.Id))
            {
                if(thisMatter.Open_Clockstopper__c)
                {
                    hasOpenClockstoppers = true;
                    break;
                }
            }
            if(!hasOpenClockstoppers && thisRequest.Open_Clockstopper__c == true)
            {
                thisRequest.Open_Clockstopper__c = false;
                updateRequests.add(thisRequest);
            }
            if(hasOpenClockstoppers && thisRequest.Open_Clockstopper__c == false)
            {
                thisRequest.Open_Clockstopper__c = true;
                updateRequests.add(thisRequest);
            }
        }                      
        
        if(!updateRequests.isEmpty())
        {
            list<Request__c> updates = new list<Request__c>();
            updates.addAll(updateRequests);
            
            database.update(updates,true);
        }
    }    
}