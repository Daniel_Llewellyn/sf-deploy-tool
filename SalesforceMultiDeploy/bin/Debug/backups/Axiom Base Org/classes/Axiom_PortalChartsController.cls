/******* Axiom Portal Charts Controller *********
@Author Daniel Llewellyn/RedKite (dllewellyn@redkitetechnologies.com)
@Date ~3/2014
@Description Queries that provide data for several of the charts in the client portal. 
**********************************************/
public without sharing class  Axiom_PortalChartsController
{

    /**
    * Remote method that finds the average days to completion for all requests in the org that are
    * accepted and complete. Grouped by year and month
    * @return list<aggregateResult> list of request records grouped by year and month
    **/
    @remoteAction
    public static list<AggregateResult> averageDaysToCompletion()
    {
        return [select 
                   CALENDAR_YEAR(Stop_Time__c)Year, 
                   CALENDAR_MONTH(Stop_Time__c)Month,
                   AVG(Days_Open__c)Avg_Days_Open
                   from Request__c 
                   where stage__c = 'Request' and
                         step__c = 'Accepted' and 
                         status__c = 'Complete'         
                   group by ROLLUP(CALENDAR_YEAR(Stop_Time__c), CALENDAR_MONTH(Stop_Time__c))];
                                                 
    }

    /**
    * Remote method that finds the average days to completion for all requests in the org for a given user 
    *(override of the averageDaysToCompletion method without userId param) that are accepted and complete. 
    * Grouped by year and month
    * @return list<aggregateResult> list of request records grouped by year and month
    **/
    @remoteAction
    public static list<AggregateResult> averageDaysToCompletion(id userid)
    {
        return [select 
                   CALENDAR_YEAR(Stop_Time__c)Year, 
                   CALENDAR_MONTH(Stop_Time__c)Month,
                   AVG(Days_Open__c)Avg_Days_Open
                   from Request__c 
                   where stage__c = 'Request' and
                         step__c = 'Accepted' and 
                         status__c = 'Complete' and   
                         OwnerId = :userId       
                   group by ROLLUP(CALENDAR_YEAR(Stop_Time__c), CALENDAR_MONTH(Stop_Time__c))];
                                                 
    }

    /**
    * Remote method that finds requests that are open currently (do not have a step__c of "cancelled" or "Accepted"
    * and do not have a status__c of "Complete"). 
    * @return list<Request__c> a list of request records to be interpreted by the client into a chart
    **/    
    @remoteAction
    public static list<Request__c> openRequestAging()
    {
        return [Select
                    Name,
                    Type_of_Record__c,
                    Request_Age__c,
                    Stage__c,
                    Step__c,
                    Status__c, 
                    Member_of_Legal_Team__c,
                    LOB_Business_Unit__c,
                    Geography__c,
                    Owner.name,
                    recordType.name
                    From Request__c
                where 
                    Step__c not in ('Cancelled','Accepted') and 
                    Status__c != 'Complete'];
    }
 

    /**
    * Gets a list of request records. No filtering, or grouping applied. 
    * @return list of requests.
    ***/
    @remoteAction
    public static list<Request__c> workBreakdown()
    {
        
        return [Select
                    Type_of_Record__c,
                    Request_Age__c,
                    Stage__c,
                    Step__c,
                    Status__c, 
                    Member_of_Legal_Team__c,
                    LOB_Business_Unit__c,
                    Geography__c,
                    RecordType.Name,
                    Owner.Name
                    From Request__c ];  
    }

     /**
    * Gets a list of request records for a specific user. Override of the workBreakdown without userId param.
    * @return list of requests for the given user
    ***/  
    @remoteAction
    public static list<Request__c> workBreakdown(id userid)
    {
        
        return [Select
                    Type_of_Record__c,
                    Request_Age__c,
                    Stage__c,
                    Step__c,
                    Status__c, 
                    Member_of_Legal_Team__c,   
                    LOB_Business_Unit__c,
                    Geography__c,
                    RecordType.Name,
                    Owner.Name
                    From Request__c 
                    where OwnerId = :userId];  
    }
 
    /**
    * calculates the average number of days to first response on a matter where the matter has not been
    * cancelled for a specific user.
    * @param userId the id of the user to calculate averageDaysToFirstResponse for
    * @return list of aggregate results data grouped by year then month of the date of the first substantive response
    ***/       
    @remoteAction 
    public static list<AggregateResult> averageDaysToFirstResponse()
    {
        return [select 
                   CALENDAR_YEAR(First_Substantive_Response__c)Year,  
                   CALENDAR_MONTH(First_Substantive_Response__c)Month,
                   AVG(Number_of_Days_to_FSR__c)Avg_Response_Days
                   from Matter__c 
                   where step__c != 'Cancelled'                   
                   group by ROLLUP(CALENDAR_YEAR(First_Substantive_Response__c),CALENDAR_MONTH(First_Substantive_Response__c))];    
    }

    /**
    * calculates the average number of days to first response on a matter where the matter has not been
    * cancelled. Override of the averageDaysToFirstResponse without the userid param.
    * @param userId the id of the user to calculate averageDaysToFirstResponse for
    * @return list of aggregate results data grouped by year then month of the date of the first substantive response
    ***/
    @remoteAction 
    public static list<AggregateResult> averageDaysToFirstResponse(id userid)
    {
        return [select 
                   CALENDAR_YEAR(First_Substantive_Response__c)Year,  
                   CALENDAR_MONTH(First_Substantive_Response__c)Month,
                   AVG(Number_of_Days_to_FSR__c)Avg_Response_Days
                   from Matter__c 
                   where step__c != 'Cancelled'     
                   and OwnerId = :userId              
                   group by ROLLUP(CALENDAR_YEAR(First_Substantive_Response__c),CALENDAR_MONTH(First_Substantive_Response__c))];    
    }      
}