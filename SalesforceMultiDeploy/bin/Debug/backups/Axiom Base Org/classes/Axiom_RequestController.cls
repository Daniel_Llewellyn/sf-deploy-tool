/******* Axiom Request Controller *********
Author Daniel Llewellyn/RedKite (dllewellyn@redkitetechnologies.com)
Date ~9/2013
Description Methods and properties related to the salesforce Request object.
**********************************************/

global class Axiom_RequestController
{
    /**
    * Uses business hour logic to calculate the number of days that passed to the first substantive response
    * for a request. Invoked via batch process Axiom_UpdateRequestBatch. Requests passed in must include createdDate and stop_time__c field. If stop time is provided it is used,
    * otherwise uses current date time. Does not perform actual update as it is meant to be invoked by the Axiom_UpdateRequestBatch
    * class.
    * @param requests list of requests to calculate days open for.
    * @return list of requests with days_open__c field calculated and ready to be updated.
    */
     public static list<Request__c> setNumberOfDaysToFsr(list<Request__c> requests)
     {
        for(Request__c thisRequest : requests)
        {
            datetime startTime = thisRequest.createdDate != null ? thisRequest.createdDate : dateTime.now();
            datetime stopTime = thisRequest.stop_time__c != null ? thisRequest.stop_time__c : dateTime.now();

            thisRequest.Days_Open__c = Axiom_Utilities.calculateBusinessDaysBetweenDates(startTime, stopTime);
        }
        return requests;
    }
    
    /**
    * When a request is accepeted it's starttime__c field is populated. When this happens all related matters need to have their owner changed to the owner of the request record
    * @param requests a list of requests whos matters need to get updated
    * @return list of matters that were updated
    **/
    public static list<Matter__c> changeMatterOwnersToRequestOwner(map<id,Request__c> oldMap, map<id,Request__c> newMap)
    {
        set<id> requestIds = new set<id>();
        for(Request__c thisRequest : newMap.values())
        {
        	if(thisRequest.Owner.Type != 'queue')
        	{	
	            //we only want to update matter owners if the start time on the request was just changed. This means that if the request was inserted with a start time we need to
	            //account for that, or if the request was just updated to have a start time, we account for that as well. So one if conidition to see if this is an insert with a start time
	            //set by checking for existance of this record in the oldmap, if no entry found and the request has a start time, process it. If the oldmap DOES have an entry for this
	            //request and the new value isn't the same as the old value, and it also isn't null then process it.
	            if( (!oldMap.containsKey(thisRequest.id) && thisRequest.Start_Time__c != null) || 
	                (oldMap.containsKey(thisRequest.id) && oldMap.get(thisRequest.id).Start_Time__c != thisRequest.Start_Time__c && thisRequest.Start_Time__c != null))
	            { 
	                requestIds.add(thisRequest.id);
	            }
        	}
        }
        
        list<matter__c> updateMatters = [select id, Request__r.ownerId from matter__c where request__c in :requestIds];
        
        for(Matter__c thisMatter : updateMatters)
        {
            thisMatter.ownerId = thisMatter.Request__r.OwnerId;
        }
        
        database.update(updateMatters,false);
        
        return updateMatters;
    }
}