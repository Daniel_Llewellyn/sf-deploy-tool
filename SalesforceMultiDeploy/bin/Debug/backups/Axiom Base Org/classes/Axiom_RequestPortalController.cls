/******* Axiom Request Portal Controller *********
@Author Daniel Llewellyn/RedKite (dllewellyn@redkitetechnologies.com)
@Date ~1/2014
@Description Methods and properties related to the request portal page for creating requests/matters. Most all data is passed into these
             methods via the jQuery serialize method which creates a url encoded string of data, like a form submission. That string 
             is then parsed into a map of string/string with the fieldname as they key and the value as the value. You'll see that
             design pattern a lot just, just to explain why almost all methods only have on argument/parameter
***********************************************/

global class Axiom_RequestPortalController {


    /** 
    * finds an existing contact in the system based on their email. If none found, return a blank contact object so the 
    * interface doesn't choke of null references 
    * @param dataString serialized form data
    * @return remoteObject containing results of the function all including the found contact. 
    **/
    @RemoteAction
    global static remoteObject findExistingContact(string dataString)
    {
        remoteObject returnObj = new remoteObject();
        try
        {     
            contact thisContact;
            //turn the serialized string of data that looks like firstname=frank&lastname=jones&email=fjones%40gmail.com into a map of strings.
            map<string,string> formData = deserializeString(dataString);
            
            //see if a contact with this email already exists. If so, update existing contact.
            
            list<contact> existingContact = [select id, 
                                                    firstname, 
                                                    lastname, 
                                                    email, 
                                                    phone, 
                                                    title, 
                                                    department, 
                                                    Geography__c,
                                                    Member_of_Legal_Team__c,
                                                    Employee_Identification_Number_EIN__c,
                                                    LOB_Business_Unit__c,
                                                    Organization_Unit_Code__c,
                                                    Customer_Supplier_Name__c,
                                                    Account.industry 
                                                    from contact where email = :formData.get('search_email')];
                                                    
            //if there is a contact add it to our result set
            if(!existingContact.isEmpty())
            {
                returnObj.sObjects = existingContact;
            }
            else
            {
                returnObj.sObjects.add(New Contact());
            }
        }
        //handle any errors nicely, return error info to the interface in a safe way
        catch(exception e)
        {
            returnObj.success = false;
            returnObj.message = e.getMessage() + ' on line ' + e.getLineNumber() + ' Type: ' + e.getTypeName();
        }
        return returnObj;            
            
    }

    /** 
    * creates or updates a contact in the system. If a contact is not found (match on email) then one is created using the data passed in. 
    * @param dataString serialized form data
    * @return remoteObject containing results of the function all including the found/created contact. 
    **/
    @RemoteAction
    global static remoteObject saveContact(string dataString)
    {
       remoteObject returnObj = new remoteObject();
        try
        {    
            contact thisContact;
            
            //turn the serialized string of data that looks like firstname=frank&lastname=jones&email=fjones%40gmail.com into a map of strings.
            map<string,string> formData = deserializeString(dataString);
            
            //see if a contact with this email already exists. If so, update existing contact.
            list<contact> existingContact = [select id, name from contact where email = :formData.get('email')];
            if(!existingContact.isEmpty())
            {
                thisContact = existingContact[0];
            }
            else
            {
                thisContact = new Contact();
            }
            
            //match this contact to the proper account based on the geographic region
            list<account> parentAccount = [select id from account where billingState = :formData.get('region')];
            if(!parentAccount.isEmpty())
            {
                thisContact.accountId = parentAccount[0].Id;
            }
            else
            {
                throw new applicationException('No matching account for this region could be found.');
            }      
            
            //iterate over all the form fields passed in and attempt to assign them to the contact. Since the form field names
            //match the contact names exactly (when they are supposed to, there will be some random hidden fields and request related fields we can skip over)
            //we can just do a nice direct matching like this.
            for(string field : formData.keySet())
            {
                try
                {
                    thisContact.put(field,formData.get(field));
                }
                catch(exception e){
                    //many invalid form fields will get passed in. Ignore them.
                }
            }      

            upsert thisContact;
            
            //query the contact again to get any values that may have been updated via workflow rules or triggers (record type in particular gets updated based on some fields), etc
            //so it can be returned to the interface

            thisContact = [select firstname,
                                  lastname, 
                                  title,
                                  email,
                                  department,
                                  geography__c,
                                  employee_identification_number_ein__c,
                                  lob_business_unit__c,
                                  Organization_Unit_Code__c,
                                  Customer_Supplier_Name__c,
                                  recordType.Name,
                                  recordType.DeveloperName,
                                  member_of_legal_team__c
                                  from contact where id = :thisContact.id]; 
            returnObj.fieldValues.put('recordType',thisContact.recordType.Name);                                                
            returnObj.sObjects.add(thisContact);   
        }
        catch(exception e)
        {
            returnObj.success = false;
            returnObj.message = e.getMessage() + ' on line ' + e.getLineNumber() + ' Type: ' + e.getTypeName();        
        }
        return returnObj;        
    }
    
    /**
    * given a serialized data string that represents fields on a request, creates and saves a request. 
    * unlike the saveMatter method, the saveRequest method will only set certain files on the request inlcuding:
    * counterparty__c, recordType (via the requestCategory param), and the requesting_contact__c (via contact__c)
    * @param dataString a url encoded/serialized string of data (ex: firstname=frank&lastname=jones&email=fjones%40gmail.com) with keys that match the paramters listed above.
    * @return a remoteObject with data about the call including the request object created and the result of the save operation.
    **/
    @RemoteAction 
    global static remoteObject saveRequest(string dataString)
    {
        remoteObject returnObj = new remoteObject();
        try
        {        
            //turn the serialized string of data that looks like firstname=frank&lastname=jones&email=fjones%40gmail.com into a map of strings.
            map<string,string> formData = deserializeString(dataString);

            request__c thisRequest = new Request__c();
            
            //the the record type we need based on the requestCategory field
            list<RecordType> requestRecordType = [select DeveloperName, IsActive, Name, SobjectType from RecordType where SobjectType = 'Request__c' and (DeveloperName = :formData.get('requestCategory') or Name = :formData.get('requestCategory'))limit 1];
            if(requestRecordType.isEmpty())
            {
                throw new applicationException('Specified record type "'+formData.get('requestCategory')+'" for Request could not be found');
            }
            
            //set fields on the request
            thisRequest.Counterparty__c = formData.get('counterparty__c');
            thisRequest.Request_Method__c = 'Request Portal';           
            thisRequest.recordTypeId = requestRecordType[0].Id;
            string counterParty = thisRequest.CounterParty__c.length() > 10 ? thisRequest.CounterParty__c.left(10) : thisRequest.CounterParty__c;
            thisRequest.Name = counterParty+'/'+requestRecordType[0].Name + '_' + date.today();
            //sometimes the name field gets really long, so we have to trim it down to 80 chars if its too long
            thisRequest.Name = thisRequest.name.length() > 80 ? thisRequest.name.left(80) : thisRequest.name;
            thisRequest.Requesting_Contact__c = formData.get('contact__c');
                   
            insert thisRequest;
            returnObj.sObjects.add(thisRequest);    
        }
        catch(exception e)
        {
            returnObj.success = false;
            returnObj.message = e.getMessage() + ' on line ' + e.getLineNumber() + ' Type: ' + e.getTypeName();        
        }
        return returnObj;    
    }
    
    /**
    * given a serialized data string that represents fields on a matter, creates and saves a matter. Any param that matches a fieldname on the matter
    * will be set. May also include a matter_type param that reprsents the api name or the label of a record type to use for that matter. Request_action may
    * also be set by including a matter_actiom param. Request_method__c is automatically set to request_portal and cannot be overriden.
    * @param dataString a url encoded/serialized string of data (ex: firstname=frank&lastname=jones&email=fjones%40gmail.com) with keys that match matter field names and values for those fields.
    * @return a remoteObject with data about the call including the matter object created and the result of the save operation.
    **/
    @RemoteAction
    global static remoteObject saveMatter(string dataString)
    {
        remoteObject returnObj = new remoteObject();
        
        try
        {
            //turn the serialized string of data that looks like firstname=frank&lastname=jones&email=fjones%40gmail.com into a map of strings.
            map<string,string> formData = deserializeString(dataString);
            
            //find alll the fields on the matter
            Map<String, Schema.SobjectField> matterFields = Matter__c.sObjectType.getDescribe().fields.getMap();
            
            matter__c thisMatter = new matter__c();
            
            //iterate over every field on the matter, then see if we have a matching field in the incoming data from the form.
            //if so set it.
            for(string fieldName : formData.keySet())
            {
                //do we have a matching field in the form data?
                if(matterFields.containsKey(fieldName))
                {
                    try
                    {
                        //find the type of field. 
                        string fieldType = matterFields.get(fieldName).getDescribe().getType().name();
                        
                        //If it is a date, we need do some special formatting. Dates come in as yyyy-mm-dd.
                        if(fieldType == 'Date' && formData.get(fieldName).length() > 0)
                        {
                            string dateString = formdata.get(fieldName);
                            dateString = dateString.replace('/','-'); //incase somehow / was used as a separator
                            list<string> dateParts = dateString.split('-');
                            
                
                            thisMatter.put(fieldName,date.newInstance(Integer.valueOf(dateParts[0]),Integer.valueOf(dateParts[1]),Integer.valueOf(dateParts[2])));
                        }   
                        else if( (fieldType == 'Currency' || fieldType == 'Double' || fieldType =='Integer' || fieldType == 'Percent') && formData.get(fieldName).length() > 0)
                        {
                            thisMatter.put(fieldName,double.valueOf(formData.get(fieldName)));
                        }                        
                        //otherwise just attempt to write the value to the field
                        else if(formData.get(fieldName).length() > 0)
                        {
                            thisMatter.put(fieldName,formData.get(fieldName));
                        }
                    }
                    catch(exception e)
                    {
                        system.debug('Unable to put field: '+fieldName+' with value ' + formData.get(fieldName) + '. Error: ' + e.getMessage());
                        returnObj.data = returnObj.data += '\n Unable to put field: '+fieldName+' with value ' + formData.get(fieldName) + '. Error: ' + e.getMessage();
                    }
                }
            }
            
            //like the request, sometimes the names get a little out of control, so we make sure to trim it down to 80 chars if needed.
            thisMatter.name = formData.get('Request_Action__c') + '_' + date.today();
            thisMatter.name = thisMatter.name.length() > 80 ? thisMatter.name.left(80) : thisMatter.name;
            
            //find the correct record type for this request
            list<RecordType> thisMatterRecordType = [select DeveloperName, 
                                                            IsActive, 
                                                            Name, 
                                                            SobjectType 
                                                     from RecordType 
                                                     where 
                                                         SobjectType = 'Matter__c' and 
                                                         (name like :(string) formData.get('matter_type') or DeveloperName like :stringToApiName((string) formData.get('matter_type')))
                                                         limit 1];
            if(thisMatterRecordType.isEmpty())
            {
                throw new applicationException('Specified record type "'+formData.get('matter_type')+'" for Work Request could not be found');
            }          
            thisMatter.recordTypeId =  thisMatterRecordType[0].Id;   
            thisMatter.Request_Action__c = formData.get('matter_action');
            thisMatter.Request_Method__c = 'Request Portal';
            insert thisMatter;          
            returnObj.sObjects.add(thisMatter);
        }
        catch(exception e)
        {
            returnObj.success = false;
            returnObj.message = e.getMessage() + ' on line ' + e.getLineNumber() + ' Type: ' + e.getTypeName();        
        }
        return returnObj;     
    
    }
    /**
    * using the Axiom Section Fields custom setting object we return the list of fields to display for that matter based on its 
    * record type. So if a matter has the record type 'research documents' we'll find all the matter fields that have that record type or '*' specified as their record type
    * and return them. Also returns important details such as the field type, if it is required, and if it's a picklist all the possible picklist values (doesn't respect record type
    * picklist value filtering, use getPicklistOptionsForRecordType() to filter it down if needed.
    * @param recordType a string that is the name of the recordType for which to find all matter fields that apply to that type of matter.
    * @return a map of string to fieldDetails. The key is the field name and the value is details about that field. All entries are fields valid for that matter record type.
    **/
    @RemoteAction
    global static Map<String, fieldDetails> getMatterFields(string recordType)
    {
        list<integer> applicableSections = new list<integer>();
               
        Map<String, fieldDetails> applicableMatterFields = new Map<String, fieldDetails>();
        
        //find all the fields that appy
        list<Client_Portal_Matter_Fields__c> fieldSections = [select Matter_Record_Type__c, 
                                                                     Matter_Field_Name__c,
                                                                     Display_Order__c,
                                                                     Default_Value__c,
                                                                     Required__c
                                                              from Client_Portal_Matter_Fields__c 
                                                              where (Matter_Record_Type__c = :recordType or 
                                                                    Matter_Record_Type__c = '*') and
                                                                    Active__c = true
                                                              order by Display_Order__c asc];
        
        //find info about about all the matter fields so we can include them in the return obj
        Map<String, Schema.SobjectField> matterFields = Matter__c.sObjectType.getDescribe().fields.getMap();
        
        //iterate over all the fields found    
        for(Client_Portal_Matter_Fields__c field : fieldSections)
        {
            //only return the field if it is actually a valid matter field (account for user creating records for fields that don't actually exist)
            //and if we don't already have a record for this field (prevent duplicates in case of multiple matching matter field records for one field)
            if(matterFields.containsKey(field.Matter_Field_Name__c) && !applicableMatterFields.containsKey(field.Matter_Field_Name__c))
            {
                //get schema info about this field from our describe call we made earlier   
                Schema.DescribeFieldResult fieldDetails = matterFields.get(field.Matter_Field_Name__c).getDescribe();
                
                //set info about the field
                fieldDetails fieldDetail = new fieldDetails();
                fieldDetail.dataType = string.valueOf(fieldDetails.getType());
                fieldDetail.required = !fieldDetails.isNillable() || field.Required__c ? true : false;
                fieldDetail.label = fieldDetails.getLabel();
                fieldDetail.apiName = fieldDetails.getName();
                fieldDetail.displayOrder = field.Display_Order__c;
                fieldDetail.isCustom = fieldDetails.isCustom();
                fieldDetail.defaultValue = field.Default_Value__c != null ? field.Default_Value__c : ''; 
                
                //if this is a picklist, include all picklist values
                if(string.valueOf(fieldDetails.getType()) == 'Picklist')
                {
                    for( Schema.PicklistEntry picklistItem : fieldDetails.getPicklistValues())
                    {
                        fieldDetail.picklistValues.put(picklistItem.getValue(),picklistItem.getLabel());                       
                    }
                }
                
                applicableMatterFields.put(field.Matter_Field_Name__c, fieldDetail);
            }
        }
        
        return applicableMatterFields;     
    }
    
    /** 
    * finds all possible record types for a given object 
    * @param objectType a string that is the api name of an sObject type
    * @returns remoteObject containing a all the record type names in the sObjects property
    **/
    @RemoteAction
    public static remoteObject getObjectRecordTypes(string objectType)
    {
        remoteObject returnObj = new remoteObject();
        try
        {    
            list<RecordType> recTypes = [select DeveloperName, IsActive, Name, SobjectType from RecordType where SobjectType = :objectType order by DeveloperName];
            
            //goofy code for putting any option with the word 'other' at the end of the list. I feel like there must be a more elegant way, but I'm crunched
            //for time and this works. Please don't hate me >.<
            list<RecordType> otherTypes = new list<recordType>();
            for(Integer i = 0; i < recTypes.size(); i++)
            {
                if(recTypes[i].Name.toLowerCase().contains('other'))
                {
                    otherTypes.add(recTypes[i]);
                    recTypes.remove(i);
                }    
            }
            recTypes.addAll(otherTypes);
            returnObj.sObjects = recTypes;
        }
        //handle any errors nicely, return error info to the interface in a safe way
        catch(exception e)
        {
            returnObj.success = false;
            returnObj.message = e.getMessage() + ' on line ' + e.getLineNumber() + ' Type: ' + e.getTypeName();
        }
        return returnObj;        
    }
    //Helper Methods
    
    /**
    * gets all possible picklist options for a given picklist field. Does not respect record type picklist filtering.
    * also puts any option that has the word 'other' at the end of the list. Items are returned as an object indexed numerically 
    * (basically an array, but not truly) so the interface can iterate over them in the order they are provided by SF and with 'other' options
    * at the end.
    * @param objectType The type of sObject that contains the field to get picklist options for
    * @param fieldName The name of the field to get picklist options for
    * @returns a map of string to string that contains the option value and option text for all options in the picklist.
    **/
    @RemoteAction
    public static map<string,string> getAllPicklistOptions(string objectType, string fieldName){
        map<string,string>options = new map<string,string>();
        
        //get global describe
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
        
        //get field info for given object type
        MAP<String,Schema.SObjectField> fieldResult = gd.get(objectType).getDescribe().fields.getMap();
         
        //get all picklist entries for the given field. 
        List<Schema.PicklistEntry> ple = fieldResult.get(fieldName).getDescribe().getPicklistValues();
        
        
        integer counter = 0;
        list<string> otherValues = new list<string>();
        
        //iterate over picklist entries
        for( Schema.PicklistEntry f : ple)
        {
            if(!f.getValue().toLowerCase().contains('other'))
            {
                options.put(string.valueOf(counter),f.getValue());
                counter++;
            }
            else
            {
                otherValues.add(f.getValue());
            }
        }       
        
        //now add any option that has the word 'other in it'
        for(string otherVal : otherValues)
        {
            options.put(string.valueOf(counter),otherVal);
            counter++;
        }
        return options;  
    }

    /**
    * gets all picklist options for a given field on the given record type. This of course DOES respect record type filtering using a nifty
    * hack involving a visualforce page and reading the content of the generated page. Not the most elegant thing but until we get metadata access through
    * apex it will have to do.
    * also puts any option that has the word 'other' at the end of the list. Items are returned as an object indexed numerically 
    * (basically an array, but not truly) so the interface can iterate over them in the order they are provided by SF and with 'other' options
    * at the end.
    **/    
    @RemoteAction
    public static map<string,string> getPicklistOptionsForRecordType(string objectType, string recordTypeName, string fieldName){
        List<string> opts = PicklistDescriber.describe(objectType, stringToApiName(recordTypeName), fieldName);
        map<string,string> options = new map<string,string>();
        
        integer counter = 0;
        list<string> otherValues = new list<string>();
        for(string opt : opts)
        {
            if(!opt.toLowerCase().contains('other'))
            {        
                options.put(string.valueOf(counter),opt);
                counter++;
            }
            else
            {
                otherValues.add(opt);
            }

        }   
        
        //now add any option that has the word 'other in it'    
        for(string otherVal : otherValues)
        {
            options.put(string.valueOf(counter),otherVal);
            counter++;
        }          
        
        //Sort the list of options (client decided they dont want logic for now)
        //options.sort();
        
        //Logic for putting the 'Other' option at the end of the list if this list contains such an option
        /*
        integer otherIndex;
        integer currentIndex = 0;
        for(string thisOption : options)
        {
            if(thisOption == 'other')
            {
                otherIndex = currentIndex;
                break;
            }
            currentIndex++;            
        }
        if(otherIndex != null)
        {
            options.remove(otherIndex);
            options.add('Other');
        }
        **/
        return options;
    }
    
    /**
    * finds additional fields to exclude from the matter action field based on contact record type and an additional field (usually LOB__c)
    * to further filter options displayed to user. So if a user has some certain conditions met we will apply further filtering to the types of
    * matter actions, request types, and matter types displayed.
    **/
    @remoteAction
    public static list<Client_Portal_Matter_Action_Filters__c> getMatterfieldExclusions(string recordType, string fieldName, string fieldValue)
    {
        list<Client_Portal_Matter_Action_Filters__c> exclusions = [select Contact_Field_Name__c,
                                                                          Contact_Field_Value__c,
                                                                          Contact_Record_Type__c,
                                                                          Filtered_Matter_Actions__c,
                                                                          Filtered_Request_Types__c,
                                                                          Filtered_Request_Types_Extended__c,
                                                                          Filtered_Matter_Types__c
                                                                          
                                                                   from
                                                                          client_Portal_Matter_Action_Filters__c 
                                                                   where 
                                                                          Contact_Record_Type__c = :recordType and
                                                                          Contact_Field_Value__c = :fieldValue and
                                                                          Contact_Field_Name__c = :fieldName];
                                                                                
        return exclusions;                                                                      
    }
    
    /**
    * Takes a string (usually a field label) and attempts to convert it into a valid Salesforce API name. This is useful for attempting 
    * to convert what may have been a user readable label into what the likely API name is. due to the architecture of the request portal
    * there are several occasions where we only have access to the label of something which later we will need to the API name for. If careful
    * by using this function we can reliably deduce the API name and perform whatever operation we need
    * @param nameToConvert a string that will be converted into a salesforce viable API name
    * @return string a converted version of the nameToConvert.
    **/
    public static string stringToApiName(string nameToConvert)
    {
        try
        {
            //regular expressions to be used for replacing
            string specialCharPatterns = '[^\\w]+';
            string multiUnderscorePattern = '_+';
            
            
            //replace special chars with underscores, and multiple underscores with one
            nameToConvert = nameToConvert.replaceAll(specialCharPatterns,'_').replaceAll(multiUnderscorePattern,'_');
            
            //remove leading underscores
            nameToConvert = nameToConvert.left(1) == '_' ? nameToConvert.substring(1) : nameToConvert;
            
            //remove trailing underscores
            nameToConvert = nameToConvert.right(1) == '_' ? nameToConvert.substring(0,nameToConvert.length()-1) : nameToConvert;
        }
        catch(exception e)
        {
        
        }            
        
            
        return nameToconvert;
    }
    /** 
    * decodes a url encoded string into a map of strings. When data is passed by the interface to this controller it comes as a url encoded/serialized
    * string which must be converted into something useable. This method can take that string of data ex: firstname=frank&lastname=jones&email=fjones%40gmail.com)
    * and convert it into a map of string to string where the key is the name of the parameter, and the value is the matching value. Assumes UTF-8 Encoding
    * @string argString a url encoded/serialized string of data to convert
    * @return a map of string to string where the key is the name of the url paramter and the value is its value
    **/    
    public static map<string,string> deserializeString(string argString)
    {
        string[] params = argString.split('&');
        map<String,String>  formParams = new map<String,String>();
        for(string p : params)
        {   
            string startParam = EncodingUtil.urlDecode(p.substring(0,p.indexOf('=')),'UTF-8');
            formParams.put(startParam,EncodingUtil.urlDecode(p.substring(p.indexOf('=')+1,p.length()),'UTF-8'));    
        }       
        return formParams;
    }    

    /**
    * container object to standaradize the return data type to the interface. Contains details about the operation, error messages, and any
    * other details that may be required by the interface to know the results of what happened via the js remoting call 
    **/
    global class remoteObject
    {
        public boolean success = true;
        public string message = 'operations successfull';
        public string data = 'success';
        public list<sObject> sObjects= new list<sObject>();        
        public list<portalObject> portalData = new list<portalObject>();
        public map<string,string> fieldValues = new map<string,string>();
    }   
    
    /**
    * wrapper class that contains a subset of the describe data for a field. Used to construct a field on a visualforce page with properties
    * similar to the one on an sObject. Useful for creating fields on the fly
    **/
    global class fieldDetails
    {
        public string dataType;
        public boolean required;
        public string label;
        public string apiName;
        public map<string,string> picklistValues = new map<string,string>();
        public boolean isCustom;
        public double displayOrder;
        public string defaultValue;
        
    } 
    
    /** 
    * Wrapper version of the getPicklistOptionsForRecordType method that returns data in a remote object instead of a map to work around 
    * a javascript bug that was causing some issues for the extJs framework.
    **/ 
    @RemoteAction
    public static remoteObject getPicklistOptionsForRecordTypeClientPortal(List<String> qr){
        
        String jsonInput = qr[0]; 
        map<String,Object> params = (map<String,Object>) System.JSON.deserializeUntyped(jsonInput);
        String objectType = (String)params.get('requestCategory');
        String recordTypeName = (String)params.get('requestCategoryDeveloper');
        String fieldName = (String)params.get('requestType');
    
        remoteObject returnObj = new remoteObject();
        try 
        {
            List<string> opts = PicklistDescriber.describe(objectType, stringToApiName(recordTypeName), fieldName);
            map<string,string> options = new map<string,string>();
            portalObject retPortalObj = null;
            
            integer counter = 0;
            list<string> otherValues = new list<string>();
        
            for(string opt : opts)
            {
                if(!opt.toLowerCase().contains('other'))
                {  
                    retPortalObj = new portalObject();
                    retPortalObj.id = string.valueof(counter);
                    retPortalObj.value = opt;                        
                    returnObj.portalData.add(retPortalObj);
                    counter++;
                }
                else
                {
                    otherValues.add(opt);
                }
            }   
        
            //now add any option that has the word 'other in it'    
            for(string otherVal : otherValues)
            {
                retPortalObj = new portalObject();
                retPortalObj.id = string.valueof(counter);
                retPortalObj.value = otherVal;                        
                returnObj.portalData.add(retPortalObj);
                counter++;
            }          
        }
        //handle any errors nicely, return error info to the interface in a safe way
        catch(exception e)
        {
            returnObj.success = false;
            returnObj.message = e.getMessage() + ' on line ' + e.getLineNumber() + ' Type: ' + e.getTypeName();
        }
        return returnObj;  
    }
    
    /** Simple class to move data in and out of the portal. **/
    public class portalObject{
        public String id {get;set;}
        public String value {get;set;}  
    }
    
    /** Allows throwing of custom exceptions **/
    public class applicationException extends Exception {}
    
}