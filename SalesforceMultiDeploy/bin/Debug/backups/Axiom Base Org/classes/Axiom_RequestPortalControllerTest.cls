@isTest
global class Axiom_RequestPortalControllerTest
{
    @isTest
    global static void testAxiom_RequestPortalController()
    {   
        //create account that created contact will be joined to by region match
        Account testParentAccount = new Account();
        testParentAccount.name = 'Test Account';
        testParentAccount.billingState = 'NY';
        insert TestParentAccount;
        
        //string of data like will be sent to this function in real application. It's a serialized string of form data.
        string contactCreateDataString = 'firstname=John&lastname=Doe&member_of_bt_legal_team__c=No&employee_identification_number_ein__c=199999999&geography__c=Europe+and+LATAM&lob_business_unit__c=BT+Public+Sector&title=Developer&phone=555-555-5555&email=test_email%40test.com&email_conf=test_email%40test.com&organization_unit_code__c=test&customer_supplier_name__c=Unknown&region='+testParentAccount.billingState;
        
        Axiom_RequestPortalController.remoteObject createContact = Axiom_RequestPortalController.saveContact(contactCreateDataString);
        system.debug(createContact);
        system.assertEquals(createContact.success,true);
        
        
        string contactFindDataString = 'search_email=test_email@test.com';
        Axiom_RequestPortalController.remoteObject findContact = Axiom_RequestPortalController.findExistingContact(contactFindDataString);
        
        system.assertEquals(findContact.success,true);
        system.assertEquals(findContact.sObjects[0].get('email'),createContact.sObjects[0].get('email'));
        
        //find all the record types for the contact object
        Axiom_RequestPortalController.remoteObject contactRecordTypes = Axiom_RequestPortalController.getObjectRecordTypes('Contact');

        //find all the record types for the matter object
        Axiom_RequestPortalController.remoteObject matterRecordTypes = Axiom_RequestPortalController.getObjectRecordTypes('Matter__c');
                
        //find all the picklist values for the legal team field on the contact regardless of record type
        map<string,string> contactLegalTeamFieldOptions = Axiom_RequestPortalController.getAllPicklistOptions('Contact', 'Member_of_Legal_Team__c');
        
        //find all the picklist values for the legal team field on the contact for the first record type we found in the record type search above.
        map<string,string> RecordTypePicklistOptions = Axiom_RequestPortalController.getPicklistOptionsForRecordType('Contact', (string) contactRecordTypes.sObjects[0].get('DeveloperName'), 'Member_of_BT_Legal_Team__c');
        
        //test the utility call for converting a regular string to the Salesforce API name (remove special chars and spaces, replace with underscores)
        string convertedString= Axiom_RequestPortalController.stringToApiName('Not a valid fieldn@me');
        system.assertEquals(convertedString,'Not_a_valid_fieldn_me');
    
        //create a client portal matter field record
        Client_Portal_Matter_Fields__c thisMatterField = new Client_Portal_Matter_Fields__c();
        thisMatterField.Matter_Record_Type__c = (string) matterRecordTypes.sobjects[0].get('Name'); 
        thisMatterField.Matter_Field_Name__c = 'name';
        thisMatterField.Display_Order__c = 1;
        thisMatterField.name = 'Test Matter Field';
        thisMatterField.active__c = true;
        insert thisMatterField;
        
        //now use method to fetch that field we just created.          
        Map<String, Axiom_RequestPortalController.fieldDetails> matterFields = Axiom_RequestPortalController.getMatterFields((string) matterRecordTypes.sobjects[0].get('Name'));
        system.assertEquals(matterFields.size(),1);
        
        list<recordType> requestRecordType = [select name, developername from recordType where sObjectType = 'request__c'];
        //lets try saving a request
        string requestFormData = 'contact__c='+findContact.sObjects[0].get('Id')+'&request__c=&contact_record_type=Non_Legal_Contact&counterparty__c=test&requestCategory='+requestRecordType[0].name+'&requestType=Provide+bid+support';
        Axiom_RequestPortalController.remoteObject createRequest = Axiom_RequestPortalController.saveRequest(requestFormData);
        
        system.debug('\n\n\n\n-------------------------------- REQUEST CREATE');
        system.debug(createRequest);
        system.debug('\n\n\n\n');
        
        system.assertEquals(createRequest.success,true);
        
        //create and find matter exclusions
        client_Portal_Matter_Action_Filters__c thisExclusion = new client_Portal_Matter_Action_Filters__c ();
        thisExclusion.Name = 'Test Matter Action';
        thisExclusion.Contact_Field_Name__c = 'LOB_Business_Unit__c';
        thisExclusion.Contact_Field_Value__c = 'GS-APAC (Northeast)';
        thisExclusion.Contact_Record_Type__c = (string) contactRecordTypes.sObjects[0].get('Name');
        thisExclusion.Filtered_Matter_Actions__c = 'none';
        thisExclusion.Filtered_Request_Types__c = '';
        thisExclusion.Filtered_Request_Types_Extended__c = 'Support training';
        thisExclusion.Filtered_Matter_Types__c = '';
        
        insert thisExclusion;  
        
        list<Client_Portal_Matter_Action_Filters__c> exclusions = Axiom_RequestPortalController.getMatterfieldExclusions(thisExclusion.Contact_Record_Type__c, thisExclusion.Contact_Field_Name__c, thisExclusion.Contact_Field_Value__c);
        system.assertEquals(exclusions.size(),1); 
        
        list<recordType> matterRecordType = [select name, developername from recordType where sObjectType = 'matter__c'];
        string matterData = 'matter_action=Provide+goverance+or+legal+approval&matter_type='+matterRecordType[0].name+'&Description__c=test&Document_Origin__c=N%2FA&Jurisdiction__c=N%2FA&Additional_jurisdiction_support_needed__c=N%2FA&Describe_Additional_Support_Needed__c=&Subject_to_any_other_law__c=&Expected_Close_Signature_Date__c=2014-02-12&Been_Involved_Before__c=N%2FA&Who_was_involved__c=&BT_Signing_Entity__c=&Description_of_Original_Document__c=&Level_of_Complexity__c=N%2FA&Business_Related_To__c=N%2FA&Government_Sensitive_Work__c=N%2FA&Notes_Additional_Context__c=&Supplier_Name__c=&Beneficiary_LOBs_BT_Entities__c=&External_Customer_Requirements__c=N%2FA&Description_of_Customer_Requirement__c=&Legal_Authority__c=&Estimated_Sum_in__c=&Requested_Deadline__c=2014-02-12';     
        matterData += '&contact__c='+createContact.sObjects[0].get('Id');
        matterData += '&request__c='+createRequest.sObjects[0].get('Id');
        
        Axiom_RequestPortalController.remoteObject createMatter = Axiom_RequestPortalController.saveMatter(matterData);
        system.assertEquals(createMatter.success,true);
        
        //Client Portal Test Code
        string jsonString = '{"requestType":"Stage__c","requestCategory":"Matter__c","requestCategoryDeveloper":"Other"}';
        Axiom_RequestPortalController.getPicklistOptionsForRecordTypeClientPortal(new list<string>{jsonString});
        
        Axiom_RequestPortalController.portalObject thisPortalObject = new Axiom_RequestPortalController.portalObject();
    }

}