/******* Axiom Task Controller *********
@Author: Daniel Llewellyn/RedKite (dllewellyn@redkitetechnologies.com)
@Date: ~9/2013
@Description: Methods and properties related to the salesforce Task object.
**********************************************/

global class Axiom_TaskController
{
     /**Reference to this task**/
    public task thisTask{get;set;}
    
     /**reference to the current parent if this class is invoked as an extension**/
    public sobject thisobj{get;set;}
    
    /** used by the task list component to find tasks for the given parent used by the Axiom_TaskRelatedList component. **/
    public id parentObjectId{get;set;} 
    
    /**used by task list component to further filter tasks returned used by the Axiom_TaskRelatedList component**/
    public string relatedTaskFilterString{get;set;} 
    
    /**custom order by statment to be used in the find related tasks query used by the Axiom_TaskRelatedList component**/
    public string orderRelatedTasksBy{get;set;}
    
    /** string that contains the api name of the type of object which has invoked this controller **/
    public string controllerType;

    
    public Axiom_TaskController(ApexPages.StandardController controller) {
        controllerType = controller.getRecord().getSObjectType().getDescribe().getName().toLowerCase();
        if(controllerType== 'task')
        {
            thisTask = (task) controller.getRecord();                
        }
        else
        {
            thisobj =  controller.getRecord();
        }    
    }
    
    /** 
    * generic empty constructor. Used for the Axiom_TaskView component. Nothing needs to be initilized here
    * it exists soley as a placeholder. It is required, but it doesn't need to do anything
    **/
    public Axiom_TaskController() {

    }

    /**
    * finds any clockstoppers related to the current task. Used for inline visualforce page to build custom related list
    * of clockstoppers. 
    */
    public list<Clockstopper__c> relatedClockstoppers
    {
        get
        {
            return [select name, 
                       start_time__c, 
                       stop_time__c, 
                       net_time__c,
                       status__c,
                       days__c,
                       Client_Contact__r.name
                       from Clockstopper__c
                       where Related_Task__c = :thisTask.id or Related_Task__c = :string.valueOf(thisTask.id).left(15)];
        }
        set;
    }

    /**
    * finds any clockstoppers related to the current task. Used for inline visualforce page to build custom related list
    * of clockstoppers. Reads from the relatedTaskFilterString, orderRelatedTaskBy public variables to adjust the
    * list of tasks returned to the component for iterating and table creation. Does not take any arguments directly but does
    * read data from the class instance which is invoked by the component.
    */    
    public list<Task> relatedTasks
    {
        get
        {
            string queryString = 'select subject, activityDate, status, priority, isClosed ,owner.name, order__c, Open_Clockstopper__c from Task where whatId = :parentObjectId and isClosed = false'; 
            if(relatedTaskFilterString != null)
            {
                queryString += ' and ' +relatedTaskFilterString;
            }
            if(orderRelatedTasksBy != null)
            {
                queryString += ' order by ' + orderRelatedTasksBy;
            }
            return database.query(queryString);
        }
        set;    
    }

    /**
    * takes a list of tasks and evaluates the status on them. Each status has a corresponding
    * field name. If a task is completed, and has a certain status that field on the parent 
    * record needs to be also set to completed which then triggers a workflow rule email to be sent.
    * so like if you had a task related to a request with a status of 'Request on Hold' and then you updated it to
    * being completed a field on the request called 'Officially_On_Hold__c' will be set to completed. the defenitions 
    * for those actions are held in the map below, but could be abstracted to a custom setting or something if desired.
    * @param tasks tasks to update request fields from
    * @return null
    */
    public static void updateRequestFieldsFromTasks(list<task> tasks)
    {       
        map<string,string> subjectToRequestMap = getTaskSubjectToRequestFieldMap();
        
        map<id,request__c> requestsToUpdate = new map<id,request__c>();
        
        //iterate over all the tasks
        for(task thisTask : tasks)
        {
            //look to see if this task is completed and it's status is one of those in our map and this task is belongs to a request__c object (using contains instead of == to avoid namespace issues)
            if(thisTask.status.toLowerCase() == 'completed' && 
               subjectToRequestMap.containsKey(thisTask.subject) && 
               thisTask.whatId.getSObjectType().getDescribe().getName().containsIgnoreCase('Request__c') ) 
            {
                //insantiate a reference to a request. We don't know if we have one with this ID get or not already
                Request__c parentRequest;
                
                //look to see if our map already has a request for this tasks whatId. If so, get that one, otherwise create a new one with the 
                //id set to the tasks whatId
                if(requestsToupdate.containsKey(thisTask.whatId))
                {
                    parentRequest = requestsToUpdate.get(thisTask.whatId);
                }
                else
                {
                    parentRequest = new Request__c(id=thisTask.whatId);
                }
                //set the field for the status to completed for the field this status belongs to.
                parentRequest.put(subjectToRequestMap.get(thisTask.subject),'completed');
                requestsToUpdate.put(parentRequest.id,parentRequest);
            }
        }
        //if we have any requests to update, do that now.
        if(!requestsToUpdate.isEmpty())
        {
            update requestsToUpdate.values();
        }
    }

    /**
    * Sets the task weight on a task to be the default value defined on the custom setting if the task does not
    * have a weight defined. The task weight is a required field, so this prevents errors when updating old tasks
    * to make like easier
    * @param tasks list of tasks to set default task weight values for 
    * @return null
    */  
    public static void setTaskDefaultWeight_before(list<Task> tasks)
    {
        double defaultValue = Axiom_Misc_Settings__c.getOrgDefaults().Default_Task_Weight__c;
        for(Task thisTask : tasks)
        {
            
            if(thisTask.task_weight__c == null)
            {
                thisTask.task_weight__c = defaultValue;
            }
        }
        //no update required because of before trigger
    }
    /**
    * Sets the number of business days open on a task. Mean to be invoked as a before trigger on in a batch process
    * since method doesnt contain an update statment. Uses business hour logic to calculate how long task has been open.
    * @param tasks list of tasks to calculate  days open time for
    * @return list<task> list of tasks with Days_open__c field calculated
    */
    public static list<task> setNumberOfDaysOpen_before(list<Task> tasks){
        for(Task thisTask : tasks)
        {
            datetime stopTime = thisTask.Stop_Time__c != null ? thisTask.Stop_Time__c : datetime.now();
            if (thisTask.Start_Time__c!=null && (thisTask.status != 'Completed' || thisTask.Days_Open__c == null || thisTask.Days_Open__c == 0.00))
            {
                thisTask.Days_Open__c = Axiom_Utilities.calculateBusinessDaysBetweenDates(thisTask.Start_Time__c, stopTime);
            }
        }
        // since this is a before trigger, we don't need an update
        return tasks;
    }  
        
    /**
    * when a task is completed that is related to a version and is the final task in the series, we need to update the matters
    * S3 fields to some finalized status.
    * @param tasks list of tasks to update matters from.
    * @return null
    */
    public static void updateMatterFromFinalizeTask(list<task> tasks)
    {
        //container to hold the matters to update.
        list<matter__c> mattersToUpdate = new list<matter__c>();
        
        //map of id's to version records. Since we can't implicitly get at the related version data from the task (stupid polymorphic relationship restriction I think)
        //we need to query for all the versions related to any of these tasks and get their data that way. The version has the Id of the matter to update.
        map<id,version__c> versionMap = new map<id,version__c>();
        
        //populate the version map keyset. Yes, the tasks may be related to things that are not versions, but it won't matter because we just arn't going to reference them.
        //its faster to just ignore the data we don't need than to try and filter it out because it's harmless to have some extra keys in the map than try to deduce what kind of
        //object a whatId is.
        for(task thisTask : tasks)
        {
            versionMap.put(thisTask.whatId,null);
        } 
        
        //query for any version that has an id in the keyset of the map we built in the last step       
        for (version__c version : [select id,matter__c from version__c where id in :versionMap.keySet()])
        {
            versionMap.put(version.id,version);
        }
        
        //okay now the version map is populated. We also now need to figure out what kind of task is the type that should modify the matter. That is defined as the task with the
        //section number that is the highest of any defined in the custom setting. There are can be N amount of sections, so lets query the custom setting for the highest section number
        //defined on any record for this object type.
        AggregateResult[] groupedResults= [SELECT MAX(Section_Number__c)maxSection FROM AxiomSectionFields__c where Object_Name__c = 'version__c'];
        integer biggestSection = integer.valueOf(groupedResults[0].get('maxSection'));
        for(task thisTask : tasks)
        {
            if( thisTask.status == 'completed' &&
                thisTask.section__c == biggestSection &&
                thisTask.whatId.getSObjectType().getDescribe().getName().containsIgnoreCase('Version__c'))
            {
                //TaskRelation versionRelation = (TaskRelation) thisTask.getTaskRelations().getRecords()[0];
                matter__c thisMatter = new matter__c(id=versionMap.get(thisTask.whatId).matter__c);
                thisMatter.stage__c = 'Finalize';
                thisMatter.step__c = 'QA Review';
                thisMatter.status__c = 'In Process';
                mattersToUpdate.add(thisMatter);
            } 
        }
        if(!mattersToUpdate.isEmpty())
        {
            database.update(mattersToUpdate);
        }
    }
    
    /**
    * When all tasks related to a matter are closed update the matter to be completed.
    * @param tasks tasks to update matters from. 
    * @return null
    */
    public static void updateMatterOnAllTasksClosed(list<task> tasks)
    {
        //container to hold the matters to update.
        list<matter__c> mattersToUpdate = new list<matter__c>();
        
        //map of id's to version records. Since we can't implicitly get at the related version data from the task (stupid polymorphic relationship restriction I think)
        //we need to query for all the versions related to any of these tasks and get their data that way. The version has the Id of the matter to update.
        map<id,version__c> versionMap = new map<id,version__c>();
        map<id,integer> versionsToOpenTaskMap = new map<id,integer>();
        
        //populate the version map keyset. Yes, the tasks may be related to things that are not versions, but it won't matter because we just arn't going to reference them.
        //its faster to just ignore the data we don't need than to try and filter it out because it's harmless to have some extra keys in the map than try to deduce what kind of
        //object a whatId is.
        for(task thisTask : tasks)
        {
            //if this task is completed, and it is related to a version object then lets add this version to the list of potentials that may have all their tasks completed
            //and hence need to have their matter record updated.
            if(thisTask.status == 'completed' && 
               thisTask.section__c != 0 &&
               thisTask.whatId.getSObjectType().getDescribe().getName().containsIgnoreCase('Version__c'))
            {
                versionMap.put(thisTask.whatId,null);
                versionsToOpenTaskMap.put(thisTask.whatId,0);
            }
        } 
        
        //query for any version that has an id in the keyset of the map we built in the last step       
        for (version__c version : [select id,matter__c from version__c where id in :versionMap.keySet()])
        {
            versionMap.put(version.id,version);
        }    
        
        //find the amount of open tasks for any version in the map we built above. If it has a row returned here then that means that it has open tasks and 
        //should not update the matter.
        list<AggregateResult> unfinishedTasks = [select COUNT(id)openTasks, whatId from task where status != 'completed' and section__c != 0 and whatId in :versionMap.keySet() group by whatId]; 
        for(AggregateResult thisVersionTasks : unfinishedTasks)
        {
            versionsToOpenTaskMap.put((id) thisVersionTasks.get('whatId'), (integer) thisVersionTasks.get('openTasks'));
        }
    
        for(Id thisVersionId : versionMap.keySet())
        {
            if(versionsToOpenTaskMap.get(thisVersionId) == 0)
            {
                matter__c thisMatter = new matter__c(id=versionMap.get(thisVersionId).matter__c);
                thisMatter.stage__c = 'Finalize';
                thisMatter.step__c = 'QA Review';
                thisMatter.status__c = 'In Process';
                mattersToUpdate.add(thisMatter);
            } 
        }
        if(!mattersToUpdate.isEmpty())
        {
            database.update(mattersToUpdate);
        }
    }
    
    /**
    * Simple method that maps task statuses to fields on the Request to update when that task is closed.
    * should probably be abstracted to a custom setting.
    * @return map of string to string to is a task subject to a request field name.
    */
    public static map<string,string> getTaskSubjectToRequestFieldMap()
    {
        //the mapping of task statuses to the field they need to update on the request
        map<string,string> subjectToRequestMap = new map<string,string>();
        subjectToRequestMap.put('Request On Hold','Officially_On_Hold__c');
        subjectToRequestMap.put('Request Needing Your Acceptance','Accepted_By_ATM__c');
        subjectToRequestMap.put('Recall Request','Officially_Recalled__c');
        subjectToRequestMap.put('Cancel Request','Officially_Cancelled__c');    
        
        return subjectToRequestMap;
    }
    
    /**
    * there is a custom setting that contains rules about updated fields on related objects when a task with a certain subject is closed
    * this handles the processing of that logic. So if a task meets certain conditions as defiend on the custom setting (AxiomTaskUpdate__c)
    * then perform the update indicated by that same custom setting.
    * @param tasks list of tasks to find rules for and perform updates based on.
    * @return null
    */
    public static void processTaskUpdateCustomSetting(list<task> tasks)
    {
        //first lets get all the rules

        
        //list of records to update. Hopefully we don't run into mixed DML statments though since I don't think a task can be realted to a setup object, I think we should be fine.
        map<id,sobject> objectsToUpdate = new map<id,sobject>();
        Map<string, schema.sobjecttype> gd = Schema.getGlobalDescribe(); 
        
        list<AxiomTaskUpdate__c> updateRules = [select id, 
                                                   Field_To_Update__c,
                                                   New_Value__c,
                                                   Related_Object_Type__c,
                                                   Task_Subject__c,
                                                   Task_Statuses__c 
                                                   from 
                                                   AxiomTaskUpdate__c];
        //iterate over all the tasks and figure out what rules apply
        for(task thisTask : tasks)
        {   
            //loop over all the rules
            for(AxiomTaskUpdate__c thisRule : updateRules)
            {
                set<string> taskStatusList = new set<string>(thisRule.Task_Statuses__c.toLowerCase().split(','));
                
                //if this task is closed, and the subject matches the rule, and so does the related object,then lets do an update
                if( (thisRule.Task_Statuses__c != null && taskStatusList.contains(thisTask.status.toLowerCase()) || thisRule.Task_Statuses__c == null) &&
                   thisTask.subject.equalsIgnoreCase(thisRule.Task_Subject__c) && 
                   thisTask.whatId.getSobjectType().getDescribe().getName().indexOf(thisRule.Related_Object_Type__c) > -1)
                {
                    sObject updateObject = objectsToUpdate.containsKey(thisTask.whatId) ? objectsToUpdate.get(thisTask.whatId) : thisTask.whatId.getSobjectType().newSobject(thisTask.whatId);
                    
                    //get data about the target objects fields
                    Map<String, Schema.SObjectField> objectFields = updateObject.getSobjectType().getDescribe().fields.getMap();
                    
                    //figure out what kind of field it is that we are updating (we have to do a different kind of update if its a boolean field)
                    string ruleFieldType = objectFields.get(thisRule.Field_To_Update__c).getDescribe().getType().Name().toLowerCase();
                    
                    //if field is boolean, get a boolean value of the update value. Otherwise just pass it in as a string
                    if(ruleFieldType == 'boolean')
                    {
                        updateObject.put(thisRule.Field_To_Update__c,Boolean.valueOf(thisRule.New_Value__c));
                    }
                    else
                    {
                        updateObject.put(thisRule.Field_To_Update__c,thisRule.New_Value__c);
                    }
                    objectsToUpdate.put(thisTask.whatId,updateObject);
                }
            }
        }
        if(!objectsToUpdate.isEmpty())
        {
            update objectsToUpdate.values();
        }
    }
    
    /**
    * This is for setting the 'section' a task is related to. The version object for example has 4 or so sections on its page layout, and as each section is completed (all fields not null)
    * the task related to that section gets closed. Since those tasks are created via workflow rule and the only real information we can set on them is subject, we need to somehow indicate
    * what task is for what section. We do this by using a regular expression that looks for a number in the subject line enclosed in square brackets. We check to make sure the content within
    * the brackets is a number and if so we set that as the section number for the task, and remove the brackets and contents from the subject. 
    * @param task list of tasks to update
    * @return list of updated tasks
    */
    public static list<task> setTaskSection(list<task> tasks)
    {
        //regular expression to find content within square brackets in the task subject.
        Pattern p = Pattern.compile('\\[(.*?)\\]');
           
        //iterate over all the tasks
        for(task thisTask : tasks)
        {          
            if(thisTask.subject != null)
            {
                Matcher matcher = p.matcher(thisTask.subject);
    
                //if there is a match on the patterns matcher, and there is a matching group (probably redundant logic, but whatever) and the content is numeric then lets do it.
                if(matcher.find() && matcher.groupCount() > 0 && matcher.group(1).isNumeric())
                {
                    //since the if statment above checked the contents to ensure they are an integer, we can safely extract them and cast them to an integer now.
                    integer section = integer.valueOf(matcher.group(1));
    
                    //update the fields. Remove the brackets and contents from the subject line of the task
                    
                    thisTask.Section__c = section;
                    thisTask.subject = thisTask.subject.replaceAll('\\[(.*?)\\]','');
                }
            }
        }
        return tasks;
    }
    
    /**
    * Integration with the timepunch app. As soon as a task is moved to in progress create a running timer to log time.
    * utilizes old and new map to make sure that only tasks that have just changed to being in progress get a new timer.
    * @param oldTasks list of tasks as they were before update
    * @param newTasks list of tasks as they were after update, or during insert.
    */
    public static void startTimer(map<id,task> oldTasks, map<id,task> newTasks)
    {
        try
        {    
            for(id taskId : newTasks.keySet())
            {
                if( ((oldTasks == null || !oldTasks.containsKey(taskId)) && newTasks.get(taskId).get('status') == 'In Progress') || 
                    (oldTasks.get(taskId).get('status') != 'In Progress' && newTasks.get(taskId).get('status') == 'In Progress'))
                {  
                    timePunch_timerController.createTimer(taskId);
                }
            }
        }
        catch(exception e)
        {
            System.debug(LoggingLevel.ERROR,'ERROR CREATING TIMECARD:');
            system.debug(LoggingLevel.ERROR, e);
        }         
    }
    
    /**
    * when a task with a record type of 'clockstopper' is created that is related to a matter we need to automatically
    * create a clockstopper record and attach it to the task/matter. We statically set a few fields before passing 
    * the task and it's child clockstopper to our prepopulate method that copies over any other matching fields from
    * the task to the clockstopper.
    * @param tasks list of tasks to evaluate to see if clockstoppers need to be created.
    * @return null
    */
    public static void createClockStopperFromTask(map<id,task> tasks)
    {
        map<id,sObject> creationMap = new map<id,sobject>();
        list<recordType> clockStopperRecordType = [select id from recordType where sobjecttype = 'Task' and developerName = 'Clockstopper'];
        
        if(clockStopperRecordType.isEmpty())
        {
            System.debug(LoggingLevel.ERROR,'Could not find Clockstopper Record Type For Task. Unable to create clockstoppers from tasks. Aborting');
            return;
        }
        
        Schema.SObjectType matterType = Matter__c.sObjectType;
        for(task thisTask : tasks.values())
        {   
            //only create clockstopper if this task is a clockstopper record type
            //and the related to/whatId is refering to a matter__c record.
            if(thisTask.recordTypeId == clockStopperRecordType[0].id && thisTask.whatId != null && thisTask.whatId.getSObjectType() == matterType)
            {   
                //set some standard fields on the clockstopper before passing it to our clone method
                //to set any addition fields that match (API name for field is exact same on task and clockstopper) 
                clockstopper__c thisClockStopper = new clockstopper__c();
                thisClockStopper.related_task__c = thisTask.id;
                thisClockStopper.Name = thisTask.subject;
                thisClockStopper.Client_Contact__c = thisTask.whoId;
                thisClockStopper.Matter__c = thisTask.whatId;
                creationMap.put(thisTask.id,thisClockStopper);
            }
        }
        
        if(!creationMap.isEmpty())
        {
            map<id,sObject> clockStoppers = Axiom_Utilities.prepopulateObject(creationMap,null);
            insert clockStoppers.values();
        }
    }
    
    /**
    * Sets the Number of Days to First Substantive response on related matter records if any of the passed in tasks
    * are related to a matter and the related matter does not have it's first_substantive_response__c field set and the task
    * has it's first_substantive_response__c flag set to true. and the task has a valid completed status 
    * @param tasks list of tasks to find and update related matters for.
    * @return null
    **/
    public static void updateMatterNumberofDaysToFSR(list<task> tasks){
        set<Id> matterIdSet = new set<Id>();
        list<Task> workingList = new list<Task>();
        map<Id, Matter__c> updatedMatterMap = new map<Id, Matter__c>(); 
        //set<string> completionStatuses =  getTaskCompletionStatuses();
         
        for (Task thisTask : tasks)
        {
            if (thisTask.First_Substantive_Response__c==true && thisTask.isClosed)
            {
                matterIdSet.add(thisTask.WhatId);
                workingList.add(thisTask);
            }
        }
        
        map<Id, Matter__c> matterMap = new map<Id, Matter__c>([select id, First_Substantive_Response__c from Matter__c where id in :matterIdSet]);
        
        for (Task thisTask : workingList)
        {
            Matter__c currMatter = matterMap.get(thisTask.WhatId);
            
            if(currMatter==null || thisTask.Stop_Time__c == null) continue;
            if(currMatter.First_Substantive_Response__c==null)
            {
                updatedMatterMap.put(currMatter.Id, currMatter);
                currMatter.First_Substantive_Response__c = thisTask.Stop_Time__c;
            }
        }
        
        if(updatedMatterMap.size()>0) update updatedMatterMap.values();
   }

    /**
    * the idea here is when a task is modified we need to see if the matter it is related to has any remaining 'Open'
    * tasks. If not, and the matter says that it does, update it to say it doesn't. If it does have open clockstopers and the
    * matter says that it doesn't, update it to say that it does not. So basically
    * 1) Make a list of all the task passed in
    * 2) Find all the matters related to any of those task
    * 3) Group the task to their related matter
    * 4) Look to see if any of the related task have open clockstoppers. If so perform update if required, and vice versa.
    * @param task a list of task for which to find related matters and update the open_clockstopper__c flag if required
    * @return null
    */
    public static void setOpenClockstopperFlagOnMatter(list<task> tasks)
    {
        map<id, matter__c> matterMap = new map<id,matter__c>();
        map<id, list<task>> matterToTasksMap = new map<id,list<task>>();
        set<matter__c> updatematters = new set<matter__c>();
        
        for(task thisTask : tasks)
        {
            if(thisTask.whatId != null && thisTask.whatId.getSobjectType().getDescribe().getName().toLowerCase() == 'matter__c')
            {
                    matterMap.put((id) thisTask.whatId,null);
                    matterToTasksMap.put((id) thistask.whatId, new list<task>());           
            }
        }   
        
        //okay now find any tasks that are not closed and have open clockstoppers for the related matters we made a list of earlier.
        for(task thistask : [select id, 
                                    status,
                                    WhatId,
                                    Open_Clockstopper__c 
                             from task
                             where whatID in : matterMap.keySet() and 
                             isClosed = false])
        {
            list<task> thismatterstasks = matterToTasksMap.get((id) thistask.WhatId); 
            thismatterstasks.add(thistask);       
            matterToTasksMap.put((id) thistask.WhatId, thismatterstasks);
        }  
        
        //now we need to find details about the matters, mostly to see if the flag is set in the way it should be. We don't want to 
        //make pointless updates.
        for(matter__c thisMatter : [select  Open_Clockstopper__c, id 
                                 from matter__c 
                                 where id in :matterMap.keySet()])
        {
            matterMap.put(thismatter.id,thismatter);
        }

        //alright now time for the actual calculation of whether the matter has open tasks or not.
        for(id thisMatterId : matterToTasksMap.keySet())
        {
            //get the details about the matter we are looking at so we know if it is marked as open or closed           
            matter__c thismatter = matterMap.get(thisMatterId);

            boolean hasOpenClockstopper = false;
            
            for(task thisTask : matterToTasksMap.get(thismatter.Id))
            {
                if(thisTask.Open_Clockstopper__c)
                {
                    hasOpenClockstopper = true;
                }               
            }
            
            if(!hasOpenClockStopper && thismatter.Open_Clockstopper__c == true)
            {
                thismatter.Open_Clockstopper__c = false;
                updatematters.add(thismatter);
            }
            if(hasOpenClockStopper && thismatter.Open_Clockstopper__c == false)
            {
                thismatter.Open_Clockstopper__c = true;
                updatematters.add(thismatter);
            }
        }                      
        
        if(!updatematters.isEmpty())
        {
            list<matter__c> updates = new list<matter__c>();
            updates.addAll(updatematters);
            
            database.update(updates,true);
        }       
    
    }
       
   //TODO: Evaluate whether we could replace this logic by looking at the isClosed flag on the task
   //Currently commented out to reduce required test coverage as we are piloting using the task.isClosed flag
   /**
   * Often we need to check to see if a task has one of the statuses that we would consider to be 'complete'
   * this method returns a set of the statuses that indicate a task has reached completion, without having to use
   * the isClosed flag, which may or may not be set depending on the tasks lifecycle.
   * @return list of strings, one for each status that marks a task as being completed.
   
   public static set<string> getTaskCompletionStatuses()
   {
       set<string> completionStatuses = new set<string>();
       completionStatuses.add('Approved'); 
       completionStatuses.add('Complete'); 
       completionStatuses.add('Rejected');
       
       return completionStatuses;
       
   }
   **/
}