@isTest
global class Axiom_TestAttachmentController
{
    @isTest
    public static void testCountAttachments()
    {
        Axiom_Utilities.generateTestData();
        
        Test.StartTest();
        Request__c thisRequest = [select id, Number_Of_Attachments__c, name from request__c limit 1];
        
        system.assertEquals(thisRequest.Number_Of_Attachments__c,0);
        
        Attachment thisAttachment = new Attachment();
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        thisAttachment.name='Test Attachment.txt';
        thisAttachment.body=bodyBlob;        
        thisAttachment.OwnerId = UserInfo.getUserId();
        thisAttachment.ParentId = thisRequest.id;
        thisAttachment.IsPrivate = false;
        
        insert thisAttachment;

        thisRequest = [select id, Number_Of_Attachments__c, name from request__c limit 1];
        
        system.assertEquals(thisRequest.Number_Of_Attachments__c,1);
        
        delete thisAttachment;
            
        thisRequest = [select id, Number_Of_Attachments__c, name from request__c limit 1];
        
        system.assertEquals(thisRequest.Number_Of_Attachments__c,0);
        Test.StopTest();    
    }
}