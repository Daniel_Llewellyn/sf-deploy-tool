@isTest
public class Axiom_TestAuditHistory
{
    static testMethod void testGetObjectFieldHistory()
    {
        Axiom_Utilities.generateTestData();
        
        Test.StartTest();
        
        Request__c thisRequest = new Request__c();
        
        Axiom_AuditHistory.getObjectFieldHistory('Request__c', date.today()-1, date.today()+1);
        
        Axiom_AuditHistory.emailAuditHistory();
        
        Axiom_AuditHistory.sendHistoryData();
        
        Axiom_AuditHistory.objectHistoryContainer thisContainer = new Axiom_AuditHistory.objectHistoryContainer();
        
        thisContainer.objectName = 'Test Object';
        thisContainer.objectId = null;
        thisContainer.objectType = 'Test__c';
        thisContainer.createdDate = dateTime.now();
        thisContainer.createdById = UserInfo.getUserId();
        
        Axiom_AuditHistory.objectHistoryRecord thisRecord = new  Axiom_AuditHistory.objectHistoryRecord();
        
        thisRecord.oldValue = '0';
        thisRecord.newValue = '1';
        thisRecord.createdDate = dateTime.now();
        thisRecord.createdById = UserInfo.getUserId();
        thisRecord.objectId = null;
        thisRecord.historyRecordId = null;
        thisContainer.historyData.add(thisRecord);
        
        Axiom_AuditHistory.postObjectHistoryToRemote(JSON.serialize(thisContainer));
        
        Test.StopTest();
        
    }
    
    static list<Request__History> getFakeHistoryRecord(id parentRecordId)
    {
           list<Request__History> fakeHistory = new list<Request__History>();
           fakeHistory.add(new Request__History(parentId=parentRecordId, 
                                                Field='Type'));
           
           return fakeHistory;
    }
}