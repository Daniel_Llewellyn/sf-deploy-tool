@isTest
global class Axiom_TestAuthRequestPortalController
{
    @isTest
    public static void testAuthRequestPortalController()
    {
        Axiom_Utilities.generateTestData();
        
        Request__c thisRequest = [select id, name from Request__c limit 1];
        User thisUser = [select name, id, Related_Contact_Id__c, userType from user where id = :UserInfo.getUserId()];
        Contact relatedContact = [select name, id from contact limit 1];
        thisUser.Related_Contact_Id__c = relatedContact.id;
        update thisUser;
        
        Test.StartTest();
             
        ApexPages.StandardController sc = new ApexPages.StandardController(thisRequest);
             
        PageReference pageRef = Page.Axiom_AuthRequestPortal;
        Test.setCurrentPage(pageRef);
        Axiom_AuthRequestPortalController controller = new Axiom_AuthRequestPortalController(sc);  
        
        Test.StopTest();      
    }
}