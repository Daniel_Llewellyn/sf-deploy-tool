@isTest
global class Axiom_TestClockstopperController
{
    public static testMethod void testClockstopperController()
    {
        
        Axiom_Utilities.generateTestData();
        
        Test.StartTest();
        
        Clockstopper__c testStopper = [select status__c from Clockstopper__c limit 1];
        testStopper.status__c = 'In Progress';
        update testStopper;
        
        testStopper.status__c = 'Closed';
        testStopper.start_time__c = datetime.now();
        
        update testStopper;
        
 
        String CRON_EXP = '0 0 0 15 3 ? 2022';
        
        String jobId = System.schedule('ScheduleApexClassTest',
                        CRON_EXP, 
                        new Axiom_UpdateClockStopperBatch());        
        Test.stopTest();
        
 
    }
}