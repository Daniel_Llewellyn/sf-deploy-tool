@isTest
global class Axiom_TestPortalChartsController
{
    @isTest
    global static void TestPortalChartsController()
    {
        Axiom_Utilities.generateTestData();
        Test.StartTest();
        
        list<AggregateResult> result =  Axiom_PortalChartsController.averageDaysToCompletion();

        list<AggregateResult> result2 =  Axiom_PortalChartsController.averageDaysToCompletion(UserInfo.getUserId());       

        list<Request__c> result3 =  Axiom_PortalChartsController.openRequestAging();


        list<Request__c> result4 =  Axiom_PortalChartsController.workBreakdown();


        list<Request__c> result5 =  Axiom_PortalChartsController.workBreakdown(UserInfo.getUserId());


        list<AggregateResult> result6 =  Axiom_PortalChartsController.averageDaysToFirstResponse();


        list<AggregateResult> result7 =  Axiom_PortalChartsController.averageDaysToFirstResponse(UserInfo.getUserId());
        Test.StopTest();
              
    }
}