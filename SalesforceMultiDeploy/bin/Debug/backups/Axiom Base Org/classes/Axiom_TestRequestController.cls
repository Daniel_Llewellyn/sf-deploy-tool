@isTest
public class Axiom_TestRequestController
{
    @isTest
    static void testRequestController()
    {
        Axiom_Utilities.generateTestData();
        
        Test.StartTest();        
        
        list<request__c> requests = [select id,createdDate, name,Start_Time__c,Stop_Time__c from request__c];
        for(Request__c req : requests)
        {
            req.Start_Time__c = dateTime.now();
            req.Stop_Time__c = dateTime.now().addHours(5);
        }
        update requests;
        
        Test.StopTest();
          
    }
}