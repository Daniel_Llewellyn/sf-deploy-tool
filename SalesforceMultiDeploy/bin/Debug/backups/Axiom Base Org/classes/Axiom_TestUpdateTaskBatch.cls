@isTest
global class Axiom_TestUpdateTaskBatch
{
    public static testMethod void testUpdateTaskBatch()
    {
        Axiom_Utilities.generateTestData();
        Task testTask = [select status from Task limit 1];
        testTask.status = 'In Progress';
        update testTask;
        
        Test.startTest();
        String CRON_EXP = '0 0 0 15 3 ? 2022';
        
        String jobId = System.schedule('ScheduleApexClassTest',
                        CRON_EXP, 
                        new Axiom_UpdateTaskBatch());        
        Test.stopTest();
 
    }
}