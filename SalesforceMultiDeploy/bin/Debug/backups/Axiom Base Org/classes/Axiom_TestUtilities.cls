@isTest
global class Axiom_TestUtilities
{
    @isTest
    public static void testUtilities()
    {
        //first generate test data
        Axiom_Utilities.generateTestData();
        
        Test.StartTest();
        
        //cancel batch jobs if they exist already
        Axiom_Utilities.cancelBatchJobs();
        
        //now schedule the batch jobs
        Axiom_Utilities.scheduleBatchJobs();

        
        //counter number of attachments on one of the records created in the test data
        Map<ID, Contact> m = new Map<ID, Contact>([SELECT Id, LastName FROM Contact]);
        
        Attachment attachment = new Attachment();
        attachment.Body = Blob.valueOf('Body of Attachment');
        attachment.Name = String.valueOf('test.txt');
        attachment.ParentId = new list<id>(m.keySet())[0]; 
        insert attachment;
  

        Axiom_Utilities.countNumberOfAttachments(new list<id>(m.keySet()));
        
        list<matter__c> allMatters = [select id from matter__c];
        //get completed task percent
        Axiom_Utilities.getCompletedTaskPercent(allMatters[0].Id);
        
        Axiom_Utilities.calculateBusinessMinutesBetweenDates(dateTime.now(), dateTime.now().addHours(5));
        
        Axiom_Utilities.calculateBusinessDaysBetweenDates(dateTime.now(), dateTime.now().addHours(1024));
        
        string documentData = 'Active__c,Default_Value__c,Display_Order__c,Input_Label_Override__c,Input_Type_Override__c,Input_Validation_Classes__c,Matter_Field_Name__c,Matter_Record_Type__c,Name,Required__c';
        documentData += 'TRUE,www.bt.com,52,Please complete the checklist linked below and attach to this request,,,BT_Checklist__c,"Other - Product, Portfolio and Standards;Review, analyse or revise documents and agreements;","Product, Portfolio-BT Checklist",FALSE';
        documentData += 'TRUE,,12,Name of Non-BT Party,,,Counterparty__c,Order Forms (AMEA);,Order Forms-Counterparty,TRUE';

        list<Folder> importDocFolder = [select id from folder where name  = 'iris import files' and type = 'Document'];
        folder docFolder;
        if(importDocFolder.isEmpty())
        {
            system.debug('\n\n\n\n------------------ ERROR TESTING IMPORT FILE. NO FOLDER NAMED "iris import files" FOUND. PLEASE CREATE AND RETRY');                
        }
        else
        {
            docFolder = importDocFolder[0];
        }
        Document thisImportDoc = new Document();
        thisImportDoc.name = 'Test Import Doc';
        thisImportDoc.body = blob.valueOf(documentData);
        thisImportDoc.type = 'csv';
        thisImportDoc.description = 'Client_Portal_Matter_Fields__c';
        thisImportDoc.folderId = docFolder.id;
        
        
        //test the import documents feature
        map<string,list<database.saveResult>> importResult = Axiom_Utilities.importCustomSettingFiles(false, true, true);
        Test.StopTest();
    }
}