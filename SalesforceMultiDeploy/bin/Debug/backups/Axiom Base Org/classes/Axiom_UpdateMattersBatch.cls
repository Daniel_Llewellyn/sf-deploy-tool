/******* Axiom Update Matters Batch *********
@Author: Daniel Llewellyn/RedKite (dllewellyn@redkitetechnologies.com)
@Date: ~3/2014
@Description: Handles updating of a few matter fields including the number of days open and the number of days to first substantial response. 
              Does this for any matter where the status is 'In Progress'. Meant to be scheduled hourly by the 
              Axiom_Utilities.scheduleBatchJobs() method.
**********************************************/
global class Axiom_UpdateMattersBatch implements Database.Batchable < sObject > , Schedulable 
{
    /** Querystring that can be modified after class instantiation  Defaults to finding all matters where First_Substantive_Response__c is null or Stop_time__c is null**/
    public string queryString = 'select name, id, createdDate, Status__c, Start_Time__c, First_Substantive_Response__c, Stop_Time__c from matter__c where First_Substantive_Response__c = null or Stop_Time__c = null';
    
    /** 
    * Finds next batch of records to send to the execute method in amounts dictated by the batch size (default 200)
    * @param ctx batch context. An instantiation of this class.
    * @return batch of records for the execute method to process
    **/
    global Database.QueryLocator  start(Database.BatchableContext ctx)
    {
        return Database.getQueryLocator(queryString);
    }

    /**
    * Calls two methods on the Axiom_MatterController class. 
    * 1) Axiom_MatterController.setNumberOfDaysOpen to set the number of days a matter has been open
    * 2) Axiom_MatterController.setNumberOfDaysToFsr to set the number of days to the first substantive response 
    * see those methods for further description of their logic
    * @param ctx batch context. Is passed automatically by the system.
    * @param scope list of sObjects to operate on.
    * @return null
    **/
    global void execute(Database.BatchableContext ctx, List <sObject> scope)
    {
        scope = Axiom_MatterController.setNumberOfDaysOpen(scope);
        scope = Axiom_MatterController.setNumberOfDaysToFsr(scope);
        
        database.update(scope,false);
    }

    /**
    * Invoked after all batches have been completed. Nothing further needs to be done as this point so we 
    * just put an entry in the system log.
    **/
    global void finish(Database.BatchableContext ctx) 
    {
        System.debug(LoggingLevel.WARN,'Axiom_UpdateMattersBatch Process Finished');
      
    }    

    /**
    * This is the method is run when the scheduled job is triggered. It instantiates an instance of this
    * class, and executes the batch job. Sends small batches due to the amoutn of logic attached to matters can
    * cause a too many SOQL queries error if large batches are sent.
    * @param sc SchedulableContext. Passed by salesforce when this scheduled job is triggered to run.
    * @return null
    **/
    global void execute(SchedulableContext SC) 
    {
        Axiom_UpdateMattersBatch updateBatch = new Axiom_UpdateMattersBatch();
        Id batchprocessid = Database.executeBatch(updateBatch, 10);
    }     
}