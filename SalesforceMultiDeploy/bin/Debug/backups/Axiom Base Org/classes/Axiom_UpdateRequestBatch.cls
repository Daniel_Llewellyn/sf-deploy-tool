/******* Axiom Update Request Batch *********
@Author: Daniel Llewellyn/RedKite (dllewellyn@redkitetechnologies.com)
@Date: ~3/2014
@Description: Handles updating of a field on the request; number of days to first substantial response. Does this for any request where the status is 'In Progress'. Meant to be scheduled hourly by the Axiom_Utilities.scheduleBatchJobs() method.
**********************************************/
global class Axiom_UpdateRequestBatch implements Database.Batchable < sObject > , Schedulable 
{
    /** Querystring that can be modified after class instantiation. Defauts to finding all requests that have a status__c of "In Progress" */
    public string queryString = 'select name, id, createdDate, Start_Time__c, Stop_Time__c from request__c where status__c = \'In Progress\' or Stop_Time__c = null';
 
     /** 
    * Finds next batch of records to send to the execute method in amounts dictated by the batch size (default 200)
    * @param ctx batch context. An instantiation of this class.
    * @return batch of records for the execute method to process
    **/
    
    global Database.QueryLocator  start(Database.BatchableContext ctx)
    {
        return Database.getQueryLocator(queryString);
    }


    /**
    * Calls one method on the Axiom_RequestController class.
    * 1) Invokes Axiom_RequestController.setNumberOfDaysToFsr() to calculate the number of days to first substantial response.
    * see those methods for further description of their logic
    * @param ctx batch context. Is passed automatically by the system.
    * @param scope list of sObjects to operate on.
    * @return null
    **/    
    global void execute(Database.BatchableContext ctx, List <sObject> scope)
    {
        scope = Axiom_RequestController.setNumberOfDaysToFsr(scope);
        
        database.update(scope,false);
    }

    /**
    * Invoked after all batches have been completed. Nothing further needs to be done as this point so we 
    * just put an entry in the system log.
    **/
    global void finish(Database.BatchableContext ctx) 
    {
        System.debug(LoggingLevel.WARN,'Axiom_UpdateRequestBatch Process Finished');
      
    }    

    /**
    * This is the method is run when the scheduled job is triggered. It instantiates an instance of this
    * class, and executes the batch job.
    * @param sc SchedulableContext. Passed by salesforce when this scheduled job is triggered to run.
    * @return null
    **/
    global void execute(SchedulableContext SC) 
    {
        Axiom_UpdateRequestBatch updateBatch = new Axiom_UpdateRequestBatch();
        ID batchprocessid = Database.executeBatch(updateBatch, 50);
    }     
}