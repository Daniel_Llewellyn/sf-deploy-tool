/******* Axiom Utilities *********
Author: Daniel Llewellyn/RedKite (dllewellyn@redkitetechnologies.com)
Date: ~9/2013
Description: Common/shared and abstracted methods that can be used by other classes/methods.
**********************************************/

global class Axiom_Utilities
{
    global static map<string,string> objectTypeToBatchJobNameMap = new map<string,string>();
    static 
    {
        
        objectTypeToBatchJobNameMap.put('task','Process Tasks On the Hour');
        objectTypeToBatchJobNameMap.put('request__c','Process Requests On the Hour');
        objectTypeToBatchJobNameMap.put('matter__c','Process Matters On the Hour');
        objectTypeToBatchJobNameMap.put('clockstopper__c','Process Clockstoppers on the Hour');
    }
    /**
    * Imports custom setting data from documents in the iris import files document folder. All documents are queried and if the document description matches
    * the API name of an sObject it attempts to import the data contained within to populate that custom setting. Files are expected to be in a CSV file format
    * without an ID or other 'system' field columns present that could cause import problems. Ideally the files will not have their values wrapped in quotes, but this
    * method can handle stripping of quotes.
    * @param allOrNothing should the import of each object type be an 'all or nothing' import where a single erroring record causes the entire batch to abort
    * @param deleteFileOnSuccess if every record import from a file was successfull should the source file be delete (helps reduce accidental duplicate import chance)
    * @return a map of documents imported with their save results
    **/
    @remoteAction
    global static map<string,list<database.saveResult>> importCustomSettingFiles(boolean allOrNothing, boolean deleteFileOnSuccess, boolean clearExistingData)
    {

        //variable to return to caller. Contains documents imported with the results of their import job.
        map<string,list<database.saveResult>> saveResults = new map<string,list<database.saveResult>>();    
        try
        {   
            //first find all object types that exist in this org so we know if a given document represents a certain sObject type/custom setting
            Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe(); 
            
            //we need to know what the types of fields are on the target sObject so we know how to cast the data for each field on the import file. This contains that data.
            Map<string,Map<string,string>> ObjectFieldTypes = new Map<string,Map<string,string>>();
            
            //find all documents in the iris import files folder that are CSV files.
            list<document> importableDocs = [select Body, contentType, DeveloperName, Description, Name, Type from document where Folder.Name = 'iris import files' and type='csv'];
            
            //if the user has set the deleteFileOnSuccess flag this will be used to track the Id's of the documents to delete at the end of processing after the import results are evaluated.
            list<string> documentsToDelete = new list<string>();       
            set<string> customSettingsToPurge = new set<string>();
            
            //iterate over all the documents.
            for(document thisDoc : importableDocs)
            {    
                //container for list of sObjects to insert for each document.
                list<sobject> objectsToImport = new list<sobject>();
                
                //we put the object type this document is for in the description field.
                if(gd.containsKey(thisDoc.description))
                {
                    if(clearExistingData)
                    {
                        customSettingsToPurge.add(thisDoc.description);
                    } 
                    
                    //reference to the name of this sObject type.
                    string objectTypeName = thisDoc.description;
                    
                    //get the map of this objects field types. This map is just a map of the field name to a string representing the field type (derived from describe info)
                    Map<string,string> thisObjectFieldTypes = objectFieldTypes.containsKey(objectTypeName) ? objectFieldTypes.get(objectTypeName) : new map<string,string>();
                    
                    //get a token for the sObject type (so we can describe it, and also make new instances of this type)
                    Schema.SObjectType objectType = gd.get(objectTypeName);
                    
                    //if this document we are iterating is the first one for this object type, then we don't have any of the field describe data, so we need to get it.
                    if(thisObjectFieldTypes.isEmpty())
                    {
                        //get describe of all fields on the source object
                        Map<String, Schema.SObjectField> objectFieldData = objectType.getDescribe().fields.getMap();
                        
                        //iterate over all the fields
                        for(string thisField : objectFieldData.keySet())
                        {
                            //add this field and it's type to the map.
                            thisObjectFieldTypes.put(thisField.toLowerCase(),objectFieldData.get(thisField).getDescribe().getType().name());
                        }
                    }
                    
                    //save our data back to the source map.
                    objectFieldTypes.put(objectTypeName,thisObjectFieldTypes);
                    
                    //get the document body
                    string documentBody = thisDoc.body.toString().trim();
    
                    //kind of a hack to detect if the fields are wrapped in quotes, and if so we remove them. Otherwise later when we import the values they all have 
                    //quotes around them. I know this approach isn't ideal but I'm not sure of a better way other than to just not have quotes in the source file.
                    if(documentBody.left(1) == '"')
                    {
                        //strip out the quotes. Yes this removes all quotes in the entire document.
                        documentBody = documentBody.replaceAll('"','');
                    }
                    
                    //parse the CSV file into a list of lists using the parseCSV function.
                    List<List<String>> parseResult = Axiom_Utilities.parseCSV(documentBody,false);
                    
                    //easy reference to the headers of the CSV file, which are in the first element of the list
                    List<string> headers = parseResult[0];
                    
                    //iterate over all the rows (except the first one, since that is the headers and we don't want to import those) in the parsed result.
                    for(integer rowIndex=1; rowIndex < parseResult.size(); rowIndex++)
                    {
                        //reference to this row of data
                        list<string> thisRow = parseResult[rowIndex];
                        
                        //create a new sObject of the type we figured out this document is for (using the text in the description field)
                        sObject thisObject = objectType.newSObject();
                        
                        //iterate over all the fields in the headers
                        for(integer fieldIndex=0; fieldIndex < headers.size(); fieldIndex++)
                        {
                            //if our describe of the object does not have an entry for this header, that means there is no field on this object by that name, so skip trying to import it.
                            //TODO: Not sure if this kind of error should cause the whole job to fail or not. I'd say probably not since we are trying to keep this easy to use and quick.
                            if(!thisObjectFieldTypes.containsKey(headers[fieldIndex].toLowerCase()))
                            {
                                continue;
                            }
    
                            //use the map of field type data to deduce what kind of this this is we are importing into so we can cast the value accordingly.                   
                            string fieldType = thisObjectFieldTypes.get(headers[fieldIndex].toLowerCase());
                            
                            //stupid if/else logic for figuring out how to cast this data to the right type. You'd really figure casting would be implicit since Apex/SF knows 
                            //the data type of the field, but alas. I really hope someone can show me a better way to do this. So clunky... stupid Apex not having even a switch/case statment...
                            if(fieldType == 'integer')
                            {
                                thisObject.put(headers[fieldIndex],integer.valueOf(thisRow[fieldIndex]));
                            }
                            else if(fieldType == 'boolean')
                            {
                                thisObject.put(headers[fieldIndex],boolean.valueOf(thisRow[fieldIndex]));
                            }  
                            else if(fieldType == 'date')
                            {
                                thisObject.put(headers[fieldIndex],date.valueOf(thisRow[fieldIndex]));
                            }                            
                            else if(fieldType == 'double')
                            {
                                thisObject.put(headers[fieldIndex],double.valueOf(thisRow[fieldIndex]));
                            }
                            else
                            {
                                thisObject.put(headers[fieldIndex],thisRow[fieldIndex]);
                            }                    
                        }
                        //add this object to our list of objects to insert
                        objectsToImport.add(thisObject);
                        
                    }
                    
                    //if we have custom settings data to purge do it now
                    if(!customSettingsToPurge.isEmpty()) 
                    {
                        for(string objectName : customSettingsToPurge)
                        {
                            string queryString = 'select id from ' + objectName;
                            database.delete(database.query(queryString));       
                        }
                    }
                    //insert the results, and put the results in the map.
                    saveResults.put(thisDoc.developerName,database.insert(objectsToImport,allOrNothing));
                    
                }   
            }
            
            //if the user has elected to delete the source file on import completion then we must now evaluate the results 
            //of each import and figure out what files to delete
            if(deleteFileOnSuccess)
            {
                //iterate over all the documents we attempted to import 
                for(string thisDoc : saveResults.keySet())
                {
                    //flag to hold if this job was a total success (all rows imported successfull)
                    boolean importSuccess = true;
                    
                    //get the list of save results for this job. One save result per row
                    list<Database.SaveResult> theseSaveResults = saveResults.get(thisDoc);
                    
                    //iterate over the results
                    for(Database.SaveResult thisResult : theseSaveResults)
                    {
                        //if this result was not a success then this import was not a total success and the document should NOT be deleted
                        if(!thisResult.isSuccess())
                        {
                            importSuccess = false;
                            break;                      
                        }                
                    }
                    //if all rows DID import now we can mark this document for delete
                    if(importSuccess)
                    {
                         documentsToDelete.add(thisDoc);
                    }
                }
                
                //if we have any documents to delete, do so now.
                if(!documentsToDelete.isEmpty())
                {
                    list<document> docs = [select id, name from document where developerName in :documentsToDelete];
                    delete docs;
                }
            }
            
        }
        catch(exception e)
        {
            system.debug('\n\n\n\n----------------- ERROR DURING IMPORT');
            system.debug(e);
        } 
        return saveResults;       
    }

    public static List<List<String>> parseCSV(String contents,Boolean skipHeaders) {
        List<List<String>> allFields = new List<List<String>>();
    
        // replace instances where a double quote begins a field containing a comma
        // in this case you get a double quote followed by a doubled double quote
        // do this for beginning and end of a field
        contents = contents.replaceAll(',"""',',"DBLQT').replaceall('""",','DBLQT",');
        // now replace all remaining double quotes - we do this so that we can reconstruct
        // fields with commas inside assuming they begin and end with a double quote
        contents = contents.replaceAll('""','DBLQT');
        // we are not attempting to handle fields with a newline inside of them
        // so, split on newline to get the spreadsheet rows
        List<String> lines = new List<String>();
        try {
            lines = contents.split('\n');
        } catch (System.ListException e) {
            System.debug('Limits exceeded?' + e.getMessage());
        }
        Integer num = 0;
        for(String line : lines) {
            // check for blank CSV lines (only commas)
            if (line.replaceAll(',','').trim().length() == 0) break;
            
            List<String> fields = line.split(',');  
            List<String> cleanFields = new List<String>();
            String compositeField;
            Boolean makeCompositeField = false;
            for(String field : fields) {
                if (field.startsWith('"') && field.endsWith('"')) {
                    cleanFields.add(field.replaceAll('DBLQT','"'));
                } else if (field.startsWith('"')) {
                    makeCompositeField = true;
                    compositeField = field;
                } else if (field.endsWith('"')) {
                    compositeField += ',' + field;
                    cleanFields.add(compositeField.replaceAll('DBLQT','"'));
                    makeCompositeField = false;
                } else if (makeCompositeField) {
                    compositeField +=  ',' + field;
                } else {
                    cleanFields.add(field.replaceAll('DBLQT','"'));
                }
            }
            
            allFields.add(cleanFields);
        }
        if (skipHeaders) allFields.remove(0);
        return allFields;       
    }
    
    /**
    * Schedules batch jobs to on a custom schedule with more granularity than is allowed by the user interface. User can configure
    * schedule by setting cron string in the the Axiom_Misc custom setting. Otherwise it defaults to every hour of every day.
    * @return list of job ids
    */
    @remoteAction
    global static list<id> scheduleBatchJobs()
    {
        list<id> jobIds = new list<id>();
        
        string defaultValue = Axiom_Misc_Settings__c.getOrgDefaults().Schedule_Job_Cron_String__c;
        String cronStr = defaultValue == null || defaultValue == '' ? '0 0 * * * ?' : defaultValue;
    
        jobIds.add(System.schedule(objectTypeToBatchJobNameMap.get('task'),cronStr,new Axiom_UpdateTaskBatch()));
        jobIds.add(System.schedule(objectTypeToBatchJobNameMap.get('request__c'),cronStr,new Axiom_UpdateRequestBatch()));
        jobIds.add(System.schedule(objectTypeToBatchJobNameMap.get('matter__c'),cronStr,new Axiom_UpdateMattersBatch()));
        jobIds.add(System.schedule(objectTypeToBatchJobNameMap.get('clockstopper__c'), cronStr, new Axiom_UpdateClockStopperBatch()));
        
        return jobIds;
        
        
    }
    

    
    /**
    * Cancels batch jobs scheduled by the scheduleBatchJobs method
    **/
    @remoteAction
    global static set<id> cancelBatchJobs()
    {
        set<string> cronJobNames = new set<string>(objectTypeToBatchJobNameMap.values());  
        set<id> cancelledJobIds = new set<id>();
         
        for(CronTrigger ct : [SELECT id, CronExpression, TimesTriggered, NextFireTime, CronJobDetail.name, CronJobDetail.jobType FROM CronTrigger])
        {
            if(cronJobNames.contains(ct.CronJobDetail.name))
            {
                system.abortJob(ct.id);
                cancelledJobIds.add(ct.id);
            }
        }
        return cancelledJobIds;
    }
    /**
    * Runs batch jobs immediatly. Can be invoked from admin tools page
    **/
    @remoteAction
    global static list<id> runBatchJobsImmediate()
    {
        list<id> jobIds = new list<id>();
        Axiom_UpdateTaskBatch updateBatch = new Axiom_UpdateTaskBatch();
        jobIds.add(Database.executeBatch(updateBatch, 50));

        Axiom_UpdateRequestBatch updateBatch2 = new Axiom_UpdateRequestBatch();
        jobIds.add(Database.executeBatch(updateBatch2, 50));

        Axiom_UpdateMattersBatch updateBatch3 = new Axiom_UpdateMattersBatch();
        jobIds.add(Database.executeBatch(updateBatch3, 50));
                                 
        Axiom_UpdateClockStopperBatch updateBatch4 = new Axiom_UpdateClockStopperBatch();
        jobIds.add(Database.executeBatch(updateBatch4, 50));
        
        return jobIds;
    }        
    /**
    * Counts the number of attachment recors attached to a set of records with the given ids.
    * @return a map of salesforce record it to an integer reprsenting how many attachments that record has
    */
    public static map<id,integer> countNumberOfAttachments(list<id> recordIds)
    {
        map<id,integer> attachmentCounts = new map<id,integer>();
        
        for(id thisId : recordIds)
        {
            attachmentCounts.put(thisId,0);
        }
        AggregateResult[] numAtts=[select count(id)total, parentId from attachment where parentid in :recordIds group by parentId];
        
        for(AggregateResult ar : numAtts)
        {
            attachmentCounts.put((id) ar.get('parentId') , (integer) ar.get('total') );
        }
        
        return attachmentCounts;
    }

    /**
    * takes a map of parent object ids to a child sObjects (which can be null if the child does not exist yet) and populated the child objects with data from the parent
    * with any matching fields copied over. So if the parent object (account) has a field called phone and so does the child object type (contact) for any account id
    * passed in a new contact will be created/updated with the value of the phone field pre populated to whatever the account has. A set of fields to skip over can be passed in as well
    * @param parentToChildIds a map of parent object id to it's child record  which to populate data on.
    * @param skipFields a set of strings that represent fields names NOT to copy from the parent record to the child record.
    * @return the same map that was passed in but with all the details of the child object populated by info from the parent
    
    * @TODO Replace single field custom setting data with custom setting data from Axiom_Field_Copy_Exceptions__c
    */
    public static map<id,sObject> prepopulateObject(map<id,sobject> parentToChildIds, set<string> skipFields)
    {   
        skipFields = skipFields == null ? new set<string>() : skipFields;
        skipFields.add('Id');
        skipFields.add('RecordTypeId');
        skipFields.add('OwnerId');
        
        map<string,set<string>> objectFieldExceptions = new map<string,set<string>>();
        
        list<Axiom_Field_Copy_Exceptions__c> exceptions = [select active__c, 
                                                                  Child_Object_Type__c, 
                                                                  Field_API_Name__c, 
                                                                  Parent_Object_Type__c                                                                  
                                                           from Axiom_Field_Copy_Exceptions__c
                                                           where active__c = true];
        
        for(Axiom_Field_Copy_Exceptions__c thisException : exceptions)
        {
            set<string> fieldExceptions = objectFieldExceptions.containsKey(thisException.parent_object_type__c) ? objectFieldExceptions.get(thisException.parent_object_type__c) : new set<string>();
            fieldExceptions.add(thisException.field_api_name__c);
            objectFieldExceptions.put(thisException.parent_object_type__c,fieldExceptions);
        }
        //Yeah, finish doing this later. Have to iterate over the exceptions, group them by the parent object type
        //then when doing the iteration below lookup the exceptions and apply them.
        
            
        map<id,sObject> returnData = new map<id,sObject>();
        
        List<id> parentIdList = new List<id>(parentToChildIds.keySet());
        
        //information about the parent object type
        Schema.DescribeSObjectResult parentObjectType = parentIdList[0].getsObjectType().getDescribe();      
        string parentObjectTypeName = parentObjectType.getName();
        Map<String, Schema.SObjectField> parentObjectFields = parentObjectType.fields.getMap();
        
        
        //information about the child object type
        Schema.DescribeSObjectResult childObjectType = parentToChildIds.values()[0].getsObjectType().getDescribe();
        Map<String, Schema.SObjectField> childObjectFields = childObjectType.fields.getMap();
        
        //run query to get all information about the parent objects so their values can be copied to the child objects
        string queryString = Axiom_Utilities.buildSelectAllStatment(parentObjectTypeName);
        queryString += ' where id in :parentIdList';
        list<sObject> parentRecords = database.query(queryString);

        for(sObject thisParent : parentRecords)
        {
            //then entry for this parent id should contain a child object to which it will copy it's data. If it does not for some reason it will create a new one.   
            sObject childObject =  parentToChildIds.get((id) thisParent.get('id')) != null ? parentToChildIds.get((id) thisParent.get('id')) : childObjectType.getSobjectType().newSobject();
            for(Schema.SObjectField thisParentField : parentObjectFields.values())
            {
               //because this is dynamic and there will be lots of instances of failure security prevents reading/writting, system fields, or they are readonly)
               //Lots of errors can happen here. Thats okay, the goal is just to collect as much info as possible, so we just take what we can get and skip over anything
               //that fails.
               try
               {     
                   set<string> objectSpecificSkipFields = objectFieldExceptions.containsKey(parentObjectTypeName) ? objectFieldExceptions.get(parentObjectTypeName) : new set<string>();
                                
                   string thisFieldName =  thisParentField.getDescribe().getName();
                   if(childObjectFields.containsKey(thisFieldName) && 
                      !skipFields.contains(thisFieldName) && 
                      !objectSpecificSkipFields.contains(thisFieldName) &&
                      (childObject.get(thisFieldName) == null || childObject.get(thisFieldName) == '') && 
                       thisParent.get(thisFieldName) != null && thisParent.get(thisFieldName) != '')
                   {
                       childObject.put(thisFieldName,thisParent.get(thisFieldName));           
                   }
                }
                catch(exception e)
                {
                    //lots of errors expected here, it's okay.
                }                
            }
            returnData.put((id) thisParent.get('id'), childObject);
        }                         
              
        
        return returnData;
    }
    
    /**
    * based on the existing tasks and their weights attached to a record get the percentage complete for that record. Sum all
    * task weights and divide by the completed percentage 
    * @param recordId the id of the record to calculate completion percent for
    * @return double percntage of all task weights complete
    */
    public static double getCompletedTaskPercent(id recordId)
    {
        list<task> thisObjectTasks = [select status, task_weight__c from task where whatId = :recordId ALL ROWS];
   
        double totalTaskWeight = 0.0;
        double completedTaskWeight = 0.0;
        double percentageComplete = 0.0;
        
        for(task thisTask : thisObjectTasks)
        {
            thisTask.task_weight__c = thisTask.task_weight__c != null ? thisTask.task_weight__c : Axiom_Misc_Settings__c.getOrgDefaults().Default_Task_Weight__c;
            totalTaskWeight += thisTask.task_weight__c;
            if(thisTask.status.toLowerCase() == 'completed')
            {
                completedTaskWeight += thisTask.task_weight__c;
            }
        }

        percentageComplete = totalTaskWeight > 0 ? (completedTaskWeight/totalTaskWeight)*100 : 0;
        return percentageComplete.round();    
    }
    
    /**
    * builds an SOQL query string that will select all fields for a given object type.
    * @param objectType a string that is the API name of the object to build the select all string for
    * @return string that can be used in a dynamic SOQL query to select all fields
    */
    public static string buildSelectAllStatment(string objectType)
    {
        Map<String, Schema.SObjectField> fldObjMap = Schema.getGlobalDescribe().get(objectType).getDescribe().fields.getMap();
        List<Schema.SObjectField> fldObjMapValues = fldObjMap.values();
        String theQuery = 'SELECT ';
        for(Schema.SObjectField s : fldObjMapValues)
        {  
           theQuery += s.getDescribe().getName() + ',';
        }
        
        // Trim last comma
        theQuery = theQuery.subString(0, theQuery.length() - 1);
        
        // Finalize query string
        theQuery += ' FROM '+objectType;    
        
        return theQuery;
    }
    
    /**
    * Uses salesforce business hour logic to calculate how many minutes between two dates fall within a companies defined business hours.
    * used for calculating things like how long a request has been open, or SLA times, etc.
    * @param startDate datetime object reprsenting the starting date
    * @param endDate datetime object representing the ending date
    * @return the number of minutes between the two days that fall into support business hours.
    */
    public static double calculateBusinessMinutesBetweenDates(dateTime startDate, dateTime endDate)
    {
        //get the default business hours for the org
        list<BusinessHours> stdBusinessHours = [select id from businesshours where isDefault = true];
        if(!stdBusinessHours.isEmpty())
        {
            return BusinessHours.diff(stdBusinessHours[0].id,startDate,endDate) / 1000 / 60;
        }
        else
        {
            //if we don't have any business hours defined, just return 0 to prevent error, but let lets the user know something is up.
            return 0;
        }
    }
    // VIP : Added for BT Demo 02/25 : calculate differences in Days (this has some funky use cases. like Start = friday noon end = mon 10am..  that will return 0)
    /**
    * simple wrapper for the calculateBusinessMinutesBetweenDates method that converts the minutes into days
    * @param startDate datetime object reprsenting the starting date
    * @param endDate datetime object representing the ending date
    * @return the number of hours between the two days that fall into support business hours.    
    */
    
    public static double calculateBusinessDaysBetweenDates(dateTime startDate, dateTime endDate)
    {
        return calculateBusinessMinutesBetweenDates(startDate, endDate) / 60 / 24;
    }


    /**
    * Generates sample data for the unit tests
    * @return null
    */   
    public static void generateTestData()
    {
        Contract_Type__c thisMatterSubType = new Contract_Type__c();
        thisMatterSubType.external_ID__c = 'DAFDAfDAFSDFASDFASDFASDFASFSA';
        thisMatterSubType.name = 'My Subtype';
    
        insert thisMatterSubType;
        
        Account thisTestAccount = new Account();
        thisTestAccount.name = 'test account';
        insert thisTestAccount;
        
        Contact thisTestContact = new Contact();
        thisTestContact.lastName = 'testguy';
        thisTestContact.accountId = thisTestAccount.id;
        insert thisTestContact;
        
        Request__c thisRequest = new Request__c();
        thisRequest.name = 'My Request';
        thisRequest.Requesting_Contact__c = thisTestContact.id;
        thisRequest.stage__c = 'test';
        thisRequest.step__c = 'testing';
        thisRequest.status__c = 'testing';
        thisRequest.Stop_Time__c = dateTime.now();
        insert thisRequest;

        Request__c thisRequest2 = new Request__c();
        thisRequest2.name = 'My Request';
        thisRequest2.Requesting_Contact__c = thisTestContact.id;
        thisRequest2.stage__c = 'Request';
        thisRequest2.step__c = 'Accepted';
        thisRequest2.status__c = 'Complete';
        thisRequest2.Stop_Time__c = dateTime.now();
        insert thisRequest2;
                
        Matter__c thisMatter = new Matter__c();
        thisMatter.name = 'My Matter';
        thisMatter.Matter_Subtype__c = thisMatterSubType.id; 
        thisMatter.Owner__c = UserInfo.getUserId();
        thisMatter.Request__c = thisRequest.id;
        thisMatter.stage__c = 'Allocate';
        thisMatter.First_Substantive_Response__c = datetime.now();
        thisMatter.Number_of_Days_to_FSR__c = 1;
        insert thisMatter;
        
        Version__c thisVersion = new Version__c();
        thisVersion.Matter__c = thisMatter.id;
        thisVersion.name = 'My Test Version';
        
        insert thisVersion;
        
        Task thisTask  = new Task();
        thisTask.subject = 'Test Subject';
        thisTask.status = 'In Progress';
        thisTask.priority = 'Normal';
        thisTask.whatId = thisMatter.id;
        thisTask.whoId = thisTestContact.id;
        thisTask.task_weight__c = 0;
        
        insert thistask;
        
        Clockstopper__c thisClockStopper = new Clockstopper__c();
        thisClockStopper.Client_Contact__c = thisTestContact.id;
        thisClockStopper.Matter__c = thisMatter.Id;
        thisClockStopper.Related_Task__c = thisTask.Id;
        thisClockStopper.Start_Time__c = datetime.now();
        thisClockStopper.status__c = 'In Progress';
        
        insert thisClockStopper;
        


       Axiom_Misc_Settings__c AXCS = Axiom_Misc_Settings__c.getInstance( UserInfo.getOrganizationId() );
       if(AXCS.Id == null)
       {
           AXCS.Audit_History_Email_Recipients__c = 'Test@Test.com';
           AXCS.Audit_History_Endpoint__c = 'http://www.apexdevnet.com';
           AXCS.Audit_History_Endpoint_Password__c = '';
           AXCS.Audit_History_Endpoint_Username__c = '';
           AXCS.Default_Task_Weight__c = 1;

           insert AXCS;
       }
        

    }
}