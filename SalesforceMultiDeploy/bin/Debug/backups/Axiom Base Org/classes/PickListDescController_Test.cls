@isTest
global class PickListDescController_Test
{
    @isTest
    public static void testPickListDescController()
    {        
        Test.StartTest();
        PageReference pageRef = Page.PickListDesc;
        pageRef.getParameters().put('sobjectType', 'Contact');
        pageRef.getParameters().put('recordTypeName', 'Legal Contact');
        pageRef.getParameters().put('picklistFieldName', 'Geography__c');
        Test.setCurrentPage(pageRef);   
                
        PickListDescController controller = new PickListDescController();
        
        Test.StopTest();
    }
}