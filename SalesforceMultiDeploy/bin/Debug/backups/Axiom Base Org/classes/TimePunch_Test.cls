@isTest
global class TimePunch_Test
{
    @isTest
    global static void testTimePunchController()
    {
        account testAccount= new Account();
        testAccount.name = 'Test Account';        
        insert testAccount;

        account testAccount2= new Account();
        testAccount2.name = 'Test Account2';        
        insert testAccount2;
              

        Profile p = [select id from profile where name='Standard User']; 
        User u = new User(alias = 'standt', email='standarduser@testorg.com', 
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
            localesidkey='en_US', profileid = p.Id, 
            timePunch_Max_Hours__c=8,
            timezonesidkey='America/Los_Angeles', username='standarduser@xeroInteractiveTestData.com');


        System.runAs(u) 
        {        
        
            Test.StartTest();
       
            Apexpages.StandardController stdController = new Apexpages.StandardController(testAccount); 
            
            timePunch_timerController tp = new timePunch_timerController(stdController);
           
            
            //Create a timer
            timePunch_timer__c newtimer = TimePunch_timerController.createTimer(testAccount.id);

            system.debug('Created Timer ID: ' + newTimer.id);
            //attempt to manually create a timecard. This timecard should be the same one that was returned from the creation of the timecard above.
            //since creating a timer will create a timecard if one does not exist or return a existing one if one does exist, attempting to create another
            //timecard should just return the timecard that already exists.
            
            timePunch_Timecard__c testCard = TimePunch_timerController.getTimeCard();
            
            system.assertEquals(newtimer.timeCard__c,testCard.id);
                        
            //Try to create another timer. this one should error since there is another timer for this user already running
            try
            {
                timePunch_timer__c newtimer2 = TimePunch_timerController.createTimer(testAccount2.id);
                newtimer2 = TimePunch_timerController.stopTimer(newtimer2.id);
                system.debug('Created Second Timer ID: ' + newtimer2.id);
                //system.assertEquals('test failure','this line should never be encountered because the line above it should error and be caught');
            }
            catch(exception e)
            {
                system.debug('good, creating the second timer failed');
            }
            //fetch the existing timers
            list<timePunch_timer__c> accountTimers = timePunch_timerController.getTimer(testAccount.id);
            system.assertEquals(accountTimers[0].task_type__c, 'account__c');
            
            //Stop the timer
            timePunch_timer__c endtimer = TimePunch_timerController.stopTimer(newtimer.id);
            
            endtimer = [select id, status__c, final_minutes__c from timePunch_timer__c where id = :endTimer.id];
            
            system.debug(endTimer);
            system.assertEquals(endtimer.status__c, 'Stopped');
            
            
            //now try to edit the record, this should result in an error
            try
            {
                endTimer.comments__c = 'test';
                update endTimer;
            }
            catch(exception e)
            {
            
            }
            
            //create another timer. We'll have to make this one manually since we have to make it 
            //seem like it's been running for a long time and the validation rules won't allow playing
            //with the start date after it's been started.
            
            timePunch_timer__c longRunningtimer = new timePunch_timer__c();
            
            Date currentDate = Date.today();
                 
            longRunningtimer.timecard__c = testCard.id;

            longRunningtimer.User__c = UserInfo.getUserId();
            longRunningtimer.put('Account__c',testAccount.id);            
            longRunningtimer.start_time__c = dateTime.now().addHours(-18);
            longRunningtimer.status__c = 'Running';          
            insert longRunningtimer;
            
            longRunningtimer.status__c = 'Stopped';
            update longRunningtimer;
            
            //now if we look at the pending hours on this timecard it should be like 18 hours.
            testCard= [select id, Committed_Hours__c from timePunch_timecard__c where id = :testCard.id];
            //system.assertEquals(testCard.Committed_Hours__c.round(),18); 

            //now that we have verified that the user has more than their maximum amount of possible shift hours
            //pending, when we call the stopOverRunningtimers method it should stop any timers that are still
            //running that would put the user over their max, and set the timer to a value that would put them back at their max.

            longRunningtimer.status__c = 'Running';
            update longRunningtimer;
                        
            //check for and stop any overrunning timers
            timePunch_methods.stopOverRunningTimers();
            
            //the timer itself should have a stopped status
            longRunningtimer = [select id, status__c from timePunch_timer__c where id = :longRunningtimer.id];
            system.assertEquals(longRunningtimer.status__c,'Stopped'); 
            
            testCard= [select id, Committed_Hours__c, approximate_pending_hours__c from timePunch_timecard__c where id = :testCard.id];
            //there should be zero pending hours
            system.assertEquals(testCard.approximate_pending_hours__c.round(),0); 
            
            
            //even after the card is stopped we should be able to update the comments without error
            timePunch_timerController.updateTimerComments(longRunningtimer.id, 'Test Comments');
            
            
            
            // Schedule the test job 
                  
            String jobId = System.schedule('timePunch_stopRunningCards',
            timePunch_stopRunningCards.CRON_EXP, 
            new timePunch_stopRunningCards());
            // Get the information from the CronTrigger API object 
                       
            CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
            NextFireTime
            FROM CronTrigger WHERE id = :jobId];
            
            // Verify the expressions are the same             
            System.assertEquals(timePunch_stopRunningCards.CRON_EXP, ct.CronExpression);
            
            // Verify the job has not run            
            System.assertEquals(0, ct.TimesTriggered);
            
            // Verify the next time the job will run            
            System.assertEquals('2022-09-03 00:00:00', 
            String.valueOf(ct.NextFireTime));
            
            Test.StopTest();
       }     
       
    }
}