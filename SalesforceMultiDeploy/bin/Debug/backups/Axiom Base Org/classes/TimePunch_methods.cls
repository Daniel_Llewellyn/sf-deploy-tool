global class TimePunch_methods
{
    /*this methods pervents a person from having multiple timers active at the same time. It can accept a list
    //of timer objects for different users
    public static void preventMultipleActiveTimers(list<timePunch_timer__c> timers)
    {
        map<Id,list<timePunch_timer__c>> userRunningTimers= new map<Id,list<timePunch_timer__c>>();
        
        //create a set of all users who have a timer they are trying to create/edit
        for(timePunch_timer__c tr : timers)
        {
            if(tr.status__c == 'Running')
            {
                //check the list to see if it aready has an entry for this user. If so, that means that the list that 
                //came in had multiple running timers for one person (like if it was from an import or something). Regardless
                //that is not okay, so attach an error.
                if(userRunningTimers.containsKey(tr.Timecard__r.user__c))
                {
                    tr.addError('Multiple running timers specified for user in list. Only one running timer per user is allowed');
                }
                else
                {
                   list<timePunch_timer__c> runningTimers = new list<timePunch_timer__c>();
                   userRunningTimers.put(tr.user__c,runningTimers);
                }
            }
            

        }

        //find any existig timers for any of the users in the list we created above
        
        //Timecard__r.user__c in  and
        list<timePunch_timer__c> currentlyActiveTimers = [select id,  Timecard__r.user__c from timePunch_timer__c where  Timecard__r.user__c in :userRunningTimers.keySet() and status__c = 'Running' order by start_time__c desc];
 
        //create a map of users to their currently active timers
        for(timePunch_timer__c tr : currentlyActiveTimers)
        {             
            list<timePunch_timer__c> runningTimers =  userRunningTimers.get(tr.Timecard__r.user__c);
            runningTimers.add(tr);
            userRunningTimers.put(tr.Timecard__r.user__c,runningTimers);                 
        }

        //loop over the timers passed in again, see if they have an entry in the active timers list that is not the timer they are trying to edit/insert
        for(timePunch_timer__c tr : timers)
        {
            
            //if the existing timer map has an entry for this user check to see if the entry is for the card we are currently modifying.
            if(userRunningTimers.get(tr.user__c) != null && userRunningTimers.get(tr.user__c).size() > 1)
            {
                    tr.addError('User already has an active timer: ' + userRunningTimers.get(tr.user__c)[0].id);
            }
        }
    }
    */
    
    //this method will find any currently running timers and stop them if the user is past their total possible hours
    //and set the timer to a value that will get their timecard back the maximum.
    public static void stopOverRunningTimers()
    {
        //find all the currently running timers
        list<timePunch_timer__c> currentlyActiveTimers = [select id,  
                                                                 Start_Time__c,
                                                                 Timecard__r.user__c,
                                                                 Timecard__r.user__r.timePunch_Max_Hours__c,
                                                                 Timecard__r.Committed_Minutes__c 
                                                          from timePunch_timer__c 
                                                          where status__c = 'Running'];
        decimal proposedTimerMinutes = 0;
        decimal timeLimitMinutes = 0;
        datetime rightNow = datetime.now();
        
        for(timePunch_timer__c timer : currentlyActiveTimers)
        {
            //the amount of minutes this timer would be for if we stopped the timer right now. If this number is greater than or
            //equal to the amount of minutes this timecard could have and still be valid, then we need to stop the timer.
            proposedTimerMinutes = timer.Timecard__r.Committed_Minutes__c + getDifferenceInMinutes(rightNow, timer.Start_Time__c);
            
            //the maximum amount of minutes this timecard can have/this timer can be for and have the timecard still be within the
            //users maximum.
            try{
                timeLimitMinutes = timer.Timecard__r.user__r.timePunch_Max_Hours__c * 60 - timer.Timecard__r.Committed_Minutes__c;
            }catch(exception e)
            {
                timeLimitMinutes = 9999999;
            }
            if(proposedTimerMinutes >= timeLimitMinutes)
            {
                timer.status__c = 'Stopped';
                timer.End_Time__c = timer.Start_Time__c.addMinutes((integer) timeLimitMinutes);                              
            }
            
        }
        update currentlyActiveTimers;
    }
    
    //this will iterate over the relationship fields on the timer (excluding the user field)
    //and see which one is populated. It will then set a simple text flag on the timer to identify 
    //what object type this timer is for.
    public static void setTimerType(list<timePunch_timer__c> timers)
    {
        Map<String, Schema.SObjectField> objectfields = Schema.SObjectType.timePunch_timer__c.fields.getMap();
        list<string> lookupFields = new list<string>();
        boolean typeAssigned = false;
        
        for(string fieldName : objectFields.keySet())
        {
           if(objectFields.get(fieldName).getDescribe().getType().name() == 'Reference' && (fieldName != 'createdbyid' && fieldName != 'timecard__c' && fieldName != 'lastmodifiedbyid'))
           {
               lookupFields.add(fieldName);
           } 
        }
                    
        for(timePunch_timer__c timer : timers)
        {
            typeAssigned = false;
            for(string fieldName : lookupFields)
            {
                if((string) timer.get(fieldName)!= null)
                {
                    if(!typeAssigned)
                    {
                        typeAssigned = true;
                        timer.Task_Type__c = fieldName;
                        timer.Related_To_Id__c = (string) timer.get(fieldName);
                    }
                    else
                    {
                        timer.addError('Timer may only be related to one object at a time');
                    }
                }
            }
        }
    }
    
    public static double getDifferenceInMinutes(datetime firstDT, datetime secondDT)
    {
       long dt1 = firstDT.getTime() / 1000 / 60;  //getTime gives us milliseconds, so divide by 1000 for seconds
       long dt2 = secondDT.getTime() / 1000 / 60;
       double d = dt1 - dt2;
       return d;
    }    
    
}