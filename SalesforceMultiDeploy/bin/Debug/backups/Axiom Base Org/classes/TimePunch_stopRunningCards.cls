global class TimePunch_stopRunningCards implements Schedulable
{
    public static String CRON_EXP = '0 0 0 3 9 ? 2022';
    global void execute(SchedulableContext ctx) 
    {               
        timePunch_methods.stopOverRunningTimers();
    }   
}