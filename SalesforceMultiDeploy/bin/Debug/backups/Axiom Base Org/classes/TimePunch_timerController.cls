global class TimePunch_timerController {

    global timePunch_timerController(ApexPages.StandardController controller) {

    }
    
    @RemoteAction
    global static list<timePunch_timer__c> getTimer(id objectId)
    {
        list<timePunch_timer__c> timers = new list<timePunch_timer__c>();
        string objectType = getObjectTypeFromId(objectId);
        
        //if the last 3 chars are not __c, then append them. All lookup fields will be the name of the object they relate to, plus __c since they are custom fields.
        //however non custom objects won't have that __c on them, so trying to create a fieldname from the objectname would fail. This fix essentially is to get standard
        //object types working.
        
        if(objectType.subString(objectType.length()-3,objectType.length()) != '__c')
        {
            objectType+= '__c';
        }
        timePunch_timer__c thisTimer;
        Date currentDate = Date.today();
        timePunch_Timecard__c thisCard = getTimeCard();
        
        string query = 'select id, Name, task_type__c, final_minutes__c, final_seconds__c, final_hours__c, running_seconds__c, status__c from timePunch_timer__c where ' + objectType + ' = \''+objectId+'\' and Timecard__r.Date__c = :currentDate';
               
        timers = database.query(query);
        
        return timers;
    
    }
    
    @RemoteAction
    global static timePunch_timer__c createTimer(id objectId)
    {
        Date currentDate = Date.today();
        timePunch_Timecard__c findCard = getTimeCard();
        timePunch_timer__c thisTimer = new timePunch_timer__c();
             
        thisTimer.timecard__c = findCard.id;
        string objectType = getObjectTypeFromId(objectId);
        if(objectType.subString(objectType.length()-3,objectType.length()) != '__c')
        {
            objectType+= '__c';
        }
        thistimer.User__c = UserInfo.getUserId();
        thistimer.put(objectType,objectId);
        
        thisTimer.start_time__c = dateTime.now();
        thisTimer.status__c = 'Running';
        
        insert thisTimer;
        
        return thisTimer;
         
    }
    
    @RemoteAction
    global static timePunch_timer__c updateTimerComments(Id timerId, string comments)
    {
        timePunch_timer__c thisTimer = new timePunch_timer__c(id=timerId);
        thisTimer.comments__c = comments;
        update thisTimer;
        return thisTimer;
    }
    
    @RemoteAction
    global static timePunch_timer__c stopTimer(id timerId)
    {
        timePunch_timer__c thisTimer = new timePunch_timer__c(id=timerId);
        thisTimer.end_time__c = dateTime.now();
        thisTimer.status__c = 'Stopped';
        update thisTimer;
        return thisTimer;
    }
    global static timePunch_Timecard__c getTimeCard()
    {
        Date currentDate = Date.today();
        timePunch_Timecard__c thisCard;
        list<timePunch_Timecard__c> findCard = [select id, submitted__c, date__c from timePunch_Timecard__c where date__c = :currentDate and user__c = :UserInfo.getUserId() limit 1];
        if(findCard.isEmpty())
        {
            thisCard = createTimeCard();
        }
        else
        {
            thisCard = findCard[0];
        }
        
        return thisCard;
    }
    
    global static  timePunch_Timecard__c createTimeCard()
    {
         timePunch_Timecard__c thisCard = new timePunch_Timecard__c();

         try
         {
             thisCard.date__c =  Date.today();
             thisCard.user__c =  UserInfo.getUserId();
             insert   thisCard;
         }
         catch(exception e)
         {
             system.debug(e.getMessage());
         }
         return thisCard;
    }
    
    global static string getObjectTypeFromId(string objectId)
    {
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe(); 
        Map<String,String> keyPrefixMap = new Map<String,String>{};
        Set<String> keyPrefixSet = gd.keySet();
        for(String sObj : keyPrefixSet)
        {
           Schema.DescribeSObjectResult r =  gd.get(sObj).getDescribe();
           String tempName = r.getName();
           String tempPrefix = r.getKeyPrefix();
           keyPrefixMap.put(tempPrefix,tempName);
        }
    
        return keyPrefixMap.get(objectId.subString(0,3));  
    }
}