<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <customSettingsType>List</customSettingsType>
    <customSettingsVisibility>Public</customSettingsVisibility>
    <description>Additional restrictions that limit the matter actions visible to a user based on their contact record type, and another field value on their contact record.</description>
    <enableFeeds>false</enableFeeds>
    <fields>
        <fullName>Contact_Field_Name__c</fullName>
        <description>The API name of a field on the contact record that contains a value to filter against</description>
        <externalId>false</externalId>
        <inlineHelpText>The API name of a field on the contact record that contains a value to filter against</inlineHelpText>
        <label>Contact Field Name</label>
        <length>200</length>
        <required>true</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Contact_Field_Value__c</fullName>
        <description>The value of the field on the contact that must be matched for this rule to apply</description>
        <externalId>false</externalId>
        <inlineHelpText>The value of the field on the contact that must be matched for this rule to apply</inlineHelpText>
        <label>Contact Field Value</label>
        <length>200</length>
        <required>true</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Contact_Record_Type__c</fullName>
        <description>The contact record type for this rule. Use the API name</description>
        <externalId>false</externalId>
        <inlineHelpText>The contact record type for this rule. Use the API name</inlineHelpText>
        <label>Contact Record Type</label>
        <length>200</length>
        <required>true</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Filtered_Matter_Actions__c</fullName>
        <description>Comma separated list of matter actions to be filtered (removed) if the contact matches this rule.</description>
        <externalId>false</externalId>
        <inlineHelpText>Comma separated list of matter actions to be filtered (removed) if the contact matches this rule.</inlineHelpText>
        <label>Filtered Matter Actions</label>
        <required>true</required>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Filtered_Matter_Types__c</fullName>
        <description>Matter record types to hide if the contact matches this rule</description>
        <externalId>false</externalId>
        <inlineHelpText>Matter record types to hide if the contact matches this rule</inlineHelpText>
        <label>Filtered Matter Types</label>
        <required>false</required>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Filtered_Request_Types_Extended__c</fullName>
        <description>Created to extend the character limit for the filter Request Types</description>
        <externalId>false</externalId>
        <label>Filtered Request Types Extended</label>
        <required>false</required>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Filtered_Request_Types__c</fullName>
        <description>Request Record Types to be hidden if a contact matches this rule</description>
        <externalId>false</externalId>
        <inlineHelpText>Request Record Types to be hidden if a contact matches this rule</inlineHelpText>
        <label>Filtered Request Types</label>
        <required>false</required>
        <type>TextArea</type>
    </fields>
    <label>Client Portal Matter Action Filters</label>
</CustomObject>
