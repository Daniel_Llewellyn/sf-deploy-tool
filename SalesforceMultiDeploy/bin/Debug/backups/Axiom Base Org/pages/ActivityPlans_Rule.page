<apex:page standardController="Activity_Plan_Rule__c" extensions="ActivityPlans_RuleController" id="page">
    <apex:variable id="showComparisonChooser" var="showComparisonChooser" value="true"/>
    <apex:sectionHeader title="Edit Rule" subtitle="{!Activity_Plan_Rule__c.Name}"/>
    <apex:pageMessages />
    

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script>
        $.noConflict();
        jQuery( document ).ready(function( $ ) {
            jQuery('.required option[value=""]').remove();

              jQuery('#page\\:form\\:fields\\:fieldTable\\:0\\:compareType').change(function(){
                  if(jQuery(this).val() == 'Static Value')
                  {
                      jQuery('#page\\:form\\:fields\\:fieldTable\\:0\\:compareValueField').hide();
                      jQuery('#page\\:form\\:fields\\:fieldTable\\:0\\:compareValueStatic').show();
                  }
                  else if(jQuery(this).val() == 'Object Field Value')
                  {
                      jQuery('#page\\:form\\:fields\\:fieldTable\\:0\\:compareValueField').show();
                      jQuery('#page\\:form\\:fields\\:fieldTable\\:0\\:compareValueStatic').hide();
                  }
              }).change();
              
              jQuery('.fieldInfo').hide();
              
              jQuery('.fieldInfo').clone().removeClass('group1').addClass('group2').appendTo('#groupTwoSection');
        });

        function getFieldType(fieldName,group) {
            ActivityPlans_RuleController.getFieldType('{!parentObjectType}',fieldName, function(result, event){
                if(event.status) {
                    console.log(result);
                    jQuery('.'+group).hide();
                    
                    jQuery('div.'+group).each(function(){
                        
                        if(jQuery(this).hasClass(result))
                        {
                            jQuery(this).show();
                        }
                    });
                }
            });
        }        
    </script>
    <apex:form id="form" rendered="{!initError==false}">

        <apex:pageBlock id="fields" >
            <apex:pageBlockButtons >
                <apex:commandButton value="Save" action="{!save}"/>
                <apex:commandButton value="Cancel" action="{!cancel}"/>                
            </apex:pageBlockButtons>     
 

            
            <apex:pageBlockTable value="{!thisRule}" var="field" id="fieldTable">
                <apex:column rendered="{!recordTypeName == 'Activity_Plan_Task_Template_Rule'}" title="Task Template" headerValue="Task Template">
                    <apex:inputField value="{!thisRule.Activity_Plan_Task_Template__c}" />
                </apex:column>
                
                <apex:column title="What field on the object are we evaluating?" headerValue="Source Field">
                     <apex:selectList multiselect="false" size="1" label="Field To Evaluate" styleClass="override" value="{!selectedSourceField}" onchange="getFieldType(this.value,'group1');">
                        <apex:selectOptions value="{!allObjectFields}"/>

                            
                    </apex:selectList>                   
                </apex:column>

                <apex:column title="What type of comparison do we need to perform?" headerValue="Logical Operator">
                    <apex:inputField value="{!thisRule.Logical_Operator__c}" styleClass="required"/>
                </apex:column>
                
              
                <apex:column title="Where is the comparison value coming from. Either a static value entered here, or from a field on the same object?" headerValue="Comparison Type" rendered="{!showComparisonChooser=='true'}">
                    <apex:inputField value="{!thisRule.Comparison_Type__c}" styleClass="required" id="compareType"/>
                </apex:column>
              
                
                
                <apex:column title="Comparison Value" headerValue="Comparison Value" style="width:300px;">
                    <apex:inputField value="{!thisRule.Comparison_Value__c}" id="compareValueStatic"/>
                    
                    
                    <apex:selectList multiselect="false" size="1" label="Field To Evaluate" styleClass="override" value="{!selectedCompareField}" id="compareValueField" rendered="{!showComparisonChooser=='true'}" onchange="getFieldType(this.value,'group2');">
                        <apex:selectOptions value="{!allObjectFields}"/>
                    </apex:selectList>
                    
                </apex:column>
            </apex:pageBlockTable> 
            <apex:pageBlockSection title="Field Info" columns="2">
            
                <apex:pageBlockSectionItem >
                    <div id="groupOneSection">
                        <div id="field_type_id" class="fieldInfo group1 REFERENCE ID">
                            This field is an ID field which means that the value it is compared against should be a 15 or 18 character string of letters and numbers, or another ID field.
                        </div>
                        
                        <div id="field_type_id" class="fieldInfo group1 BOOLEAN">
                            This field is a Boolean value, which means that it must be compared against a true or false value. Enter the word true, or false or compare against a another boolean field.
                        </div>
                        
                        <div id="field_type_id" class="fieldInfo group1 CURRENCY DOUBLE PERCENT ">
                            This field is a number with decimal points. It must be compared against another number that has decimal points or another numeric field
                        </div>
                        
                        <div id="field_type_id" class="fieldInfo group1 DATE">
                            This field is a date field, so it must be compared against text that can be parsed as a date, such as "3/21/1988" (Without quotes. The exact format depends on your locale), or another date field.
                        </div>

                        <div id="field_type_id" class="fieldInfo group1 DATETIME">
                            This field is a datetime field, so it must be compared against text that can be parsed as a date with a time component, such as "3/21/1988 11:42  AM" (Without quotes. The exact format depends on your locale), or another date field.
                        </div>            
                        
                        <div id="field_type_id" class="fieldInfo group1 STRING PICKLIST">
                            This field is a string field, meaning it can contain any series of letters, numbers or special characters. Thus it can be compared against any value.
                        </div>            
                    </div>
                </apex:pageBlockSectionItem> 
    
                <apex:pageBlockSectionItem id="groupTwoSection" >
                    <div id="groupTwoSection"></div>
                </apex:pageBlockSectionItem>                               
            </apex:pageBlockSection>
                
                      
            
        </apex:pageBlock>
    </apex:form>
</apex:page>