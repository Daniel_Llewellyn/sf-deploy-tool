trigger Axiom_AttachmentTriggers on Attachment (after delete, after insert, after undelete, 
after update) 
{
    if(!Trigger.isDelete)
    {
        Axiom_AttachmentController.setAttachmentCountOnRequest(Trigger.New);
    }
    else
    {
        Axiom_AttachmentController.setAttachmentCountOnRequest(Trigger.Old);
    }
}