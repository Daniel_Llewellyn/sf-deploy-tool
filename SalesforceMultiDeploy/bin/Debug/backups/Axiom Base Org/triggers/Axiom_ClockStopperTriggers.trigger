trigger Axiom_ClockStopperTriggers on Clockstopper__c (before insert, before update, after insert, after update) 
{

    if(Trigger.isBefore)
    {
        Axiom_ClockstopperController.calculateNetTime(trigger.new);
    }
    if(Trigger.isAfter)
    {
         Axiom_ClockstopperController.sumMatterClockstoppers(trigger.new);
         Axiom_ClockstopperController.setOpenClockstoppersFlagOnTask(trigger.new);
    }
}