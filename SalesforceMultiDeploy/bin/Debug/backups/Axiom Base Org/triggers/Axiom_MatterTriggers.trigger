trigger Axiom_MatterTriggers on Matter__c (before insert, before update, after insert, after update, after delete, after undelete) 
{
    
    if(Trigger.isBefore)
    {        
        Axiom_MatterController.setNumberOfDaysToFsr(Trigger.New);
        Axiom_MatterController.setNumberOfDaysOpen(Trigger.New);
        if(Trigger.isInsert)
        {
            Axiom_MatterController.initMatterToRequest(Trigger.New);            
        }
        Axiom_MatterController.setNumberOfDaysOpen(trigger.new);
        Axiom_MatterController.setNumberOfDaysToFsr(trigger.new);        
    }
    
    if(Trigger.isAfter)
    {     
        if(!Trigger.isDelete)
        {
            ActivityPlansController.findMatchingActivityPlans(Trigger.New);
            Axiom_MatterController.setOpenClockStoppersFlagOnRequest(trigger.new);
            Axiom_MatterController.setNumMattersOnRequest(trigger.new);
        }
        else if(Trigger.isDelete)
        {
            Axiom_MatterController.setNumMattersOnRequest(trigger.old);
            Axiom_MatterController.setOpenClockStoppersFlagOnRequest(trigger.old);
        }
    }
}