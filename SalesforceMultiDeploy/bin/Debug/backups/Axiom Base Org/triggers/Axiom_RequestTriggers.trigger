trigger Axiom_RequestTriggers on Request__c (before update, after insert, after update) 
{
    if(Trigger.isBefore)
    {
        Axiom_RequestController.setNumberOfDaysToFsr(Trigger.new);
    }
    if(Trigger.isAfter)
    {
        if(Trigger.isinsert)
        {
            Axiom_RequestController.changeMatterOwnersToRequestOwner(new map<id,Request__c>(), trigger.newMap);
        }
        else if(Trigger.isUpdate)
        {
            Axiom_RequestController.changeMatterOwnersToRequestOwner(trigger.oldMap, trigger.newMap);
        }
    }
}