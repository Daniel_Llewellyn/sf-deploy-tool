trigger Axiom_TaskTriggers on Task (before insert, before update, after insert, after update, after delete) 
{    
    if(trigger.isBefore)
    {
        //sets default weight on task to prevent errors since field is required.
        Axiom_TaskController.setTaskDefaultWeight_before(trigger.new); 
        Axiom_TaskController.updateMatterNumberofDaysToFSR(trigger.new);
                
        if(trigger.isInsert)
        {   
            //set the section number based on the last char in the task
            Axiom_TaskController.setTaskSection(trigger.new);
        }
        
        if(trigger.isUpdate)
        {
            Axiom_TaskController.setNumberOfDaysOpen_before(trigger.new);      
        }
    }
    else if(trigger.isAfter)
    {       
        if(!Trigger.isDelete)
        {   
            //there is a special kind of task that when it is completed needs to update the matters S3 fields to being completed
            Axiom_TaskController.updateMatterOnAllTasksClosed(trigger.new);
            
            Axiom_TaskController.processTaskUpdateCustomSetting(trigger.new);
            
            Axiom_TaskController.setOpenClockstopperFlagOnMatter(trigger.new);
            
            if(trigger.isUpdate)
            {
                Axiom_TaskController.startTimer(trigger.oldMap, trigger.newMap);

                Axiom_TaskController.updateMatterNumberofDaysToFSR(trigger.new);            
            }
            else if(trigger.isInsert)
            {
                Axiom_TaskController.startTimer(new map<id,task>(), trigger.newMap);
                
                Axiom_TaskController.createClockStopperFromTask(trigger.newMap);
            }
        }
        else
        {
            Axiom_TaskController.setOpenClockstopperFlagOnMatter(trigger.old);
        }
    }
}