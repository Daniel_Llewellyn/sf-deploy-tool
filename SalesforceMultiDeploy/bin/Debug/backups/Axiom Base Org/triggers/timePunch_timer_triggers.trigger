trigger timePunch_timer_triggers on timePunch_Timer__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) 
{
    timePunch_Timer__c[] newTimers = Trigger.new;
    timePunch_Timer__c[] oldTimers = Trigger.old;
    //This set of triggers is responsible for all actions related to respondent__c objects.
    
    //Before execution Triggers
    if(Trigger.isBefore)
    {
        if(Trigger.isInsert)
        {           
            //timePunch_methods.preventMultipleActiveTimers(newTimers);
            timePunch_methods.setTimerType(newTimers);
        }
        else if(Trigger.isUpdate)
        {
            //timePunch_methods.preventMultipleActiveTimers(newTimers);
            timePunch_methods.setTimerType(newTimers);
        }
    }
}