<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>Tatapetto is the work powered pet. Like a business tamagotchi, by being productive and driving progress your pet grows stronger. Raise them and battle them for fun and motivation.</description>
    <label>Tatapetto</label>
</CustomApplication>
