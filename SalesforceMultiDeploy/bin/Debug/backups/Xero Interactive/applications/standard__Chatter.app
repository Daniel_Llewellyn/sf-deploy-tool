<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <tab>standard-Chatter</tab>
    <tab>standard-UserProfile</tab>
    <tab>standard-OtherUserProfile</tab>
    <tab>standard-CollaborationGroup</tab>
    <tab>standard-File</tab>
    <tab>Party_Force_Event__c</tab>
    <tab>Data_Log__c</tab>
    <tab>Event__c</tab>
    <tab>RSS_Feed__c</tab>
    <tab>Chatter_Swagger__c</tab>
    <tab>Chatter_Swagger_Topic__c</tab>
    <tab>Package__c</tab>
    <tab>Sales_Order__c</tab>
    <tab>timePunch_Timecard__c</tab>
    <tab>timePunch_Timer__c</tab>
    <tab>Clicked_User__c</tab>
    <tab>SW_World__c</tab>
    <tab>SW_Behavior_Script__c</tab>
    <tab>Chatter_Dialects_Dialect__c</tab>
</CustomApplication>
