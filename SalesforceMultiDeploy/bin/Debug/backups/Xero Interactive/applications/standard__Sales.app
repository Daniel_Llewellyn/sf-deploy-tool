<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <tab>standard-Chatter</tab>
    <tab>standard-File</tab>
    <tab>standard-Campaign</tab>
    <tab>standard-Lead</tab>
    <tab>standard-Account</tab>
    <tab>standard-Contact</tab>
    <tab>standard-Opportunity</tab>
    <tab>standard-AdvForecast</tab>
    <tab>standard-Contract</tab>
    <tab>standard-Case</tab>
    <tab>standard-Solution</tab>
    <tab>standard-Product2</tab>
    <tab>standard-report</tab>
    <tab>standard-Document</tab>
    <tab>standard-Dashboard</tab>
    <tab>Package__c</tab>
    <tab>Sales_Order__c</tab>
    <tab>timePunch_Timecard__c</tab>
    <tab>timePunch_Timer__c</tab>
    <tab>Clicked_User__c</tab>
    <tab>SW_World__c</tab>
    <tab>SW_Behavior_Script__c</tab>
    <tab>Chatter_Dialects_Dialect__c</tab>
</CustomApplication>
