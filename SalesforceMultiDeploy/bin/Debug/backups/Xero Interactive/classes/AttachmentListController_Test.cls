//Test class for AttachmentList_Controller class.
@isTest
private class AttachmentListController_Test{
    static testMethod void aLC_test() {
        //create test account
        Account testAccount = new Account(Name = 'Test Account');
        
        insert testAccount;
        
        //execute the Controller without attachments
        ApexPages.StandardController stdController = new ApexPages.StandardController(testAccount); 
        AttachmentList_Controller aLC = new AttachmentList_Controller(stdController);
        
        //create attachment records to be associated with account
        list<Attachment> testAttachmentList = new list<Attachment>();
        
        for(Integer i=0;i<10;i++) {
            testAttachmentList.add(new Attachment(ParentId = testAccount.Id, Name='test attachment ' + i, body = blob.valueOf('body ' + i)));
        }
        
        insert testAttachmentList;
        
        system.assertEquals(10,testAttachmentList.size());
        
        //execute controller again when account has attachments associated with it
        aLC = new AttachmentList_Controller(stdController);
        
        //execute method to create library picklist
        list<SelectOption> selOptions =  aLC.getLibraryRecordsOptions();
        
        //set selectAllCheck to false
        aLC.selectAllCheck = false;
        
        //call SelectDeselectAll method to de-select all records
        aLC.SelectDeselectAll();
        
        //call MoveToContent method. this will throw error as no records selected
        aLC.MoveToContent();
        
        //selectAllCheck to true
        aLC.selectAllCheck = true;
        
        //call SelectDeselectAll method to select all records
        aLC.SelectDeselectAll();
        
        //call MoveToContent method. this will create content data as the records are selected.
        aLC.MoveToContent();
        
        //call back method to navigate user to account record page
        aLC.back();
    }
}