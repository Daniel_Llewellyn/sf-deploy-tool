public with sharing class AttachmentList_Controller {
    //Account Record to fetch data from standard controller
    public Account accountRecord {get;set;}
    
    //wrapper attachment list that binds account's attachments with a boolean flag
    public list<WrapAttachment> wrapAttachmentList {get;set;}
    
    //boolean to select/de-select all records when user clicks header checkbox
    public boolean selectAllCheck {get;set;}
    
    //Library Id selected by User
    public Id LibraryRecord {get;set;}
    
    //Controller
    public AttachmentList_Controller(ApexPages.StandardController controller) {
        //instantiate AccountRecord if null
        if(accountRecord == null) {
            accountRecord = new Account();
        }
        
        //associate account details from standard controller
        accountRecord = (Account)controller.getRecord();
        
        //call method to get list of attachments
        AttachmentRecords();
    }
    
    //method to get list of attachments
    public void AttachmentRecords() {
        //attachment list
        list<Attachment> attachmentList = new list<Attachment>();
        
        //instantiate wrapper list
        wrapAttachmentList = new list<WrapAttachment>();
        
        //get account's attachment records
        attachmentList = [Select Id, ParentId, Name, BodyLength, ContentType    
                            From Attachment 
                            where ParentId =: accountRecord.Id 
                            LIMIT : (limits.getLimitQueryRows() - limits.getQueryRows())];
        //loop through attachments
        for(Attachment a:attachmentList) {
            //add attachment record in wrapper list
            wrapAttachmentList.add(new wrapAttachment(a));
        }
        if(wrapAttachmentList.isEmpty()) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Attachment does not exist for this Account.'));
        }
    }
    
    //inner class/wrapper class to associate boolean flag with attachment record
    public class WrapAttachment {
        public Attachment att {get;set;}
        public boolean isSelected {get;set;}
        
        //controller
        public WrapAttachment(Attachment a) {
            att = a;
            isSelected = false;
        }
    }
    
    //method invoked when user selects/de-selects header checkbox
    public void SelectDeselectAll() {
        //if user selects the header checkbox
        if(selectAllCheck == true) {
            //loop through wrapper list and set all checkbox to true
            for(WrapAttachment wAL:wrapAttachmentList) {
                wAL.isSelected = true;
            }
        }
        //else loop
        else {
            //loop through wrapper list and set all checkbox to false
            for(WrapAttachment wAL:wrapAttachmentList) {
                wAL.isSelected = false;
            }
        }
    }
    
    //method to create content from selected attachment
    public pageReference MoveToContent() {
        //content version list
        list<ContentVersion> contentVersionList = new list<ContentVersion>();
        
        //set of Ids created from selected attachments
        Set<Id> attachmentIdSet = new Set<Id>();
        
        //Loop through wrapper list
        for(WrapAttachment wAL:wrapAttachmentList) {
            //Add attachment id to set if isSelected is true
            if(wAL.isSelected == true) {
                attachmentIdSet.add(wAL.att.Id);
            }
        }
        
        //Check attachmentIdSet is not empty
        if(!attachmentIdSet.isEmpty()) {
            //get attachments along with body field based on Id in attachmentIdSet
            for(Attachment[] att:[Select Id, ParentId, Name, BodyLength, ContentType, Body    
                            From Attachment 
                            where Id in : attachmentIdSet 
                            LIMIT : (limits.getLimitQueryRows() - limits.getQueryRows())]) {
                for(Attachment a : att) {
                    //Create content version row in list
                    contentVersionList.add(new ContentVersion(PathOnClient = a.Name + a.ContentType, VersionData = a.Body,FirstPublishLocationId  = LibraryRecord));                 
                }   
            }
            //insert content version list
            if(!contentVersionList.isEmpty()) {
                try {
                    insert contentVersionList;
                }
                catch(Exception e) {
                    //add error on page
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Cannot Process request.'));
                }
            }
            //navigate user to Account page
            return new PageReference('/' + accountRecord.Id);
        }
        //if attachmentIdSet is empty. i.e. no record selected by user on page
        else {
            //add error on page
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Please select Attachment to move to Content.'));           
        }
        //keep user on same page
        return null;
    }
    
    //method to retun to account page when user clicks back button
    public pageReference back() {
        return new PageReference('/' + accountRecord.Id);
    }
    
    //List of selectoptions returning the available Library records as picklist
    public list<SelectOption> getLibraryRecordsOptions() {
        //select option list
        list<SelectOption> LibraryRecordsOptions = new list<SelectOption>();
        
        //get contentworkspace records and add name as label and id as value
        for(ContentWorkspace[] cWArray : [Select Id, Name from ContentWorkspace LIMIT : (limits.getLimitQueryRows() - limits.getQueryRows())]) {
            for(ContentWorkspace cW : cWArray) {
                LibraryRecordsOptions.add(new SelectOption(cW.Id,cW.Name));  
            }
        }
        if(LibraryRecordsOptions.isEmpty()) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Library does not exist.'));
        }
        //return list of select options
        return LibraryRecordsOptions;
    }
}