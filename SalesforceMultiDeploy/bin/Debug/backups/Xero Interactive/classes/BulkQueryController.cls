global class BulkQueryController
{

    //function for finding all the objects in this org that can be searched for. This includes custom objects
    //returns them as a map of the object label to the object name. Meant to be invoked via the interface at runtime.
    @remoteAction
    global static map<string,string> getSearchableObjects()
    {
        //list of object labels and names to return
        map<string,string> searchableObjects = new map<string,string>();
        
        //global describe of all objects in the org.
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe(); 
        
        //iterate over all objects
        for(string objType : gd.keySet())
        {
            //only add this object type if it is searchable, since this list will be used to create an interface
            //where the user can select which objects to search. No sense in including non searchable objects eh?
            if(gd.get(objType).getDescribe().isSearchable())
            {
                searchableObjects.put(gd.get(objType).getDescribe().getLabel(),objType);
            }
        }
        return searchableObjects;
    }
    
    //gets all the fields and some related data for a given object.
    @remoteAction 
    public static map<string,map<string,string>> getObjectFields(string objectType)
    {
        //map of data to return about fields
        map<string,map<string,string>> fields = new map<string,map<string,string>>();

        //describe the object. get map of fields
        Map<String, Schema.SObjectField> objectFields = Schema.getGlobalDescribe().get(objectType).getDescribe().fields.getMap();
   
        //iterate over returned field data
        for(String s : objectFields.keySet()) 
        {
        
            //get the fields data and create an entry in the list
           Schema.DescribeFieldResult fieldDescribe =  objectFields.get(s).getDescribe();
           map<string,string> fieldData = new map<string,string>();
           
           //field data points
           try
           {
               fieldData.put('type',string.valueOf(fieldDescribe .getType()));
               fieldData.put('label',fieldDescribe .getLabel());
               fieldData.put('isCustom',string.valueOf(fieldDescribe.isCustom()));
               fieldData.put('soapType',string.valueOf(fieldDescribe.getSOAPType()));
           }
           catch(exception e)
           {
           }
           fields.put(fieldDescribe.getName(),fieldData);
        }      
        return fields;    
    }

    //simple method for running a dynamic query.
    @remoteAction 
    public static map<string,list<sObject>> runQueryWithFilter(string fieldList, string objectType, string filterField, list<string> filterObjects)
    {
        map<string,list<sobject>> returnObject = new map<string,list<sobject>>();
  
        for(string filterObject : filterObjects)
        {
            returnObject.put(filterObject,new list<sobject>());
        }        
        string query = 'select ' + fieldList + ' from '+objectType+' where '+filterField +' in :filterObjects';
        list<sobject> results = database.query(query);
        
        for(sObject thisResult : results)
        {
            //returnObject.get((string) thisResult.get(filterField)).add(thisResult);
            system.debug(thisResult.get(filterField));
            
            list<sobject> objects = returnObject.get((string) thisResult.get(filterField));
            objects.add(thisResult); 
            returnObject.put((string)thisResult.get(filterField),objects);                
        }

        return returnObject;
    }  
    
    @remoteAction
    public static id createExcelSpreadhseet(map<string,list<sObject>> objectData,list<string> fieldNames, boolean includeHeader)
    {
        document thisDoc = new document();
        string spreadsheetData = '<table>';
        
        if(includeHeader)
        {
            spreadsheetData += '<thead><tr>';
            for(string fieldName : fieldNames)
            {
                spreadsheetData += '<th>'+fieldName+'</th>';
            }
            spreadsheetData += '</tr></th>';
        }
        
        spreadsheetData += '<tbody>';
        
        for(string filterCondition : objectData.keySet())
        {
            for(sObject obj : objectData.get(filterCondition))
            {
                spreadsheetData += '<tr><td>'+filterCondition+'</td>';
                for(string field : fieldNames)
                {
                    spreadsheetData += '<td>'+obj.get(field)+'</td>';
                }
                spreadsheetData += '</tr>';
            }
        }
        
        spreadsheetData += '</tbody></table>';

        Folder thisFolder = [select id from folder limit 1];
        thisDoc.Name = 'XLS Export of Data';
        String myContent = spreadsheetData;
        thisDoc.Body = Blob.valueOf(myContent);
        thisDoc.ContentType = 'application/vnd.ms-excel';
        thisDoc.Type = 'xls';
        thisDoc.FolderId = thisFolder.id;
        insert thisDoc;
        
        return thisDoc.id; 
    }  
}