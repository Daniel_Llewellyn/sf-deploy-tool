public class CalendarEditOverride {

    public Xero_Calendar__c   thisCal              {get; set;}
    public List<selectOption> selectedDetailFields {get; set;}
    public List<selectOption> objects{
        get {
            Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();     
            List<SelectOption> options = new List<SelectOption>();
            
            List<string> sortedObjectList = new List<string>(gd.keySet());
            sortedObjectList.sort();
            
            for(string objectName : sortedObjectList)
            {
               DescribeSObjectResult objectDescribe = gd.get(objectName).getDescribe(); 
               if(objectDescribe.isAccessible() && objectDescribe.isQueryable())
               {
                   options.add(new SelectOption(objectDescribe.getName(),objectDescribe.getLabel()));
               }
            }
            return options;        
        }
        set{}
    }
    public List<selectOption> objectFields{
        get {
            return getObjectFields(null); 
        }
        set{}
    }
    
    
    public List<selectOption> dateFields { 
        get {
            set<string> dateFields = new set<string>();
            dateFields.add('datetime');
            dateFields.add('date');
            return getObjectFields(dateFields);     
        }
        set {}
    }
    
    //Standard edit override methods

    public CalendarEditOverride(ApexPages.StandardController controller) {
        thisCal = (Xero_Calendar__c)controller.getRecord();
        selectedDetailFields = new list<selectOption>();
        
        if(thisCal.Id !=null)
        {
            string queryString = buildSelectAllStatment(thisCal.getSObjectType().getDescribe().getName());
            id calId = thisCal.id;
            queryString += ' Where id = :calId';
            thisCal = database.query(queryString);
            
            string[] currentSelectedDetailFields = thisCal.Detail_Data_Fields__c != null ? thisCal.Detail_Data_Fields__c.split(',') : new list<string>(); 
            for(string thisSelectOption : currentSelectedDetailFields)
            {
                selectedDetailFields.add(new SelectOption(thisSelectOption, thisSelectOption));
            } 
        }
        else
        {
            thisCal.Object_Name__c = 'Contact';
        }
        
         
    }

    /**
    * @Description overrides save method for use in controller extension. Saves calendar using information on the visualforce override page.
    * @Return pagereference reference to the calendar record in view mode
    **/
    public pagereference saveCalendar(){
     
        try
        {
            string currentSelectedDetailFields = '';
            for ( SelectOption so : selectedDetailFields ) {
                 currentSelectedDetailFields += so.getValue() +',';
            }
            currentSelectedDetailFields = currentSelectedDetailFields.substring(0,currentSelectedDetailFields.length()-1);
            thisCal.Detail_Data_Fields__c = currentSelectedDetailFields;
                
            database.upsertResult sr= database.upsert(thisCal);
            if(!sr.errors.isEmpty())
            {
                for(database.error err : sr.errors)
                {
                    //if the error is field level, attach it at the field level
                    if(!err.fields.isEmpty())
                    {
                        for(string fieldName : err.fields)
                        {
                            //thisCase.subject.addError(err.message);
                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error during save:' + err.message));
                        }
                    }
                    //otherwise just plaster it on the page
                    else
                    {
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error during save:' + err.message));
                    }
                }
            }
            else
            {
                PageReference pageRef = new PageReference('/'+thisCal.id);
                pageRef.setRedirect(true);
                return pageRef;            
            }
        }
        catch(exception ex)
        {
            system.debug('\n\n\n\n-------------------- ERROR OCCURED DURING SAVE!');
            system.debug(ex);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error during save:' + ex.getMessage() + ' on line ' +ex.getLineNumber()));
        }
        return null;          
    }
    

    /**
    * @Description Gets all object fields and returns them in a select list. Used for edit/create page overrides when creating a calendar, rule or template.
    * @Return list of select options for each field on the object.
    **/
    public List<selectOption> getObjectFields(set<string> typeFilters) 
    {       
        List<SelectOption> options = new List<SelectOption>();

        Map<String, Schema.SobjectField> objectDef = Schema.getGlobalDescribe().get(thisCal.Object_Name__c).getDescribe().getSObjectType().getDescribe().fields.getMap();           
        list<string> sortedFieldsList = new list<string>(objectDef.keySet());
        sortedFieldsList.sort();
        for(String fieldId : sortedFieldsList)
        {
            DescribeFieldResult fieldDescribe = objectDef.get(fieldId).getDescribe();
                        
            if( typeFilters == null || typeFilters.isEmpty() || typeFilters.contains(fieldDescribe.getType().Name().toLowerCase()))
            {
                options.add(new SelectOption(fieldDescribe.getName(),fieldDescribe.getLabel() + ' (' + fieldDescribe.getType().Name()+')'));
            }
        }

        return options;
    }
    

    /**
    * @Description given an sObject type gets all fields and their type for that object. Used to create the event pop ups with the object data.
    * @Return a map of field names to their input/display type.
    **/    

    public map<string,string> getObjectFieldMap()
    {
        map<string,string> fieldMap = new map<string,string>();
        Map<String, Schema.SobjectField> objectDef = Schema.getGlobalDescribe().get(thisCal.Object_Name__c).getDescribe().getSObjectType().getDescribe().fields.getMap();
        for(String fieldId : objectDef.keySet())
        {
            fieldMap.put(fieldId.toLowerCase(),objectDef.get(fieldId).getDescribe().getLabel());
        }
        return fieldMap;
    }

    /**
    * builds an SOQL query string that will select all fields for a given object type.
    * @param objectType a string that is the API name of the object to build the select all string for
    * @return string that can be used in a dynamic SOQL query to select all fields
    */
    public static string buildSelectAllStatment(string objectType)
    {
        Map<String, Schema.SObjectField> fldObjMap = Schema.getGlobalDescribe().get(objectType).getDescribe().fields.getMap();
        List<Schema.SObjectField> fldObjMapValues = fldObjMap.values();
        String theQuery = 'SELECT ';
        for(Schema.SObjectField s : fldObjMapValues)
        {  
           theQuery += s.getDescribe().getName() + ',';
        }
        
        // Trim last comma
        theQuery = theQuery.subString(0, theQuery.length() - 1);
        
        // Finalize query string
        theQuery += ' FROM '+objectType;    
        
        return theQuery;
    }
    
    public class calException extends Exception {}                


}