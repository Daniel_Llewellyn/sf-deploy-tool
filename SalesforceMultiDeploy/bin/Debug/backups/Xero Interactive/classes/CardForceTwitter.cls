public class CardForceTwitter 
{


    public  String playerId {get; set;}
    private String token;
    private String tokenSecret;
    private String verifier;

    private static final String REQUEST_TOKEN_URL = 'https://api.twitter.com/oauth/request_token';
    private static final String AUTHORIZATION_URL = 'https://api.twitter.com/oauth/authorize';    
    private static final String ACCESS_TOKEN_URL = 'https://api.twitter.com/oauth/access_token';
    private static final String siteUrl = 'http://xerointeractive-developer-edition.na9.force.com/partyForce';
    
    
    private Map<String,String> parameters = new Map<String,String>();
    
    public static string twitterSearchUrl = 'http://search.twitter.com/search.json';
    
	//simple wrapper object to make evaluating the result of a remoting request easier
    public class remoteObject
    {
        public boolean success = true;
        public string message = 'Operation ran successfully';
        public string data = null;
        public list<sObject> sObjects = new list<sObject>(); 
    }    
    public String message { get; set; }

    /**
    * @date 07/06/2011
    * @description login method that automatically gets called everytime the page loads. Handles the oauth process
    */       
    public PageReference login()
    {
        this.token = ApexPages.currentPage().getParameters().get('oauth_token');
        this.verifier = ApexPages.currentPage().getParameters().get('oauth_verifier');            
        this.playerId = ApexPages.currentPage().getParameters().get('id');
        
        if(this.token != null && this.verifier != null)
        {
            completeAuthorization(token,verifier);
        }
        else if(playerId != null)
        {
            String authURL = newAuthorization();
            System.debug('Authorization URL: '+ authURL);
            return new PageReference(authURL);
        }       
        return null;
    }

    /**
    * @date 07/06/2011
    * @description New Authorization for the first step in the oauth prcess
    * @returns String for the redirect url for the authorization page
    */   
    public String newAuthorization()
    {
        list<CardForce_Event__c> event = [select Twitter_Consumer_Key__c, Twitter_Consumer_Secret_Key__c  from CardForce_Event__c where active__c = true];
        Oauth oa = new Oauth();
        oa.REQUEST_TOKEN_URL = REQUEST_TOKEN_URL;
        oa.AUTHORIZATION_URL = AUTHORIZATION_URL;    
        oa.ACCESS_TOKEN_URL = ACCESS_TOKEN_URL;
        oa.PAGEURL = Page.TwitterOauth.getUrl();
        oa.consumerKey = event[0].Twitter_Consumer_Key__c;
        oa.consumerSecret = event[0].Twitter_Consumer_Secret_Key__c;
		oa.callbackUrl = EncodingUtil.urlEncode(siteUrl+'/'+'CardForce_TwitterOAuth?id=' + playerId,'UTF-8');
                
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        req.setEndpoint(REQUEST_TOKEN_URL);
        System.debug('Request body set to: '+ req.getBody());

        oa.sign(req);
        HttpResponse res = null;
        if(Test.isRunningTest()) {
            res = new HttpResponse();
        } else {
            res = h.send(req);
        }
        System.debug('Response from request token request: ('+res.getStatusCode()+')'+res.getBody());
        if(res.getStatusCode()>299) 
        {
            message = 'Failed getting a request token. HTTP Code = '+res.getStatusCode()+'. Message: '+res.getStatus()+'. Response Body: '+res.getBody();
            return null;
        }
        String resParams = Test.isRunningTest() ? 'oauth_token=token&oauth_token_secret=token_secret' : res.getBody();
        Map<String,String> rp = oa.getUrlParams(resParams);
        
        CardForce_Player__c thisPlayer = new CardForce_Player__c(id=playerId);
        thisPlayer.Code__c = rp.get('oauth_token');
        thisPlayer.Twitter_Access_Token__c = rp.get('oauth_token_secret');
        
        update thisPlayer;
        
        System.debug('Got request token: '+thisPlayer.Code__c+'('+rp.get('oauth_token')+')');
        
        if(AUTHORIZATION_URL.contains('?'))
        {
            return AUTHORIZATION_URL+'&oauth_token='+EncodingUtil.urlDecode(thisPlayer.Code__c,'UTF-8')+'&oauth_consumer_key='+event[0].Twitter_Consumer_Key__c;
        } 
        else
        {
            return AUTHORIZATION_URL+'?oauth_token='+EncodingUtil.urlDecode(thisPlayer.Code__c,'UTF-8')+'&oauth_consumer_key='+event[0].Twitter_Consumer_Key__c;
        }
    }


    /**
    * @date 07/06/2011
    * @description Complete Authorization for the third step in the oauth prcess
    * @returns Boolean response successful
    */   
    public boolean completeAuthorization(String token, String verifier) {
        System.debug('Completing authorization for request token ' + token + ' with verifier '+ verifier);
        list<CardForce_Event__c> event = [select Twitter_Consumer_Key__c, Twitter_Consumer_Secret_Key__c  from CardForce_Event__c where active__c = true];
        list<CardForce_Player__c> player = [select Code__c, Twitter_Access_Token__c from CardForce_Player__c where id = :this.playerId];
        Oauth oa = new Oauth();
        oa.REQUEST_TOKEN_URL = REQUEST_TOKEN_URL;
        oa.AUTHORIZATION_URL = AUTHORIZATION_URL;    
        oa.ACCESS_TOKEN_URL = ACCESS_TOKEN_URL;
        oa.PAGEURL = Page.TwitterOauth.getUrl();
        oa.consumerKey = event[0].Twitter_Consumer_Key__c;
        oa.consumerSecret = event[0].Twitter_Consumer_Secret_Key__c;
        oa.token = player[0].Code__c;
        oa.tokenSecret = player[0].Twitter_Access_Token__c;
       

        if(verifier!=null) {
            this.verifier = EncodingUtil.urlEncode(verifier,'UTF-8');
        }
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setEndpoint(ACCESS_TOKEN_URL);
        req.setBody('');
        oa.sign(req);
        HttpResponse res = null;
        if(Test.isRunningTest()) {
            res = new HttpResponse();
        } else {
            res = h.send(req);
            System.debug('Response from request token request: ('+res.getStatusCode()+')'+res.getBody());
        }
        if(res.getStatusCode()>299) {
            message = 'Failed getting an access token. HTTP Code = '+res.getStatusCode()+'. Message: '+res.getStatus()+'. Response Body: '+res.getBody();
            return false;
        }
    
        String resParams = Test.isRunningTest() ? 
            'oauth_token=token&oauth_token_secret=token_secret' : res.getBody();
    
        Map<String,String> rp = new Map<String,String>();
        for(String s : resParams.split('&')) {
            List<String> kv = s.split('=');
            rp.put(kv[0],kv[1]);
            System.debug('Access token response param: '+kv[0]+'='+kv[1]);
        }
        
        
        player[0].Twitter_Access_Token__c = rp.get('oauth_token');
        player[0].Code__c = rp.get('oauth_token_secret');
        
        update player[0];
        
        return true;
    }
    
    public static string getRecentTweets(Id playerId)
    {
    	list<CardForce_Event__c> event = [select Twitter_Consumer_Key__c, Twitter_Consumer_Secret_Key__c  from CardForce_Event__c where active__c = true];
    	list<CardForce_Player__c> player = [select Code__c, Twitter_Access_Token__c from CardForce_Player__c where id = :playerId];

        Oauth oa = new Oauth();    
        oa.consumerKey = event[0].Twitter_Consumer_Key__c;
        oa.token = player[0].Twitter_Access_Token__c;
        
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        string endPoint = 'https://api.twitter.com/1/statuses/user_timeline.json?include_entities=true&include_rts=true&screen_name=Kenji776&count=2 ';	
        req.setEndpoint(endPoint);
    	oa.sign(req);

    	HttpResponse res = null;
    	res = h.send(req);
    		
    	return res.getBody();    	
   	
    }
    public static string Tweet(Id playerId, string message)
    {
    	list<CardForce_Event__c> event = [select Twitter_Consumer_Key__c, Twitter_Consumer_Secret_Key__c  from CardForce_Event__c where active__c = true];
    	list<CardForce_Player__c> player = [select Code__c, Twitter_Access_Token__c from CardForce_Player__c where id = :playerId];

        Oauth oa = new Oauth();    
        oa.consumerKey = event[0].Twitter_Consumer_Key__c;
        oa.token = player[0].Twitter_Access_Token__c;
        
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        string endPoint = 'https://api.twitter.com/1/statuses/update.json?status=Test';	
        req.setEndpoint(endPoint);
    	oa.sign(req);
    	
    	HttpResponse res = null;
    	res = h.send(req);
    		
    	return res.getBody();
    }

	public static remoteObject getTweetsByTag(string tag)
	{
		remoteObject returnObj = new remoteObject();
        try
        {
            string reqURL =twitterSearchUrl+'?q=%23'+tag;
            Http httpObj = new Http();                       
            HttpRequest req = new HttpRequest();
            req.setEndpoint(reqURL);
            req.setMethod('GET');
  
            if(!Test.isRunningTest())
            { 
                HttpResponse res = httpObj.send(req);    
				returnObj.data = res.getBody();
            }
            
        }
        catch(exception e)
        {
            returnObj.success = false;
            returnObj.message = 'Error getting twitter post list';
            returnObj.data = e.getMessage() + ' ' + e.getCause() + ' ' + e.GetLineNumber();          
        }		
        return returnObj;
	}    
}