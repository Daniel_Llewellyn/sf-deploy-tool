global class CaseMergeTest
{
    @isTest
    global static void runTestCaseMerge()
    {
            Account testAccount = new Account(name='My Test Account');
            insert TestAccount;    
    
            Contact testContact = new Contact(Firstname='Frank', 
                                                Lastname='Jones', 
                                                AccountID=Testaccount.id, 
                                                email = 'none@none.com');
            insert testContact;   

            Case testCase1 = new case(contactid = testContact.id,  status = 'Open',  Origin = 'Web',  subject='Test case 1', suppliedEmail = 'none@none.com');       
            insert testCase1;            

            Case testCase2 = new case(contactid = testContact.id,  status = 'Open',  Origin = 'Web',  subject='Test case 2', suppliedEmail = 'none@none.com');       
            insert testCase2;    

            Case testCase3 = new case(contactid = testContact.id,  status = 'Open',  Origin = 'Web',  subject='Test case 3', suppliedEmail = 'none@none.com');       
            insert testCase3;    

            Case testCase4 = new case(contactid = testContact.id,  status = 'Open',  Origin = 'Web',  subject='Test case 4', suppliedEmail = 'none@none.com');       
            insert testCase4; 
            
            list<caseComment> comments = new list<caseComment>();
            CaseComment comment1 = new CaseComment();
            comment1.CommentBody = 'test 1';
            comment1.ParentId = testcase1.id;
            comments.add(comment1);

            CaseComment comment2 = new CaseComment();
            comment2.CommentBody = 'test 2';
            comment2.ParentId = testcase2.id;
            comments.add(comment2);    

            CaseComment comment3 = new CaseComment();
            comment3.CommentBody = 'test 3';
            comment3.ParentId = testcase3.id;
            comments.add(comment3);                

            CaseComment comment4 = new CaseComment();
            comment4.CommentBody = 'test 4';
            comment4.ParentId = testcase4.id;
            comments.add(comment4);                
                   
            insert comments;
            
            list<task> tasks = new  list<task>();
            
            task task1 = new task();
            task1.WhatId = testcase1.id;
            task1.Type = 'email';
            task1.whoId = testContact.id;
            task1.Subject = 'Test';
            tasks.add(task1);
            
           task task2 = new task();
            task2.WhatId = testcase2.id;
            task2.Type = 'email';
            task2.whoId = testContact.id;
            task2.Subject = 'Test';
            tasks.add(task2);
            
           task task3 = new task();
            task3.WhatId = testcase3.id;
            task3.Type = 'email';
            task3.whoId = testContact.id;
            task3.Subject = 'Test';
            tasks.add(task3);
            
           task task4 = new task();
            task4.WhatId = testcase4.id;
            task4.Type = 'email';
            task4.whoId = testContact.id;
            task4.Subject = 'Test';                                    
            tasks.add(task4);
            
            insert tasks;
            
            caseMerge caseMerger = new caseMerge(new ApexPages.StandardController(testCase1));
            caseMerger.findMergableCount();
            
            //make sure we get 3 cases back that are found as potential duplicates of this first case (4 cases total, -1 excluding itself)
            system.assertEquals(caseMerger.count,3);

            PageReference indexPage= Page.caseMergeSelect;
            
            //call the list finder without giving it an id. It should return an empty list
            system.test.setCurrentPageReference(indexPage);
            caseMerger.findMergableList();      
            system.assertEquals(caseMerger.mergableCases.size(),0);         
            
            //now give it an id of a case to use. It should return all 4 cases
            indexPage.getParameters().put('id',testcase1.id);   
            system.test.setCurrentPageReference(indexPage);
            caseMerger.findMergableList();                  
            
            //the size of the list returned should have 4 cases in it.
            system.assertEquals(caseMerger.mergableCases.size(),4);          
            
            list<id> mergeCases = new list<id>{testcase2.id,testcase3.id,testcase4.id};
            list<case> mergedCases = caseMerge.mergeCases(testcase1.id,mergeCases,true,true); 
            
            //make sure all the cases got closed and got their parent field set.
            for(case c : mergedCases)
            {
                system.assertEquals(c.status,'Closed');
                system.assertEquals(c.master_case__c,testCase1.id);
            }
            
            //after the merge case1 should have 4 comments and 4 tasks.
            list<task> case1tasks = [select id from task where whatId = :testcase1.id];
            system.assertEquals(case1tasks.size(),4);
            
            list<caseComment> case1comments = [select id from casecomment where parentid = :testcase1.id];
            system.assertEquals(case1comments.size(),4);             
    }
}