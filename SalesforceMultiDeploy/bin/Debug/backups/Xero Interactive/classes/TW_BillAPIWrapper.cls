/***********************************************
* @Name TW_BillApiWrapper
* @Description API Wrapper for the Bill.com API. Allows for integration with Bill.com via their API. Not all methods implimented. Most methods are remoted static to allow for 
               invocation via javascript remoting.
* @Author Redkite (Daniel Llewelyn/ dllewellyn@redkitetechnologies.com)
* @Date ~6/17/2014
************************************************/

global class TW_BillAPIWrapper
{
    public static string loginApiEndpoint = 'https://api.bill.com/api/v2/';
    
    /**
    * @Description logs into the bill.com API using the given credentials. Returns a session ID and an endpoint to use in any further calls. The login HTTP call is different from 
    *              the others in that the data for the request is included as a URL encoded post body, not a JSON object, so the sendRequest method has some special login for handling that.
    * @Param email the email/username to use to login
    * @Param password the password to use to login
    * @Param orgId the bill.com organization to log into. 
    * @devKey the application/developer key given by bill.com for this application.
    * @Return a map of key and objects constructed from the response data object returned by bill.com. See documentation for included keys/values. 
    **/
    @remoteAction
    global static Map<String, Object> login(string email, string password, string orgId, string devKey)
    {

        apiRequestWrapper requestData = new apiRequestWrapper();
        requestData.devKey = devKey;
        requestData.fieldValues.put('userName',email);
        requestData.fieldValues.put('password',password);
        requestData.fieldValues.put('orgId',orgId);
        requestdata.apiMethod = 'Login.json';
        requestData.endPoint = loginApiEndpoint;
        
        return TW_BillAPIWrapper.sendRequest(requestData);  
    }

    /**
    * @Description gets a list of valid organization ids and names for the the given email account
    * @Param email the email/username to use to check organization ids for
    * @Param password the password for the given email
    * @devKey the application/developer key given by bill.com for this application.
    * @Return a map of key and objects constructed from the response data object returned by bill.com. See documentation for included keys/values. 
    **/
    @remoteAction
    global static Map<String, Object> listOrgs(string email, string password, string devKey)
    {

        apiRequestWrapper requestData = new apiRequestWrapper();
        requestData.devKey = devKey;
        requestData.fieldValues.put('userName',email);
        requestData.fieldValues.put('password',password);        
        requestdata.apiMethod = 'ListOrgs.json';
        requestData.endPoint = loginApiEndpoint;
        
        return TW_BillAPIWrapper.sendRequest(requestData);  
    }

    /**
    * @Description: lists any kind of supported entity in the bill.com system. The JSON passed in must contain a start and max param. It may optionally include filters and sorting as outlined in 
    *              http://developer.bill.com/api-documentation/api/list. Will return JSON encoded list of matching entities.
    * @param entity the type of thing to list. See documentation linked above for full list of supported entities
    * @param listJson a json string which contains the request to send to bill.com. Must contain start and max param. May contain filters and sort params.
    * @sessionId the session Id returned by the login call that must have been called previously
    * @endPoint the specific API endpoint to connect to. Returned by the login call.
    * @devKey the application/developer key given by bill.com for this application.
    * @return a map of key and objects constructed from the response data object returned by bill.com. See documentation for included keys/values. 
    **/        
    @remoteAction
    global static Map<String, Object> listEntity(string entity, string listJson, string sessionId, string endPoint, string devKey)
    {

        apiRequestWrapper requestData = new apiRequestWrapper();
        requestData.jsonRequest = listJson;
        requestData.apiMethod = '/List/'+entity+'.json';
        requestData.devKey = devKey;
        requestData.sessionId = sessionId;
        requestData.endPoint = endPoint;
        
        return TW_BillAPIWrapper.sendRequest(requestData);  
    }
    
    /**
    * @Description: Creates a bill in the bill.com API. Takes a json object that reprsents a bill and its line items as outlined at http://developer.bill.com/api-documentation/api/bill
    *               Any parameter designated as being required on that page for a create call is required to be in the billJson object. As of this
    *               Writting that includes: vendorId, invoiceNumber, invoiceDate, dueDate.
    *               Additionally the bill must contain line items (billJson.billLineItems), which are represented as a list of of map of string to string. 
    *               The required params for this are also outlined on the same page, and as of this writting the only required param is amount.
    * @Param billJSon the JSON string to send to the create bill method.
    * @sessionId the session Id returned by the login call that must have been called previously
    * @endPoint the specific API endpoint to connect to. Returned by the login call.
    * @devKey the application/developer key given by bill.com for this application.
    **/
    @remoteAction
    global static Map<String, Object> createBill(string billJson, string sessionId, string endPoint, string devKey)
    {
        apiRequestWrapper requestData = new apiRequestWrapper();
        requestData.devKey = devKey;
        requestData.sessionId = sessionId;
        requestdata.apiMethod = '/Crud/Create/Bill.json';
        requestData.jsonRequest = billJson;
        requestData.endPoint = endPoint;   
 
        return TW_BillAPIWrapper.sendRequest(requestData);  
    }
        
    private static Map<String, Object> sendRequest(apiRequestWrapper requestData)
    {
        system.debug('\n\n\n ---------------- Request Data');
        system.debug(requestData);
        
        Http http = new Http();
        HttpRequest req = new HttpRequest();

        req.setEndpoint(requestData.endPoint + '' + requestData.apiMethod);    

        req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        req.setMethod(requestData.requestMethod);
        
        req.setEndpoint(requestData.endPoint + '' + requestData.apiMethod + '?sessionId='+requestData.sessionId+'&devKey='+requestData.devKey); 
        
        system.debug('\n\n\n ----------- ENDPOINT: ' + req.getEndpoint());
               
        //if there are field values that means this is a request where the data should be included as a post body not a JSON content body. So 
        //we create the proper kind of data to include in the request.
        if(!requestData.fieldValues.isEmpty())
        {
            req.setBody(createPostBody(requestData.fieldValues));
        }
        else
        {
            req.setBody('data='+requestData.jsonRequest);
        }
        HttpResponse res = http.send(req);
         
        return parseResponse(res); 
    }
    
    /**
    * @Description if we have to make a post request to the bill.com server (such as with a login call) this method will convert the field values in a apiRequestWrapper
    *              to a valid post body to include in the request.
    * @Param requestParams a set of key value pairs to include in the post body
    * @Return string a valid post body to include in the HTTP request with the included requestParams
    **/
    private static string createPostBody(map<string,string> requestParams)
    {
        string dataString = '';
        for(string keyName : requestParams.keySet())
        {
            if(requestParams.get(keyName) != null)
            {
                dataString += EncodingUtil.urlEncode(keyName,'UTF-8') + '='+  EncodingUtil.urlEncode(requestParams.get(keyName),'UTF-8') + '&';
            }
        }
        dataString = dataString.substring(0,dataString.length()-1);
        return dataString;
    }
    
    /**
    * @Description converts raw JSON response into an object. Uses untyped deserialize to handle different response types
    * @Param res an HTTP response. Should contain valid JSON.
    * @Return a key value mapping of the JSON object.
    **/
    private static Map<String, Object> parseResponse(HttpResponse res)
    {
        return (Map<String, Object>) JSON.deserializeUntyped(res.getBody());
    }
    
    /**
    * Description: Object type to be passed to the sendRequest method. Contains all the data 
    *              required to create a request and send it to Bill.com API. Not all fields are required
    *              for every method/request type
    **/
    private class apiRequestWrapper
    {
        public map<string,string> fieldValues = new map<string,string>();
        public string requestMethod = 'POST';
        public string jsonRequest;
        public string apiMethod;
        public string sessionId;
        public string endPoint;
        public string devKey;
    }

    
    public class customException extends Exception {}
}