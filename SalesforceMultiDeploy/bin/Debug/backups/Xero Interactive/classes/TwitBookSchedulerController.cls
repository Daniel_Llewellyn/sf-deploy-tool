/**
* @author Joey Q. Chan (joeyqchan@gmail.com)
* @date 07/06/2011
* @description Scheduler script for Twitter
*/
public class TwitBookSchedulerController {
    public PageReference activateFacebook(){

        return null;
    }
    public PageReference activateTwitter(){
        String sch = '0 0 * * * ?';
        try{
            String jobId = System.schedule('TwitterStatusScheduler', sch, new TwitterStatusScheduler());
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Twitter script will now run every hour.'));
        }catch(Exception e){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,e.getMessage()));
        }
        return null;
    }
    public PageReference runFacebookNow(){

        return null;
    }
    public PageReference runTwitterNow(){
        TwitterStatusBatch tBatch = new TwitterStatusBatch('Select Id, Name, User__c, Hashtag_to_follow__c, Consumer_Key__c, Consumer_Secret__c, AccessToken__c, Code__c, Get_All_Tweets__c From TwitterUser__c Where Active__c = true  AND AccessToken__c != null AND Code__c != null');
        ID batchprocessid = Database.executeBatch(tBatch,1);
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Twitter Batch script started. It may take a few seconds for the chatter post to appear in the chatter profiles.'));
        return null;
    }
}