/**
* @author Joey Q. Chan (joeyqchan@gmail.com)
* @date 07/06/2011
* @description TwitBookSChedulerControllerTest Test Method
*/

@isTest
private class TwitBookSchedulerControllerTest {
    static testMethod void testController(){
    	Test.startTest();
    	TwitBookSchedulerController controller = new TwitBookSchedulerController();
        controller.runFacebookNow();
    	controller.runTwitterNow();    	
    	controller.activateFacebook();
    	controller.activateFacebook(); //Invoke the error of not allowing the user to schedule twice
        controller.activateTwitter();
    	controller.activateTwitter(); //Invoke the error of not allowing the user to schedule twice
        Test.stopTest();        
    }
}