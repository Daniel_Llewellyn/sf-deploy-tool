/**
* @author Joey Q. Chan (joeyqchan@gmail.com)
* @date 07/06/2011
* @description Twitter Oauth v1.0 implementation
*/
public class TwitterOauthController {


    public  String recordId {get; set;}
    private String token;
    private String tokenSecret;
    private String verifier;

    private static final String REQUEST_TOKEN_URL = 'https://api.twitter.com/oauth/request_token';
    private static final String AUTHORIZATION_URL = 'https://api.twitter.com/oauth/authorize';    
    private static final String ACCESS_TOKEN_URL = 'https://api.twitter.com/oauth/access_token';
    
    
    private Map<String,String> parameters = new Map<String,String>();
    
    public String message { get; set; }

    /**
    * @date 07/06/2011
    * @description login method that automatically gets called everytime the page loads. Handles the oauth process
    */       
    public PageReference login(){
        this.token = ApexPages.currentPage().getParameters().get('oauth_token');
        this.verifier = ApexPages.currentPage().getParameters().get('oauth_verifier');            
        this.recordId = ApexPages.currentPage().getParameters().get('id');
        if(this.token != null && this.verifier != null){
            completeAuthorization(token,verifier);
        }
        else if(recordId != null){
            String authURL = newAuthorization();
            System.debug('Authorization URL: '+ authURL);
            return new PageReference(authURL);
        }       
        return null;
    }

    /**
    * @date 07/06/2011
    * @description New Authorization for the first step in the oauth prcess
    * @returns String for the redirect url for the authorization page
    */   
    public String newAuthorization() {
        TwitterUser__c tuser = [select consumer_key__c, consumer_secret__c  from TwitterUser__c where id = :recordId];
        Oauth oa = new Oauth();
        oa.REQUEST_TOKEN_URL = REQUEST_TOKEN_URL;
        oa.AUTHORIZATION_URL = AUTHORIZATION_URL;    
        oa.ACCESS_TOKEN_URL = ACCESS_TOKEN_URL;
        oa.PAGEURL = Page.TwitterOauth.getUrl();
        oa.consumerKey = tuser.Consumer_Key__c;
        oa.consumerSecret = tuser.Consumer_Secret__c;
		//oa.callbackUrl = 'http://xerointeractive-developer-edition.na9.force.com/partyForce/TwitterOauth?id=' + recordId;

        if(oa.callbackUrl == null) { 
            oa.callbackUrl = EncodingUtil.urlEncode('http://xerointeractive-developer-edition.na9.force.com/partyForce/TwitterOauth?id=' + recordId,'UTF-8');
        }
                
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        req.setEndpoint(REQUEST_TOKEN_URL);
        System.debug('Request body set to: '+ req.getBody());

        oa.sign(req);
        HttpResponse res = null;
        if(Test.isRunningTest()) {
            res = new HttpResponse();
        } else {
            res = h.send(req);
        }
        System.debug('Response from request token request: ('+res.getStatusCode()+')'+res.getBody());
        if(res.getStatusCode()>299) {
            message = 'Failed getting a request token. HTTP Code = '+res.getStatusCode()+
                      '. Message: '+res.getStatus()+'. Response Body: '+res.getBody();
            return null;
        }
        String resParams = Test.isRunningTest() ? 'oauth_token=token&oauth_token_secret=token_secret' : res.getBody();
        Map<String,String> rp = oa.getUrlParams(resParams);
        tuser.Code__c = rp.get('oauth_token');
        tuser.AccessToken__c = rp.get('oauth_token_secret');
        
        update tuser;
        
        System.debug('Got request token: '+tuser.Code__c+'('+rp.get('oauth_token')+')');
        
        if(AUTHORIZATION_URL.contains('?')) {
            return AUTHORIZATION_URL+'&oauth_token='+EncodingUtil.urlDecode(tuser.Code__c,'UTF-8')+'&oauth_consumer_key='+tuser.consumer_key__c;
        } else {
            return AUTHORIZATION_URL+'?oauth_token='+EncodingUtil.urlDecode(tuser.Code__c,'UTF-8')+'&oauth_consumer_key='+tuser.consumer_key__c;
        }
    }


    /**
    * @date 07/06/2011
    * @description Complete Authorization for the third step in the oauth prcess
    * @returns Boolean response successful
    */   
    public boolean completeAuthorization(String token, String verifier) {
        System.debug('Completing authorization for request token ' + token + ' with verifier '+ verifier);
        TwitterUser__c tuser = [select Consumer_Key__c, Consumer_Secret__c, Code__c, AccessToken__c from TwitterUser__c where id = :this.recordId];
        Oauth oa = new Oauth();
        oa.REQUEST_TOKEN_URL = REQUEST_TOKEN_URL;
        oa.AUTHORIZATION_URL = AUTHORIZATION_URL;    
        oa.ACCESS_TOKEN_URL = ACCESS_TOKEN_URL;
        oa.PAGEURL = Page.TwitterOauth.getUrl();
        oa.consumerKey = tuser.Consumer_Key__c;
        oa.consumerSecret = tuser.Consumer_Secret__c;
        oa.token = tuser.Code__c;
        oa.tokenSecret = tuser.AccessToken__c;
       

        if(verifier!=null) {
            this.verifier = EncodingUtil.urlEncode(verifier,'UTF-8');
        }
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setEndpoint(ACCESS_TOKEN_URL);
        req.setBody('');
        oa.sign(req);
        HttpResponse res = null;
        if(Test.isRunningTest()) {
            res = new HttpResponse();
        } else {
            res = h.send(req);
            System.debug('Response from request token request: ('+res.getStatusCode()+')'+res.getBody());
        }
        if(res.getStatusCode()>299) {
            message = 'Failed getting an access token. HTTP Code = '+res.getStatusCode()+'. Message: '+res.getStatus()+'. Response Body: '+res.getBody();
            return false;
        }
    
        String resParams = Test.isRunningTest() ? 
            'oauth_token=token&oauth_token_secret=token_secret' : res.getBody();
    
        Map<String,String> rp = new Map<String,String>();
        for(String s : resParams.split('&')) {
            List<String> kv = s.split('=');
            rp.put(kv[0],kv[1]);
            System.debug('Access token response param: '+kv[0]+'='+kv[1]);
        }
        
        
        tuser.AccessToken__c = rp.get('oauth_token');
        tuser.Code__c = rp.get('oauth_token_secret');
        
        update tuser;
        
        return true;
    }
    
    public string Tweet(Id twitterUser, string message)
    {
    	TwitterUser__c tuser = [select Consumer_Key__c, Consumer_Secret__c, Code__c, AccessToken__c from TwitterUser__c where id = :twitterUser];

        Oauth oa = new Oauth();    
        oa.consumerKey = tuser.Consumer_Key__c;
        oa.token = tuser.AccessToken__c;
        
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        string endPoint = 'https://api.twitter.com/1/statuses/update.json?status=Test&trim_user=true&include_entities=true';	
        req.setEndpoint(endPoint);
        req.setBody('');    	
    	
    	oa.sign(req);
    	
    	HttpResponse res = null;
    	res = h.send(req);
    	
    	
    	return res.getBody();
    }
}