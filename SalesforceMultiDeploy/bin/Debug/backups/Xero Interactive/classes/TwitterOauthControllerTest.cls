/**
* @author Joey Q. Chan (joeyqchan@gmail.com)
* @date 07/06/2011
* @description Test Method for TwitterOauthController
*/

@isTest
private class TwitterOauthControllerTest {

    static testMethod void myUnitTest() {
        TwitterUser__c tuser = new TwitterUser__c(Active__c = true,
                                                  Consumer_Key__c = 'VCino9IWidtvbnof62WA1',
                                                  Consumer_Secret__c = 'ORplEKdQk47OoDi0IRWTl2torsWMB5D78uxeJyoIrfk1',
                                                  AccessToken__c = '106595753-JjSz02mBenHwseXtdkGQH4UWIbHZKqr73cqKcMjF1',
                                                  Code__c = 'LUHfAAe4LRIzdwa784SjPue08afhY24NDq6UDJyas1',
                                                  Hashtag_to_follow__c = '#ch',
                                                  User__c = UserInfo.getUserId());    
        insert tuser;    
        
        ApexPages.currentPage().getParameters().put('id', tuser.Id);
        TwitterOauthController controller = new TwitterOauthController();
        
        controller.login();
        
        ApexPages.currentPage().getParameters().put('oauth_token', 'token');
        ApexPages.currentPage().getParameters().put('oauth_verifier', 'verifier');
        controller.login();

        ApexPages.currentPage().getParameters().put('oauth_token', 'AccessToken__c');
        ApexPages.currentPage().getParameters().put('oauth_token_secret', 'oauth_key');
        controller.login();    
    }
}