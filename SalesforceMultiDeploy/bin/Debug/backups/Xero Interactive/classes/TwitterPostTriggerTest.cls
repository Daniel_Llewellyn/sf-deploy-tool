/**
* @author Joey Q. Chan (joeyqchan@gmail.com)
* @date 07/06/2011
* @description Test Method for TwitterPostTrigger
*/

@isTest
private class TwitterPostTriggerTest {
    static testMethod void TwitterPostTest() {
        //Test for bulk
        TwitterUser__c tUser = new TwitterUser__c(Active__c = true,
                                                     Consumer_Key__c = '122642784451634',
                                                     Consumer_Secret__c = 'a4447263a33f02818573bcea076b9f4b',
                                                     Hashtag_to_follow__c = '#ch',
                                                     User__c = UserInfo.getUserId());     
        
        insert tUser;
        List<Twitter_Post__c> postList = new List<Twitter_Post__c>();
        for(Integer i = 0 ; i < 200 ; i++){
            postList.add(new Twitter_Post__c(ExternalId__c = tUser.Id + '-' + i,
                                              Twitter_User__c = tUser.Id,
                                              Body__c = 'TestPost',
                                              User__c = UserInfo.getUserId()));
           
        }
        Test.startTest();
        insert postList;

        List<UserFeed> userFeeds =  [SELECT Id FROM UserFeed Where ParentId = :UserInfo.getUserId() AND CreatedDate = TODAY Limit 200];
        System.assert(userFeeds.size() >= 200, 'There should be at least 200 posts');

        Test.stopTest();
        
    }       
}