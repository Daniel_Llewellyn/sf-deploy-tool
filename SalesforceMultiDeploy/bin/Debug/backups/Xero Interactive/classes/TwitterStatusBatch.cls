/**
* @author Joey Q. Chan (joeyqchan@gmail.com)
* @date 07/06/2011
* @description Batch script that fetches the last 25 posts from twitter and creates the necessary Twitter Status that then creates the FeedPost
*/
global class TwitterStatusBatch implements Database.Batchable<sobject>,Database.AllowsCallouts
{
    global final String Query;
    global final String Entity;
    global final String Field;
    global final String Value;
    
    
    global TwitterStatusBatch(String q){
        Query=q;
    } 
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        
        
        for(Sobject s : scope){
            TwitterUser__c tUser = (TwitterUser__c) s;
            System.debug(LoggingLevel.WARN, tUser);
            
            TwitterStatusUtil tUtil = new TwitterStatusUtil();      
            List<TwitterStatus> timeline = tUtil.getTimeline(tuser);
            
            List<Twitter_Post__c> twitterPostsToUpsert = new List<Twitter_Post__c>();
            for(TwitterStatus tStatus : timeline){
                Twitter_Post__c tPost = new Twitter_Post__c(Twitter_User__c = tUser.Id,
                                                               User__c = tUser.User__c,
                                                               ExternalId__c = tStatus.id + tUser.Id); //Use the twitter status id and the user Id as the key for checking for preventing duplicqtes
                if(tStatus.text != null){
                    List<String> hashtags = csvToList(tUser.Hashtag_to_follow__c);
                    if(containsHashtag(tStatus.text, hashtags) || tUser.Get_All_Tweets__c){
                        tPost.Body__c = cleanse(tStatus.text);
                        twitterPostsToUpsert.add(tPost);
                    }
                }
            }            
            if(twitterPostsToUpsert.size() > 0) upsert twitterPostsToUpsert ExternalId__c;                    
        }
    }
    private String cleanse(String msg){
       return msg.replace('\\/', '/');
    }
    private Boolean containsHashtag(String body, List<String> hashtags){
       for(String hashtag : hashtags){
           if(body.contains(hashtag.trim()))
               return true;
       }
       return false;
    }
    private List<String> csvToList(String csv){
       if (csv != null && csv != ''){
           return csv.split(',');
       }else{
           return new List<String>();
       }           
    }
    
    global void finish(Database.BatchableContext BC)
    {
        System.debug(LoggingLevel.WARN,'Batch Process Finished');
    }
}