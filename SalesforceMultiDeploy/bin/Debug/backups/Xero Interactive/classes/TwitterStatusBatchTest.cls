/**
* @author Joey Q. Chan (joeyqchan@gmail.com)
* @date 07/06/2011
* @description Test Method for TwitterStatusBatch
*/

@isTest
private class TwitterStatusBatchTest {

    static testMethod void testBatch(){
        TwitterUser__c tuser = new TwitterUser__c(Active__c = true,
                                                  Consumer_Key__c = 'VCino9IWidtvbnof62WA1',
                                                  Consumer_Secret__c = 'ORplEKdQk47OoDi0IRWTl2torsWMB5D78uxeJyoIrfk1',
                                                  AccessToken__c = '106595753-JjSz02mBenHwseXtdkGQH4UWIbHZKqr73cqKcMjF1',
                                                  Code__c = 'LUHfAAe4LRIzdwa784SjPue08afhY24NDq6UDJyas1',
                                                  Hashtag_to_follow__c = '#ch',
                                                  User__c = UserInfo.getUserId());    
        insert tuser;    

        TwitterStatusBatch tBatch = new TwitterStatusBatch('Select Id, Name, User__c, Hashtag_to_follow__c, Consumer_Key__c, Consumer_Secret__c, AccessToken__c, Code__c, Get_All_Tweets__c From TwitterUser__c Where Active__c = true Limit 1');
        Test.startTest();
        ID batchprocessid = Database.executeBatch(tBatch,1);
        Test.stopTest();
        
        AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email from AsyncApexJob where Id = :batchprocessid];
        System.debug('\n\nFinal results are: '+a);
        System.AssertEquals('Completed', a.status);
    }
}