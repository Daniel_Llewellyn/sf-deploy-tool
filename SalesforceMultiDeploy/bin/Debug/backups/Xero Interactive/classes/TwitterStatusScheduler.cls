global class TwitterStatusScheduler implements Schedulable {  
    global void execute(SchedulableContext ctx) {
        TwitterStatusBatch tBatch = new TwitterStatusBatch('Select Id, Name, User__c, Hashtag_to_follow__c, Consumer_Key__c, Consumer_Secret__c, AccessToken__c, Code__c, Get_All_Tweets__c From TwitterUser__c Where Active__c = true AND AccessToken__c != null AND Code__c != null ');
        ID batchprocessid = Database.executeBatch(tBatch,1);
    }   
}