/**
* @author Joey Q. Chan (joeyqchan@gmail.com)
* @date 07/06/2011
* @description Test Method for TwitterStatusScheduler
*/

@istest
private class TwitterStatusSchedulerTest {

    static testmethod void testScheduler() {
        TwitterUser__c tUser = new TwitterUser__c(Active__c = true,
                                                     Consumer_Key__c = '122642784451634',
                                                     Consumer_Secret__c = 'a4447263a33f02818573bcea076b9f4b',
                                                     Hashtag_to_follow__c = '#ch',
                                                     User__c = UserInfo.getUserId());     
        
        insert tUser;

        Test.startTest();
        
        // Schedule the test job every hour
        String sch = '0 0 * * * ?';
        String jobId = System.schedule('testTwitterStatusSchedulerTest1', sch, new TwitterStatusScheduler());
        
        // Get the information from the CronTrigger API object  
        CronTrigger ct = [SELECT id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        
        // Verify the expressions are the same  
        System.assertEquals(sch, ct.CronExpression);
        Test.stopTest();
    }
}