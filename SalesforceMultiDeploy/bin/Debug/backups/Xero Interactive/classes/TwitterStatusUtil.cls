public class TwitterStatusUtil {
    private List<TwitterStatus> parseStatuses(Dom.Document resDoc){
        List<TwitterStatus> statuses = new List<TwitterStatus>();     

        Dom.XMLNode statusesNode = resDoc.getRootElement();               
        for(Dom.XMLNode statusType : statusesNode.getChildElements())
        {
            if (statusType.getChildElement('id', null) != null && statusType.getChildElement('text', null) != null){
                TwitterStatus tStatus = new TwitterStatus();
                tStatus.id = statusType.getChildElement('id', null).getText();
                tStatus.text = statusType.getChildElement('text', null).getText();              
                System.debug('Debugging' + tStatus);
                statuses.add(tStatus);
            }            
        }   
        return statuses;
    }
    public List<TwitterStatus> getTimeline(TwitterUser__c tuser) {
        Oauth oa = new Oauth();
        oa.consumerKey = tuser.Consumer_Key__c;
        oa.consumerSecret = tuser.Consumer_Secret__c;
        oa.token = tuser.AccessToken__c;
        oa.tokenSecret = tuser.Code__c;
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        req.setEndpoint('https://api.twitter.com/1/statuses/user_timeline.xml?trim_user=true');
        oa.sign(req);
        System.debug('Sending request...');
        Dom.Document resDoc;
        if(!Test.isRunningTest()){
            resDoc = h.send(req).getBodyDocument();
        }else{
            resDoc = new Dom.Document();            
            resDoc.load('<statuses type="array"> <status> <created_at>Fri Jun 03 16:49:32 +0000 2011</created_at> <id>76692000430239744</id> <text>Testing Twitter to Chatter #ch #awesome #forcedotcom</text> <source>web</source> <truncated>false</truncated> <favorited>false</favorited> <in_reply_to_status_id></in_reply_to_status_id> <in_reply_to_user_id></in_reply_to_user_id> <in_reply_to_screen_name></in_reply_to_screen_name> <retweet_count>0</retweet_count> <retweeted>false</retweeted> <user> <id>106595753</id> </user> <geo/> <coordinates/> <place/> <contributors/> </status> <status> <created_at>Mon May 30 05:57:45 +0000 2011</created_at> <id>75078421977374721</id> <text>Sample Tweet using Chouette Social by @raygao #awesome</text> <source>&lt;a href=&quot;https://chouette-social.are4.us&quot; rel=&quot;nofollow&quot;&gt;Chouette_dev&lt;/a&gt;</source> <truncated>false</truncated> <favorited>false</favorited> <in_reply_to_status_id></in_reply_to_status_id> <in_reply_to_user_id></in_reply_to_user_id> <in_reply_to_screen_name></in_reply_to_screen_name> <retweet_count>0</retweet_count> <retweeted>false</retweeted> <user> <id>106595753</id> </user> <geo/> <coordinates/> <place/> <contributors/> </status> </statuses>');
        }
        System.debug(resDoc.toXmlString());
        return parseStatuses(resDoc);
    }
}