public class TwitterUtil{

/*
    Sample Usage
    TwitterUtil tweet = new TwitterUtil();
    tweet.Tweet('a0aE0000000WZdP', 'This-tweet-works');
    tweet.Tweet('a0aE0000000WZdP', 'This doesnt');
*/

    public string Tweet(Id twitterUser, string message)
    {
        TwitterUser__c tuser = [select Consumer_Key__c, Consumer_Secret__c, Code__c, AccessToken__c from TwitterUser__c where id = :twitterUser];

        

        Oauth oa = new Oauth();
        oa.consumerKey = tuser.Consumer_Key__c;
        oa.consumerSecret = tuser.Consumer_Secret__c;
        oa.token = tuser.AccessToken__c;
        oa.tokenSecret = tuser.Code__c;
        
        
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        
        string endPoint = 'https://api.twitter.com/1/statuses/update.json?status=' + EncodingUtil.urlEncode(message, 'UTF-8') + '&trim_user=true&include_entities=true';            
        req.setEndpoint(endPoint);
        oa.sign(req);
        
        
        HttpResponse res = null;
        res = h.send(req);
        
        System.debug('Response details: ' + res);
        System.debug('Response body: ' + res.getBody());
        return res.getBody();
    }


/*
When run in execute anonymous I get the following for an authorization header

Authorization: OAuth oauth_callback="null", oauth_version="1.0", oauth_nonce="-4826959798104393453", oauth_signature_method="HMAC-SHA1", oauth_consumer_key="B2ee7sHdaVTe68OI9k7sA", oauth_token="244148317-u57VoTAM5FikktV6HMi7BwIM3vM1IGf0hmkcT0ZH", oauth_timestamp="1312221291", oauth_signature="lJsVa2BgpXO1LrhId9PlWdLCAXk%3D"

and twitter responds with a statuscode 401, unauthorized. 
*/
}