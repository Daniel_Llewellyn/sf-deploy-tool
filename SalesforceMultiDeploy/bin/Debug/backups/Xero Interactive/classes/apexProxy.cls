public class apexProxy { 
    private PageReference page;
    private string action;
    private string verb;
    private string session;
    private string result;

    public apexProxy() {
        //constructor, let's grab the session token here
        session = userInfo.getSessionId();
 
        page = ApexPages.currentPage();
        this.action = page.getParameters().get('action');
        this.verb = page.getParameters().get('verb');
        if(this.verb == null)
        {
            this.verb = 'GET';
        }
    }
 
    public void retrieve() {
        //we'll call the REST API here
        http h = new http();
 
        httprequest req = new httprequest();
        //note that 'na1' may be different for your organization
        req.setEndpoint('https://na9.salesforce.com/services/data/v20.0/' + action);
        req.setMethod(verb);
        req.setHeader('Authorization', 'OAuth ' + session);
        req.setTimeout(1000);
        
        if( ApexPages.currentPage().getHeaders().get('param1') != null)
        {
            req.setBody( ApexPages.currentPage().getHeaders().get('param1') );
        }
 
        httpresponse res = h.send(req);
 
        result = res.getBody();
    }
 
    //getter method to return JSON result
    public string getResult() {
        return this.result;
    }

}