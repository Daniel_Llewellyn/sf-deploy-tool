global class auctionForce_controller 
{
    /*********************************************************
    AuctionForce_Controller
    Author: Kenji776
    Date: 2/25/2012
    Description: Provides back end functionality for the AuctionForce web application.
    Code Coverage: 82% (Can't get it any higher because the untested chunks are my catch statments in the select methods that I can't force to error)
    ********************************************************/
    
    //gets a list of active auctions (where the start date is greater than now, and the end date is less than now)
    @RemoteAction
    global static remoteObject getAuctions()
    {
        remoteObject returnObj = new remoteObject();
        try
        {
            dateTime currentTime = datetime.now();
            returnObj.sObjects = [select name, id from auction__c where start_date__c < :currentTime and end_date__c > :currentTime];
        }
        catch(exception e)
        {
            returnObj.success = false;
            returnObj.message = e.getMessage();
            returnObj.data = 'Error Type: ' + e.getTypeName() + ' ' + e.getCause() + ' ' + ' on line: ' +e.getLineNumber();
        }                
        return returnObj;
    }
    
    //get a list of auction items for a given auction ordered by a provided sortField using a provided sortOrder
    @RemoteAction
    global static remoteObject getItems(id AuctionId, string sortField, string sortOrder)
    {
        remoteObject returnObj = new remoteObject();
        try
        {
            dateTime currentTime = datetime.now();
            
            //populate the params if they are null so the query doesn't bomb out
            if(sortField == null || sortField == '')
            {
                sortField = 'Bidding_Closes__c';
            }
            if(sortOrder == null || sortOrder == '')
            {
                sortOrder = 'desc';
            }
            
            //a big of dynamic soql here for the conditional ordering 
            string queryString = 'select name, id, Starting_Bid__c, Description__c, Is_Active__c, High_Bid__c, Most_Recent_Bid__c, Bidding_Closes__c, stated_value__c, minimum_bid_incriment__c, Number_of_Bidders__c, Number_of_Bids__c,  (select id, name, description from Attachments  where name like \'%thumbnail%\' order by name asc)  from auction_item__c where auction__c = :auctionId order by ' + sortField + ' ' + sortOrder;
            returnObj.sObjects = database.query(queryString);
        }
        catch(exception e)
        {
            returnObj.success = false;
            returnObj.message = e.getMessage();
            returnObj.data = 'Error Type: ' + e.getTypeName() + ' ' + e.getCause() + ' ' + ' on line: ' +e.getLineNumber();
        }                
        return returnObj;
    } 

    //gets a single auction item by id. Also includes atached bids, comments (notes) and pictures (attachments)
    @RemoteAction
    global static remoteObject getItem(id itemId)
    {
        remoteObject returnObj = new remoteObject();
        try
        {
            dateTime currentTime = datetime.now();
            returnObj.sObjects = [select name, 
                                         id, 
                                         Bidding_Closes__c, 
                                         stated_value__c, 
                                         minimum_bid_incriment__c, 
                                         High_Bid__c, 
                                         Donated_By__c, 
                                         Description__c,
                                         Most_Recent_Bid__c,
                                         Starting_Bid__c,
                                         Is_Active__c, 
                                         Number_of_Bidders__c, 
                                         Number_of_Bids__c, 
                                         (select name, id, bid_amount__c, createdDate from Auction__R order by createdDate desc), 
                                         (select parentid, id, body, title, createdDate from Notes order by createdDate asc), 
                                         (select id, name, description from Attachments where not name like '%thumbnail%' order by name asc) 
                                         from auction_item__c 
                                         where id = :itemId];
        }
        catch(exception e)
        {
            returnObj.success = false;
            returnObj.message = e.getMessage();
            returnObj.data = 'Error Type: ' + e.getTypeName() + ' ' + e.getCause() + ' ' + ' on line: ' +e.getLineNumber();
        }                
        return returnObj;
    } 
           
    //gets a single attachment (photo) by id. The data is returned as a base64 string that can be plugged into an html img tag to display the image.
    @RemoteAction
    global static remoteObject getAttachment(id attachmentId)
    {   
        remoteObject returnObj = new remoteObject();
        try
        {
            list<Attachment> docs = [select id, body from Attachment where id = :attachmentId limit 1]; 
            if(!docs.isEmpty())
            {
                returnObj.data = EncodingUtil.base64Encode(docs[0].body); 
            }    
        }
        catch(exception e)
        {
            returnObj.success = false;
            returnObj.message = e.getMessage();
            returnObj.data = 'Error Type: ' + e.getTypeName() + ' ' + e.getCause() + ' ' + ' on line: ' +e.getLineNumber();        
        } 
        return returnObj;    
    }   

    //Takes a url encoded string of data where each param represents a form field and it's value is the value to save to that field. Also takes a string representing the object type.
    //decodes the url params and uses it to create an sObject instance and save it to the database.
    @RemoteAction
    global static remoteObject saveObject(string dataString, string objectType)
    {
        remoteObject returnObj = new remoteObject();
        try
        {
        //describe the object passed in the url
            Schema.sObjectType objectDef = Schema.getGlobalDescribe().get(objectType).getDescribe().getSObjectType();
            
            
            //keep track of any fields that we are not able to save to the sObject
            returnObj.data = 'Removed fields: ';
            
            //create a map of all fields on the object.
            Map<String, Schema.SobjectField> ObjectFieldsMap = objectDef.getDescribe().fields.getMap();

            //deserialize the url query string in a map of string to string (key, value)
            map<string,string> formData = deserializeString(dataString);
            
            //list of objects to insert (may support more than one in the future)
            list<sObject> objects = new list<sObject>();
            
            //this individual object
            sObject updateObj;
            if(formData.containsKey('id'))
            {
                string queryString = 'select id from ' +objectType+ ' where id = \''+formData.get('id')+'\'';
                updateObj = database.query(queryString);
            }
            else
            {
               updateObj = objectDef.newSobject();
            }
            
            //loop over ever param we got passed in and try to assign it to the sObject.
            for(String key : formData.keySet())
            {
                try
                {
                    //figure out the type of this field so we can cast it to the correct type
                    string fieldType = ObjectFieldsMap.get(key).getDescribe().getType().name().ToLowerCase();
                    
                    //since I don't know how to do, or if it's even possible to do dynamic casting we need a 
                    //series of if statments to handle the casting to numeric types. I think all the others should
                    //be fine if left as a string.
                    
                    
                    if(fieldType == 'currency' || fieldType == 'double' || fieldType == 'percent' || fieldType == 'decimal' )
                    {
                        updateObj.put(key, decimal.valueOf(formData.get(key).trim())); 
                    }
                    else if(fieldType == 'boolean')
                    {
                        updateObj.put(key, Boolean.valueOf(formData.get(key))); 
                    }                   
                    else if(fieldType == 'date')
                    {
                        updateObj.put(key, date.valueOf(formData.get(key))); 
                    }                
                    else
                    {
                        if(updateObj.get(key) != null)
                        {
                            updateObj.put(key,updateObj.get(key)+';'+formData.get(key));
                        }
                        else
                        {
                            updateObj.put(key,formData.get(key));
                        }
                    }                
                }
                //if assigning a value didn't go well, lot it and return that to the user. We'll keep going and use the rest of the data, but the user will at least know that some of the data didn't get inserted.
                catch(Exception e)
                {
                    system.debug('Field: ' +key +' bad value ' + formData.get(key) + ' ERR: ' + e.getMessage() + '\n');
                    returnObj.data += 'Field: ' +key +' bad value ' + formData.get(key) + ' ERR: ' + e.getMessage() + '\n';
                }       
            }
            
            //save this object to the list
            objects.add(updateObj);
            
            //if we have an id key on this object then do an update. Otherwise just do an insert.
            if(formData.containsKey('id'))
            {
                update objects;
            }
            else
            {
                insert objects;
            }
            
            //return the list of created objects to the user (in reality it's only one object per request, the the returnObj wants a list)
            returnObj.sObjects = objects;
        }
        catch(exception e)
        {
            returnObj.success = false;
            returnObj.data = e.getMessage() + ' Line ' + e.getLineNumber();
        }                    
        return returnObj;
    }
                
    //decodes a url encoded string into a map of strings.    
    public static Map<string,string> deserializeString(String argString)
    {   
        argString = EncodingUtil.urlDecode(argString,'UTF-8');
        string[] params = argString.split('&');
        map<String,String>  formParams = new map<String,String>();
        for(string p : params)
        {       
            formParams.put(EncodingUtil.urlDecode(p.substring(0,p.indexOf('=')),'UTF-8'),EncodingUtil.urlDecode(p.substring(p.indexOf('=')+1,p.length()),'UTF-8'));    
        }       
        return formParams;
    }

   //simple wrapper object to make evaluating the result of a remoting request easier
    global class remoteObject
    {
        public boolean success = true;
        public string message = 'Operation Successfull';
        public string data = null;
        public list<sObject> sObjects = new list<sObject>(); 
    }

    public class customException extends Exception {}             
    
    @isTest
    public static void testAuctionForce_Controller()
    {
        //First create an auction object
        auction__c testAuction = new auction__c();
        testAuction.name = 'Test Auction';
        testAuction.Start_Date__c = datetime.now().addDays(-3);
        testAuction.End_Date__c = datetime.now().addDays(3);
        insert testAuction;
        
        //get a list of active auctions
        remoteObject auctions = auctionForce_controller.getAuctions();
        
        //now create an auction item
        Auction_Item__c testItem = new Auction_Item__c();
        testItem.Auction__c = testAuction.id;
        testItem.Bidding_Closes__c = datetime.now().addDays(3);
        testItem.Description__c  = 'This is a test item';
        testItem.Minimum_Bid_Incriment__c = 15.00;
        testItem.Stated_Value__c = 50.00;
        testItem.Starting_bid__c = 50.00;
        
        insert testItem;
        
        //now get a list of items for this auction
        remoteObject items = auctionForce_controller.getItems(testAuction.id,null,null);
        
        //there had better be one item in that list.
        system.assertEquals(1,items.sObjects.size());
        
        //make get items fail by passing an invalid field to filter on
        remoteObject failItems = auctionForce_controller.getItems(testAuction.id,'herp','derp');
          
        //now create a bid for this item.
        Auction_Bid__c testBid = new Auction_Bid__c();
        testBid.Auction_Item__c = testItem.id;
        testBid.Bid_Amount__c = testItem.Stated_Value__c +  testItem.Minimum_Bid_Incriment__c;
        testBid.Bidder_Name__c = 'Test Guy!';
        testBid.Bidder_Number__c = 1000;
        
        insert testBid;
        
        //now lets get that specific item, so we can see if it has the bid attached to it that we think it should.
        remoteObject thisItem = auctionForce_controller.getItem(testItem.id); 
        
        system.assertEquals(true,thisItem.success);
        
        //okay now lets use the saveObject method to create a comment for this bid.
        string queryString = 'badField=Ill get removed&title=testComment&body=wakka wakka wakka&parentId='+testItem.id;
        
        remoteObject saveCommentResult = auctionForce_controller.saveObject(queryString,'Note'); 
        
         system.assertEquals(true, saveCommentResult.success);
         
        Note testNote = [select title, body from note where id = :saveCommentResult.sObjects[0].Id];
        system.assertEquals('testComment',testNote.title);
        
        
        //now lets do an update call on that same comment by attaching an id to the querystring
        queryString+= '&id='+testNote.Id;
                    
        remoteObject updateCommentResult = auctionForce_controller.saveObject(queryString,'Note'); 
                
        system.assertEquals(true,updateCommentResult.success);
        
        //now make the save object call fail by passing junk data
        remoteObject saveCommentFail = auctionForce_controller.saveObject('RingRingRing','BananaPhone!'); 
        
        system.assertEquals(false,saveCommentFail.success);
        
        //make get attachment return an empty result set
        remoteObject emptyGetAttachment = auctionForce_controller.getAttachment(testItem.id);
                
    } 
}