global class boxConnect
{
    public static string apiKey = 'nc62q2u01pgtao91g90igexoafej854p';   
    public static string endPoint = 'https://www.box.net/api/1.0/rest';
    public static string ticketUrl = 'https://www.box.net/api/1.0/auth/';

    public static string ticket;
    public static string authToken;
    public static string userToken;
    
    public class boxException extends Exception{}
    public string getTicketUrl()
    {
        return ticketUrl;
    }
    public string getTicket()
    {
        string xmlResponse =  brokerHttpRequest('get_ticket', null);
        if(utilities.readXMLelement(xmlResponse,'status') == 'get_ticket_ok')
        {
            ticket = utilities.readXMLelement(xmlResponse,'ticket'); 
        }
        else
        {
             throw new boxException('Error getting box ticket: ' + utilities.readXMLelement(xmlResponse,'status') );
        }

        return ticket;
    }

    //wrapper method for calling setUserToken from a VF page as the action method.

    public String getUserToken() {
        return getUsertoken(null);
    }   
     
    public pageReference setUserToken()
    {
            map<string,string> params = ApexPages.CurrentPage().getParameters();

            User thisUser = new User(id=UserInfo.getUserId());
            thisUser.Box_token__c =params.get('auth_token');
            update thisUser;
            return null;
  
    }
    
    public string getUsertoken(string userId)
    {
        try
        {
            if(userId==null)
            {
                userid = UserInfo.getUserId();
            }        
            User thisUser = [select Box_token__c from user where id = :userId];
            
            if(thisUser.Box_token__c == null)
            {
                throw new boxException('Not Box Authorization token on file for user. Please authenticate first');
            }
            return thisUser.Box_token__c;
        }
        catch(exception e)
        {
            return e.getMessage();
        }
    }
    
    public string getAuthToken()
    {
        map<string,string> params = new map<string,string>();     
        params.put('ticket', ticket);    
        string xmlResponse = brokerHttpRequest('get_auth_token', params);

        if(utilities.readXMLelement(xmlResponse,'status') == 'get_auth_token_ok')
        {
            authToken = utilities.readXMLelement(xmlResponse,'auth_token'); 
        }
        else
        {
            throw new boxException('Error getting box auth token: ' + utilities.readXMLelement(xmlResponse,'status') );
        }        
        return authToken;    
    }
    
    public boxCreateFolderResponse createFolder(map<string,string> params)
    {
        boxCreateFolderResponse newFolder = new boxCreateFolderResponse();
        string xmlResponse = brokerHttpRequest('create_folder', params); 
        if(utilities.readXMLelement(xmlResponse,'status') == 'create_ok')
        {
            newFolder.folder_id = Integer.valueOf(utilities.readXMLelement(xmlResponse,'folder_id')); 
            newFolder.folder_name = utilities.readXMLelement(xmlResponse,'folder_name'); 
            newFolder.user_id =  Integer.valueOf(utilities.readXMLelement(xmlResponse,'user_id')); 
            newFolder.path = utilities.readXMLelement(xmlResponse,'path'); 
            newFolder.shared =  Integer.valueOf(utilities.readXMLelement(xmlResponse,'shared')); 
            newFolder.public_name = utilities.readXMLelement(xmlResponse,'public_name'); 
            newFolder.parent_folder_id =  Integer.valueOf(utilities.readXMLelement(xmlResponse,'parent_folder_id')); 
            newFolder.password = utilities.readXMLelement(xmlResponse,'password'); 
        }
        else
        {
             throw new boxException('Error creating box folder: ' + utilities.readXMLelement(xmlResponse,'status') );
        }
        return newFolder ;        
    }
    
    public string brokerHttpRequest(string actionName, map<string,string> params)
    {
        string returnXML;
        
        string requestUrl = endPoint + '?action='+actionName+'&api_key='+apiKey;
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');  
       
       if(userToken != null)
       {
           params.put('auth_token', userToken);
       }
       if(params != null)
       {
            for(string param : params.keySet())
            {
                requestUrl += '&'+param+'='+params.get(param);
            }
        }
        
        req.setEndpoint( requestUrl );
        Http http = new Http();
        HTTPResponse res = http.send(req); 
        
        returnXML = res.getBody();
        system.debug(returnXML);
        return returnXML;
    }
    
    global class boxCreateFolderResponse
    {
        string folder_name;
        integer folder_id;
        integer user_id;
        string path;
        integer shared;
        string public_name;
        integer parent_folder_id;
        string password;
    }
    
    global class boxFile
    {
        integer id;
        integer file_name;
        string keyword;
        integer shared;
        integer size;
        integer created;
        integer updated;
        list<string> tags;
    }
}