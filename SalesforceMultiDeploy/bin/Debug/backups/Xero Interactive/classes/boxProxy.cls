public class boxProxy {

    public static boolean isRunningTest = false;
    public string boxResponse;
    
    public class boxResponse
    {
        boolean success = true;
        string data ='';
        string url ='';
    }
    
    public string getBoxResponse()
    {
        
        map<string,string> params =  ApexPages.currentPage().getParameters();
       
        
        string url = 'https://www.box.com/api/1.0/rest?_=rand';
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        
        for(string p : params.keySet())
        {
            url += '&'+p+'='+params.get(p);
        } 
        
        system.debug(url);
        req.setEndpoint(url);
        req.setMethod('GET');
        req.setTimeout(60000); 
        HttpResponse res = h.send(req);
        string response = res.getBody();
        boxResponse thisResponse = new boxResponse();
        thisResponse.url = url;
        if(params.get('action') == 'get_ticket')
        {
            if(readXMLelement(response, 'status') == 'get_ticket_ok')
            {
                thisResponse.data = readXMLelement(response, 'ticket');
                
            }
            else
            {
                thisResponse.data = readXMLelement(response, 'status');
                thisResponse.success = false;
            }
            response = JSON.serialize(thisResponse);      
        } 
        else if(params.get('action') == 'get_auth_token')
        {
            if(readXMLelement(response, 'status') == 'get_token_ok')
            {
                thisResponse.data = readXMLelement(response, 'token');                
            }  
            else
            {
                thisResponse.data = readXMLelement(response, 'status');
                thisResponse.success = false;
            }            
            response = JSON.serialize(thisResponse);      
        }
        return response;          
          
    }

    public static String readXMLelement(String xml, String element)
    {
        String elementValue = 'NOT FOUND'; 
        
        try
        {
            Xmlstreamreader reader = new Xmlstreamreader(xml);
            while (reader.hasNext()) 
            {
                if (reader.getEventType() == XmlTag.START_ELEMENT && reader.getLocalName() == element)
                {
                    reader.next();
                    if(reader.getEventType() == XmlTag.CHARACTERS)
                    {
                        elementValue = EncodingUtil.urlDecode(reader.getText(), 'UTF-8').trim();
                    }                 
                }     

                reader.next();
            }
            return elementValue;
        }
        catch(exception e)
        {
            system.debug(e);
            return e.getTypeName() + ' ' + e.getCause() + ' ' + e.getMessage() + ' on line ' + e.getLineNumber();
        }
    }


    public static Map<string,string> deserializeString(String argString)
    {   
        string[] params = argString.split('&');
        map<String,String>  formParams = new map<String,String>();
        for(string p : params)
        {       
            formParams.put(EncodingUtil.urlDecode(p.substring(0,p.indexOf('=')),'UTF-8'),EncodingUtil.urlDecode(p.substring(p.indexOf('=')+1,p.length()),'UTF-8'));    
        }       
        return formParams;
    } 

    
}