global class cardForce 
{
    
    
    @remoteAction
    global static remoteObject registerUser(string dataString)
    {
        remoteObject returnObj = new remoteObject();
        returnObj.message = 'Account Created';
        
        
        try
        {
            map<string,string> formData = deserializeString(dataString);

        }
        catch(exception e)
        {
            returnObj.success = false;
            returnObj.message = 'Error creating account. Please try again later.';
            returnObj.data = e.getMessage();
        }
        
        
        return returnObj;       
    }

    @remoteAction
    global static remoteObject loginUser(string dataString)
    {
        remoteObject returnObj = new remoteObject();

        returnObj.message = 'Login Successful';
             
        
        try
        {
            throw new customException('Bad username or password.');
        }
        catch(Exception e)
        {
            returnObj.success = false;
            returnObj.message = 'Error during login';
            returnObj.data = e.getMessage();
        }
        
        
        return returnObj;       
    }

    @remoteAction
    global static remoteObject getRecentTweets(string twitterId, integer numberOfTweets, string tags)
    {
        remoteObject returnObj = new remoteObject();

        returnObj.message = 'Search Successful';
        try
        {
        
            string searchUrl = 'http://search.twitter.com/search.json?q=%40'+tags+'%20from:'+twitterId;     
            Http h = new Http();
            HttpRequest req = new HttpRequest();
            req.setMethod('GET');
            req.setEndpoint(searchUrl);
    
            HttpResponse res = null;
            if(Test.isRunningTest())
            {
                res = new HttpResponse();
            } 
            else 
            {
                res = h.send(req);
            }
            if(res.getStatusCode()>299) 
            {
                throw new customException('Failed getting a request token. HTTP Code = '+res.getStatusCode()+'. Message: '+res.getStatus()+'. Response Body: '+res.getBody());
            }    
            returnObj.data = res.getBody();     
        }
        catch(Exception e)
        {
            returnObj.success = false;
            returnObj.message = 'Error getting tweets';
            returnObj.data = e.getMessage();
        }
        
        
        return returnObj;     
    }
    
    @remoteAction
    global static remoteObject getCards()
    {
        remoteObject returnObj = new remoteObject();

        try
        {

            list<CardForce_Card__c> cards = [select 
                                                Name,
                                                Demand__c,
                                                Event__c,
                                                Event__r.Prize__c,
                                                (SELECT Attachment.Name, Attachment.Id FROM CardForce_Card__c.Attachments)
                                                Id from CardForce_Card__c where Event__r.Active__c = true];
            returnObj.sObjects = cards;
        }
        catch(Exception e)
        {
            returnObj.success = false;
            returnObj.message = 'Error getting cards';
            returnObj.data = 'Error Type: ' + e.getTypeName() + ' ' + e.getCause() + ' ' + ' on line: ' +e.getLineNumber();
        }
        
        
        return returnObj;       
    }

    //gets a single attachment (photo) by id. The data is returned as a base64 string that can be plugged into an html img tag to display the image.
    @RemoteAction
    global static remoteObject getAttachment(id attachmentId)
    {   
        remoteObject returnObj = new remoteObject();
        try
        {
            list<Attachment> docs = [select id, body from Attachment where id = :attachmentId limit 1]; 
            if(!docs.isEmpty())
            {
                returnObj.data = EncodingUtil.base64Encode(docs[0].body); 
            }    
        }
        catch(exception e)
        {
            returnObj.success = false;
            returnObj.message = e.getMessage();
            returnObj.data = 'Error Type: ' + e.getTypeName() + ' ' + e.getCause() + ' ' + ' on line: ' +e.getLineNumber();        
        } 
        return returnObj;    
    }   

    
    public static Map<string,string> deserializeString(String argString)
    {   
        string[] params = argString.split('&');
        map<String,String>  formParams = new map<String,String>();
        for(string p : params)
        {       
            formParams.put(EncodingUtil.urlDecode(p.substring(0,p.indexOf('=')),'UTF-8'),EncodingUtil.urlDecode(p.substring(p.indexOf('=')+1,p.length()),'UTF-8'));    
        }       
        return formParams;
    }

    global class remoteObject
    {
        public boolean success = true;
        public string message = 'operation successful';
        public string data = null;
        public list<sObject> sObjects = new list<sObject>();
    }
    
    
    public class customException extends Exception {}
    
    static testMethod void cardForceTest()
    {

              
    }
}