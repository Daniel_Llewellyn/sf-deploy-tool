/***********************************
Name: caseMerge
Date: 8/29/2012
Author: Daniel Llewellyn/Kenji776@gmail.com
Description: Apex controller that provides back end functionality to the caseMerge and caseMergeSelect visualforce pages. 
                          These pages are used to inform a user that there are cases that are likely duplicates (based on the contacts email address)
                          and allow the user to merge the case information and close the duplicates. The merging consists of cloning case comments (since they cannot be re-parented)
                          and re-assigning all related tasks (including emails) from the merged cases into the selected master. The merged cases are closed and marked as duplicates. A
                          Additionally, their ParentId field is set to the master case, and a custom lookup field called master_case__c is also populated on the closed cases with the ID of 
                          the master case. This makes it easy to see which cases were merged together, and what their parent case was
                          
**************************************/

global class caseMerge {
    public Case thisCase{get;set;}
    public integer count{get;set;}
    public list<case> mergableCases{get;set;}
    
    //what is the text to use as the reason for closing the cases that get merged?
    public static string caseCloseReason = 'duplicate';
    
    //standard controller instantiation
    public caseMerge(ApexPages.StandardController controller) 
    {
        //put reference to case in shared scope
        thisCase = (Case)controller.getRecord();
    }
    
    //returns an integer containing the amount of open cases that are likely related to this one. If the supplied email is the same as this cases supplied email, or the contact email is the case, and the case
    //is open, then it is counted toward the total. The current case is of course excluded to prevent every case looking like a duplicate of itself.
    public void findMergableCount()
    {
        //if a case was passed in, then find the count. Otherwise just return 0 to avoid an error.
        if(thisCase != null )
        {
            //use aggregate query to get a count.
            count = [select count() from case where IsClosed = false and (suppliedEmail = :thisCase.suppliedEmail  or contact.email = :thisCase.contact.email) and id != :thisCase.id];
        }
        else
        {
            count = 0;
        }
    }

    //returns a list of cases that are likely related to each other, using the same filter logic as the function above. In this case however, we do not exclude the current case, since we don't know which
    //case the user will want to set as the master.
    public void findMergableList()
    {
           //get the id of the presumed master case from the url. this is just used to find any cases related to each other
           Id caseId = ApexPages.currentPage().getParameters().get('id');
           if(caseId != null)
           {
               //find the details about the given case
               case thisCase = [select id, CaseNumber, status, suppliedEmail, contact.email from case where id = :caseId];
               
               //find all cases with the same suppliedEmail or contact email that arn't closed.
               mergableCases = [select id,status,CaseNumber,Description,Subject,master_case__c from case where IsClosed = false and (suppliedEmail = :thisCase.suppliedEmail  or contact.email = :thisCase.contact.email)];
            }
            else
            {
                mergableCases = new list<case>();
            }
    }
    
    //remote javascript method to merge cases. This will clone any existing case comments (since you cannot change the parent of a case comment, you have to clone them).
     
    @remoteAction
    global static list<case> mergeCases(id masterCase, list<id> casesToMerge, boolean mergeComments, boolean mergeTasks)
    {
        list<case> casesToClose = new list<case>();
       
        //if the user wants to merge comments then do that
        if(mergeComments)
        {
            list<caseComment> commentsToInsert = new list<caseComment>();
            
            //find any case comments where the parentId is in our list of cases to merge
            for(list<caseComment> ccl :[select id, parentId from caseComment where parentId in :casesToMerge])
            {
                //iterate over every case comment in the list
                for(caseComment cc : ccl)
                {
                    //deep clone the comment while getting a new id, but preserving datetimes and autonumbers
                    caseComment thisComment = cc.clone(false,true,true,true);
                    //set the parent id on this comment
                    thisComment.parentId = masterCase;
                    //add it to the list of comments to insert.   
                    commentsToInsert.add(thisComment);   
                }        
            }
            //insert the comments. 
            database.insert(commentsToInsert,false);            
        }
        
        //if the user wants to merge tasks, lets do that.
        if(mergeTasks)
        {
            //find any task related to any of the cases in the merge list
            for(list<task> taskList : [select id, whatid from task where whatId in :casesToMerge])
            {
                //loop over every 
                for(task task: taskList)
                {
                    task.whatId = masterCase;
                }      
                //I know updates in SOQL loops are frowned upon, but it is seriously, seriously unlikely it would ever cause an issue here.
                //it's just the easiest most efficient way to perform this update.
                 database.update(taskList); 
            }      
        }
        
        //now we need to loop over all the id's of the cases to close, create references to them, set their attributes and close them.                  
        for(id caseId : casesToMerge)
        {
            //create case sObject reference.
            case thisCase = new case(id=caseId);
            
            //set fields on this case.
            thisCase.parentId = masterCase;
            thisCase.Reason = caseCloseReason;
            thisCase.Status = 'Closed';
            thisCase.master_case__c = masterCase;
            
            //add case to list of updates.
            casesToClose.add(thisCase);
        }
        //close case.
        database.update(casesToClose,false);
        
        //return the now (hopefully) closed cases.
        return casesToClose;
        
    }
}