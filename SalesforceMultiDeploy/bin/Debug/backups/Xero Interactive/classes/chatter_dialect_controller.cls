/***********************************
Name: chatter_dialect_controller
Date: 8/30/2012
Author: Daniel Llewellyn/Kenji776@gmail.com
Description: Apex controller that provides back end functionality to the chatter dialect app. Chatter Dialect allows for translation of 
                          salesforce date from one language to another. This is done by passing objects, with a list of fields on the object that you want to translate
                          into the translateObjectList call. When the list of objects is received, the owner of the records will be queried to find their specified dialect.
                          The translated words for that dialect will be queries and all replacments will be made.
                          
                          Additionally, this class contains logic for importing translations from a remote CSV file. By entering an import source URL on the dialect object
                          and clicking the import button a web request is created to get CSV content from the remote source. The CSV file should have only two columns,
                          one labelled target__c and one labeled source__c. The CSV file will be pased and translations will be created for every word pair. Duplicate
                          checking is implimented via a workflow rule that updated a field which is marked as unique to prevent duplicate translations.
                          
                          The translation function can take any sObjects, and any number of fields. The sObjects may have different owners and different applicable translation rules.
                          
                           Users set their preferred dialect on their user page. This is implimented via an inline visualforce page which creates a picklist out of the currently available dialects 
                           specified in the org.
                           
                           The application may be enabled and disabled via a custom setting called 'chatter dialect'. If there are no records for this setting, one will be created upon the first
                           attempted translation. It defaults to being activated. You may uncheck the box to disable all the functionality. Only the most recently created custom setting record is referened.
                           It is not advisable to have more than 1 record. It is suggested to create one, and set it as the org default. Having more than one will be useless and may end up causing confusiong for
                           other administrators.
                          
**************************************/
global class chatter_dialect_controller {

    //global variable for holding if dialects are enabled.
    public static boolean dialectEnabled;

    
    void onInstall(InstallContext context){
        createCustomSetting();
    }
    
    //create the default custom setting incase one is not found
    public static void createCustomSetting()
    {
        Chatter_Dialects__c defaultDialectSetting = new Chatter_Dialects__c();
        defaultDialectSetting.Enable_Dialects__c = true;
        insert defaultDialectSetting;    
    }
    
    //returns true if chatter dialects is enabled, or custom setting was not found. Returns false only if a record is found with 'enable_dialects__c' is set to false.
    public static boolean getDialectEnabled()
    {
       list<Chatter_Dialects__c> isEnabled = [select id, Enable_Dialects__c from Chatter_Dialects__c order by createdDate desc];
       
       if(isEnabled.isEmpty())
       {
           createCustomSetting();
           dialectEnabled = true;
       }    
       else if(isEnabled[0].Enable_Dialects__c)
       {
           dialectEnabled = true;
       }
       else
       {
           dialectEnabled = false;
       }
       return dialectEnabled;
    }
    
    //standard controller wrapper to allow inline visualforce page on users standard page.
    public chatter_dialect_controller(ApexPages.StandardController controller) {

    }
    
    //returns a set of picklist options containing all the available dialects for use on the visualforce page.
    //the id of the dialect is the value, and the name is the label.
    public List<SelectOption> getDialects() 
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('none','None'));
        for(list<Chatter_Dialects_Dialect__c> dls : [select name, id from Chatter_Dialects_Dialect__c])
        {
            for(Chatter_Dialects_Dialect__c dialect : dls)
            {
                options.add(new SelectOption(dialect.id,dialect.name));
            }
        }
        return options;
    }
    
    //function to translate specified fields on a list of objects to the dialect of their owner/creator. 
    public static void translateObject(list<sObject> recordsToFilter, list<string> fieldsToTranslate)
    {
        set<id> userIds = new set<id>();
        map<id,list<Chatter_Dialects_Translation__c>> dialectToTranslationsMap = new map<id,list<Chatter_Dialects_Translation__c>>();
        map<id,string> userToDialectMap = new map<id,string>();
        
        //make sure that dialects are enabled. If not, abort
        if(!getDialectEnabled())
        {
            return;
        }
        
        
        //first we need to add all the Id's of the users who own these records. We want to apply the dialect of the record owners to the records.
        //most records will use the createdById field, but chatter posts will use the parentId field. So first try createdBy, if that turns up null, use the parentid.
        //if those are both null, just give up and don't transate the record.
        for(sObject record : recordsToFilter)
        {
            //we need to figure out which users dialect to use. In most cases it would be the createdById of the record.
           id userId;
           userid = (id) recordsToFilter[0].get('createdById');
           if(userid == null)
           {
               userid = (id) recordsToFilter[0].get('parentId');
           }
           userIds.add(userid);
       }    
       
       //get the dialects used by any of the users who are owners of the inbound records.
       for(list<user> users : [select dialect__c from user where id in :userids])
       {
           for(user user : users)
           {
               userToDialectMap.put(user.id,user.dialect__c);
           }
       }

       //find all the translations for any dialect used by any user. Then store them in a map keyed by the dialect id.
       for( list<Chatter_Dialects_Translation__c> translations :  [select Source__c, target__c,dialect__c from Chatter_Dialects_Translation__c where Dialect__c in :userToDialectMap.values()] )
       {
           for(Chatter_Dialects_Translation__c translation : translations)
           {
               list<Chatter_Dialects_Translation__c> thisDialectsTransations = new list<Chatter_Dialects_Translation__c>();
               if(dialectToTranslationsMap.containsKey(translation.dialect__c))
               {
                   thisDialectsTransations = dialectToTranslationsMap.get(translation.dialect__c);
               }
               thisDialectsTransations.add(translation);
               dialectToTranslationsMap.put(translation.dialect__c,thisDialectsTransations);
           }
       }
       
       //now lets actually do the translations by looping over every record
       for(sObject post : recordsToFilter)
       {
            try
            {
                //we need to translate every field the user requested so loop over each field
                for(String postBodyField : fieldsToTranslate)
                {
                    //get an easy reference to the text in the field
                    String postBody = string.valueOf(post.get(postBodyField));           
                    if(postBody == null)
                    {
                        continue;
                    }
                    
                    //again we need to find the id of the person who created this thing. Ugly code I know, but blame salesforce for not having a consistant owner field across all objects
                    //including chatter posts (the chatter schema is a complete cluster-fuck IMHO).
                    id userId;
                    userid = (id) post.get('createdById');
                    if(userid == null)
                    {
                       userid = (id) post.get('parentId');
                    }      
                    
                    //now iterate over every translation for the dialect the user has specified.            
                    for(Chatter_Dialects_Translation__c translation : dialectToTranslationsMap.get(userToDialectMap.get(userid)))
                    {                    
                        //perform a case insensative replacment.       
                        postBody = postBody.replaceAll('(?i)'+translation.source__c,translation.target__c);
                        
                        //capitolize the first char of the text post, since the replacment is generally lowercase.
                        postBody = postBody.substring(0,1).toUpperCase() +  postBody.substring(1).toLowerCase();           
                        
                        //save the results back to the object.        
                        post.put(postBodyField,postBody);  
                    }
                    
                }
            }
            catch(exception e)
            {
                system.debug('Error translating!');
                system.debug(e.getMessage() + ' Line Number: ' + e.getLineNumber());
            }
        }
    }       
    
    //imports translation data for a given dialect by using a CSV file located at the given URL. CSV file should have 2 columns, one labelled target__C and one labelled source__c.
    webservice static string importTranslations(string dialect, string url)
    {
        string result = 'pending';
        try
        {
            string responseBody;
            
            //create http request to get import data from
            HttpRequest req = new HttpRequest();
            req.setEndpoint(url);
            req.setMethod('GET');         
            Http http = new Http();
            
            //if this is not a test actually send the http request. if it is a test, hard code the returned results.
            if(!Test.isRunningTest())
            {
                HTTPResponse res = http.send(req);
                responseBody = res.getBody();
            }
            else
            {
                responseBody = 'target__c,source__c\nAft,The back of the ship\nAhoy,Hello\nArr,yes\nAye,yes\nAye Aye,ok\nBarbary Coast,Mediterranean coast off of North Africa\nBe,am,\nBe,ar\nBe,is\nBilboes,Leg irons\nBilge,dirtiest\nBilge rat,idiot\nBilge rat,jerk\n';
            }
            
            //the data should come back in in CSV format, so hand it off the the parsing function which will make a list of translation objects, with their dialect (parent) set as the dialect passed into this
            //function.
            list<Chatter_Dialects_Translation__c> translations = parseTranslationCSV(dialect, responseBody);
            
            //simple counter to keep track of successfull translation creations.
            integer created = 0;
            
            //if there are any translations to attempt to create, lets do that.
            if(!translations.isEmpty())
            {
                //use a non all or nothing insert (so just insert what you can, ignore the rest, due to duplicats that will error)
                Database.SaveResult[] MySaveResult = database.insert(translations,false);
                
                //now lets count how many were actual successful out of the overall insert.
                for(Database.SaveResult saveResult : MySaveResult)
                {
                    if( saveResult.isSuccess())
                    {
                        created++;
                    }
                }
            }    
            
            //k lets update the dialect object to mark that it has just had an import job run.
            Chatter_Dialects_Dialect__c dialectToUPdate = new Chatter_Dialects_Dialect__c(id=dialect);
            dialectToUpdate.Last_Import__c = dateTime.now();
            update dialectToUpdate;
            
            //set the return message based on the created amount.
            if(created == 0)
            {
                result = 'No new translations found to create';
            }
            else
            {
                result = 'Translations imported. Created '+created+'/'+translations.size()+' translations.';
            }
        }
        catch(exception e)
        {
            result = 'Error importing translations. ' + e.getMessage() + ' on line ' + e.getLineNumber();
        }
        return result;
    }
    
    //returns a list of chatter dialect translations by parsing CSV data and assigning the translation to the given dialect.
    public static list<Chatter_Dialects_Translation__c> parseTranslationCSV(id dialect, string csvData)
    {
       list<Chatter_Dialects_Translation__c> translations = new list<Chatter_Dialects_Translation__c>();
       List<List<String>> fields= parseCSV(csvData,false);
       
       for(list<string> field : fields)
       {
           Chatter_Dialects_Translation__c thisTranslation = new Chatter_Dialects_Translation__c();
           thisTranslation.put(fields[0][0],field[0]);
           thisTranslation.put(fields[0][1],field[1]);
           thisTranslation.put('Dialect__c',dialect);
           translations.add(thisTranslation);
       }
       return translations;
    }

    //parses a csv file. REturns a list of lists. Each main list is a row, and the list contained is all the columns.
    public static List<List<String>> parseCSV(String contents,Boolean skipHeaders)
    {
        List<List<String>> allFields = new List<List<String>>();
    
        // replace instances where a double quote begins a field containing a comma
        // in this case you get a double quote followed by a doubled double quote
        // do this for beginning and end of a field
        contents = contents.replaceAll(',"""',',"DBLQT').replaceall('""",','DBLQT",');
        // now replace all remaining double quotes - we do this so that we can reconstruct
        // fields with commas inside assuming they begin and end with a double quote
        contents = contents.replaceAll('""','DBLQT');
        // we are not attempting to handle fields with a newline inside of them
        // so, split on newline to get the spreadsheet rows
        List<String> lines = new List<String>();
        try {
            lines = contents.split('\n');
        } catch (System.ListException e) {
            System.debug('Limits exceeded?' + e.getMessage());
        }
        Integer num = 0;
        for(String line : lines) {
            // check for blank CSV lines (only commas)
            if (line.replaceAll(',','').trim().length() == 0) break;
            
            List<String> fields = line.split(',');  
            List<String> cleanFields = new List<String>();
            String compositeField;
            Boolean makeCompositeField = false;
            for(String field : fields) {
                if (field.startsWith('"') && field.endsWith('"')) {
                    cleanFields.add(field.replaceAll('DBLQT','"'));
                } else if (field.startsWith('"')) {
                    makeCompositeField = true;
                    compositeField = field;
                } else if (field.endsWith('"')) {
                    compositeField += ',' + field;
                    cleanFields.add(compositeField.replaceAll('DBLQT','"'));
                    makeCompositeField = false;
                } else if (makeCompositeField) {
                    compositeField +=  ',' + field;
                } else {
                    cleanFields.add(field.replaceAll('DBLQT','"'));
                }
            }
            
            allFields.add(cleanFields);
        }
        if (skipHeaders) allFields.remove(0);
        return allFields;       
    }
}