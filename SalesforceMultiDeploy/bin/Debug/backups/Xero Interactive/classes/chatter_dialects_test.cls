public class chatter_dialects_test
{
    @isTest
    public static void chatter_dialect_controller_test()
    {
         list<FeedItem> posts = new list<FeedItem>();
        
         
        //first create a dialect
        Chatter_Dialects_Dialect__c thisDialect = new Chatter_Dialects_Dialect__c();
        thisDialect.name = 'Pirate Test';
        thisDialect.Import_Source_URL__c = 'https://docs.google.com/spreadsheet/pub?key=0Ak4fc3zIG4iydEUtUlY2RDQ5clhpMmwzMEZVYnBUc0E&single=true&gid=0&output=csv';
        insert thisDialect;
        
        //now lets import the translations for it. In real life this would do a callout to the specified URL, but in test it's going to use some pre created data.
        string result =  chatter_dialect_controller.importTranslations(thisDialect.id,thisDialect.Import_Source_URL__c);

        //importing it again should not cause duplicated.
        string result2 =  chatter_dialect_controller.importTranslations(thisDialect.id,thisDialect.Import_Source_URL__c);
                
        //check to make sure some translations got created.
        list<Chatter_Dialects_Translation__c> translations = [select id,source__c,target__c,dialect__c from Chatter_Dialects_Translation__c where dialect__c = :thisDialect.id];

        Profile thisProfile = [select id from profile where name = 'Chatter Free User'];
       
        User testUser = new User();
       
        String username = 'TestGuy';
        String domain = 'TestDomain';
   
        username += string.valueOf(Math.random());
        domain += string.valueOf(Math.random());
 
        testUser.username = username+'@'+domain+'.com';
       
        testUser.alias = 'Test';
        testUser.communityNickname = username;
        testUser.firstname = 'Test';
        testUser.lastname = 'Guy';
        testUser.email = 'Test@Test.com';
        testUser.LocaleSidKey = 'en_US';
        testUser.languageLocaleKey = 'en_US';
        testUser.EmailEncodingKey = 'ISO-8859-1';
        testUser.TimeZoneSidKey = 'GMT';
        testUser.profileid = thisProfile.id;
        testUser.Dialect__c = thisDialect.id;


         insert testUser;
         
         chatter_dialect_controller ctlr = new chatter_dialect_controller(new ApexPages.StandardController(testUser));
        //k, lets make sure the list of dialects works
        List<SelectOption> options = ctlr.getDialects();
        
        //This post should not trigger the above rule to fire as it does not contain an account number.
        FeedItem fItem = new FeedItem();
        fItem.Type = 'TextPost';
        fItem.ParentId = testUser.id;
        fItem.Body = 'hello there friend. This is a test';
        posts.add(fItem);
        
        //This post should trigger the above rule, so afterwards we can query for a tracking object.
        FeedItem fItem2 = new FeedItem();
        fItem2.Type = 'TextPost';
        fItem2.ParentId = testUser.id;
        fItem2.Body = 'Hey Jim. want to go meet with the customer to grab some lunch?';
        posts.add(fItem2);
       
        insert posts;         
        
        //there should now be a rule that says that the dialect is enabled.
        system.assertEquals(chatter_dialect_controller.getDialectEnabled(),true);
        
         Chatter_Dialects__c setting = [select id, Enable_Dialects__c from Chatter_Dialects__c order by createdDate desc limit 1];
         setting.Enable_Dialects__c = false;
         update setting;
         
         posts.clear();
         
        //This post should trigger the above rule, so afterwards we can query for a tracking object.
        FeedItem fItem3 = new FeedItem();
        fItem3.Type = 'TextPost';
        fItem3.ParentId = testUser.id;
        fItem3.Body = 'Hey Jim. want to go meet with the customer to grab some lunch?';
        posts.add(fItem3);     
        
        insert posts; 
        
        FeedComment comment1 = new FeedComment();
        comment1.CommentBody = 'Yes I would love that';
        comment1.FeedItemId = fItem3.id;   
 
        
        insert comment1;
    }  
}