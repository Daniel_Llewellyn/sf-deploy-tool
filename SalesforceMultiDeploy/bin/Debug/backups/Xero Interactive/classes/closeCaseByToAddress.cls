global class closeCaseByToAddress implements Messaging.InboundEmailHandler
{
     global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) 
     {

         // declare the result variable
         Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        
         // declare a string variable to catch our custom debugging messages
         result.success = true;
         

         list<case> casesToClose = [select suppliedEmail, owner.email, ownerId, id from case where suppliedEmail in :email.toAddresses and Owner.email = :email.fromAddress  and status = 'open' order by createdDate desc];
         

         if(!casesToClose.isEmpty())
         {
             list<attachment> attachmentsToCreate = new list<attachment>();
             
             for(case thisCase : casesToClose)
             {
                 Attachment thisAttachment = new Attachment();
                 thisAttachment.Body = blob.valueOf(email.plainTextBody);
                 thisAttachment.Description = 'Email sent from ' + email.fromAddress;
                 thisAttachment.ParentId = thisCase.id;
                 thisAttachment.ContentType = 'txt';
                 thisAttachment.Name = 'Case Close Email';
                 attachmentsToCreate.add(thisAttachment);
                 
                 thisCase.status = 'Closed';
             }
             
             update casesToClose;
             
             insert attachmentsToCreate;
         }
         else
         {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            //Set email address
            String[] toAddresses = new String[]{email.fromAddress};
            mail.setToAddresses(toAddresses);
            mail.setReplyTo('noreply@salesforce.com'); //the reply address doesn't matter
            mail.setSubject('Salesforce Case Closer Couldn\'t Close your Case!');
            mail.setBccSender(false);  //we don't want to Bcc ourselves on this
            mail.setPlainTextBody('Sorry we couldn\'t find any cases to close for that user!');

            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); //send the email    
            
            result.success = false;     
         }
     
         return result;
     }
     
     @isTest
     global static void testCloseCaseByToAddress()
     {
        closeCaseByToAddress emailProcess = new closeCaseByToAddress();
        Contact newContact = new Contact();
        newContact.FirstName = 'Test';
        newContact.LastName = 'Test';
        newContact.MobilePhone = '(999) 999-9999';
        
        insert newContact;
                 
        user caseOwner = [select id, email from user where isactive = true limit 1];
        caseOwner.email = 'myTestEmail@testtesttest.com';
        update caseOwner;
                 
        Case thisCase = new Case();
        thisCase.OwnerId = caseOwner.id; 
        thisCase.suppliedEmail = 'toemailaddress@destination.com';
        thisCase.status = 'Open';
        thisCase.subject = 'test of case closer';
        
        insert thisCase;
                 
        // create a new email and envelope object
        Messaging.InboundEmail email1 = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
                    
        //Test email with no match email
        email1.subject = '';
        email1.fromAddress = 'nomatchemailcasesanerror@errorerrorerror.com';   
        email1.fromname = 'Unit Test';
        email1.plainTextBody = 'This case is resolved';
        email1.toAddresses = new string[]{'toemailaddress@destination.com'};
        
        Messaging.InboundEmailResult noMatch = emailProcess.handleInboundEmail(email1, env);  
        system.assertEquals(noMatch.success,false);

        // create a new email and envelope object
        Messaging.InboundEmail email2 = new Messaging.InboundEmail() ;
                    
        //Test email with good email
        email2.subject = '';
        email2.fromAddress = caseOwner.email;   
        email2.fromname = 'Unit Test';
        email2.plainTextBody = 'This case is resolved';
        email2.toAddresses = new string[]{'toemailaddress@destination.com'};      

        Messaging.InboundEmailResult match = emailProcess.handleInboundEmail(email2, env);  
        system.assertEquals(match.success,true);          
     }
}