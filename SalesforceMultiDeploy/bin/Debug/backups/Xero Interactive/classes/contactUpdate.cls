global class contactUpdate 
{
    @RemoteAction
    global static map<id,contact> getContacts()
    {
        map<id,contact> contacts  = new map<id,contact>();
        list<contact> contactList = [select firstname, lastname from contact order by firstname, lastname limit 500];
        for(contact c : contactList)
        {
            contacts.put(c.id,c);
        }
        return contacts;
    }
    
    @RemoteAction
    global static map<string,string> getObjectFieldMap(string objectType)
    {
        map<string,string> fieldMap = new map<string,string>();
        Map<String, Schema.SobjectField> objectDef = Schema.getGlobalDescribe().get(objectType).getDescribe().getSObjectType().getDescribe().fields.getMap();
        for(String fieldId : objectDef.keySet())
        {
            fieldMap.put(fieldId,objectDef.get(fieldId).getDescribe().getLabel());
        }
        return fieldMap;
    }
    
    @RemoteAction
    global static map<id,saveResult> saveObject(list<sObject> objects, boolean allOrNothing)
    {
        map<id,saveResult> saveResults = new map<id,saveResult>();   
        
        Database.SaveResult[] saves = Database.update(objects, allOrNothing);
        
        for(Database.SaveResult sr: saves)
        {     
             saveResult thisResult = new saveResult();
             thisResult.success = sr.isSuccess();
             list<string> errors = new list<string>();
             if(!sr.isSuccess())
             {               
                 for(Database.Error error : sr.getErrors())
                 {
                     errors.add(error.getMessage());
                 }                 
             }
             thisResult.errors = errors;
             saveResults.put(sr.getId(),thisResult);
             
        }        
        return  saveResults;
    
    }
    
    @RemoteAction
    global static map<id,string> getContactRecordTypes()
    {
        map<id,string> recordTypes = new map<id,string>();
        list<RecordType> recordTypesQuery = [select name, id from RecordType where SobjectType = 'Contact' order by name];
        for(RecordType r : RecordTypesQuery)
        {
            recordTypes.put(r.id,r.name);
        }
        return recordTypes;
    }
    
    global class saveResult
    {
        boolean success;
        list<string> errors;
    }    

}