global class createSampleFormController 
{

    public createSampleFormController(ApexPages.StandardController controller) {

    }

    @RemoteAction
    global static contact createContact(contact newContact)
    {
        contact thisContact = new contact();
        
        insert thisContact;
        return thisContact;
    }
}