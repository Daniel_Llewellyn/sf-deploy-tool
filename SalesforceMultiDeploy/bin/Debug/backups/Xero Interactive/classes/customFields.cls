global class customFields
{

    public customFields(ApexPages.StandardController controller) {

    }


    
    @RemoteAction
    global static map<string,string> getObjects()
    {
         map<string,string> options = new map<string,string>();

        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe(); 

        for (String objectName : gd.keySet())
        {
            string label = gd.get(objectName).getDescribe().getLabel();
            options.put(objectName,label);
        }
        return options;
    }    

    @RemoteAction
    global static map<string,string> getFields(string objectTypeId)
    {
        map<string,string> options = new map<string,string>();

        Schema.sObjectType objectDef= Schema.getGlobalDescribe().get(objectTypeId).getDescribe().getSObjectType();
        Map<String, Schema.SobjectField> objectFields = objectDef.getDescribe().fields.getMap();
        
        for (String objectField : objectFields.keySet())
        {
             string label = objectFields.get(objectField).getDescribe().getLabel();
             options.put(objectField,label);
        }
        return options;
    }        
}