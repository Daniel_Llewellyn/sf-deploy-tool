public class dragToAttach {

    public dragToAttach(ApexPages.StandardController controller) {

    }

    @RemoteAction
    public static string UploadFile(string fileData, string fileName, string objectId) 
    {
        string message = 'File uploaded successfully!';
        try
        {    
            Attachment myAttachment  = new Attachment();
            fileData = fileData.substring(fileData.indexOf(',')+1,fileData.length());
            fileData = fileData.replace( ' ','+');
            blob fileContent = EncodingUtil.base64Decode(fileData);
            myAttachment.Body =fileContent;
            myAttachment.Name = fileName;
            myAttachment.ParentId = objectid;  
            insert myAttachment;
        }
        catch(exception e)
        {
            message = e.getMessage();
        }        
        return message;   
    }
    
    @RemoteAction 
    public static list<Attachment> getAttachments(string objectId)
    {
        return [select id, name from attachment where parentId = :objectId];
    }
}