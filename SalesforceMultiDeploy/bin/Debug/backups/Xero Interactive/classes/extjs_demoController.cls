global class extjs_demoController 
{
    @RemoteAction
    global static list<user> getUsers()
    {
        return [select id, firstname, lastname from user];
    }
}