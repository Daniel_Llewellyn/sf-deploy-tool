public class fusion 
{
    @remoteAction
    public static map<string,string> getObjects()
    {
        List<Schema.SObjectType> gd = Schema.getGlobalDescribe().Values();     
        map<string,string> options = new map<string,string>();
        
        for(Schema.SObjectType f : gd)
        {
            
            if(f.getDescribe().isQueryable() && f.getDescribe().isSearchable() && f.getDescribe().isDeletable())
            {
                options.put(f.getDescribe().getName(),f.getDescribe().getLabel());
            }
        }
        return options;
    }
    
    @remoteAction
    public static list<list<sObject>> soslSearch(string objectType, string searchTerm)
    {
        String searchquery = 'FIND\''+searchTerm+'*\'IN ALL FIELDS RETURNING '+objectType+'(id,name,CreatedBy.Name,CreatedDate,LastModifiedBy.Name,LastModifiedDate)'; 
        return search.query(searchquery);    
    }   
    
    @remoteAction 
    public static list<sObject> getObjectDetails(list<id> objectIds)
    {
        // Initialize setup variables
        String objectName = getObjectFromId(objectIds[0]);  // modify as needed
        String query = 'SELECT';
        Map<String, Schema.SObjectField> objectFields = getObjectFieldData(objectName);
        
        // Grab the fields from the describe method and append them to the queryString one by one.
        for(String s : objectFields.keySet()) {
           query += ' ' + s + ',';
        }
        
        
        // Strip off the last comma if it exists.
        if (query.subString(query.Length()-1,query.Length()) == ','){
            query = query.subString(0,query.Length()-1);
        }
        
        // Add FROM statement
        query += ' FROM ' + objectName;
        
        // Add on a WHERE/ORDER/LIMIT statement as needed
        query += ' WHERE id in :objectIds'; 

        system.debug(query);
        
        return database.query(query);
     
    }
    
    public static Map<String, Schema.SObjectField> getObjectFieldData(string objectType)
    {
        return Schema.getGlobalDescribe().get(objectType).getDescribe().fields.getMap();
    }
    
    
    public static string getObjectFromId(id objectId)
    {
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe(); 
        Map<String,String> keyPrefixMap = new Map<String,String>{};
        Set<String> keyPrefixSet = gd.keySet();
        string strObjId = (string) objectId;
        strObjId = strObjId.substring(0,3);
        string returnVal = null;
        for(String sObj : keyPrefixSet)
        {
           Schema.DescribeSObjectResult r =  gd.get(sObj).getDescribe();
           if(r.getKeyPrefix() == strObjId)
           {
               returnVal = r.getName();
               break;
           }
        }   
         return returnVal; 
    }
    
    
    @isTest
    public static void testFusion()
    {
        Account acc1 = new Account();
        acc1.Name = 'Primary Account';
        insert acc1;
        
        Account acc2 = new Account();
        acc2.Name = 'Child Account 1';
        acc2.AccountNumber = '12345';
        insert acc2;
        
        Account acc3 = new Account();
        acc3.Name = 'Child account 2';
        acc3.NumberOfEmployees = 500;
        insert acc3;
        
        map<string,string> objects = getObjects();
        
        list<list<sObject>> searchResults = soslSearch('account', 'primary');
        system.debug(searchResults);
        
        list<id> accountIds = new list<id>();
        accountIds.add(acc1.id);
        accountIds.add(acc2.id);
        accountIds.add(acc3.id);
        list<account> accountDetails = getObjectDetails(accountIds);
        
        list<Account> mergeChildren = new list<Account>();
        mergeChildren.add(acc2);
        mergeChildren.add(acc3);
        
        
    }
}