public with sharing class getDirections 
{
	public static String getDirections(String origin, String destination, String dataType, Boolean expandAbbrev)
	{
		String returnString;
		integer numDirections = 0;
		String dirData = '';
		String message = 'Ok';		
		Boolean success = true;
		
		
		try
		{
			//Prepare data for URL request google maps style
			origin = origin.replaceAll(' ','+');
			destination = destination.replaceAll(' ','+');
			list<string> directions = new list<string>();
						
			//create the http request
			Http httpObj = new Http();
		    HttpRequest req = new HttpRequest();
		    String url = 'http://maps.google.com/?output=kml&saddr='+origin+'&daddr='+destination;
		    req.setEndpoint(url);
		    req.setMethod('GET');
		    HttpResponse res = httpObj.send(req);   
		    
		    if(res.getStatusCode() == 200)
		    {
			    //store the response data
			    string XmlData = res.getBody();
			    
			    
			    Xmlstreamreader reader = new Xmlstreamreader(XmlData);
			    
			    //this statement is overkill. I don't need to loop over every xml element, only those contained in
			    //xml->kml->document then each placemark the heirarchy is
			    /*
			    XML
			    	- KML
			    		- Document
			    			- Style
			    			- Snippit
			    			- Placemark (repeating, one for every direction)
			    				- name <---- The data I actually want
			    				- description
			    				- address
			    */
			    
			    //so ideally I could just loop over every placemark attribute, get the name attribute that is it's
			    //direct child, then skip to the next placemark. That would be much faster.
	            if(dataType.toLowerCase() != 'rawxml')
	            {
		            while (reader.hasNext()) 
		            {
		                if (reader.getEventType() == XmlTag.START_ELEMENT && reader.getLocalName() == 'name')
		                {
		                	reader.next();
		                    directions.add(getDecodedString(reader));
		                }         
		                reader.next();
		            }
	            }

				//if the user wants to expand street abbreviations, then do that now
				if(expandAbbrev)
				{
					list<String> tempList = directions.clone();
					directions.clear();
					for(String dir : tempList)
					{
						//We have to add an extra space when parsing abbreviations so if the abbreviation is the
						//last item in the string, like "Pine Street NW" it still gets parsed. The extra space
						//shouldn't hurt anything and makes sure that the parsing happens.
						directions.add(expandDirectionsAbbreviations(dir+' '));
					}
				}
				
			    if(dataType.toLowerCase() == 'json')
			    {
			    	returnString = '{';
			    	returnString += '"SUCCESS":"'+success+'",';
			    	returnString += '"NUMDIRECTIONS":"'+directions.size()+'",';
			    	returnString += '"MESSAGE":"'+EncodingUtil.urlEncode(message,'UTF-8')+'",';
			    	returnString += '"ORIGIN":"'+EncodingUtil.urlEncode(origin,'UTF-8')+'",';
			    	returnString += '"DESTINATION":"'+EncodingUtil.urlEncode(destination,'UTF-8')+'",';
					returnString += '"DIRECTIONS": [';
						            
		            for(String dir : directions)
		            {
		            	returnString += '"'+EncodingUtil.urlEncode(dir,'UTF-8')+'",';
		            }	
		            returnString = returnString.substring(0, returnString.length() - 1);
		            returnString += ']}';   
			    }
			    else if(dataType.toLowerCase() == 'list')
			    {
		            for(String dir : directions)
		            {
		            	returnString += dir + ';';
		            	returnString = returnString.substring(0, returnString.length() - 1);
		            }			    	
			    }

			    else if(dataType.toLowerCase() == 'urllist')
			    {
		            for(String dir : directions)
		            {
		            	returnString += dir + ';';
		            	returnString = returnString.substring(0, returnString.length() - 1);
		            	returnString += '"'+EncodingUtil.urlEncode(returnString,'UTF-8')+'",';
		            }			    	
			    }
			    			    
			    else if(dataType.toLowerCase() == 'rawxml')
			    {
			    	returnString = XmlData;
			    }
			    else
			    {
			    	throw new customException('Please select a valid datatype for return data: JSON, rawXML, list or UrlList');
			    }
	    	
	    		            
		    }
		    else
		    {
		    	throw new customException('Error during http request' +res.getStatus() + ' ' +res.getStatusCode());
		    }
		}
		catch(Exception e)
		{
			success = false;
			message = 'There was an error during this request.' +e.getMessage();
		}
	    

	    return returnString;
	}

    public static String getDecodedString(Xmlstreamreader reader)
    {
        return EncodingUtil.urlDecode(reader.getText(), 'UTF-8').trim();
    }
    
    public static String expandDirectionsAbbreviations(String direction)
    {
    	direction = direction.toUpperCase();
    	direction = direction.replaceAll(' LN ', ' Lane ');
    	direction = direction.replaceAll(' DR ', ' Drive ');
		direction = direction.replaceAll(' BLVD ', ' Boulevard ');
		direction = direction.replaceAll(' RD ', ' Road ');
		direction = direction.replaceAll(' ST ', ' Street ');
		direction = direction.replaceAll(' AVE ', ' Avenue ');
		direction = direction.replaceAll(' NW ', ' North West ');
		direction = direction.replaceAll(' NE ', ' North East ');
		direction = direction.replaceAll(' SE ', ' South East ');
		direction = direction.replaceAll(' SW ',  'South West ');			
		direction = direction.replaceAll(' N ', ' North ');
		direction = direction.replaceAll(' E ', ' East ');
		direction = direction.replaceAll(' W ', ' West ');
		direction = direction.replaceAll(' S ',  'South ');
		direction = direction.replaceAll(' CR ',  'Circle ');	
		direction = direction.toUpperCase();
		
		return direction;
    }
   
	public class customException extends Exception {}
}