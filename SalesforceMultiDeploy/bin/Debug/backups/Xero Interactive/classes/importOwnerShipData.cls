global class importOwnerShipData implements Messaging.InboundEmailHandler
{	
	public class customException extends Exception {}
    
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope)
    {
    	List<List<String>> parseResult;
    	/* Data expected format
    	Col 1: vin_number (string)
    	Col 2: drive (string)
    	Col 3: model (string)
    	Col 4: modelseries (string)
    	Col 5: make (string)
    	Col 6: enginepower (integer)
    	Col 7: co2 (integer)
    	Col 8: Id name
    	*/
    	
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        try
        {   
        	//Get the contents of the file.
            if (email.binaryAttachments != null && email.binaryAttachments.size() > 0)
            {             
            	//loop over every attachment 
                for (integer i = 0 ; i < email.binaryAttachments.size() ; i++) 
                {
                	//try/catch to prevent one bad file from killing the whole process
                	try
                	{
                		//parse the contesnts of the file
						parseResult = parseCSV(email.binaryAttachments[i].body.toString(), false);
			            
			            //list<Vehicle__c> vehicles = new list<Vehicle__c>();
			            list<Account> owners = new list<Account>();
			            		            			            
			            list<Ownership__c> ownerships = csvToOwnership(parseResult);
			            
			            database.saveResult[] saveResult = database.insert(ownerships,false);
			            
			            integer success = 0;
			            integer failures = 0;
			            list<string> errors = new list<string>();
			            for(database.saveResult r : saveResult)
			            {
			            	if(r.isSuccess())
			            	{
			            		success++;
			            	}
			            	else
			            	{
			            		failures++;
			            		for(Database.Error e : r.getErrors())
			            		{
			            			errors.add(e.getMessage());
			            		}
			            	}
			            }
			            
			            
			            sendEmail(email.replyTo, 'Your file has been parsed and imported. Result summary can be seen below. \n'+success+' success \n'+failures+' failures. \nErrors\n'+errors, 'Ownership data imported completed' );
                	}
                	catch(Exception e)
                	{
                		sendEmail(email.replyTo, 'Error importing ownership data ' +e.getMessage(), 'Error importing ownership data' );
                	}
                }
            
            }


            //send email to let user know their shit uploaded
            
        }
        catch(Exception e)
        {
            sendEmail(email.replyTo, 'Error importing ownership data ' +e.getMessage(), 'Error importing ownership data' );
        }     
        return result;
    }
    
    public static list<Ownership__c> csvToOwnership(List<List<String>> parsedCSV)
    {
    	list<Ownership__c> ownerships = new list<Ownership__c>();
    	list<string> headers = new list<string>();
    	
    	for(list<string> row : parsedCSV)
    	{
    		for(string col : row)
    		{
    			headers.add(col);
    		}
    		break;
    	}
    	   	
    	integer rowNumber = 0;
    	for(list<string> row : parsedCSV)
    	{
    		if(rowNumber == 0)
    		{
    			rowNumber++;
    			continue;
    		}
    		else
    		{
	    		Ownership__c thisOwnership = new Ownership__c();
	    		integer colIndex = 0;
	    		for(string col : row)
	    		{
	    			string headerName = headers[colIndex];
	    			if(headerName.length() > 0)
	    			{
		    			try
		    			{						
							thisOwnership.put(headerName,col);
			    			colIndex++;
		    			}
		    			catch(exception e)
		    			{
		    				system.debug('Invalid field specified in header' + headerName);
		    			}
	    			}
	    		} 
	    		ownerships.add(thisOwnership);
	    		rowNumber++;
    		}  		
    	}
    	return ownerships;
    }
    	
	public static List<List<String>> parseCSV(String contents,Boolean skipHeaders) 
	{
		List<List<String>> allFields = new List<List<String>>();
	
		// replace instances where a double quote begins a field containing a comma
		// in this case you get a double quote followed by a doubled double quote
		// do this for beginning and end of a field
		contents = contents.replaceAll(',"""',',"DBLQT').replaceall('""",','DBLQT",');
		// now replace all remaining double quotes - we do this so that we can reconstruct
		// fields with commas inside assuming they begin and end with a double quote
		contents = contents.replaceAll('""','DBLQT');
		// we are not attempting to handle fields with a newline inside of them
		// so, split on newline to get the spreadsheet rows
		List<String> lines = new List<String>();
		try 
		{
			lines = contents.split('\n');
		} 
		catch (System.ListException e) 
		{
			System.debug('Limits exceeded?' + e.getMessage());
		}
		Integer num = 0;
		for(String line : lines) 
		{
			// check for blank CSV lines (only commas)
			if (line.replaceAll(';','').trim().length() == 0) break;
			
			List<String> fields = line.split(';');	
			List<String> cleanFields = new List<String>();
			String compositeField;
			Boolean makeCompositeField = false;
			for(String field : fields) 
			{
				if (field.startsWith('"') && field.endsWith('"')) 
				{
					cleanFields.add(field.replaceAll('DBLQT','"'));
				} 
				else if (field.startsWith('"')) 
				{
					makeCompositeField = true;
					compositeField = field;
				} 
				else if (field.endsWith('"')) 
				{
					compositeField += ',' + field;
					cleanFields.add(compositeField.replaceAll('DBLQT','"'));
					makeCompositeField = false;
				} 
				else if (makeCompositeField) {
					compositeField +=  ',' + field;
				} 
				else 
				{
					cleanFields.add(field.replaceAll('DBLQT','"'));
				}
			}
			
			allFields.add(cleanFields);
		}
		if (skipHeaders) allFields.remove(0);
		return allFields;		
	}

    public static void sendEmail(string sendTo, string emailBody, string emailTitle)
    {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        //Set email address
        list<string> toAddresses = new list<string>();
        toAddresses.add(sendTo);
        mail.setToAddresses(toAddresses);
        mail.setReplyTo('noreply@salesforce.com'); //the reply address doesn't matter
        mail.setSubject(emailTitle);
        mail.setBccSender(false);  //we don't want to Bcc ourselves on this
        mail.setPlainTextBody(emailbody);
   
        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); //send the email    
    }
    
    public class ownership
    {
    	string vin;
    	string ssn;	
    }
    
}