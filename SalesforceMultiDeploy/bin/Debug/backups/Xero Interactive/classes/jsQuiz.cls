global class jsQuiz 
{
      
    @RemoteAction
    public static string getQuestionJson(string jsonURL)
    {
        string returnString;
        try
        {
            HttpRequest req = new HttpRequest();
            req.setMethod('GET');  
            req.setEndpoint( jsonURL );
            Http http = new Http();
            if(jsonUrl != 'test')
            {
                HTTPResponse res = http.send(req);            
                returnString = res.getBody();
            }
            else
            {
                returnString = '{success : true}';
            }
        }
        catch(Exception ex)
        {
            returnString = ex.getMessage();
        }
        return returnString;
    }
    
    static testMethod void testJsQuiz()
    {
        //run a test call
        string resultString = getQuestionJson('test');
        system.assertEquals(resultString,'{success : true}');
        
        //run an error call
        string resultStringError = getQuestionJson(null);
        
    }
}