public class jsonp_controller
{
    public string returnFunction{get;set;}
    
    public pageReference getReturnFunction()
    {
        //get the parameters from the get/post request and stash em in a map
        map<string,string> params  = ApexPages.currentPage().getParameters();
        
        //set your data to return here
        string returnData = 'blah';
        
        if(params.containsKey('callback'))
        {
            returnFunction = params.get('callback') + '(' + returnData + ');';
        }
        
        return null;
    }
    
    @isTest
    public static void test_jsonp_controller()
    {
        jsonp_controller controller = new jsonp_controller();
        system.assertEquals(controller.returnFunction,'blah');
        
        
    }
}