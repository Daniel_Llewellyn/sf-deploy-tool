global class  leadClasses implements Schedulable
{	

	global void execute(SchedulableContext ctx) 
	{
		updateLeadStatus();
	}

	global boolean updateLeadStatus()
	{
		DateTime UpdateDate = System.now();
		boolean success = true;
		string emailMessage;
		string emailrecip = 'laura@crowdgravity.com';
		try
		{
			List<Lead> openBounceItems = [Select Id, Email, Status From Lead WHERE EmailBouncedDate != Null FOR UPDATE];
		
			emailMessage = 'Found ' + openBounceItems.size() + ' leads to set status on. ';

			for (Lead l: openBounceItems ) 
			{
					system.debug('Setting status on ' + l.id);
					l.Status = 'Bad Contact Info';
					l.notes__c = l.notes__c + '\r\nEmail '+l.email+' was found to be bad on ' +UpdateDate.format('EEEE, MMMM d, yyyy');
			}
			database.update(openBounceItems,false);

			//Send an email to the User after your batch completes
			Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
			String[] toAddresses = new String[] {emailrecip};
			mail.setToAddresses(toAddresses);
			mail.setSubject('Salesforce Lead Statuses Updated');
			mail.setPlainTextBody(emailMessage);
			Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
					
		}
		catch(Exception e)
		{
			success = false;
		}
		
		return success;
	}
}