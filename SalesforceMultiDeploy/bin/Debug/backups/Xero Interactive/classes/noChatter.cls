public class noChatter
{
    public static map<id,boolean> getUserChatterEnabledFlag(set<id> userIds)
    {
        map<id,boolean> userChatterEnabledMap = new map<id,boolean>();
        
        for(list<user> users : [select id, Chatter_Enabled__c from user where id in :userIds])
        {
            for(user u : users)
            {
                userChatterEnabledMap.put(u.id,u.Chatter_Enabled__c);
            }
        }
        return userChatterEnabledMap;
    }
    
    public static void blockChatterDisabledUsersFromPosting(list<sobject> post)
    {
        set<id> userIds = new set<id>();
        //the user id can be stored in either the createdById field or the parentId field depending on the type of chatter object
        //passed in. We'll use some ternary logic to make a guess at which one it is.
        string userIdField = post[0].get('createdById') != null ? 'createdById' : 'parentId';
        
        //get a list of the all the record owners
        for(sObject record : post)
        {
           userIds.add((id) record.get(userIdField));
        }    
       
        //get a map of who is chatter enabled and who isn't
        map<id,boolean> userIsChatterEnabled = getUserChatterEnabledFlag(userIds);    
        
        for(sObject record : post)
        {
           //figure out who owns this post
           id userId = (id) record.get(userIdField);

           //check to see if the map has an entry for this use (I don't know how it wouldn't, but better safe than sorry) and if their allow chatter flag is set to false
           //then deny them with the error messages specified in the custom setting.
           if(userIsChatterEnabled.containsKey(userid) && userIsChatterEnabled.get(userid) == false)
           {
               record.addError(Label.NoChatter_Deny_Message);
           }
        }     
    }
}