@isTest
public class noChatterTest
{
   
    static testMethod void noChatterTest()
    {
        list<FeedItem> posts = new list<FeedItem>();
        
        
        Profile thisProfile = [select id from profile where name = 'System Administrator'];
       
        User testUser = new User();
       
        String username = 'TestGuy';
        String domain = 'TestDomain';
   
        username += string.valueOf(Math.random());
        domain += string.valueOf(Math.random());
 
        testUser.username = username+'@'+domain+'.com';
       
        testUser.alias = 'Test';
        testUser.communityNickname = username;
        testUser.firstname = 'Test';
        testUser.lastname = 'Guy';
        testUser.email = 'Test@Test.com';
        testUser.LocaleSidKey = 'en_US';
        testUser.languageLocaleKey = 'en_US';
        testUser.EmailEncodingKey = 'ISO-8859-1';
        testUser.TimeZoneSidKey = 'GMT';
        testUser.profileid = thisProfile.id;
        testUser.Chatter_Enabled__c = true;
        insert testUser;

        id successFeedItemId;
        
        System.runAs ( testUser ) {
            //create a post and insert it. there should be no problem since this users flag is set to allow chatter posts.
            FeedItem fItem = new FeedItem();
            fItem.Type = 'TextPost';
            fItem.ParentId = testUser.id;
            fItem.Body = 'hello there friend. This is a test';
            insert fItem;         
            successFeedItemId = fItem.id;
            
            FeedComment fComment = new FeedComment();
            fComment.commentBody = 'test test test';
            fComment.CommentType = 'TextComment';
            fComment.feedItemId = successFeedItemId;
            insert fComment;
        
        }
        
        testUser.Chatter_Enabled__c = false;
        update testUser;
        
        boolean fItemBlocked = false;
        boolean fCommentBlocked = false;
        
        System.runAs ( testUser ) {
        
            try
            {
                //create a post and insert it. there should be no problem since this users flag is set to allow chatter posts.
                FeedItem fItem = new FeedItem();
                fItem.Type = 'TextPost';
                fItem.ParentId = testUser.id;
                fItem.Body = 'hello there friend. This is a test';
                insert fItem;                    

            }
            catch(exception e)
            {
                fItemBlocked = true;
            }
                      
            try
            {
                FeedComment fComment = new FeedComment();
                fComment.commentBody = 'test test test';
                fComment.CommentType = 'TextComment';
                fComment.feedItemId = successFeedItemId;
                insert fComment;
            }
            catch(exception e)
            {
                fCommentBlocked = true;
            }
        } 
        system.assertEquals(fItemBlocked,true,'Post was not blocked. Something is wrong with noChatter');
        system.assertEquals(fCommentBlocked,true,'Comment was not blocked. Something is wrong with noChatter');
    }  
}