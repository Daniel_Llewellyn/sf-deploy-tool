@RestResource(urlMapping='/restService/v1/*') 
global class ownershipImportService 
{
    
    //Methods for retreiving data from the system. All query methods accept 
    // fields parameter which specifies data returned in the query. All methods also accept
    // a filter param which can accepts an SOQL where string to refine results.
    //Methods include:
    
    // /customer - get all customers
    // /customer/Id - get customer based on dmp code, SSN, or Salesforce Record ID
 
    // /car - get all cars
    // /car/Id - get car by VIN or Salesforce record Id

    // /ownership - get all ownerships
        
    //Ex: https://cs7.salesforce.com/services/apexrest/restService/v1/customer?fields=name,firstname,email&filter=firstname='frank' 
    //get the name, firstname, and email for any contact with a firstname frank
    
    //Ex: https://cs7.salesforce.com/services/apexrest/restService/v1/car?filter=type__c='truck'
    //Find all cars that are of type truck
    
    //EX: https://cs7.salesforce.com/services/apexrest/restService/v1/ownership?filter=SSN__r.id='123456' 
    //Get all ownerships where the contact id is 123456
    
    //EX: https://cs7.salesforce.com/services/apexrest/restService/v1/ownership/?SSN__r.SSN__c='121212'
    //Get all ownerships where the contact SSN is 121212 
    
    
    @HttpGet
    global static list<sObject> doGet(RestRequest req, RestResponse res) 
    {
        list<sObject> returnValues = new list<sObject>();
        string url= req.requestURI;
        if(url.indexOf('/v1/') > 0)
        {   
            url = url.substring(url.indexOf('/v1/')+3,url.length());
        }        
        list<String> URLParams = url.split('/');
        
     
        String queryObject = URLParams[1];            
        string queryFields = 'id,name';    
        string queryFilter;      
        string sObjectType;
        
        
        //variouns branch statments to handle each object case.  
        if(queryObject == 'customer')
        {
            sObjectType = 'Account';
            if(URLParams.size() > 2)
            {
                if(isValidId(URLParams.get(2)))
                {
                    queryFilter = 'where id = \''+URLParams[2]+'\' limit 1';     
                } 
                else
                {
                    queryFilter = 'where SSN__c = \''+URLParams[2]+'\' or PersonEmail = \''+URLParams[2]+'\' limit 1';    
                }   
                
                queryFields = 'SSN__c, personEmail, phone, id';      
            }                          
        }
        else if(queryObject == 'car')
        {
            sObjectType = 'Auto__c';
            if(URLParams.size() > 2)
            {
                if(isValidId(URLParams.get(2)))
                {
                    queryFilter = 'where id = \''+URLParams[2]+'\'';     
                } 
                else
                {
                    queryFilter = 'where VIN__c = \''+URLParams[2]+'\'';    
                }
            }                    
        }

        else if(queryObject == 'ownership')
        {
            sObjectType = 'ownership__c';
            if(URLParams.size() > 2)
            {
                queryFilter = 'where SSN__r.SSN__c = \''+URLParams[2]+'\' or VIN__r.VIN__c = \''+URLParams[2]+'\' limit 1';    
            }
            queryFields = 'VIN__c, SSN__c, Status__c';              
        }
        
        else
        {
            throw new myException('Unsupported object  "'+queryObject+'" specified in query call. Supported objects are customer, ownership and car');
        }

        //If the user wants to override the default selected fields, let them.
        if (req.params.containsKey('fields'))
        {
            queryFields = req.params.get('fields');
        }
        
        //if the filter is null, that will cause an error, so just set it to a space
        if(queryFilter == null)
        {
            queryFilter = ' ';
        }
        
        //run the query                        
        returnValues = querySObject(sObjectType, queryFields, queryFilter);
                            
        return returnValues;
    }

    @HttpPost
    global static list<sObject> doPost(RestRequest req, RestResponse res) 
    {
        list<sObject> returnValues = new list<sObject>();
        string url= req.requestURI;
        if(url.indexOf('/v1/') > 0)
        {   
            url = url.substring(url.indexOf('/v1/')+3,url.length());
        }        
        list<String> URLParams = url.split('/');  
        
        //The type of object they are querying for is held in the 7th element of the array
        String queryObject =URLParams[5];  
        string sObjectType;
                
        //variouns branch statments to handle each object case.  
        if(queryObject == 'customer')
        {
            sObjectType = 'Account';                       
        }
        else if(queryObject == 'car')
        {
            sObjectType = 'Auto__c';             
        }

        else if(queryObject == 'ownership')
        {
            sObjectType = 'ownership__c';     
        }
        
        else
        {
            throw new myException('Unsupported object  "'+queryObject+'"  specified in query call. Supported objects are customer, ownership and car');
        }
      
        returnValues = saveSObject(sObjectType, null, req);
        
        return returnValues;
    }


    //method for updating objects..
    
    //EX: https://cs7.salesforce.com/services/apexrest/restService/v1/account/001M0000008UNPl/?firstname=test&ssn__c=12345
    
    @HttpPut
    global static list<sObject> doPut(RestRequest req, RestResponse res) 
    {
        list<sObject> returnValues = new list<sObject>();
        string url= req.requestURI;
        if(url.indexOf('/v1/') > 0)
        {   
            url = url.substring(url.indexOf('/v1/')+3,url.length());
        }        
        list<String> URLParams = url.split('/');
        
        
        if(URLParams.size() < 3)
        {
            throw new myException('Bad URI: ' + req.requestURI + '. Parsed as ' + url + ' list has ' + URLParams.size() + ' elements');
        }        
        
        //The type of object they are querying for is held in the 7th element of the array
        String queryObject =URLParams[5];  
        string sObjectType;
        String recordId; 
        
        if(!isValidId(URLParams.get(6)))
        {     
            throw new myException('Valid Salesforce Record ID required for update call. Please provide an ID. Provided '+ URLParams.get(8));       
        }
        else
        {
          recordId = URLParams[2];            
        }
                
        //variouns branch statments to handle each object case.  
        if(queryObject == 'customer')
        {
            sObjectType = 'Account';                       
        }
        else if(queryObject == 'car')
        {
            sObjectType = 'Auto__c';             
        }

        else if(queryObject == 'ownership')
        {
            sObjectType = 'ownership__c';     
        }
        
        else
        {
            throw new myException('Unsupported object  "'+queryObject+'"  specified in query call. Supported objects are customer, ownership and car');
        }
      
        returnValues = saveSObject(sObjectType, recordId, req);       
        return returnValues;
    }
    
        
    public static list<sObject> querySObject(string objectType, string queryFields, string queryFilter)
    {
        list<sObject> sObjects = new list<sObject>();     
        string queryString = 'select ' + queryFields + ' from ' + objectType + ' ' + queryFilter;
        sObjects = Database.query(queryString);
        return sObjects;
    }   

    public static list<sObject> saveSObject(string objectType, string recordid, RestRequest req)
    {
        list<sObject> sObjects = new list<sObject>();
        
        Schema.sObjectType objectDef = Schema.getGlobalDescribe().get(objectType).getDescribe().getSObjectType();
        
        Map<String, Schema.SobjectField> ObjectFieldsMap = objectDef.getDescribe().fields.getMap();
        
        sObject updateObj;       
        
        if(recordId != null)
        {
            updateObj = objectDef.newSobject(recordid);
        }
        else
        {
            updateObj = objectDef.newSobject();
        }    
        // populate the object's fields
        for (String key : req.params.keySet())
        {
            // only add params if they are valid field on the object and not on the no-update list
            if (ObjectFieldsMap.containsKey(key))
            {
                updateObj.put(key,req.params.get(key)); 
            }
            else
            {
                system.debug('Invalid field: '+ key + ' for object type ' + objectType);
            }
        }
        upsert updateObj;
        sObjects.add(updateObj);
        
        return sObjects;
        
    }

    public static Boolean isValidId(String s) 
    {
        Id validId;
        try 
        {
           validId = s;
           return true; 
        } 
        catch (Exception ex) 
        {
            return false;
        }
    } 
        

    public class myException extends Exception {}

   
}