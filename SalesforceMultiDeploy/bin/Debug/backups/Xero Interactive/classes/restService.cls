@RestResource(urlMapping='/cloudSpokes/*')
global class restService 
{
	public static sObject thisObj{get;set;}
		
	@HttpGet
	global static list<sObject> getRecord(RestRequest req, RestResponse res)
	{
		//No idea if this is the right way to get url arguments. Seems clunky and wrong but its from the sample code
		string objectType = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
		string objectId = req.requestURI.substring(req.requestURI.lastIndexOf('/')+2);
		
		string queryString = 'select name, id from ' + objectType + ' where id = \''+objectId+'\''; 
		list<sObject> objects = Database.query(queryString);
		
		return objects;	
	}
	
	@HttpPost
	global static sObject createRecord(RestRequest req, RestResponse res)
	{
		
		return thisObj;
	}
	
	@HttpPut
	global static sObject updateRecord(RestRequest req, RestResponse res)
	{
		
		return thisObj;
	}
	
	@HttpDelete
	global static void deleteRecord(RestRequest req, RestResponse res)
	{
		//No idea if this is the right way to get url arguments. Seems clunky and wrong but its from the sample code
		string objectType = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
		string objectId = req.requestURI.substring(req.requestURI.lastIndexOf('/')+2);
		
		string queryString = 'select name, id from ' + objectType + ' where id = \''+objectId+'\''; 
		list<sObject> objects = Database.query(queryString);
		
		delete objects;
	}	

}