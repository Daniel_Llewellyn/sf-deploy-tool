global class scarletWestController 
{

    @RemoteAction
    global static list<SW_Player__c> getPlayer(string email)
    {
        return [select 
                                        Avatar__c, 
                                        Email_Address__c, 
                                        Hit_Points__c, 
                                        Score__c, 
                                        Top_offset__c,
                                        Left_Offset__c,
                                        World_Cell__c
                                        from SW_Player__c 
                                        where Email_Address__c = :email];
    }
        
    @RemoteAction
    global static list<SW_Cell__c> getCell(id world, integer lat, integer lng)
    {
        return  [select 
                    Height__c, 
                    Width__c, 
                    Id, 
                    Background_Texture_Document_Name__c, 
                    name, 
                    (select Height__c, Image_Fill_Method__c, Item_Image__c, Left_Offset__c, Passable__c, Top_offset__c, Width__c from CellItems__r)
                    from SW_Cell__c 
                    where World__c = :world and 
                    latitude__c = :lat and 
                    longitude__c = :lng];
    }

    @RemoteAction
    global static list<SW_Behavior__c> getBehaviors(id cellItem)
    {
        return [select id, 
                                     name, 
                                     Scarlet_West_Behavior_Script__r.name,
                                     Scarlet_West_Behavior_Script__r.Javascript_Function__c,
                                     Scarlet_West_Behavior_Script__r.Trigger__c,
                                     Scarlet_West_Behavior_Script__r.Additional_Data__c
                                     from SW_Behavior__c 
                                     where Scarlet_West_Cell_Item__c = :cellItem]; 
    }
    
    @RemoteAction
    global static string getDocument(string documentName)
    {   
        string returnString;
        try
        {
            list<Document> docs = [select id, body from Document where name = :documentName limit 1]; 
            if(!docs.isEmpty())
            {
                returnString = EncodingUtil.base64Encode(docs[0].body); 
            }    
        }
        catch(exception e)
        {
            returnString = 'Error Type: ' + e.getTypeName() + ' ' + e.getCause() + ' ' + ' on line: ' +e.getLineNumber();        
        } 
        return returnString;    
    }   
    
    @isTest
    global static void testScarletWestController()
    {
        id worldId;
        id cellId;
        list<SW_Player__c> thisPlayer = getPlayer('Kenji776@gmail.com');
        list<SW_Cell__c> thisCell = getCell(worldId, 0, 0);
        list<SW_Behavior__c> thisBehavior = getBehaviors(cellId); 
        string docData = getDocument('test');
    }
}