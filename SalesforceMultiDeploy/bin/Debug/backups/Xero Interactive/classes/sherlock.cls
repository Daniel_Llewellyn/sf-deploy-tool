/* Sherlock Salesforce Gloabl Search Utility Controller
Description: Sherlock is a Salesforce search replacment utility that can perform full text searches against any searchable object, including
             searching the content of uploaded content documents and chatter posts. The interface is provided via a jQuery plugin on a visualforce page.
Author: Daniel Llewellyn/Kenji776 (@Kenji776)
Date: 12/05/2012

*/

global class sherlock
{
    //wrapper object to standardize search results. The search program expects to receivea list of these. 
    //all the data found from the original sObject is included as well in the objectData property
    global class searchResult
    {
        string thumbnail = '';
        string title = '';
        string description = '';
        string link = '';
        integer size = 0;
        string type = '';
        id recordId;
        sObject objectData;
    }
    
    
    /*remote method to be accessed via javascript for performing a search.
      arguments
          searchTerm: a string at least two characters long (not including wildcards) to search for
          objectList: a list/array of sObject names which to query for
          objectLabels: a map/javascript object of sObject names to a friendly label to use in the result. Multiple sObjects can have the same label and will be returned in the same group.
          typeLimit: a maxmimum amount of each kind of object to find. This is per sObject type, not per label or for the total search.  
    */      
    @RemoteAction
    global static map<string,list<searchResult>> globalSearch(string searchTerm, list<string>objectList,map<string,string> objectLabels, integer typeLimit)
    {
        system.debug('\n\n\n-------------------- Entering Global Search Method');
        //map to hold the results. The results will have the objectLabels or names (if no label for that object type is provided) and a list of search results for it.
        map<string,list<searchResult>> results = new  map<string,list<searchResult>>();
        try
        { 
            //string to hold the SOSL querystring.
            string customObjectQuery = '';
            
            //for each object passed in, build a querystring addition that should be safe in all circumstances
            for(string obj : objectList)
            {
                customObjectQuery += obj+'(Name,Id,createdById,CreatedDate LIMIT '+typeLimit+'),';
            }
            
            //append the last of the information we need to our querystring, including stuff for finding documents, feed items and users.
            string objectsQuery = 'FIND \''+searchTerm+'\' IN ALL FIELDS RETURNING '+customObjectQuery+' FeedItem(id, title, body, InsertedBy.Name, InsertedById LIMIT '+typeLimit+'), ContentVersion(ContentDocumentId,ContentSize,ContentUrl,Description,IsLatest,PublishStatus,TagCsv,Title LIMIT '+typeLimit+'), User(id, name, SmallPhotoUrl, email  LIMIT '+typeLimit+')';
            
            //find all objects where the name matches the search term.
            List<List<SObject>> searchList = search.query(objectsQuery);
                                
            string objType = '';
            string objLabel = '';
            
            //iterate over all the search results so we can build searchResult items from them.
            //the search result is broken in a list of sObjects, each of which contain a list of results. 
            //similar to how we want to evenetually return the data, but we gotta do some formatting to it first.
            for(List<sObject> thisObjList : searchList)
            {
                for(sObject thisObj : thisObjList)
                {
                    //create a new search result object.
                    searchResult thisResult = new searchResult();
                    
                    //find the sObject type of this object.
                    objType = string.valueOf(thisObj.getsObjectType());
                    objLabel = objType;
                    
                    //set some default information that all returned objects should have.
                    thisResult.type = objType;
                    thisResult.recordid = (id) thisObj.get('Id');
                    thisResult.link = URL.getSalesforceBaseUrl().toExternalForm() + '/' + (string) thisObj.get('Id');
                    thisResult.objectData = thisObj;
                    
                    //if a label for this object type was provided set it now. Otherwise the name of the object is also the label.
                    if(objectLabels.containsKey(objType))
                    {
                        objLabel = objectLabels.get(objType);
                    }
                    
                    //if this result is a content version object then set its properties accordingly.            
                    if(objType == 'ContentVersion')
                    {
                        thisResult.title = (string) thisObj.get('Title');
                        thisResult.description = (string) thisObj.get('Description');
                        thisResult.link = URL.getSalesforceBaseUrl().toExternalForm() + '/' +(string) thisObj.get('ContentDocumentId');
                        thisResult.recordid = (id) thisObj.get('ContentDocumentId');
                        thisResult.size = (integer) thisObj.get('ContentSize');
                    }
                    
                    //if this result is a user object then set its properties accordingly, including setting the photo
                    else if(objType == 'User')
                    {
                        thisResult.thumbnail = (string) thisObj.get('SmallPhotoUrl');
                        thisResult.title = (string) thisObj.get('name');                                     
                    }
                    
                    //if this result is a content version object then set its properties accordingly, including getting the posting users name to 
                    //use for the title.  
                    else if(objType == 'FeedItem')
                    {    
                        sObject postingUser = thisObj.getsObject('insertedBy');                      
                        thisResult.title = 'Post by ' + postingUser.get('Name');
                        thisResult.description = (string) thisObj.get('body');
                    }
                    
                    //if it is any other object type, then just grab the name and set it as the title. You could also set some kind of description here, maybe
                    //including the created date or something.
                    else
                    {
                        thisResult.title = (string) thisObj.get('Name');             
                    }
                    
                    
                    //append this search result to the proper list using the object label as the key to find the right list.
                    if(!results.containsKey(objLabel))
                    {
                        results.put(objLabel,new list<searchResult>());
                    }
                    results.get(objLabel).add(thisResult);                
                }
            }                          
        }
        catch(Exception e)
        {
            system.debug('\n\n\n\n ----------------------------- ERROR!!!!');
            system.debug(e);
        }
        
        return results;
    }
    
    
    //function for finding all the objects in this org that can be searched for. This includes custom objects
    //returns them as a map of the object label to the object name. Meant to be invoked via the interface at runtime.
    @remoteAction
    global static map<string,string> getSearchableObjects()
    {
        //list of object labels and names to return
        map<string,string> searchableObjects = new map<string,string>();
        
        //global describe of all objects in the org.
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe(); 
        
        //iterate over all objects
        for(string objType : gd.keySet())
        {
            //only add this object type if it is searchable, since this list will be used to create an interface
            //where the user can select which objects to search. No sense in including non searchable objects eh?
            if(gd.get(objType).getDescribe().isSearchable())
            {
                searchableObjects.put(gd.get(objType).getDescribe().getLabel(),objType);
            }
        }
        return searchableObjects;
    }
    
    @isTest
    global static void testSherlock()
    {
        //array for holding the Ids of objects that will be returned from the SOSL query
        list<id> fixedSearchResults= new list<id>();
        
        //get a list of searchable objects
        map<string,string> objectsList = getSearchableObjects();

        //create a content document
        ContentVersion testContent =new ContentVersion(); 
        testContent.ContentURL='http://www.google.com/'; 
        testContent.Title ='Test'; 
        testContent.Description = 'Test Document';
        insert testContent;     
        
        fixedSearchResults.add(testContent.id);    
        
        //create an account
        Account testAccount = new Account();
        testAccount.name = 'Test';
        insert testAccount;
        
        fixedSearchResults.add(testAccount.id);    
         
        //create a user
        User createUser;
    
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
            Profile p = [select id from profile where name = 'System Administrator'];
            createUser = new User(alias = 'test', email='standarduser@testorg.com', 
                emailencodingkey='UTF-8', lastname='Test', 
                languagelocalekey='en_US', 
                localesidkey='en_US', profileid = p.Id,
                timezonesidkey='America/Los_Angeles', 
                username='standarduser'+Datetime.Now().getTime()+'@testorg.com');
                
            insert createUser;
            fixedSearchResults.add(createUser.id);    
        }
               
        
        //create a chatter post
        FeedItem fitem = new FeedItem();
        fitem.type = 'TextPost';
        fitem.ParentId = UserInfo.getUserId();
        fitem.Title = 'Test';  //This is the title that displays for the LinkUrl
        fitem.body = 'Test';

        insert fitem;     
        
        fixedSearchResults.add(fItem.id);    
        
        string[] objectsToSearch = new  string[]{'Account'};
        map<string,string> labels = new map<string,string>();
        labels.put('Account','Clients');

        //set the results of the SOSL query
        Test.setFixedSearchResults(fixedSearchResults);
               
        map<string,list<searchResult>> searchResults = sherlock.globalSearch('test*', objectsToSearch ,labels, 20);  
        
        system.assertEquals(searchResults.containsKey('Clients'),true,'Custom label key for accounts not found!');
        
        system.assertEquals(searchResults.get('Clients').size(),1,'Unexpected amount of accounts returned for search');
        
        system.assertEquals(searchResults.get('ContentVersion').size(),1,'Unexpected amount of ContentVersion objects (CRM content) returned for search');
        
        system.assertEquals(searchResults.get('User').size(),1,'Unexpected amount of User records returned for search');     
    }
}