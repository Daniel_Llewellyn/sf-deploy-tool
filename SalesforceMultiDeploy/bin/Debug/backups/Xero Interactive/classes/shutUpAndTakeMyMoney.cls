global class shutUpAndTakeMyMoney
{
    @remoteAction 
    public static list<sObject> runQuery(string query)
    {
        return database.query(query);
    }

    //gets a single attachment (photo) by id. The data is returned as a base64 string that can be plugged into an html img tag to display the image.
    @RemoteAction
    global static remoteObject getAttachment(id attachmentId)
    {   
        remoteObject returnObj = new remoteObject();

        list<Attachment> docs = [select id, body from Attachment where id = :attachmentId limit 1]; 
        if(!docs.isEmpty())
        {
            returnObj.data = EncodingUtil.base64Encode(docs[0].body); 
        }    

        return returnObj;    
    }   

    global class remoteObject
    {
        public boolean success = true;
        public string message = 'operation successful';
        public string data = null;
        public list<sObject> sObjects = new list<sObject>();
    } 
        
    @isTest
    global static void shutUpAndTakeMyMoney()
    {
        Account testAccount = new Account();
        testAccount.name = 'test account';
        insert testAccount;
        
        //test run query function
        list<sObject> accounts = runQuery('select id, name from account');
        system.assertEquals(accounts.isEmpty(),false);
        
        Attachment a = new Attachment();
        a.name='test';
        a.parentId=testAccount.id;
        a.body= blob.valueOf('this is gonna be one corrupt file');
        
        insert a;
        
        remoteObject getAttachment = getAttachment(a.id);

        
    }
}