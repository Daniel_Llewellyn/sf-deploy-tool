public class testContact {

    public String sortField{get { 
        if(sortField==null)
        {
            sortField = 'name';
        }
        return sortField;
    }set;}
    
    public String sortDir{get { 
        if(sortDir==null)
        {
            sortDir = 'asc';
        }
        return sortDir;
    }set;}


    public List<contact> contactList
    {
        get
        {
            string queryString;
            List<contact> objs;
            try
            {
                queryString = 'select ' + getQueryFields() + ' from contact order by ' + sortField + ' ' + sortDir;
                objs = database.query(queryString);
            }
            catch(Exception ex)
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Error running query' + ex.getMessage() + ' ' + queryString));
            }
            return objs;
        }
        set;
    }

    public string getQueryFields()
    {
        return ' id,firstname,lastname,email,phone ';    
    }
    
  
    public testContact(ApexPages.StandardController controller) {

    }

}