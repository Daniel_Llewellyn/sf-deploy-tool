@RestResource(urlMapping='/testPublicRest')
global class testPublicRest {
    @HttpGet
    global static String doGet() {
        String name = RestContext.request.params.get('name');
        return 'Hello '+name;
    }


    @isTest
    global static void testRespondentPortal()
    {
        // set up the request object
        System.RestContext.request = new RestRequest();
        System.RestContext.response = new RestResponse();
        
        //First lets try and create a contact.
        RestContext.request.requestURI = '/testservice';
        RestContext.request.params.put('name','test');
        //send the request
        testPublicRest.doGet();
    }
}