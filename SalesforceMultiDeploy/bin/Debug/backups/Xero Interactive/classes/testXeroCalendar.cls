@isTest
global class testXeroCalendar{

    @isTest
    public static void testFullCalendar()
    {
        //lets test some of our methods to make sure they work properly before we do anything else
        
        //first lets see if our most basic filter evaluation works. Lets evaluate the firstname field on the contact. It should come back wrapped in quotes.
        string testSingleString = xeroCalendarController.evalFilterValue('bob', 'contact', 'firstname', 'equal');
        system.assertEquals(testSingleString, '\'bob\'');
        
        //okay, now lets test a single number 
        string testSingleNumber = xeroCalendarController.evalFilterValue('10', 'Opportunity', 'TotalOpportunityQuantity', 'equal');
        //system.assertEquals( integer.valueOf(testSingleNumber), 10);
        
        //sweet, lets test a list of strings as you would use for an in/contains statment
         string testListStrings = xeroCalendarController.evalFilterValue('bob,frank,jones', 'Contact', 'Firstname', 'in');
        system.assertEquals(testListStrings, '(\'bob\',\'frank\',\'jones\')');

        //finally lets test a set of numbers
        string testListNumber = xeroCalendarController.evalFilterValue('10,15,20', 'Opportunity', 'TotalOpportunityQuantity', 'not in');
        system.assertEquals(testListNumber, '(10,15,20)');
                        
        Account TestAccount = new Account(name='My Test Account');
        insert TestAccount;

        string color1 = xeroCalendarController.getValueHexColor('test');
        date birthdate = date.parse('03/21/1988');
        Contact TestContact = new Contact(Firstname='Frank', 
                                            Lastname='Jones', 
                                            AccountID=TestAccount.id, 
                                            email = 'none@none.com');
        DateTime dT = System.now();                                                
        date closeDate = Date.newInstance(dT.year(),dT.month(),dT.day());       
        
        dateTime startDate = datetime.now();
        dateTime endDate = startDate.addMinutes(60);
        
        Opportunity TestOpportunity = new Opportunity(Name = 'Test Bid',                                     
                                                StageName = 'Calendar',
                                                Start_time__c = startDate,
                                                End_time__c = endDate,
                                                CloseDate = closeDate);
        insert TestOpportunity;                 
        
        Opportunity ConfirmOpp = [select End_time__c,  Start_time__c from opportunity where id=:testOpportunity.id];
        
        
        Xero_Calendar__c thisCalendar = new Xero_Calendar__c();
        thisCalendar.object_name__c = 'Opportunity';
        thisCalendar.Start_Datetime__c = 'Start_time__c';
        thisCalendar.End_Datetime__c = 'End_time__c';
        thisCalendar.detail_data_fields__c = 'description, StageName, type';
        thisCalendar.Event_Name_Field__c = 'stageName';
         insert thisCalendar;
          
        Xero_Calendar_Filter__c filter = new Xero_Calendar_Filter__c();
        filter.Field_ID__c = 'StageName';
        filter.Calendar__c = thisCalendar.id;
        filter.Logical__c = 'Equal';
        filter.Value__c = 'Calendar';
        filter.active__c = true;
        
        insert filter;

        // In a managed package the SObjectType will be 'Xero_Interactive_Xero_Calendar_Style__c' and in unmanaged it will just be 'Xero_Calendar_Style__c'  
        Schema.DescribeSObjectResult someCustomObjectDescribe = Xero_Calendar_Style__c.sObjectType.getDescribe();  
        string someCustomObjectSObjectType = someCustomObjectDescribe.getName();  
          
        RecordType AutoColorRecordType = [Select r.SobjectType, r.Name, r.Id From RecordType r where sObjectType = :someCustomObjectSObjectType and name = 'Autocolor Rule']; 

        Xero_Calendar_Style__c styleRule1 = new Xero_Calendar_Style__c();
        styleRule1.Field_ID__c = 'StageName';
        styleRule1.recordtypeId = AutoColorRecordType.id;
        styleRule1.active__c = true;
        styleRule1.Calendar__c = thisCalendar.id;
        styleRule1.label__c = 'test';
        insert styleRule1; 
                
        Xero_Calendar_Style__c styleRule2 = new Xero_Calendar_Style__c();
        styleRule2.Field_ID__c = 'StageName';
        styleRule2.Logical__c = '==';
        styleRule2.Value__c = 'Calendar';
        styleRule2.Background_Color__c = 'red'; 
        styleRule2.active__c = true;
        styleRule2.Calendar__c = thisCalendar.id;
        styleRule2.Affected_Element__c = 'Border Color';
        styleRule2.label__c = 'test';
        insert styleRule2;
        
        Xero_Calendar_Style__c styleRule3 = new Xero_Calendar_Style__c();
        styleRule3.Field_ID__c = 'StageName';
        styleRule3.Logical__c = '==';
        styleRule3.Value__c = 'Cancelled';
        styleRule3.Background_Color__c = 'red'; 
        styleRule3.active__c = true;
        styleRule3.Calendar__c = thisCalendar.id;
        styleRule3.Affected_Element__c = 'Font Color';
        styleRule3.label__c = 'test'; 
        insert styleRule3;
               
        string updateString = 'id='+styleRule1.id+'&Background_Color__c=green&active__c=true';
        
        xeroCalendarController.remoteObject saveRule = xeroCalendarController.saveObject(updateString,'Xero_Calendar__Xero_Calendar_Style__c');
        
        xeroCalendarController.remoteObject failRule = xeroCalendarController.saveObject('bahdfsdf&adfsd','Xero_Calendar_Style__c');
        
        xeroCalendarController.remoteObject failRule2 = xeroCalendarController.saveObject('bahdfsdf&adfsd','blah');
        
        map<string,string> contactFields = xeroCalendarController.getObjectFieldMap('Contact',null);
                        
        list<xeroCalendarController.calendarEvent> events = XeroInteractive.xeroCalendarController.getEvents(thisCalendar.id, ConfirmOpp.Start_Time__c.getTime()/1000, ConfirmOpp.End_Time__c.getTime()/1000);

        //now update the calendar to using a date only field, instead of a date time field
        thisCalendar.object_name__c = 'Opportunity';
        thisCalendar.Start_Datetime__c = 'CloseDate';
        thisCalendar.End_Datetime__c = 'CloseDate';
        update thisCalendar;
        
        list<xeroCalendarController.calendarEvent> events2 = XeroInteractive.xeroCalendarController.getEvents(thisCalendar.id, ConfirmOpp.Start_Time__c.getTime()/1000, ConfirmOpp.End_Time__c.getTime()/1000);
                
        system.assertEquals('=',xeroCalendarController.evalFilterLogical('equal'));
        system.assertEquals('!=',xeroCalendarController.evalFilterLogical('doesnotequal'));
        system.assertEquals('>',xeroCalendarController.evalFilterLogical('greaterthan'));
        system.assertEquals('<',xeroCalendarController.evalFilterLogical('lessthan'));
        system.assertEquals('in',xeroCalendarController.evalFilterLogical('contains'));
        system.assertEquals('not in',xeroCalendarController.evalFilterLogical('doesnotcontain'));
        
        //Test the deserialize string method
        map<string,string> decodedString = xeroCalendarController.deserializeString('param1=test&param2=woot');
        system.assertEquals(decodedString.get('param1'),'test');
        system.assertEquals(decodedString.get('param2'),'woot');
        
        //Start Test non static/instance methods
        
        PageReference pageRef = Page.XeroCalendar;
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController sc = new ApexPages.standardController(thisCalendar);
        xeroCalendarController thisController = new xeroCalendarController(sc);
        List<selectOption> objectsList = thisController.getObjects();
        List<selectOption> objectFields = thisController.getObjectFields(); 
        Xero_Calendar__c thisControllersCalendar = thisController.getCal();
        
        thisController.save();
    }
}