global class timePunch_timerController {

    global timePunch_timerController(ApexPages.StandardController controller) {

    }
    
    @RemoteAction
    global static list<timePunch_timer__c> getTimer(id objectId)
    {
        list<timePunch_timer__c> timers = new list<timePunch_timer__c>();
        string objectType = getObjectTypeFromId(objectId);
        
        //if the last 3 chars are not __c, then append them. All lookup fields will be the name of the object they relate to, plus __c since they are custom fields.
        //however non custom objects won't have that __c on them, so trying to create a fieldname from the objectname would fail. This fix essentially is to get standard
        //object types working.
        
        if(objectType.subString(objectType.length()-3,objectType.length()) != '__c')
        {
            objectType+= '__c';
        }
        timePunch_timer__c thisTimer;
        Date currentDate = Date.today();
        timePunch_Timecard__c thisCard = getTimeCard();
        
        string query = 'select id, Name, task_type__c, final_minutes__c, final_seconds__c, final_hours__c, running_seconds__c, status__c from timePunch_timer__c where ' + objectType + ' = \''+objectId+'\' and Timecard__r.Date__c = :currentDate and user__c = \'' +UserInfo.getUserId() + '\'';
               
        timers = database.query(query);
        
        return timers;
    
    }
    
    @RemoteAction
    global static timePunch_timer__c createTimer(id objectId)
    {
        Date currentDate = Date.today();
        timePunch_Timecard__c findCard = getTimeCard();
        timePunch_timer__c thisTimer = new timePunch_timer__c();
             
        thisTimer.timecard__c = findCard.id;
        string objectType = getObjectTypeFromId(objectId);
        if(objectType.subString(objectType.length()-3,objectType.length()) != '__c')
        {
            objectType+= '__c';
        }
        thistimer.User__c = UserInfo.getUserId();
        thistimer.put(objectType,objectId);
        
        thisTimer.start_time__c = dateTime.now();
        thisTimer.status__c = 'Running';
        
        insert thisTimer;
        
        return thisTimer;
         
    }
    
    @RemoteAction
    global static timePunch_timer__c updateTimerComments(Id timerId, string comments)
    {
        timePunch_timer__c thisTimer = new timePunch_timer__c(id=timerId);
        thisTimer.comments__c = comments;
        update thisTimer;
        return thisTimer;
    }
    
    @RemoteAction
    global static timePunch_timer__c stopTimer(id timerId)
    {
        timePunch_timer__c thisTimer = new timePunch_timer__c(id=timerId);
        thisTimer.end_time__c = dateTime.now();
        thisTimer.status__c = 'Stopped';
        update thisTimer;
        return thisTimer;
    }
    global static timePunch_Timecard__c getTimeCard()
    {
        Date currentDate = Date.today();
        timePunch_Timecard__c thisCard;
        list<timePunch_Timecard__c> findCard = [select id, submitted__c, date__c from timePunch_Timecard__c where date__c = :currentDate and user__c = :UserInfo.getUserId() limit 1];
        if(findCard.isEmpty())
        {
            thisCard = createTimeCard();
        }
        else
        {
            thisCard = findCard[0];
        }
        
        return thisCard;
    }
    
    global static  timePunch_Timecard__c createTimeCard()
    {
         timePunch_Timecard__c thisCard = new timePunch_Timecard__c();

         try
         {
             thisCard.date__c =  Date.today();
             thisCard.user__c =  UserInfo.getUserId();
             insert   thisCard;
         }
         catch(exception e)
         {
             system.debug(e.getMessage());
         }
         return thisCard;
    }
    
    global static string getObjectTypeFromId(string objectId)
    {
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe(); 
        Map<String,String> keyPrefixMap = new Map<String,String>{};
        Set<String> keyPrefixSet = gd.keySet();
        for(String sObj : keyPrefixSet)
        {
           Schema.DescribeSObjectResult r =  gd.get(sObj).getDescribe();
           String tempName = r.getName();
           String tempPrefix = r.getKeyPrefix();
           keyPrefixMap.put(tempPrefix,tempName);
        }
    
        return keyPrefixMap.get(objectId.subString(0,3));  
    }

    @isTest 
    public static void testTimePunchController()
    {
        account testAccount= new Account();
        testAccount.name = 'Test Account';        
        insert testAccount;

        account testAccount2= new Account();
        testAccount2.name = 'Test Account2';        
        insert testAccount2;
              

        Profile p = [select id from profile where name='Standard User']; 
        User u = new User(alias = 'standt', email='standarduser@testorg.com', 
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
            localesidkey='en_US', profileid = p.Id, 
            timePunch_Max_Hours__c=8,
            timezonesidkey='America/Los_Angeles', username='standarduser@xeroInteractiveTestData.com');


        System.runAs(u) 
        {        
        
       
            Apexpages.StandardController stdController = new Apexpages.StandardController(testAccount); 
            
            timePunch_timerController tp = new timePunch_timerController(stdController);
           
            
            //Create a timer
            timePunch_timer__c newtimer = createTimer(testAccount.id);

            system.debug('Created Timer ID: ' + newTimer.id);
            //attempt to manually create a timecard. This timecard should be the same one that was returned from the creation of the timecard above.
            //since creating a timer will create a timecard if one does not exist or return a existing one if one does exist, attempting to create another
            //timecard should just return the timecard that already exists.
            
            timePunch_Timecard__c testCard = getTimeCard();
            
            system.assertEquals(newtimer.timeCard__c,testCard.id);
                        
            //Try to create another timer. this one should error since there is another timer for this user already running
            try
            {
                timePunch_timer__c newtimer2 = createTimer(testAccount2.id);
                newtimer2 = stopTimer(newtimer2.id);
                system.debug('Created Second Timer ID: ' + newtimer2.id);
                //system.assertEquals('test failure','this line should never be encountered because the line above it should error and be caught');
            }
            catch(exception e)
            {
                system.debug('good, creating the second timer failed');
            }
            //fetch the existing timers
            list<timePunch_timer__c> accountTimers = timePunch_timerController.getTimer(testAccount.id);
            system.assertEquals(accountTimers[0].task_type__c, 'account__c');
            
            //Stop the timer
            timePunch_timer__c endtimer = stopTimer(newtimer.id);
            
            endtimer = [select id, status__c, final_minutes__c from timePunch_timer__c where id = :endTimer.id];
            
            system.debug(endTimer);
            system.assertEquals(endtimer.status__c, 'Stopped');
            
            
            //now try to edit the record, this should result in an error
            try
            {
                endTimer.comments__c = 'test';
                update endTimer;
            }
            catch(exception e)
            {
            
            }
            
            //create another timer. We'll have to make this one manually since we have to make it 
            //seem like it's been running for a long time and the validation rules won't allow playing
            //with the start date after it's been started.
            
            timePunch_timer__c longRunningtimer = new timePunch_timer__c();
            
            Date currentDate = Date.today();
                 
            longRunningtimer.timecard__c = testCard.id;

            longRunningtimer.User__c = UserInfo.getUserId();
            longRunningtimer.put('Account__c',testAccount.id);            
            longRunningtimer.start_time__c = dateTime.now().addHours(-18);
            longRunningtimer.status__c = 'Running';          
            insert longRunningtimer;
            
            //now if we look at the pending hours on this timecard it should be like 18 hours.
            testCard= [select id, approximate_pending_hours__c from timePunch_timecard__c where id = :testCard.id];
            system.assertEquals(testCard.approximate_pending_hours__c.round(),18); 

            //now that we have verified that the user has more than their maximum amount of possible shift hours
            //pending, when we call the stopOverRunningtimers method it should stop any timers that are still
            //running that would put the user over their max, and set the timer to a value that would put them back at their max.
            
            //check for and stop any overrunning timers
            timePunch_methods.stopOverRunningTimers();
            
            //the timer itself should have a stopped status
            longRunningtimer = [select id, status__c from timePunch_timer__c where id = :longRunningtimer.id];
            system.assertEquals(longRunningtimer.status__c,'Stopped'); 
            
            testCard= [select id, Committed_Hours__c, approximate_pending_hours__c from timePunch_timecard__c where id = :testCard.id];
            //there should be zero pending hours
            system.assertEquals(testCard.approximate_pending_hours__c.round(),0); 
            
            //there should be 8 committed hours
            system.assertEquals(8,testCard.Committed_Hours__c.round()); 
            
            //even after the card is stopped we should be able to update the comments without error
            timePunch_timerController.updateTimerComments(longRunningtimer.id, 'Test Comments');
            
            
            
            // Schedule the test job 
                  
            String jobId = System.schedule('timePunch_stopRunningCards',
            timePunch_stopRunningCards.CRON_EXP, 
            new timePunch_stopRunningCards());
            // Get the information from the CronTrigger API object 
                       
            CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
            NextFireTime
            FROM CronTrigger WHERE id = :jobId];
            
            // Verify the expressions are the same             
            System.assertEquals(timePunch_stopRunningCards.CRON_EXP, ct.CronExpression);
            
            // Verify the job has not run            
            System.assertEquals(0, ct.TimesTriggered);
            
            // Verify the next time the job will run            
            System.assertEquals('2022-09-03 00:00:00', 
            String.valueOf(ct.NextFireTime));
        
         
                
        }        
    }
}