public class urlFix 
{
    public static string session = userInfo.getSessionId();
    @remoteAction
    public static string getPageContent(string url)
    {
            string responseBody;
            
            //create http request to get import data from
            HttpRequest req = new HttpRequest();
            req.setEndpoint(url + '&session='+session);
            req.setMethod('GET');         
            req.setHeader('Authorization', 'OAuth ' + session);
            Http http = new Http();
            
            //if this is not a test actually send the http request. if it is a test, hard code the returned results.
            if(!Test.isRunningTest())
            {
                HTTPResponse res = http.send(req);
                responseBody = res.getBody();
            }
            else
            {
                responseBody = 'target__c,source__c\nAft,The back of the ship\nAhoy,Hello\nArr,yes\nAye,yes\nAye Aye,ok\nBarbary Coast,Mediterranean coast off of North Africa\nBe,am,\nBe,ar\nBe,is\nBilboes,Leg irons\nBilge,dirtiest\nBilge rat,idiot\nBilge rat,jerk\n';
            }    
            return responseBody;
    }

    public static Map<String, Schema.DescribeFieldResult> getFieldMetaData(Schema.DescribeSObjectResult dsor, list<String> fields) {
     
      // the map to be returned with the final data
      Map<String,Schema.DescribeFieldResult> finalMap = 
        new Map<String, Schema.DescribeFieldResult>();
      // map of all fields in the object
      Map<String, Schema.SObjectField> objectFields = dsor.fields.getMap();
     
      // iterate over the requested fields and get the describe info for each one. 
      // add it to a map with field name as key
      for(String field : fields){
        // skip fields that are not part of the object
        if (objectFields.containsKey(field)) {
          Schema.DescribeFieldResult dr = objectFields.get(field).getDescribe();
          // add the results to the map to be returned
          finalMap.put(field, dr); 
        }
      }
      return finalMap;
    } 

    @remoteAction
    public static Map<String, Schema.DescribeFieldResult> getObjectFieldData(string objectType, list<string> fields)
    {
        Map<String, Schema.DescribeFieldResult> finalMap = getFieldMetaData(Data_Log__c.getSObjectType().getDescribe(), fields);
     
        return finalMap; 
    }
}