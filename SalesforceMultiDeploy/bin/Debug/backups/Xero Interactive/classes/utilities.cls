global class utilities 
{
    public static list<sObject> csvTosObject(List<List<String>> parsedCSV, string objectType)
    {
        Schema.sObjectType objectDef = Schema.getGlobalDescribe().get(objectType).getDescribe().getSObjectType();
        system.debug(objectDef);
        
        list<sObject> objects = new list<sObject>();
        list<string> headers = new list<string>();
        
        for(list<string> row : parsedCSV)
        {
            for(string col : row)
            {
                headers.add(col);
            }
            break;
        }
            
        integer rowNumber = 0;
        for(list<string> row : parsedCSV)
        {
            if(rowNumber == 0)
            {
                rowNumber++;
                continue;
            }
            else
            {
                sObject thisObj = objectDef.newSobject();
                integer colIndex = 0;
                for(string col : row)
                {
                    string headerName = headers[colIndex];
                    if(headerName.length() > 0)
                    {
                        try
                        {                       
                            thisObj.put(headerName,col);
                            colIndex++;
                        }
                        catch(exception e)
                        {
                            system.debug('Invalid field specified in header' + headerName);
                        }
                    }
                } 
                objects.add(thisObj);
                rowNumber++;
            }       
        }
        return objects;
    }
    public static void deepClone(sObject objects)
    {
        //get the type of object this list is, like contact, account, etc
        Schema.sObjectType objectType = objects.getSObjectType();
        
        //global describe of objects
        Map<String, Schema.SObjectType> globalDescribe = Schema.getGlobalDescribe(); 
        
        //describe object
        Schema.DescribeSObjectResult objectDescribe = objectType.getDescribe();
                
        //get all the valid fields on this object
        Map<String, Schema.SobjectField> objectFields = objectType.getDescribe().fields.getMap();
        
        //create list of all the child relationships (objects which we will also need to clone and set the parent as the new object)
        list<Schema.ChildRelationship> relationshipFields = objectDescribe.getChildRelationships();
        
        //variable to hold list of fields on this object to be used in some dynamic query later
        string fieldList = '';
        for(string field : objectFields.keySet())
        {
          fieldList += field + ', ';
        }
        
        //clean up the trailing space and , for query. 
        fieldList = fieldList.substring(0,fieldList.length()-2);
        
        //create the query string to send to the database
        String queryString = 'select ' + fieldList + ' from ' + objectType + ' where id = \'' + objects.get('id') + '\'';
        
        //store the result in an sObject
        sObject objectFieldValues = database.query(queryString);    
        
        system.debug(objectFieldValues);
        
        //create a new object of the same time we got passed
        sObject clonedObj = objectType.newsObject();
        
        //iterate over every field that exists on this object type, copy the value from the original object and copy to the new one
        for(string field : objectFields.keySet())
        {
            try
            {
                //Todo, cast this variable as the correct type... somehow
                clonedObj.put(field, (string) objectFieldValues.get(field));
            }
            catch(exception e)
            {
                system.debug('Unable to write value '+objectFieldValues.get(field)+ ' to field ' + field + '. Error: ' + e.getMessage());   
            }
            
        }       
        system.debug(relationshipFields);
        
        insert clonedObj;
        
        for(Schema.ChildRelationship childObject : relationshipFields)
        {
            /*psudeo code
            
                select * from object represented by relationship where relationshipname reference = object id
                iterate over results, create new records exactly mimicing, but replace relationship reference with original object id
                insert new records
            */
        }
    }

    public static String readXMLelement(String xml, String element)
    {
        String elementValue = 'NOT FOUND'; 
        
        try
        {
            Xmlstreamreader reader = new Xmlstreamreader(xml);
            while (reader.hasNext()) 
            {
                if (reader.getEventType() == XmlTag.START_ELEMENT && reader.getLocalName() == element)
                {
                    reader.next();
                    if(reader.getEventType() == XmlTag.CHARACTERS)
                    {
                        elementValue = EncodingUtil.urlDecode(reader.getText(), 'UTF-8').trim();
                    }                 
                }     

                reader.next();
            }
            return elementValue;
        }
        catch(exception e)
        {
            system.debug(e);
            return e.getTypeName() + ' ' + e.getCause() + ' ' + e.getMessage() + ' on line ' + e.getLineNumber();
        }
    }


    public static Map<string,string> deserializeString(String argString)
    {   
        string[] params = argString.split('&');
        map<String,String>  formParams = new map<String,String>();
        for(string p : params)
        {       
            formParams.put(EncodingUtil.urlDecode(p.substring(0,p.indexOf('=')),'UTF-8'),EncodingUtil.urlDecode(p.substring(p.indexOf('=')+1,p.length()),'UTF-8'));    
        }       
        return formParams;
    } 

    @RemoteAction
    global static remoteObject generateRandomString(integer strLength)
    {
        remoteObject returnObj = new remoteObject();
        returnObj.success = true;
        returnObj.message = 'Code generated';
                         
        try
        { 
            string charString = 'abcdefghijklmnopqrstuvwxyz1234567890';
            string returnString = '';
            for(integer i = 0; i < strLength; i++)
            {
                Double x = math.random() * charString.length();
                               
                returnString += charString.substring(x.intValue(),x.intValue()+1);
            }
            returnObj.data = returnString.toUpperCase();
        }
        catch(Exception e)
        {
            
        }
        return returnObj;    
    }
            
    //this function will take a list of sObjects and convert their data into a json encoded string
    public static String objectToJson(List<sObject> records)
    {
        String jsonString = ' "data": [';
                        
        if (!records.isEmpty()) 
        {
 
            //This is a mapping of the column names returned by the query
            Map<String, Schema.SObjectField> columns = records.getSObjectType().getDescribe().fields.getMap();
            
                    
            for (sObject c : records) 
            {
                //Now for every column in the query, I need to construct a new JSON "element"
                //I can do that statically by typing something like this
                //cjson.putOpt('"id"', new JSONObject.value(c.Id));

                jsonString = jsonString + '{';
                for(String columnName : columns.keySet()) 
                {   
                     try 
                     {
                         
                        //Try to get the value of the column name. If it isn't there, then it tosses
                        //an error, no big deal.
                        
                        string cellVal = String.valueOf( c.get(columnName));
                        jsonString = jsonString + '"'+columnName+'": "'+cellVal.replace('"','\'')+'",'; 
                     } 
                     catch(System.Exception ex) 
                     {
                         //jsonString = jsonString + '"'+columnName+'": "'+ex.getMessage()+'",';
                     }
                     
                     
                }
                jsonString = jsonString.substring(0, jsonString.length() - 1);
                jsonString = jsonString + '},';  
                                                   
            }  
            jsonString = jsonString.substring(0, jsonString.length() - 1); 
        }
        else
        {
            jsonString = jsonString + '"No Records Found"';
        }
            
        jsonString = jsonString + ']}';
        
        return jsonString;
    }  

    public static string convertNumberToWord(string numToConvert)
    {
        string numberString = string.valueOf(numToConvert);
        string returnWord = '';
        Integer i=0;
        do
        {
            if(i<numberString.length())
            {
                returnWord = returnWord + ' ' + digitToWord(numberString.substring(i,i+1));
            }
            i++;
        }
        while(i < numberString.length());
        
        return returnWord.trim();
    }
    
    public static string digitToWord(string digit)
    {
        if(digit == '0')
        {
            return 'zero';  
        }   
        else if(digit == '1')
        {
            return 'one';   
        }           
        else if(digit == '2')
        {
            return 'two';   
        }   
        else if(digit == '3')
        {
            return 'three'; 
        }   
        else if(digit == '4')
        {
            return 'four';  
        }   
        else if(digit == '5')
        {
            return 'five';  
        }   
        else if(digit == '6')
        {
            return 'six';   
        }   
        else if(digit == '7')
        {
            return 'seven'; 
        }   
        else if(digit == '8')
        {
            return 'eight'; 
        }   
        else if(digit == '9')
        {
            return 'nine';  
        }               
        else
        {
            return 'not a digit';
        }                                       
    }        
    
    static testMethod void testUtilities()
    {
        Account thisTestAccount = new Account();
        thisTestAccount.name = 'Test Account';
        list<Account> myAccounts = new list<Account>();
        myAccounts.add(thisTestAccount);
        
        string xmlData = '<?xml version="1.0"?><emailData><token>wrongToken</token><phoneNumber>(999) 999-9999</phoneNumber><message>Test Message from Spin SMS System</message></emailData>';
        string token = utilities.readXMLelement(xmlData, 'token');
        
        string contactJSON = utilities.objectToJson(myAccounts);
        
        //test random string generation
        generateRandomString(15);
        
        string numWord = convertNumberToWord('123456789S');
          
    }
    global class remoteObject
    {
        public boolean success{get;set;}
        public string message{get;set;}
        public string data{get;set;}
        public list<sObject> sObjects {get;set;} 
    }       
}