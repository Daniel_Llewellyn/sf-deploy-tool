global class weThePeople
{
    public static boolean isRunningTest = false;
    public static boolean inFutureContext = false;
            
 
     @future (callout=true)  // future method needed to run callouts from Triggers
     static public void getStoreLocation(list<id> storeIds)
     {
         inFutureContext = true;
        // gather account info
        list<Store__c> stores = [SELECT Street_Address__c,City__c,State__c FROM store__c WHERE id in :storeIds and Store_Location__Latitude__s = null];
 
        for(Store__c store : stores)
            {
            // create an address string
            String address = '';
            if (store.Street_Address__c != null)
                address += store.Street_Address__c +', ';
            if (store.City__c != null)
                address += store.City__c +', ';
            if (store.State__c != null)
                address += store.State__c +' ';
     
            address = EncodingUtil.urlEncode(address, 'UTF-8');
     
            // build callout
            Http h = new Http();
            HttpRequest req = new HttpRequest();
            req.setEndpoint('http://maps.googleapis.com/maps/api/geocode/json?address='+address+'&sensor=false');
            req.setMethod('GET');
            req.setTimeout(60000);
     
            try
            {
                // callout
                string JsonData;
                if(!isRunningTest)
                {
                    HttpResponse res = h.send(req);
                    JsonData = res.getBody();
                }
                else
                {
                    JsonData = '{ "results": [ { "address_components": [ { "long_name": "11021", "short_name": "11021", "types": [ "street_number" ] }, { "long_name": "Bittersweet St NW", "short_name": "Bittersweet St NW", "types": [ "route" ] }, { "long_name": "Coon Rapids", "short_name": "Coon Rapids", "types": [ "locality", "political" ] }, { "long_name": "Anoka", "short_name": "Anoka", "types": [ "administrative_area_level_2", "political" ] }, { "long_name": "Minnesota", "short_name": "MN", "types": [ "administrative_area_level_1", "political" ] }, { "long_name": "United States", "short_name": "US", "types": [ "country", "political" ] }, { "long_name": "55433", "short_name": "55433", "types": [ "postal_code" ] } ], "formatted_address": "11021 Bittersweet St NW, Coon Rapids, MN 55433, USA", "geometry": { "location": { "lat": 45.170777, "lng": -93.33601 }, "location_type": "ROOFTOP", "viewport": { "northeast": { "lat": 45.1721259802915, "lng": -93.3346610197085 }, "southwest": { "lat": 45.1694280197085, "lng": -93.33735898029151 } } }, "partial_match": true, "types": [ "street_address" ] } ], "status": "OK" }';
                }
                 
                // parse coordinates from response
                JSONParser parser = JSON.createParser(JsonData);
                double lat = null;
                double lon = null;
                while (parser.nextToken() != null)
                {
                    if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'location'))
                    {
                        parser.nextToken(); // object start
                        while (parser.nextToken() != JSONToken.END_OBJECT)
                        {
                            String txt = parser.getText();
                            parser.nextToken();
                            if (txt == 'lat')
                                lat = parser.getDoubleValue();
                            else if (txt == 'lng')
                                lon = parser.getDoubleValue();
                        }
     
                    }
                }
     
                // update coordinates if we get back
                if (lat != null)
                {
                    store.Store_Location__Latitude__s = lat;
                    store.Store_Location__Longitude__s = lon;
                    store.Latitude__c = string.valueOf(lat);
                    store.Longitude__c = string.valueOf(lon);
                    
                }
                update stores;
           }
           catch (Exception e) 
           {
               system.debug('\n\n\n ------------------ Error geocoding account addresses');
           }
        } 
    }

    @remoteAction
    public static user updateUserLocation(decimal lat, decimal lon)
    {
        User u = new User(id=UserInfo.getUserId());
        u.Last_Location__Latitude__s = lat;
        u.Last_Location__Longitude__s = lon;
        
        update u;
        return u;
        
    }
     
    @remoteAction
    public static boolean placeVote(id ideaId, string voteType)
    {
        Townhall_Idea_Vote__c thisVote = new Townhall_Idea_Vote__c();
        thisVote.Townhall_Idea__c = ideaId;
        thisVote.Vote_Type__c = voteType;
        try
        {
            insert thisVote;
        }
        catch(exception e)
        {
            return false;
        }
        return true;
    }
    
    @remoteAction 
    public static list<sObject> runQuery(string query)
    {
        return database.query(query);
    }

    //gets a single attachment (photo) by id. The data is returned as a base64 string that can be plugged into an html img tag to display the image.
    @RemoteAction
    global static remoteObject getAttachment(id attachmentId)
    {   
        remoteObject returnObj = new remoteObject();
        try
        {
            list<Attachment> docs = [select id, body from Attachment where id = :attachmentId limit 1]; 
            if(!docs.isEmpty())
            {
                returnObj.data = EncodingUtil.base64Encode(docs[0].body); 
            }    
        }
        catch(exception e)
        {
            returnObj.success = false;
            returnObj.message = e.getMessage();
            returnObj.data = 'Error Type: ' + e.getTypeName() + ' ' + e.getCause() + ' ' + ' on line: ' +e.getLineNumber();        
        } 
        return returnObj;    
    }   

    global class remoteObject
    {
        public boolean success = true;
        public string message = 'operation successful';
        public string data = null;
        public list<sObject> sObjects = new list<sObject>();
    } 
        
    @isTest
    global static void testWeThePeople()
    {
        weThePeople.isRunningTest =  true;
        Store__c store = new store__c();
        store.city__c = 'Coon Rapids';
        store.state__c = 'mn';
        store.Street_Address__c = '11021 Bittersweet street';
        store.name = 'test store';
        
        insert store;
        
        Townhall_Meeting__c thm = new Townhall_Meeting__c();
        thm.Store__c = store.id;
        thm.name = 'meeting 1';
        thm.Meeting_Start__c = datetime.now();
        thm.Maximum_Number_of_Attendees__c = 10;
        
        insert thm;
        
        list<sObject> stores = runQuery('select id,name from store__c');
        system.assertEquals(stores.size(),1);
        
        system.assertEquals((string) stores[0].get('name'),store.name);
        
        user updateLocation = updateUserLocation(45.05625,-93.304507);
        
        user checkUser = [select Last_Location__Latitude__s, Last_Location__Longitude__s from user where id = :UserInfo.getUserId()];
        system.assertEquals(checkUser.Last_Location__Latitude__s,45.05625);
        system.assertEquals(checkUser.Last_Location__Longitude__s,-93.304507);
        
        Townhall_Idea__c thisIdea = new Townhall_Idea__c();
        thisIDea.Description__c = 'Test Idea';
        thisIdea.Idea_type__c = 'Praise';
        thisIdea.title__c = 'This is awesome!';
        thisIdea.Townhall_Meeting__c = thm.id;
        
        insert thisIdea;
        
        boolean voteSuccess1 = placeVote(thisIdea.id, 'up');
        system.assertEquals(voteSuccess1,true);
        //secondary vote should fail
        
        boolean voteSuccess2;
        try
        {
            voteSuccess2 = placeVote(thisIdea.id, 'up');
        }
        catch(exception e)
        {
            system.debug('duplicate vote errored. Good!');
        }
        system.assertEquals(voteSuccess2,false);
        
    }
}