/*****************************
* @Name xeroCalendarController
* @Description Controller for the Xero Calendar application. Most static remoted
* @Author Daniel Llewellyn (@Kenji776 Kenji776@gmail.com)
* @Date ~6/2014
******************************/

global class xeroCalendarController 
{
    public string objectType{get;set;}
    public Xero_Calendar__c thisCal{get;set;}
    public List<selectOption> Objects;
    public List<selectOption> ObjectFields;
    
    //Standard edit override methods
    global xeroCalendarController(ApexPages.StandardSetController controller) {
      
    }


    global xeroCalendarController(ApexPages.StandardController controller) {
         this.thisCal = (Xero_Calendar__c)controller.getRecord();
    }

    //gets information about a calendar object using field sets
    public Xero_Calendar__c getCal() 
    {
        Xero_Calendar__c thisCal = new Xero_Calendar__c();
        try
        {
            if(thisCal.id == null)
            {
                return new Xero_Calendar__c();
            }
            set<string> fieldNames = new set<string>();
            
            List<Schema.FieldSetMember> fields = new List<Schema.FieldSetMember>();
    
            fieldNames.add('id');
    
            for(Schema.FieldSetMember f : getDetailFields()) 
            {
                fieldNames.add(f.getFieldPath().toLowerCase());
            }
                    
            string fieldString = '';
            for(string field : fieldNames)
            {
            fieldString += field +',';
            }       
            fieldString = fieldString.substring(0,fieldString.length()-1);
            
            string query = 'select ' + fieldString + ' FROM Xero_Calendar__c where id=\''+thisCal.id+'\' LIMIT 1';
            
            list<Xero_Calendar__c> cals = Database.query(query);
            
            if(!cals.isEmpty())
            {
                thisCal = cals[0];
            }
            else
            {
                new calException('Calendar could not be located with ID: ' + thisCal.id);
            }
        }
        catch(exception ex)
        {
            system.debug('\n\n\n\n-------------------- ERROR OCCURRED DURING GETTING CALENDAR DATA!');
            system.debug(ex);        
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Error while fetching calendar data. ' + ex.getMessage() + ' on line ' + ex.getLineNumber()));        
        }
        return thisCal;
    }

      public List<Schema.FieldSetMember> getDetailFields() {
        return SObjectType.Xero_Calendar__c.FieldSets.XeroInteractive__Standard_Fields.getFields();
    }

        
    //override for the calendar save functionality
    public pagereference save(){
     
        try
        {
            database.upsertResult sr= database.upsert(thisCal);
            if(!sr.errors.isEmpty())
            {
                for(database.error err : sr.errors)
                {
                    //if the error is field level, attach it at the field level
                    if(!err.fields.isEmpty())
                    {
                        for(string fieldName : err.fields)
                        {
                            //thisCase.subject.addError(err.message);
                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error during save:' + err.message));
                        }
                    }
                    //otherwise just plaster it on the page
                    else
                    {
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error during save:' + err.message));
                    }
                }
            }
            else
            {
                PageReference pageRef = new PageReference('/'+thisCal.id);
                //pageRef.setRedirect(true);
                return pageRef;            
            }
        }
        catch(exception ex)
        {
            system.debug('\n\n\n\n-------------------- ERROR OCCURED DURING SAVE!');
            system.debug(ex);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error during save:' + ex.getMessage() + ' on line ' +ex.getLineNumber()));
        }
        return null;          
    }
    
        
    @RemoteAction
    global static list<calendarEvent> getEvents(id calendarId, double queryStart, double queryEnd)
    {
        list<calendarEvent> events = new list<calendarEvent>();
        try
        {
            string nameField = 'name';
            string queryFields = '';
            string queryFilters = '';
            string baseQueryString = '';
            boolean editable = false;
            map<string,string> nameToPropertyMap = new map<string,string>();
            nameToPropertyMap.put('Background Color','backgroundColor');
            nameToPropertyMap.put('Font Color','textColor');
            nameToPropertyMap.put('Border Color','borderColor');
            
            map<string,string> valueToColorMap = new map<string,string>();
            
            Xero_Calendar__c thisCalendar = [select Default_Event_Color__c, 
                                   Detail_Data_Fields__c, 
                                   Event_Name_Field__c, 
                                   End_DateTime__c, 
                                   Object_Name__c, 
                                   Start_Datetime__c,
                                   Calendar_Type__c,
                                   Allow_Reschedule__c,
                                   XeroInteractive__Default_Event_Text_Color__c,
                                   XeroInteractive__Default_Event_Border_Color__c,
                                   (select Field_ID__c, Calendar__r.Object_Name__c, Logical__c, Value__c from XeroInteractive__Filters__r where XeroInteractive__active__c = true),
                                   (select RecordType.name, Field_ID__c,  Logical__c, Value__c, Background_Color__c, Affected_Element__c, Label__c from XeroInteractive__Styles__r where XeroInteractive__active__c = true order by XeroInteractive__Priority__c asc) 
                                   from Xero_Calendar__c where id = :calendarId];
                                       
                                  
            nameField = thisCalendar.Event_Name_Field__c;
            editable = thisCalendar.Allow_Reschedule__c;
            
    
            
            //thisCalendar.Detail_Data_Fields__c = thisCalendar.Detail_Data_Fields__c.replaceAll('[^,_a-zA-Z0-9]','');
                            
            //So now we need to build the list of fields to query for. Of course we need the event name field, start date,
            //end date, id, name and the fields specified in the detail data of the calendar.         
            set<string> fields = new set<string>();
            fields.add(thisCalendar.Event_Name_Field__c.toLowerCase().trim());
            fields.add(thisCalendar.Start_Datetime__c.toLowerCase().trim());
            fields.add(thisCalendar.End_Datetime__c.toLowerCase().trim());
            fields.add('id');
            fields.add('name');
            
    
            
            //loop over all the fields entered in the detail data section. They are comma separated, so we just need to split
            //the string and each element should reprsent a valid fieldname on the source object. So check and make sure it is valid,
            //and if so, enter it in the set of fields to query for.
            for(string fieldName : thisCalendar.Detail_Data_Fields__c.trim().toLowerCase().split(','))
            {
                fields.add(fieldName.toLowerCase().trim());
            }
    
            //get all the fields referenced in the style rules
            for(Xero_Calendar_Style__c styleRule : thisCalendar.Styles__r)
            {
                fields.add(styleRule.XeroInteractive__Field_ID__c.toLowerCase().trim());
            }
    
            //get all the fields referenced in the filter rules
            for(Xero_Calendar_Filter__c filter : thisCalendar.Filters__r)
            {
                fields.add(filter.XeroInteractive__Field_ID__c.toLowerCase().trim());
            }
                            
            //so now we have a set of fields to query for, we need to convert it into an SOQL query string. So just add a comma after
            //each field, and trim of the last trailing comma.
            for(string  field :  fields)
            {
                queryFields += field+',';
            }
            queryFields = queryFields.substring(0,queryFields.length()-1);                        
    
            system.debug('----------------------------------------------' + queryFields);
            //Append the filters into the querystring.
            //we don't have to worry about trimming the last AND statment, becuase the date filters will be appended to the query
            //before it is acutally run. 
            for(Xero_Calendar_Filter__c filter : thisCalendar.Filters__r)
            {
                string logicalExpression = evalFilterLogical(filter.Logical__c);
                string fieldValue = evalFilterValue(filter.Value__c,filter.Calendar__r.Object_Name__c,filter.Field_ID__c, logicalExpression);
                queryFilters += filter.Field_ID__c+ ' '+ logicalExpression + ' ' + fieldValue + ' and ';     
            }    
            baseQueryString = 'select ' + queryFields + ' from ' + thisCalendar.Object_Name__c + ' where ' + queryFilters;
                    
            
            //create start and end datetimes from the epoch time passed in.
            datetime startDateTimeFilter = datetime.newInstance(0).addSeconds(integer.valueOf(queryStart));
            datetime endDateTimeFilter = datetime.newInstance(0).addSeconds(integer.valueOf(queryEnd));
                           
            //get the baseQueryString to build onto.
            string queryString = baseQueryString;
            
                   
            if(thisCalendar.Calendar_Type__c == 'datetime')
            {
                queryString += thisCalendar.Start_Datetime__c+' >=:startDateTimeFilter and '+thisCalendar.End_Datetime__c+'<=:endDateTimeFilter';
            }
            else
            {
                //now create dates out of those datetimes, since the type of field we are filtering on is date field instead of a datetime.
                date startDateFilter = date.newInstance(startDateTimeFilter .Year(), startDateTimeFilter .Month(), startDateTimeFilter .day());
                date endDateFilter = date.newInstance(endDateTimeFilter .Year(),endDateTimeFilter .Month(), endDateTimeFilter .Day());
                queryString += thisCalendar.Start_Datetime__c+' >=:startDateFilter and '+thisCalendar.End_Datetime__c+'<=:endDateFilter';
            }
                 
            //ok, now actually get the list of events by running the query.                
            list<sObject> queryEvents = database.query(queryString);
            for(sObject e : queryEvents)
            {
                //create a new calendar object out of the queried event to be returned to the calendar.
                calendarEvent thisEvent = new calendarEvent();
                
                //specify the object type, for future proofing, since we may allow more than one object.
                thisEvent.objectType = thisCalendar.Object_Name__c;
                thisEvent.id = e.id;
                
                //assign the title of the event as whatever the user has the nameField set as.
                thisEvent.title = string.valueOf(e.get(nameField));
                
                //If this is an all day type of calendar (meaning they specified a date field for a start date, or didn't specify an end date,
                //or the end date on this object is blank, then mark this event as an all day event.
                if(thisCalendar.Calendar_Type__c == 'date' || e.get(thisCalendar.End_Datetime__c) == null)
                {
                    thisEvent.allDay = true;
                }
                else
                {
                    thisEvent.allDay = false;
                }
                
                thisEvent.editable = editable;
                //set the default color on the event (may be overriden by style rules)
                thisEvent.backgroundColor = thisCalendar.XeroInteractive__Default_Event_Color__c;
                thisEvent.textColor = thisCalendar.XeroInteractive__Default_Event_Text_Color__c;
                thisEvent.borderColor = thisCalendar.XeroInteractive__Default_Event_Border_Color__c;
                //set the start and end field names on the object. Not totally required, but again future proofing, incase
                //we allow for multiple object types on one calendar.
                thisEvent.startTimeField = thisCalendar.Start_Datetime__c;
                thisEvent.endTimeField = thisCalendar.End_Datetime__c;      
               
                //so now we have to go over the style rules.
                //We iterate over them in their priority order, with any autocolor rules coming first.
                for(Xero_Calendar_Style__c rule : thiscalendar.Styles__r)
                {
                    string fieldValue = (string) e.get(rule.Field_ID__c);
                    if(fieldValue == null)
                    {
                         fieldValue = 'none';
                    }
                    string color;      
                    boolean ruleMatch = false;
                             
                    if(rule.recordType.name == 'Autocolor Rule')
                    {              
                        ruleMatch = true;      
                        if(valueToColorMap.containsKey(fieldValue))
                        {
                            color = (string) valueToColorMap.get(fieldValue);
                        }
                        else
                        {
                             color = (string) getValueHexColor(fieldValue);
                             valueToColorMap.put(fieldValue,color);
                        }
                    }
                    else
                    {
                        if(e.get(rule.Field_ID__c) == rule.Value__c)
                        {
                           color = rule.Background_Color__c;
                           ruleMatch = true;
                        }
                    }
                    
                    if(ruleMatch)
                    {
                        thisEvent.matchedStyles.add(rule);
                        
                        if(rule.XeroInteractive__Affected_Element__c.contains('Background Color'))
                        {
                            thisEvent.backgroundColor = color;
                            thisEvent.backgroundColoringFieldValue = fieldValue;
                            thisEvent.backgroundcoloringField = rule.Field_ID__c;
                            thisEvent.backgroundColoringFieldLabel = rule.Label__c;
                        }
                        if(rule.XeroInteractive__Affected_Element__c.contains('Border Color'))
                        {
                            thisEvent.borderColor = color;
                            thisEvent.borderColoringFieldValue = fieldValue;
                            thisEvent.bordercoloringField = rule.Field_ID__c;  
                            thisEvent.borderColoringFieldLabel = rule.Label__c;                  
                        }
                        if(rule.XeroInteractive__Affected_Element__c.contains('Font Color'))
                        {
                            thisEvent.textColor = color;
                            thisEvent.textColoringFieldValue = fieldValue;
                            thisEvent.textcoloringField = rule.Field_ID__c;     
                            thisEvent.textColoringFieldLabel = rule.Label__c;               
                        }
                    }
                }
                
                //So now this gets a little weird. The calendar expects to receive an epoch representation of a datetime.
                //so if this is a calendar that is just using date fields, we need to create datetimes out of dates. Otherwise
                //we just use the gettime on the datetime field to get the epoch representation.
                long startDateTime;
                long endDateTime;
                
                if(thisCalendar.Calendar_Type__c == 'datetime')
                {
                    startDateTime = (dateTime.valueOf(e.get(thisCalendar.Start_Datetime__c)).getTime()/1000);
                    endDateTime = (dateTime.valueOf(e.get(thisCalendar.End_Datetime__c)).getTime()/1000);
                }
                else
                {    
                    date startDate = date.valueOf(e.get(thisCalendar.Start_Datetime__c));
                    date endDate = date.valueOf(e.get(thisCalendar.End_Datetime__c));
                    
                    datetime startTime =  datetime.newInstance(startDate.year(), startDate.month(), startDate.day());
                    datetime endTime =  datetime.newInstance(endDate.year(), endDate.month(), endDate.day());
                    
                    startDateTime = (startTime.getTime()/1000 +10);
                    endDateTime = (endTime.getTime()/1000 + 10);                        
                }                        
    
                thisEvent.eventStartEpoch = startDateTime;
                thisEvent.eventEndEpoch = endDateTime;
    
                //we need to pass back all the data queried for in the detail data section so that can be displayed to the user in the popup.
                //we put this data in it's own object instead of in the root object so when we loop over it we don't give the user un-needed
                //or unwanted data.
                //thisEvent.detailData = e;
                
                Map<String, Schema.SobjectField> objectDef = Schema.getGlobalDescribe().get(thisCalendar.Object_Name__c).getDescribe().getSObjectType().getDescribe().fields.getMap();
                thisEvent.detailData = new map<string,string>();
                for(String fieldName : objectDef.keySet())
                {
                    try{
                        thisEvent.detailData.put(fieldName.toLowerCase(),(string) e.get(fieldName));
                    }catch(exception ex){
                        system.debug('\n\n\n\n----------- COULDNT PUT FIELD ' + fieldName + ' ' + ex.getMessage());
                    }
                }
                
                
                events.add(thisEvent);
            }
        }
        catch(exception ex)
        {
            system.debug('\n\n\n\n-------------------- ERROR OCCURRED DURING FETCH EVENTS!');
            system.debug(ex);        
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Error while fetching events. ' + ex.getMessage() + ' on line ' + ex.getLineNumber()));
        }
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Found ' + events.size() + ' events'));        
        return events;
    }
   
    /**
    * @Description Translates a logical comparison word into its equivilent programatic expression. Such as 'equal' becomming '=='
    * @Param logicalString the word to translate into a programatic logical
    * @Return string equivilent programatic logical expression
    **/
    public static string evalFilterLogical(string logicalString)
    {
        string logical = logicalString.toLowerCase().replace(' ','');
        
        if(logical == 'equal')
        {
            logical = '=';
        }
        else if(logical == 'notequal' || logical == 'doesnotequal')
        {
            logical = '!=';
        }
        else if(logical == 'greaterthan')
        {
            logical = '>';
        }         
        else if(logical == 'lessthan')
        {
            logical = '<';
        }                          
        else if(logical == 'contains')
        {
            logical = 'in';
        }       
        else if(logical == 'doesnotcontain')
        {
            logical = 'not in';
        }                                   
        return logical;
    }
    
    //this method will prepare any value to be used in an SOQL query based on it's field type, and if it is in a cotains statment.
    //it will wrap text fields in quotes, and if this is for a contains filter, will wrap the expression in parenthesis
    public static string evalFilterValue(string fieldValue, string objectType, string fieldName, string filterType)
    {
        string value;
        
        Schema.sObjectType objectDef = Schema.getGlobalDescribe().get(objectType).getDescribe().getSObjectType();
        string fieldType = objectDef.getDescribe().fields.getMap().get(fieldName).getDescribe().getType().name();
        fieldType = fieldType.toLowerCase();
        
        if(fieldType == 'currency' || fieldType == 'double' || fieldType == 'integer' || fieldType == 'percent')
        {
            fieldType = 'numeric';
        } 
        else
        {
            fieldType = 'string';
        }

        
        //they may have passed in a comma separated list of values for an 'in' or 'not in' statment.
        if(filterType == 'in' || filterType == 'not in')
        {
            value = '(';
            list<string> allValues = fieldValue.split(',');
            
            //if the field type is a string, then all the values need to be wrapped in quotes. If not, then we can just leave it alone.
            if(fieldType == 'string')
            {
                for(string val : allValues)
                {
                    value += '\'' + val + '\',';
                }
             }   
             else
             {
                for(string val : allValues)
                {
                    value += val + ',';
                }                     
             }
            value = value.substring(0,value.length()-1);
            value += ')';
         }
         else
         {
             if(fieldType == 'string')
             {
                 value = '\'' + fieldValue + '\'';
             }
             else
             {
                 value=fieldValue;
             }
         }                    
        
        
        return value;
    }      
    global class calendarEvent
    {
        string objectType{get;set;}
        string id{get;set;}
        string title = 'Event';
        string backgroundColor = 'blue';
        string backgroundColoringField = 'None';
        string backgroundColoringFieldLabel = 'None';
        string backgroundColoringFieldValue = 'null';
        string borderColoringField = 'None';
        string borderColoringFieldLabel = 'None';
        string borderColoringFieldValue = 'null';        
        string borderColor{get;set;}
        string textColoringField = 'None';
        string textColoringFieldValue = 'null';          
        string textColoringFieldLabel = 'null';
        string textColor{get;set;}
        boolean allDay = false;
        long eventStartEpoch = 0;
        long eventEndEpoch = 0;
        string startTimeField = 'unknown';
        string endTimeField = 'unknown';
        string url{get;set;}
        map<string,string> detailData{get;set;}
        boolean editable = false;
        list<Xero_Calendar_Style__c> matchedStyles = new list<Xero_Calendar_Style__c>();
    }
    
    global class remoteObject
    {
        public boolean success = true;
        public string message = 'operations successfull';
        public string data = 'success';
        public list<sObject> sObjects= new list<sObject>();
        public map<string,string> fieldValues = new map<string,string>();
    }

    
    public List<selectOption> getObjects() {
        List<Schema.SObjectType> gd = Schema.getGlobalDescribe().Values();     
        List<SelectOption> options = new List<SelectOption>();
        
        for(Schema.SObjectType f : gd)
        {
           options.add(new SelectOption(f.getDescribe().getLabel(),f.getDescribe().getLabel()));
        }
        return options;
    }

    public List<selectOption> getObjectFields() 
    {
        List<SelectOption> options = new List<SelectOption>();
        try
        {
            if(objectType == null)
            {
                objectType = 'contact';
            }
            Map<String, Schema.SobjectField> objectDef = Schema.getGlobalDescribe().get(objectType).getDescribe().getSObjectType().getDescribe().fields.getMap();
            
            
            for(String fieldId : objectDef.keySet())
            {
                options.add(new SelectOption(objectDef.get(fieldId).getDescribe().getLabel(),objectDef.get(fieldId).getDescribe().getLabel()));
            }
        }
        catch(exception ex)
        {
            system.debug('\n\n\n\n-------------------- ERROR OCCURRED DURING Get Object Fields!');
            system.debug(ex);        
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Error while fetching object fields. ' + ex.getMessage() + ' on line ' + ex.getLineNumber()));        
        }    
        return options;
    }
        
    @RemoteAction
    global static map<string,string> getObjectFieldMap(string objectType, string typeFilter)
    {
        map<string,string> fieldMap = new map<string,string>();
        Map<String, Schema.SobjectField> objectDef = Schema.getGlobalDescribe().get(objectType).getDescribe().getSObjectType().getDescribe().fields.getMap();
        for(String fieldId : objectDef.keySet())
        {
            if( (typeFilter != null && string.valueOf(objectDef.get(fieldId).getDescribe().getType()).toLowerCase() == typeFilter.toLowerCase()) || typeFilter == null)
            {
                fieldMap.put(fieldId.toLowerCase(),objectDef.get(fieldId).getDescribe().getLabel());
            }
        }
        return fieldMap;
    }

    /**
    * @Description given a datastring and an object type the data is converted into an sObject and saved. Used for rescheduling events from the calendar mostly, but could be extended
    *              to provide inline editing of events, or even saving of events. This method seems overly verbose and probably not required. Should look into JSON serializing the 
    *              event data, then deserializing it into the proper object type, or even into a generic sObject and performing the update using that. This method was written a while ago
    *              and newer techniques likely have made it obsolete.
    * @Param datastring a url encoded string of object data. Such as field=value&field2=value2. Should not including beginning ?
    * @Param objectType the type of sObject to create, populate and update using the data from the dataString
    * @Return remoteObject an object containing the results of the call.
    **/
    @RemoteAction
    global static remoteObject saveObject(string dataString, string objectType)
    {
        
        remoteObject returnObj = new remoteObject();
        returnObj.data = 'Removed fields: ';
        
        try
        {
            Schema.sObjectType objectDef = Schema.getGlobalDescribe().get(objectType).getDescribe().getSObjectType();
            Map<String, Schema.SobjectField> ObjectFieldsMap = objectDef.getDescribe().fields.getMap();
            map<string,string> formData = deserializeString(dataString);
            returnObj.fieldValues = formData;
            list<sObject> objects = new list<sObject>();
            
            sObject updateObj;

            if(formData.containsKey('id'))
            {
                updateObj = objectDef.newSobject(formData.get('id'));
            }
            else
            {
                updateObj = objectDef.newSobject();
            }    
            for(String key : formData.keySet())
            {
                try
                {
                    //figure out the type of this field so we can cast it to the correct type
                    string fieldType = ObjectFieldsMap.get(key).getDescribe().getType().name().ToLowerCase();
                    
                    //since I don't know how to do, or if it's even possible to do dynamic casting we need a 
                    //series of if statments to handle the casting to numeric types. I think all the others should
                    //be fine if left as a string. Dates might explode, not sure.
                    
                    
                    if(fieldType == 'currency' || fieldType == 'double' || fieldType == 'percent' || fieldType == 'decimal' )
                    {
                        updateObj.put(key, decimal.valueOf(formData.get(key).trim())); 
                    }
                    else if(fieldType == 'boolean')
                    {
                        updateObj.put(key, Boolean.valueOf(formData.get(key))); 
                    }                   
                    else if(fieldType == 'date')
                    {
                        dateTime thisDate = datetime.newInstance(Long.valueOf(formData.get(key)));
                            
                        updateObj.put(key, date.newInstance(thisDate.year(), thisDate.month(), thisDate.Day())); 
                    }     
                    else if(fieldType == 'datetime')
                    {
                        updateObj.put(key, datetime.newInstance(Long.valueOf(formData.get(key)))); 
                    }
                    else if(fieldType == 'multipicklist')
                    {
                        updateObj.put(key, string.valueOf(formData.get(key)).replace('&',';')); 
                    }                                 
                    else if(key != 'id')
                    {                      
                        if(updateObj.get(key) != null)
                        {
                            updateObj.put(key,updateObj.get(key)+';'+formData.get(key));
                        }
                        else
                        {
                            updateObj.put(key,formData.get(key));
                        }
                    }                
                }
                catch(Exception e)
                {
                    returnObj.data += 'Field: ' +key +' bad value ' + formData.get(key) + ' ERR: ' + e.getMessage() + '\n';
                }       
            }
            objects.add(updateObj);
            returnObj.sObjects = objects;
            if(formData.containsKey('id'))
            {
                update objects;
            }
            else
            {
                insert objects;
            }
            returnObj.sObjects = objects;
        }
        catch(exception ex)
        {
            system.debug('\n\n\n\n-------------------- ERROR OCCURRED DURING Save!');
            system.debug(ex);        
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Error while saving. ' + ex.getMessage() + ' on line ' + ex.getLineNumber()));
                      
            returnObj.success = false;
            returnObj.data =  returnObj.data  + ' Error String: ' + ex.getMessage();
        }                    
        return returnObj;
    }  

    /**
    * @Description Creates a color based on a string value by hashing the string to MD5 (which is hexidecimal) and using the first 6 characters of the resulting digest value
    *              In this method we can generate a mostlye unique repeatable color from any string
    * @Param sourceValue the value to generate a color from
    * @Return string a hexidecimal color code, including the # sign.
    **/
    private static string getValueHexColor(string sourceValue)
    {      
        string keystring = sourceValue;
        Blob keyblob = Blob.valueof(keystring);
        string colorCode = '#'+encodingUtil.convertToHex(Crypto.generateDigest('MD5',keyblob)).substring(0,6);
        return colorCode;
    }
    
    /**
    * @Description Converts a string of url arguments (name/value pairs) into a map of string to string. Handles URL decoding as well. 
    * @Param argString a string of name value pairs in the URl encoded format. Such as field=value&field2=value2. Should not include beginning ? mark.
    * @Return a map of keys to their values.
    **/
    private static map<string,string> deserializeString(string argString)
    {
            string[] params = argString.split('&');
            map<String,String>  formParams = new map<String,String>();
            for(string p : params)
            {   
                string startParam = EncodingUtil.urlDecode(p.substring(0,p.indexOf('=')),'UTF-8');
                formParams.put(startParam,EncodingUtil.urlDecode(p.substring(p.indexOf('=')+1,p.length()),'UTF-8'));    
            }       
            return formParams;
    }   
    
    public class calException extends Exception {}                

}