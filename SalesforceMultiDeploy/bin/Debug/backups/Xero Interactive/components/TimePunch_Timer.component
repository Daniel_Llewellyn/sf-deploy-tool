<apex:component >

    <apex:attribute name="recordId" type="string" description="the id of the object to create this timer on" required="true"/>
    <apex:attribute name="style" type="string" description="a valid jquery ui style" required="false" default="dark-hive"/>
    
    <link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/{!style}/jquery-ui.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.js" />
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.js" />
    
    <script type="text/javascript">
    
    //A few global variables to track timer data
    var startTime = 0;
    var runningSeconds = 0;
    var timerId;
    timerRunning = false;
    
    $(document).ready(function() 
    {
            //Check to see if there are any running timers, if so set the timer value, populate the old timers dialog
            getTimerData('{!recordId}');
            
            //register some dialog boxes        
            $('#stopTimerForm').dialog({
                autoOpen: false,
                modal: true,
                height: 150                
            })

            $('#infoNotice').dialog({
                autoOpen: false,
                modal: true,
                height: 150                 
            })            

            $('#oldTimers').dialog({
                autoOpen: false,
                modal: false,
                height: 150                 
            })              
            
            //register some buttons
            $( "#start_Button" ).button({
                icons: {primary: "ui-icon-play"},
                text: false,
                disabled: false
            });      

            $( "#stop_Button" ).button({
                icons: {primary: "ui-icon-stop"},
                text: false,
                disabled: true
            });              

            $( "#show_oldTimers" ).button({
                icons: {primary: "ui-icon-comment"},
                text: false,
                disabled: true
            }); 
            
            $( "#submit_Button" ).button();                          
            
            //For nice styling, turn some of the divs on the timer into tabs
            $("#container").tabs();
            $( "#timerDisplay" ).tabs();    
            $( "#statusDisplay" ).tabs(); 
            
            //register some onClick handlers for the various buttons
             $( "#start_Button" ).click(function(){
                 createTimer('{!recordId}');
             });

             $( "#show_oldTimers" ).click(function(){
                 $('#oldTimers').dialog('open');
             });
             
             $( "#stop_Button" ).click(function(){                 
                 //stop the current timer, save to the DB
                 stopTimer(timerId);                
                 //re-fetch the timer data so the one that was just running clears, and is then
                 //added to the old timers list
                 getTimerData('{!recordId}');
                 //open the dialog to let a user add comments
                 $('#stopTimerForm').dialog('open');                 
             });     
              
             $( "#submit_Button" ).click(function(){
                 $('#stopTimerForm').dialog('close');
                 submitComment(timerId, $('#comments').val());
                 return false;
             });  
            
             //tell the timer that is displayed to the user to start running (only actually incriments time if there is a
             //currently running timer. Otherwise it just displays.
             setInterval("incrimentRunningTimer();",1000);        
    });    
    
    function submitComment(timerid, comments)
    {
        try
        {           
            timePunch_timerController.updateTimerComments(timerid,comments, function(result, event)
            {
                if(event.status)
                {  
            
                }
            }, {escape:true}); 
        }
        catch(exception)
        {
            showInfoNotice(exception);
        }     
    }
    
    //method for getting all the timers for this object, for this user, for this day.
    //it will also kick off the running timmer if one is running. It also is responsible
    //for populating the old timers dialog box.
    function getTimerData(objectId)
    {
        try
        {
            timePunch_timerController.getTimer(objectId, function(result, event)
            {
                if(event.status)
                {  
                    //reset the global variables back to default.
                    timerRunning = false;
                    startTime = 0;
                    runningSeconds = 0;
                    
                    //disable the stop button, enable the start button (this will be reversed later if there is a running timer)              
                    $('#stop_Button').button("option", "disabled", true );  
                    $('#start_Button').button("option", "disabled", false );              
                    $('#statusDisplay').html('Pending');     
                    
                    //variable to hold the contents of the older timers dialog box.                                   
                    oldTimerList = '<ul>';
                    
                    //loop over all the timer objects we got from the remoting method/query
                    for(var i = 0; i < result.length; i++)
                    {
                        var thisTimer = result[i];
                        
                        //if we find a running timer, set the global timerId and then tell the timer 
                        //to actually start running (the one displayed to the user) 
                        if(thisTimer.Status__c == 'Running')
                        {
                            timerId = thisTimer.Id;
                            startTimer(thisTimer.Running_Seconds__c);
                        }
                        //if it isnt a running timer we are looking at, add it to the old timers list that the user can review by clicking the 
                        //button which launches a popup.
                        else
                        {
                            $('#show_oldTimers').button("option", "disabled", false );
                            var timer = secondsToTime(thisTimer.Final_Seconds__c);
                            var timerString = pad2(timer.h) +':' +  pad2(timer.m)  + ':' +  pad2(timer.s);                                                  
                            oldTimerList += '<li>'+thisTimer.Name+' - '+timerString+'</li>';
                        }
                    }  
                    oldTimerList += '</ul>'; 
                    $('#oldTimers').html(oldTimerList);               
                }
            }, {escape:true}); 
        }
        catch(exception)
        {
            showInfoNotice(exception);
        }           
    }
    
    function showInfoNotice(message)
    {
        $('#infoNotice').html(message);
        $('#infoNotice').dialog('open');
        
    }
    
    //send a request to apex to create a new timer record for the given record (objectId)
    function createTimer(objectId)
    {
        try
        {
            timePunch_timerController.createTimer(objectId, function(result, event)
            {
               //provided the creation went well, set the running timer id and start the timer.
               timerId = result.Id;
               startTimer(0);
            }, {escape:true});  
        }        
        catch(exception)
        {
            showInfoNotice(exception);
        }        
    }

    //send request to stop the running timer, specifid by the id of the timer we want to stop
    function stopTimer(timerId)
    {
        try
        {
            timePunch_timerController.stopTimer(timerId, function(result, event)
            {
               //once the timer has stopped, 'refresh' all the data be calling the getTimerData method, which
               //basically resets everything so a new timer can be started if desierd.              
               getTimerData('{!recordId}');
            }, {escape:true});  
        }        
        catch(exception)
        {
            showInfoNotice(exception);
        }        
    }    
    
    //simple method for enabled and disabling buttons and setting a few variables
    //that need to get setup when a timer starts running (moves from pending to running)
    function startTimer(timerInitValue)
    {
        try
        {
            $('#start_Button').button("option", "disabled", true );
            $('#stop_Button').button("option", "disabled", false );
            $('#statusDisplay').html('Running');
            startTime = timerInitValue;
            timerRunning  = true;   
        } 
        catch(exception)
        {
            showInfoNotice(exception);
        }     
    }
    
    //method for ajusting the display timer to show the time elapsed since the timer started.
    function incrimentRunningTimer()
    { 
        try
        {      
            if(timerRunning)
            {
                runningSeconds ++; 
            }    
            var totalSeconds = runningSeconds +  startTime;
            var timer = secondsToTime(totalSeconds)
            $('#timerDisplay').html( pad2(timer.h) +':' +  pad2(timer.m)  + ':' +  pad2(timer.s) );
        } 
        catch(exception)
        {
            showInfoNotice(exception);
        }         
    }

    //formats a number for display in the timer. Pads with a leading 0 if needed.
    function pad2(number) {
         return (number < 10 ? '0' : '') + number
    }

    //converts a number of seconds into it's hour, minute, and second equivilent (decimal time)
    function secondsToTime(secs)
    {
        var hours = Math.floor(secs / (60 * 60));
        
        var divisor_for_minutes = secs % (60 * 60);
        var minutes = Math.floor(divisor_for_minutes / 60);
    
        var divisor_for_seconds = divisor_for_minutes % 60;
        var seconds = Math.ceil(divisor_for_seconds);
        
        var obj = {
            "h": hours,
            "m": minutes,
            "s": seconds
        };
        return obj;
    }
    
    </script>
    
    <style>
        #container
        {
            width: 520px;
            height: 50px;
            margin-left:auto;
            margin-right:auto;
            margin-top:50px;
            padding-left:5px;
            padding-right:5px;
            padding-top:0x;
        }
        #logoDisplay
        {
            width: 38px;
            height: 38px
            padding: 3px;
            float:left;
            padding-top:10px;
        }
        #timerDisplay
        {
            width:130px;
            height:38px;
            text-align:center;
            font-size:28px;            
            margin-right:2px;
            float:left;
            margin-top:2px;
            
        }
        #statusDisplay
        {
            margin-top:2px;
            width: 120px; 
            height:38px;
            font-size:28px; 
            text-align:center;
            margin-left: 10px;
            float:left;
        }
        
        #timerButtons
        {
            float:right;
            width:150px;
            margin-left: 5px;
        }
        
        label
        {
            display:block;
        }
    </style>
    <div id="container">
        <div id="logoDisplay"><img src="https://c.na9.content.force.com/servlet/servlet.ImageServer?id=015E0000000taGH&oid=00DE0000000Hrgg&lastMod=1326755962000" title="TimePunch!" /></div>
        <div id="timerDisplay"></div>
        <div id="statusDisplay">Pending</div>
        <div id="timerButtons">
            <button id="start_Button">Start</button>
            <button id="stop_Button">End</button>
            <button id="show_oldTimers">Old Timers</button>
        </div>
    </div>
    
    <div id="stopTimerForm" title="Comments">
        <form id="timerFinishForm">
            <textarea name="comments" id="comments"></textarea>
            <button id="submit_Button">Finish</button>
        </form>
    </div>
    
    <div id="oldTimers" title="Stopped Timers">
    
    </div>
    <div id="infoNotice" title="Alert!">
    
    </div>
</apex:component>