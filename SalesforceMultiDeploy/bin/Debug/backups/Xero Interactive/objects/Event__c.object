<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>A stampforce event container object for which sponsors and stamps are related.</description>
    <enableActivities>false</enableActivities>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <fields>
        <fullName>Active__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Is this event active?</description>
        <externalId>false</externalId>
        <inlineHelpText>Is this event active?</inlineHelpText>
        <label>Active</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Address__c</fullName>
        <description>The address this event is happening at</description>
        <externalId>false</externalId>
        <inlineHelpText>The address this event is happening at</inlineHelpText>
        <label>Address</label>
        <length>50</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>City__c</fullName>
        <description>City this event is occurring in</description>
        <externalId>false</externalId>
        <inlineHelpText>City this event is occurring in</inlineHelpText>
        <label>City</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Default_Stamp_Image__c</fullName>
        <defaultValue>&quot;http://xerointeractive-developer-edition.na9.force.com/partyForce/resource/1313094448000/stampforce/img/check.gif&quot;</defaultValue>
        <description>An image to use as the stamp if the sponsor does not have a custom stamp image</description>
        <externalId>false</externalId>
        <inlineHelpText>An image to use as the stamp if the sponsor does not have a custom stamp image</inlineHelpText>
        <label>Default Stamp Image</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Url</type>
    </fields>
    <fields>
        <fullName>Description__c</fullName>
        <description>A short description of this event.</description>
        <externalId>false</externalId>
        <inlineHelpText>A short description of this event.</inlineHelpText>
        <label>Description</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>5</visibleLines>
    </fields>
    <fields>
        <fullName>Enable_QR_Codes__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Should sponsors be able to generate QR codes for badges, and should users be able to have their QR codes scanned to get badges?</description>
        <externalId>false</externalId>
        <inlineHelpText>Should sponsors be able to generate QR codes for badges, and should users be able to have their QR codes scanned to get badges?</inlineHelpText>
        <label>Enable QR Codes</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>End__c</fullName>
        <description>When does this event end?</description>
        <externalId>false</externalId>
        <inlineHelpText>When does this event end?</inlineHelpText>
        <label>End</label>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Missing_Stamp_Image__c</fullName>
        <defaultValue>&quot;http://xerointeractive-developer-edition.na9.force.com/partyForce/resource/1313094448000/stampforce/img/redx.gif&quot;</defaultValue>
        <description>The icon to be displayed to a user if they do not have a particular stamp.</description>
        <externalId>false</externalId>
        <inlineHelpText>The icon to be displayed to a user if they do not have a particular stamp.</inlineHelpText>
        <label>Missing Stamp Image</label>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Url</type>
    </fields>
    <fields>
        <fullName>Number_of_Stamps_Available__c</fullName>
        <description>How many stamps are available to get in this event?</description>
        <externalId>false</externalId>
        <inlineHelpText>How many stamps are available to get in this event?</inlineHelpText>
        <label>Number of Stamps Available</label>
        <summaryFilterItems>
            <field>Sponsorship__c.Is_Stamp_Provider__c</field>
            <operation>equals</operation>
            <value>true</value>
        </summaryFilterItems>
        <summaryForeignKey>Sponsorship__c.Event__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Postal_Code__c</fullName>
        <description>Zip/postal code this event is occurring in.</description>
        <externalId>false</externalId>
        <inlineHelpText>Zip/postal code this event is occurring in.</inlineHelpText>
        <label>Postal Code</label>
        <length>10</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Stamp_Acquired_Image__c</fullName>
        <description>Default image used for acquired stamps (if not over written by the sponsor)</description>
        <externalId>false</externalId>
        <formula>IMAGE( Default_Stamp_Image__c, &apos;The stamp image provided if a user has a stamp&apos; )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Default image used for acquired stamps (if not over written by the sponsor)</inlineHelpText>
        <label>Stamp Acquired Image</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Stamp_Missing_Image__c</fullName>
        <description>Image to be used if a given stamp has not been collected by a user</description>
        <externalId>false</externalId>
        <formula>IMAGE( Missing_Stamp_Image__c , &apos;Image to use if stamp is not available&apos;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Image to be used if a given stamp has not been collected by a user</inlineHelpText>
        <label>Stamp Missing Image</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Start__c</fullName>
        <description>When does this event start?</description>
        <externalId>false</externalId>
        <inlineHelpText>When does this event start?</inlineHelpText>
        <label>Start</label>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>State__c</fullName>
        <description>Which state is this event occurring in</description>
        <externalId>false</externalId>
        <inlineHelpText>Which state is this event occurring in</inlineHelpText>
        <label>State</label>
        <length>2</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Title__c</fullName>
        <description>A title for this event to be displayed to users</description>
        <externalId>false</externalId>
        <inlineHelpText>A title for this event to be displayed to users</inlineHelpText>
        <label>Title</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Event</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Event Name</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Events</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
</CustomObject>
