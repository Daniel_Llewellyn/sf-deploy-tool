trigger ClickedCallTriggers on Clicked_Call__c  (before insert, after insert)
{
    if(Trigger.isBefore)
    {
        for(Clicked_Call__c call : Trigger.new)
        {
            call.status__c = 'Queued';
        }
    }
    if(Trigger.isAfter)
    {
        clicked_controller.placeTwilioCall(Trigger.newMap.keySet());
    }
}