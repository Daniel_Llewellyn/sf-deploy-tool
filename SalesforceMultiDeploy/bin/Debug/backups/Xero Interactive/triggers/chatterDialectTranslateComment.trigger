trigger chatterDialectTranslateComment on FeedComment (before insert, before update) 
{
    String[] filterFields = new String[] {'CommentBody'};
    chatter_dialect_controller.translateObject(trigger.new,filterFields); 
}