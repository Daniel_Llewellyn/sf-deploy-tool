trigger noChatterFeedComment on FeedComment (before insert, before update) 
{
    noChatter.blockChatterDisabledUsersFromPosting(trigger.new); 
}