trigger noChatterFeedItem on FeedItem (before insert, before update) 
{
    noChatter.blockChatterDisabledUsersFromPosting(trigger.new); 
}