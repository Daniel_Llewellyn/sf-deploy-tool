trigger populateNotifierLookup on Challenge_Comment_Notifier__c (before insert, before update)
{
    //populate the member__c lookup field using the data in the member_reference field if it is provided
    challengeService.populatLookup(trigger.new);
}