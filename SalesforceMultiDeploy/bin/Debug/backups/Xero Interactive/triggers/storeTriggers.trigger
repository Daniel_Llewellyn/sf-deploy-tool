trigger storeTriggers on Store__c (after insert, after update) {

    if(!weThePeople.inFutureContext)
    {
        list<id> storeIds = new list<id>();
        
        for (store__c store : trigger.new)
        {       
            if (store.Store_Location__Latitude__s == null)
            {
                storeIds.add(store.id);
            }
        }
        weThePeople.getStoreLocation(storeIds);
    }
}