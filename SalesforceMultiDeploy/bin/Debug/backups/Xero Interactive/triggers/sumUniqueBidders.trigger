/**********************************
Name:  sumUniqueBidders
Author: Daniel Llewellyn
Date: 3/16/2008
Description: Will find the amount of unique bidders on an item and update the value Number_of_Bidders__c accordingly. Fires when bids are saved, so the totals should always be accurate

************************************/
trigger sumUniqueBidders on Auction_Bid__c (after update, after insert, after delete, after undelete) 
{
    list<Auction_Bid__c> bids;
    map<id,set<string>> itemToBiddersMap = new map<id,set<string>>();
    
    
    if(trigger.isDelete)
    {
        bids = trigger.Old;
    }
    else
    {
        bids = trigger.New;
    }
    
    for(Auction_Bid__c bid : bids)
    {
         itemToBiddersMap.put(bid.auction_item__c, new set<string>());
    }
    
    list<Auction_item__c> items = [select id, (select uniqueBidderId__c from Auction__r) from Auction_item__c where id in :itemToBiddersMap.keySet()];
    
    for(Auction_item__c item : items)
    {
        set<string> uniqueBidders = itemToBiddersMap.get(item.id);
        for(Auction_Bid__c bid : item.Auction__r)
        {
            uniqueBidders.add(bid.uniqueBidderId__c);
        }
        itemToBiddersMap.put(item.id,uniqueBidders);
    }
    
    for(Auction_item__c item : items)
    {
        item.Number_of_Bidders__c =  itemToBiddersMap.get(item.id).size();
    }
    
    update items; 
}