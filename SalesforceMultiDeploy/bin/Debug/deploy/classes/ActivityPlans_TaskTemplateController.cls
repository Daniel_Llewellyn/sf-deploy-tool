/******* Activity Plans Task Template Controller *********
@Author Daniel Llewellyn/RedKite (dllewellyn@redkitetechnologies.com)
@Date ~4/2014
@Description Handles UI overide for the creation and editing of the activity plan task templates. There are many
              fields which need to contain specific kinds of data that are not supported by Salesforce out of the box
              so we override the UI with more user friendly controls (mostly dynamically generated picklists created from
              data within the org).
**********************************************/
public class ActivityPlans_TaskTemplateController {

    public Activity_Plan_Task_Template__c thisTemplate { get; set;}
    
    //select list option containers
    public SelectOption[] allTaskStatuses      { get; set; }   
    public SelectOption[] allTaskPriorities    { get; set; }
    public SelectOption[] allTaskRecordTypes   { get; set; }
    public SelectOption[] allEmailTemplates    { get; set; }
    public SelectOption[] allObjectFields      { get; set; }   
    public SelectOption[] allAssignToFields    { get; set; }
    public SelectOption[] allRelatedToFields   { get; set; }
    public SelectOption[] allDateFields        { get; set; }
    
    //selected item containers
    public string selectedTaskStatus           { get; set; }
    public string selectedTaskPriority         { get; set; }
    public string selectedTaskRecordType       { get; set; }
    public string selectedEmailTemplate        { get; set; }
    public string selectedAssignedToVal        { get; set; }
    public string selectedRelatedToVal         { get; set; }
    public string selectedDueDateOffsetField   { get; set; }
    public SelectOption[] selectedTaskStatuses { get; set; }
    
    public string parentObjectType;
    public string objectLabel{get;set;}
    public String message { get; set; }
    public boolean initError{get;set;}
    
    /**
    * Constructor for the page. Handles finding values for the picklists and building the lists of options. These
    * picklist options are then returned to the UI which uses them to draw the options on the screen for the user to choose from.
    **/
    public ActivityPlans_TaskTemplateController(ApexPages.StandardController controller) {
        //get record detail
        try
        {
            thisTemplate = (Activity_Plan_Task_Template__c) controller.getRecord();
            
            map<string,set<string>> excludedValues = new map<string,set<string>>();
            
            //for some reason i don't understand the reference to the object passed into the controller doesnt have all the fields, so I have to run a query to get all the detail data I need about the object.
            if(thisTemplate.id != null)
            {
                thisTemplate = [select  id, 
                                         Name,
                                         Activity_Plan__c,
                                         Assigned_To_Type__c,
                                         Assigned_To_Value__c,
                                         Due_Date_Type__c,
                                         Due_Date_Value__c,
                                         Priority__c,
                                         Related_To_Type__c,
                                         Related_To_Value__c,
                                         Task_Status__c,
                                         Record_Type_Name__c,
                                         Allow_Duplicate_Tasks__c,
                                         Depends_On__c,
                                         Depends_On_Completion_Statuses__c,
                                         Depends_On__r.Name,
                                         Description__c,
                                         Notification_Email_Template__c,
                                         Task_Weight__c,
                                         Order__c,
                                         Assigned_To_Lookup__c,
                                         Due_Date_Static_Date__c, 
                                         Substantive_Response__c,
                                         Due_Date_Offset_Source__c 
                                  from Activity_Plan_Task_Template__c 
                                  where id = :thisTemplate.id];
            }
            
            id parentPlanId = thisTemplate.Activity_Plan__c != null ? thisTemplate.Activity_Plan__c : ApexPages.currentPage().getParameters().get('CF00Ni000000CTHHL_lkid');  
    
            if(parentPlanId == null)
            {
                throw new applicationException('Error: Unable to determine parent activity plan. Cannot deduce object type. Please create rules from the Activity Plan Related List');                   
            }             

            selectedTaskStatus = thisTemplate.Task_Status__c;
            selectedTaskPriority = thisTemplate.Priority__c;
            selectedTaskRecordType = thisTemplate.Record_Type_Name__c;
            selectedEmailTemplate = thisTemplate.Notification_Email_Template__c;
            selectedAssignedToVal = thisTemplate.Assigned_To_Value__c;
            selectedRelatedToVal = thisTemplate.Related_To_Value__c;
            selectedDueDateOffsetField = thisTemplate.Due_Date_Offset_Source__c;
                             
            //init all the select option lists.
            selectedTaskStatuses = new List<SelectOption>();
            allTaskStatuses      = new List<SelectOption>();
            allTaskPriorities    = new List<SelectOption>();
            allTaskRecordTypes   = new List<SelectOption>();
            allEmailTemplates    = new List<SelectOption>();
            allObjectFields      = new List<SelectOption>();
            allAssignToFields    = new List<SelectOption>();
            allRelatedToFields   = new List<SelectOption>();
            allDateFields        = new List<SelectOption>();
            
            List<Schema.PicklistEntry> taskStatusField = Task.Status.getDescribe().getPicklistValues();
            List<Schema.PicklistEntry> taskPriorityField = Task.Priority.getDescribe().getPicklistValues();
            
            
            //get parent object type
            Activity_Plan__c parentPlan = [select id, Excluded_Picklist_Values__c, Object_Types__c from Activity_Plan__c where id = :thisTemplate.Activity_Plan__c];
            parentObjectType = parentPlan.Object_Types__c;
            
            //the parent plan has exlcuded picklist values held in a JSON object, so lets deserialize that now map of a list of strings (field name to excluded values)
            if(parentPlan.Excluded_Picklist_Values__c != null)
            {
                try
                {
                    excludedValues = (map<string,set<string>>) JSON.deserialize(parentPlan.Excluded_Picklist_Values__c, map<string,set<string>>.class);
                    system.debug('\n\n\n\n-------------------------------------------------------  CONSTRUCTED MAP FROM EXCLUDED PICKLIST VALUES!');
                    system.debug(excludedValues);
                    system.debug('\n\n\n\n');
                }
                catch(exception ex)
                {
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'Unable to parse field value exclusions from parent record. ' + ex.getMessage());
                    ApexPages.addMessage(myMsg);                   
                }
            }
            List<Schema.DescribeSObjectResult> describeSobjectsResult = Schema.describeSObjects(new List<String>{parentObjectType}); // this can accept list of strings, we describe only one object here
            objectLabel = describeSobjectsResult[0].getLabel();
 
             //add some default values for the email tempate and date fields.
            allEmailTemplates.add(new SelectOption('', 'None'));
            allDateFields.add(new SelectOption('', 'None (Use Triggering Date)'));
            allRelatedToFields.add(new SelectOption('Id','('+objectLabel+') Record Id'));
                       
            //set some defaults that are likely going to be what the user wants
            if(thisTemplate.Related_To_Value__c == null)
            {
                thisTemplate.Related_To_Value__c = 'Id';
            }
            if(thisTemplate.Related_To_Type__c == null)
            {
                thisTemplate.Related_To_Type__c = 'Field Value';
            }
            if(thisTemplate.Due_Date_Type__c == null)
            {
                thisTemplate.Due_Date_Type__c = 'Day Offset';
            }
            if(thisTemplate.Assigned_To_Type__c == null)
            {
                thisTemplate.Assigned_To_Type__c = 'Field Value';
            }
            if(thisTemplate.Assigned_To_Value__c == null)
            {
                thisTemplate.Assigned_To_Value__c = 'ownerid';
            }
            string[] completionStatuses = thisTemplate.Depends_On_Completion_Statuses__c != null ? thisTemplate.Depends_On_Completion_Statuses__c.split(',') : new list<string>(); 
            for(string thisSelectOption : completionStatuses)
            {
                if(!excludedValues.containsKey('Depends_On_Completion_Statuses__c') || !excludedValues.get('Depends_On_Completion_Statuses__c').contains(thisSelectOption))
                {
                    selectedTaskStatuses.add(new SelectOption(thisSelectOption, thisSelectOption));
                }
            }
            //Get all status field picklist entries and add to list
            for ( Schema.PicklistEntry f : taskStatusField) {
                if(!excludedValues.containsKey('Task_Status__c') || !excludedValues.get('Task_Status__c').contains(f.getValue()))
                {
                    allTaskStatuses.add(new SelectOption(f.getLabel(), f.getValue()));
                }
            }   
    
            //get all priority field picklist entries and add to list
            for ( Schema.PicklistEntry f : taskPriorityField) {
                if(!excludedValues.containsKey('Priority__c') || !excludedValues.get('Priority__c').contains(f.getValue()))
                {
                    allTaskPriorities.add(new SelectOption(f.getLabel(), f.getValue()));
                }
            }  
            
            //get all task record types and add to list
            list<RecordType> taskRecordTypes = [select developername, name from recordType where sObjectType = 'task'];
            for ( RecordType r : taskRecordTypes) {
                if(!excludedValues.containsKey('Record_Type_Name__c') || !excludedValues.get('Record_Type_Name__c').contains(r.developerName))
                {
                    allTaskRecordTypes.add(new SelectOption(r.developerName, r.Name));
                }
            }             
            
            //get all email templates and add to list         
            list<EmailTemplate> emailTemplates = [select developername, name, TemplateType from EmailTemplate where isActive = true];
            for ( EmailTemplate e : emailTemplates) {
                if(!excludedValues.containsKey('Notification_Email_Template__c') || !excludedValues.get('Notification_Email_Template__c').contains(e.developerName))
                {
                    allEmailTemplates.add(new SelectOption(e.developerName, '('+e.templateType+')'+e.name));
                }
            }   
            
            //get all fields on the parent object type to extract relatedTo and AssignedTo fields
            String[] types = new String[]{parentObjectType};
            Schema.DescribeSobjectResult[] results = Schema.describeSObjects(types);
            if(!results.isEmpty())
            {            
                
                Map<String, Schema.SObjectField> objectFields = results[0].fields.getMap();
                list<string> fieldNames = new list<string>(objectFields.keySet());
                fieldNames.sort();
                
                for(String field : fieldNames)
                {
                    Schema.DescribeFieldResult dr = objectFields.get(field).getDescribe();
                    allObjectFields.add(new SelectOption(field, dr.getLabel()));
                    string fieldType = dr.getType().name();
                   
                    if(fieldType == 'reference' )
                    {
                        //for this to be a valid item to assign to the lookup must be a reference to user
                        List <Schema.sObjectType> referenceToTypes = dr.getReferenceTo();
                        Set<string> referenceToObjectTypesSet = new set<string>();
                        for(Schema.sObjectType refObjectType : referenceToTypes)
                        {
                            if(!excludedValues.containsKey('Related_To_Value__c') || !excludedValues.get('Related_To_Value__c').contains(refObjectType.getDescribe().getName()))
                            {
                                referenceToObjectTypesSet.add(refObjectType.getDescribe().getName()); 
                            }
                        }
                        if(referenceToObjectTypesSet.contains('User') && (!excludedValues.containsKey('Assigned_To_Value__c') || !excludedValues.get('Assigned_To_Value__c').contains(field)) )
                        {                                
                            allAssignToFields.add(new SelectOption(field, '('+objectLabel+') ' + dr.getLabel()));
                        }
    
                        allRelatedToFields.add(new SelectOption(field, '('+objectLabel+') ' + dr.getLabel()));
                    }
                    if(fieldType == 'date')
                    {
                        allDateFields.add(new SelectOption(field, '('+objectLabel+') ' + dr.getLabel()));
                    }
                } 
            }
        }
        catch(exception ex)
        {
            initError = true;
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,ex.getMessage());
            ApexPages.addMessage(myMsg);         

        }       
    }

    /** 
    * Removes a given picklist value from a picklist. Useful for removing options you may not want a user to be able to select in the interface
    * when it is being dynamically built, but do not want to remove the option from the source entirly.
    * @param options list of selectOption objects to find value to remove in
    * @param values list of strings that contains the values to remove
    * @return list of select options with given value removed if present. Otherwise identical list is returned.
    **/
    public static list<selectOption> removePicklistValue(list<selectOption> options, list<string> values)
    {
        set<string> valuesSet = new set<string>(values);
        list<integer> indexesToRemove = new list<integer>();
        
        integer counter = 0;
        //loop over options
        for(selectOption thisOption : options)
        {
            //see if this options value in contains in the list of things to remove. If so records it's index position.
            if(valuesSet.contains(thisOption.getValue()))
            {
                indexesToRemove.add(counter);
            } 
            counter++;
        }
        
        //iterate over indexes backwards to prevent collapsing the array and getting everything all out of order.
        for(integer index = indexesToRemove.size(); index > 0; index--)
        {
            options.remove(indexesToRemove[index-1]);
        }
        return options;
    }
    
    
    /**
    * Handles saving of the editing task template record. Any special data massaging that needs to happen to get the data
    * from the UI format to acceptable database format happens here (like constructing a comma separated list of values from an array)
    * happens here.
    * @return pageReference where to return the user after save. Null means return to the same page.
    **/
    public PageReference save() {
        message = 'Selected Completion Statuses: ';
        string selectedCompletionStatuses = '';
        for ( SelectOption so : selectedTaskStatuses ) {
             selectedCompletionStatuses += so.getValue() +',';
        }
        thisTemplate.Depends_On_Completion_Statuses__c = selectedCompletionStatuses;
        thisTemplate.Task_Status__c = selectedTaskStatus;
        thisTemplate.Priority__c = selectedTaskPriority;
        thisTemplate.Record_Type_Name__c = selectedTaskRecordType;
        thisTemplate.Notification_Email_Template__c = selectedEmailTemplate;
        thisTemplate.Assigned_To_Value__c = selectedAssignedToVal;
        thisTemplate.Related_To_Value__c = selectedRelatedToVal;
        thisTemplate.Due_Date_Offset_Source__c = selectedDueDateOffsetField;

        upsert thisTemplate;
        
        PageReference newpage = new PageReference('/'+thisTemplate.id);

        newpage.setRedirect(true);
        return newpage;
    }
    
    public class applicationException extends Exception {}
}