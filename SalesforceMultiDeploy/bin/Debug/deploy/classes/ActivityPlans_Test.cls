@isTest
global class ActivityPlans_Test
{
    @isTest
    global static void testActivityPlans()
    {   
        //first create a new activity plan
        Activity_Plan__c thisPlan = new Activity_Plan__c();
        thisPlan.active__c = true;
        thisPlan.Allow_Duplicate_Tasks__c = false;
        thisPlan.Object_Types__c = 'Account';
        
        insert thisPlan;
        
        list<EmailTemplate> emailTemplates = [select developername, name, TemplateType from EmailTemplate where isActive = true and TemplateType = 'visualforce'];
        
        Activity_Plan_Task_Template__c taskTemplate = new Activity_Plan_Task_Template__c();
        taskTemplate.Name = 'Template 1';
        taskTemplate.Activity_Plan__c = thisPlan.Id;
        taskTemplate.Description__c = 'this is a test of task template engine on [object.name]';
        taskTemplate.Assigned_To_Type__c = 'Field Value';
        taskTemplate.Assigned_To_Value__c = 'OwnerId';
        taskTemplate.Due_Date_Type__c = 'Day Offset';
        taskTemplate.Due_Date_Value__c = '0';
        taskTemplate.Priority__c = 'High';
        taskTemplate.Related_To_Type__c = 'Field Value';
        taskTemplate.Related_To_Value__c = 'Id';
        taskTemplate.Task_Status__c = 'In Progress';
        taskTemplate.Depends_On_Completion_Statuses__c = '';
        taskTemplate.Order__c = 1;
        taskTemplate.Substantive_Response__c = true;
        taskTemplate.Task_Weight__c = 1;
        if(!emailTemplates.isEmpty())
        {
            taskTemplate.Notification_Email_Template__c = emailTemplates[0].Id;
        }
        
        insert taskTemplate;

        Activity_Plan_Task_Template__c taskTemplate2 = new Activity_Plan_Task_Template__c();
        taskTemplate2.Name = 'Template 2';
        taskTemplate2.Activity_Plan__c = thisPlan.Id;
        taskTemplate2.Description__c = 'this is a test of task template engine on [object.name]';
        taskTemplate2.Assigned_To_Type__c = 'Field Value';
        taskTemplate2.Assigned_To_Value__c = 'OwnerId';
        taskTemplate2.Due_Date_Type__c = 'Day Offset';
        taskTemplate2.Due_Date_Value__c = '0';
        taskTemplate2.Priority__c = 'High';
        taskTemplate2.Related_To_Type__c = 'Field Value';
        taskTemplate2.Related_To_Value__c = 'Id';
        taskTemplate2.Task_Status__c = 'In Progress';
        taskTemplate2.Depends_on__c = taskTemplate.id;
        taskTemplate2.Depends_On_Completion_Statuses__c = 'Complete';
        taskTemplate2.Order__c = 1;
        taskTemplate2.Substantive_Response__c = true;
        taskTemplate2.Task_Weight__c = 1;
        if(!emailTemplates.isEmpty())
        {
            taskTemplate2.Notification_Email_Template__c = emailTemplates[0].Id;
        }
        
        insert taskTemplate2;
                
        Activity_Plan_Rule__c thisRule = new Activity_Plan_Rule__c();
        thisRule.Activity_Plan__c = thisPlan.Id;
        thisRule.Comparison_Value__c = 'MN';
        thisRule.Field_Name__c = 'billingstate';
        thisRule.Logical_Operator__c = 'Equal';
        thisRule.Comparison_Type__c = 'Static Value';
        insert thisRule;
        
        Account thisAccount = new Account();
        thisAccount.name = 'My Test Account';
        thisAccount.billingstate = 'MN';
        thisAccount.ownerId = UserInfo.getUserId();
        insert thisAccount;
        
        list<account> accountList = new list<account>();
        accountList.add(thisAccount);
        ActivityPlansController.findMatchingActivityPlans(accountList);
        
        list<task> taskData = [select id, subject, activitydate, priority, status, ownerId from task where whatId = :thisAccount.id];
        //system.assert(taskData.size() > 0);
        //system.assertEquals(taskTemplate.Task_Status__c,taskData[0].status);
        
        update thisAccount;
        
        system.assertEquals(ActivityPlansController.dynamicIf(1,'equal',1),true);
        system.assertEquals(ActivityPlansController.dynamicIf(1,'notequal',2),true);
        system.assertEquals(ActivityPlansController.dynamicIf(1,'greaterthan',0),true);
        system.assertEquals(ActivityPlansController.dynamicIf(1,'lessthan',3),true);
        system.assertEquals(ActivityPlansController.dynamicIf('mystring','contains','string'),true);
        system.assertEquals(ActivityPlansController.dynamicIf('mystring','doesnotcontain','bannana'),true);
        
        //Test Page Overrides
        
        ApexPages.StandardController sc = new ApexPages.standardController(taskTemplate);
        ActivityPlans_TaskTemplateController templateController = new  ActivityPlans_TaskTemplateController(sc);

        PageReference pageRef = Page.ActivityPlans_TaskTemplate;
        pageRef.getParameters().put('id', taskTemplate.Id);
        Test.setCurrentPage(pageRef);    
        
        templateController.selectedTaskStatus = 'closed';
        templateController.selectedTaskPriority = 'high';
        templateController.selectedAssignedToVal = 'Static Value';
        templateController.selectedRelatedToVal = 'OwnerId';

  
        
        templateController.save();
        
        // Test Rule UI Override
        ApexPages.StandardController sc2 = new ApexPages.standardController(thisRule);
        ActivityPlans_RuleController ruleController = new ActivityPlans_RuleController(sc2);

        pageRef = Page.ActivityPlans_Rule;
        pageRef.getParameters().put('id', thisRule.Id);
        Test.setCurrentPage(pageRef);    
        
        ruleController.save();  
        
        ruleController.thisrule.Comparison_Type__c = 'Object Field Value';
        ruleController.thisrule.Comparison_Value__c = 'Name';
        ruleController.selectedCompareField = 'Name';
        ruleController.save();
        
        
        //Test Add Library Task Page

        ApexPages.StandardController sc3 = new ApexPages.standardController(thisPlan);
        ActivityPlans_AddLibraryTaskController addLibTaskController = new ActivityPlans_AddLibraryTaskController(sc3);

        pageRef = Page.ActivityPlans_AddLibraryTask;
        pageRef.getParameters().put('id', thisPlan.Id);
        Test.setCurrentPage(pageRef);    
        
        
        //Search for a task template
        addLibTaskController.searchText = taskTemplate2.Name; 
        addLibTaskController.search();
        
        //ensure one result
        system.assertEquals(addLibTaskController.planTemplates.size(),1);
        
        //mark the planes for cloning
        for(ActivityPlans_AddLibraryTaskController.taskTemplateWrapper thisPlanWrapper : addLibTaskController.planTemplates)
        {
            thisPlanWrapper.isChecked = true;
        }
        
        addLibTaskController.save();
                
        string fieldData = ActivityPlans_RuleController.getFieldType('Contact','Name'); 
    }
    
    @isTest(seeAllData=true)
    global static void testSendEmail()
    {
        list<EmailTemplate> emailTemplates = [select developername, name, TemplateType from EmailTemplate where isActive = true and TemplateType = 'visualforce'];
        list<contact> testContact = [select id, name, phone from contact];
        if(!emailTemplates.isEmpty())
        {
            ActivityPlansController.sendEmailTemplate(emailTemplates[0].id,UserInfo.getUserId(), testContact[0].id);
        }
    }
    
    
}