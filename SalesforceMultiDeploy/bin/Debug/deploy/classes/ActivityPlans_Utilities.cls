/******* ActivityPlans_Utilities *********
Author: Daniel Llewellyn/RedKite (dllewellyn@redkitetechnologies.com)
Date: ~5/2014
Description: Common/shared and abstracted methods that can be used by other classes/methods.
**********************************************/

global class ActivityPlans_Utilities
{

    /**
    * builds an SOQL query string that will select all fields for a given object type.
    * @param objectType a string that is the API name of the object to build the select all string for
    * @return string that can be used in a dynamic SOQL query to select all fields
    */
    public static string buildSelectAllStatment(string objectType)
    {
        Map<String, Schema.SObjectField> fldObjMap = Schema.getGlobalDescribe().get(objectType).getDescribe().fields.getMap();
        List<Schema.SObjectField> fldObjMapValues = fldObjMap.values();
        String theQuery = 'SELECT ';
        for(Schema.SObjectField s : fldObjMapValues)
        {  
           theQuery += s.getDescribe().getName() + ',';
        }
        
        // Trim last comma
        theQuery = theQuery.subString(0, theQuery.length() - 1);
        
        // Finalize query string
        theQuery += ' FROM '+objectType;    
        
        return theQuery;
    }
}