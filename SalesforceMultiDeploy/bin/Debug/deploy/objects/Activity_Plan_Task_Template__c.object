<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <content>ActivityPlans_TaskTemplate</content>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Visualforce</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <content>ActivityPlans_TaskTemplate</content>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Visualforce</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <content>ActivityPlans_TaskTemplate</content>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Visualforce</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>A single task to be created as a result of the parent activity plan being triggered</description>
    <enableActivities>true</enableActivities>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <fields>
        <fullName>Activity_Plan__c</fullName>
        <description>The parent activity plan this template is assigned to</description>
        <externalId>false</externalId>
        <inlineHelpText>The parent activity plan this template is assigned to</inlineHelpText>
        <label>Activity Plan</label>
        <referenceTo>Activity_Plan__c</referenceTo>
        <relationshipLabel>Activity Plan Task Templates</relationshipLabel>
        <relationshipName>Activity_Plan_Task_Templates</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Allow_Duplicate_Tasks__c</fullName>
        <externalId>false</externalId>
        <formula>Activity_Plan__r.Allow_Duplicate_Tasks__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Allow Duplicate Tasks</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Assigned_To_Lookup__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Field used to back user lookup for the visualforce page. This is used to search for a specific user to assign a task to. Value from this field if populated (and assign to type is set to static user) will be copied to the Assigned To field.</description>
        <externalId>false</externalId>
        <inlineHelpText>Field used to back user lookup for the visualforce page. This is used to search for a specific user to assign a task to. Value from this field if populated (and assign to type is set to static user) will be copied to the Assigned To field.</inlineHelpText>
        <label>Assigned To Lookup</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Activity_Plan_Task_Templates</relationshipName>
        <required>false</required>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Assigned_To_Type__c</fullName>
        <description>How is the assignee of the task determined. Is it pre-set on this template to a specific person or queue, or a field on the triggering object?</description>
        <externalId>false</externalId>
        <inlineHelpText>How is the assignee of the task determined. Is it pre-set on this template to a specific person or queue, or a field on the triggering object?</inlineHelpText>
        <label>Assigned To Type</label>
        <picklist>
            <picklistValues>
                <fullName>Static Value</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>Field Value</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Assigned_To_Value__c</fullName>
        <description>The name of the person or queue if Assigned To Type is static value, or the name of the field if the assigned to type is field value.</description>
        <externalId>false</externalId>
        <inlineHelpText>The name of the person or queue if Assigned To Type is static value, or the name of the field if the assigned to type is field value.</inlineHelpText>
        <label>Assigned To Value</label>
        <length>255</length>
        <required>true</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Creation_Method__c</fullName>
        <externalId>false</externalId>
        <label>Creation Method</label>
        <picklist>
            <picklistValues>
                <fullName>Automatic</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>Manual</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Depends_On_Completion_Statuses__c</fullName>
        <description>When the depended on task has a status matching any of the ones defined here (comma separated) create this task.</description>
        <externalId>false</externalId>
        <inlineHelpText>When the depended on task has a status matching any of the ones defined here (comma separated) create this task.</inlineHelpText>
        <label>Depends On Completion Statuses</label>
        <length>255</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Depends_On__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>When the depended on task has reached any of the given launch statuses create this task</description>
        <externalId>false</externalId>
        <inlineHelpText>When the depended on task has reached any of the given launch statuses create this task</inlineHelpText>
        <label>Depends On</label>
        <referenceTo>Activity_Plan_Task_Template__c</referenceTo>
        <relationshipLabel>Activity Plan Task Templates</relationshipLabel>
        <relationshipName>Activity_Plan_Task_Templates</relationshipName>
        <required>false</required>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Description__c</fullName>
        <description>Description to set on the created task. You may reference fields from the parent object by using {object.fieldname} notation.</description>
        <externalId>false</externalId>
        <inlineHelpText>Description to set on the created task. You may reference fields from the parent object by using {object.fieldname} notation.</inlineHelpText>
        <label>Description</label>
        <length>32768</length>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Due_Date_Offset_Source__c</fullName>
        <description>The source field on the parent object the due date is calculated from when using offsets. This must either be a date field on the parent object, or left blank to use the triggering date as the base</description>
        <externalId>false</externalId>
        <inlineHelpText>The source field on the parent object the due date is calculated from when using offsets. This must either be a date field on the parent object, or left blank to use the triggering date as the base</inlineHelpText>
        <label>Due Date Offset Source</label>
        <length>200</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Due_Date_Static_Date__c</fullName>
        <description>If using a static due date this field contains the desired due date.</description>
        <externalId>false</externalId>
        <inlineHelpText>If using a static due date this field contains the desired due date.</inlineHelpText>
        <label>Due Date Static Date</label>
        <required>false</required>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Due_Date_Type__c</fullName>
        <description>What kind of due date does this task get assigned? A static date, such as &apos;2014-5-31&apos; or a number of days (positive or negative) for the date the rule is triggered.</description>
        <externalId>false</externalId>
        <inlineHelpText>What kind of due date does this task get assigned? A static date, such as &apos;2014-5-31&apos; or a number of days (positive or negative) for the date the rule is triggered.</inlineHelpText>
        <label>Due Date Type</label>
        <picklist>
            <picklistValues>
                <fullName>Static Date</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>Day Offset</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Due_Date_Value__c</fullName>
        <description>A pre defined date in the format &apos;YYYY-MM-DD&apos; or a numeric offset from the date this template is triggered.</description>
        <externalId>false</externalId>
        <inlineHelpText>A pre defined date in the format &apos;YYYY-MM-DD&apos; or a numeric offset from the date this template is triggered.</inlineHelpText>
        <label>Due Date Value</label>
        <length>255</length>
        <required>true</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Notification_Email_Template__c</fullName>
        <description>The name or Id of the email template to use to inform a user they have a new task</description>
        <externalId>false</externalId>
        <inlineHelpText>The name or Id of the email template to use to inform a user they have a new task</inlineHelpText>
        <label>Notification Email Template</label>
        <length>250</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Order__c</fullName>
        <externalId>false</externalId>
        <label>Order</label>
        <precision>7</precision>
        <required>false</required>
        <scale>2</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Priority__c</fullName>
        <defaultValue>&apos;Normal&apos;</defaultValue>
        <description>The priory this task is assigned.</description>
        <externalId>false</externalId>
        <inlineHelpText>The priory this task is assigned.</inlineHelpText>
        <label>Priority</label>
        <length>255</length>
        <required>true</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Record_Type_Name__c</fullName>
        <description>The API name of the record type to use for this task.</description>
        <externalId>false</externalId>
        <inlineHelpText>The API name of the record type to use for this task.</inlineHelpText>
        <label>Record Type Name</label>
        <length>200</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Related_To_Type__c</fullName>
        <description>How is the related to field of this task set? Is it a static ID of a record, or the value of a field on the parent record?</description>
        <externalId>false</externalId>
        <inlineHelpText>How is the related to field of this task set? Is it a static ID of a record, or the value of a field on the parent record?</inlineHelpText>
        <label>Related To Type</label>
        <picklist>
            <picklistValues>
                <fullName>Field Value</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Static ID</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Related_To_Value__c</fullName>
        <defaultValue>&apos;Id&apos;</defaultValue>
        <description>The Id of the record to relate this task to if static assigned, or the name of the field on the parent object where the ID of the related object resides.</description>
        <externalId>false</externalId>
        <inlineHelpText>The Id of the record to relate this task to if static assigned, or the name of the field on the parent object where the ID of the related object resides.</inlineHelpText>
        <label>Related To Value</label>
        <length>255</length>
        <required>true</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Substantive_Response__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Does this task quality as a Substantive Response when completed?</description>
        <externalId>false</externalId>
        <inlineHelpText>Does this task quality as a Substantive Response when completed?</inlineHelpText>
        <label>Substantive Response</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Task_Status__c</fullName>
        <defaultValue>&apos;Not Started&apos;</defaultValue>
        <description>The status this task will be created in</description>
        <externalId>false</externalId>
        <inlineHelpText>The status this task will be created in</inlineHelpText>
        <label>Task Status</label>
        <length>255</length>
        <required>true</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Task_Weight__c</fullName>
        <defaultValue>1</defaultValue>
        <description>amount of work this task represents. Used to determine how much total progress has been made on a plan.</description>
        <externalId>false</externalId>
        <inlineHelpText>amount of work this task represents. Used to determine how much total progress has been made on a plan.</inlineHelpText>
        <label>Task Weight</label>
        <precision>5</precision>
        <required>true</required>
        <scale>0</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>Activity Plan Task Template</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Activity_Plan__c</columns>
        <columns>Assigned_To_Type__c</columns>
        <columns>Assigned_To_Value__c</columns>
        <columns>Due_Date_Type__c</columns>
        <columns>Due_Date_Value__c</columns>
        <columns>Priority__c</columns>
        <columns>Related_To_Type__c</columns>
        <columns>Related_To_Value__c</columns>
        <columns>Task_Status__c</columns>
        <columns>Order__c</columns>
        <columns>Task_Weight__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Activity Plan Task Template Name</label>
        <type>Text</type>
    </nameField>
    <namedFilters>
        <fullName>nf_01IC0000000jWdcMAE_00NC0000005xq9bMAA</fullName>
        <active>true</active>
        <errorMessage>Selected Task Template must be attached to the same Activity Plan as this rule.</errorMessage>
        <field>Activity_Plan_Rule__c.Activity_Plan_Task_Template__c</field>
        <filterItems>
            <field>Activity_Plan_Task_Template__c.Id</field>
            <operation>equals</operation>
            <valueField>Activity_Plan_Task_Template__c.Id</valueField>
        </filterItems>
        <isOptional>false</isOptional>
        <name>Activity Plan Rule_Activity Plan Task Te</name>
    </namedFilters>
    <namedFilters>
        <fullName>nf_01IG0000002ZxS0MAK_00NG000000DDPIFMA5</fullName>
        <active>true</active>
        <field>Activity_Plan_Task_Template__c.Depends_On__c</field>
        <filterItems>
            <field>$Source.Id</field>
            <operation>equals</operation>
            <valueField>$Source.Id</valueField>
        </filterItems>
        <isOptional>false</isOptional>
        <name>Activity Plan Task Template_Depends On</name>
        <sourceObject>Activity_Plan_Task_Template__c</sourceObject>
    </namedFilters>
    <pluralLabel>Activity Plan Task Templates</pluralLabel>
    <searchLayouts>
        <lookupDialogsAdditionalFields>Activity_Plan__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Allow_Duplicate_Tasks__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Assigned_To_Lookup__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Assigned_To_Type__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Assigned_To_Value__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Creation_Method__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Depends_On__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Activity_Plan__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Depends_On__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Order__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Priority__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Task_Status__c</lookupPhoneDialogsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <webLinks>
        <fullName>Add_Library_Task</fullName>
        <availability>online</availability>
        <description>Add a task template from the task library</description>
        <displayType>button</displayType>
        <encodingKey>UTF-8</encodingKey>
        <hasMenubar>false</hasMenubar>
        <hasScrollbars>true</hasScrollbars>
        <hasToolbar>false</hasToolbar>
        <height>400</height>
        <isResizable>true</isResizable>
        <linkType>url</linkType>
        <masterLabel>Add Library Task</masterLabel>
        <openType>newWindow</openType>
        <position>none</position>
        <protected>false</protected>
        <showsLocation>false</showsLocation>
        <showsStatus>false</showsStatus>
        <url>/apex/ActivityPlans_AddLibraryTask?id={!Activity_Plan_Task_Template__c.Activity_PlanId__c}&amp;core.apexpages.devmode.url=0</url>
        <width>500</width>
    </webLinks>
</CustomObject>
